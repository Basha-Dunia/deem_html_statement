export class EppCalculator{
    eppAmount:number;
    tenor:string;
    monthlyFlatInterestRate:number;
    eppMonthlyInstallment:string;
}