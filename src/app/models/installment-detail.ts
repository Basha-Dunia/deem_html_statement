export class InstallmentDetail{
    statementDate: "2020-12-16";
    tenor: string;
    pendingInstallments: number;
    outstanding: number;
}