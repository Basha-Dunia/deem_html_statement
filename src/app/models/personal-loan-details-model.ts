
export class PersonalLoanDetails{

    statementDate:string;
    statementPeriod:string;
    loanAccountNumber:string;
    principalOutstanding:number;
    paymentDue:number;
    towardsPrincipal:number;
    towardsInterest:number;
    latefeeOtherCharges:number;
    paymentDueDate:string;
    amountBorrowed:number;
    tenor:string;
    interestRatePa:number;
    monthlyInstallment:number;
    lastPaymentAmount:number;
    lastPaymentDate:number;
    installmentRemaining:number;
    totalpaymentDue:string;

}