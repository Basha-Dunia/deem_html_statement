

export class EppTransaction{
    tenor: string;
    interestRate: string;
    processingFee: string;
    processingFeeType: string;
    foreClosureFee: string;
    niPlanNumber:string;
    txnAuthCode:string;  
}