export class PersonalLoanSummary{

    statementDate:string;
    dueOn:string;
    statementPeriod:string;
    loanAccountNumber:string;
    principalOutstanding:number;
    paymentDue:number;
    totalpaymentDue:number;
    towardsPrincipal:number;
    towardsInterest:number;
    lateFeeOtherCharges:number;
    paymentDueDate:string;
    amountBorrowed:string;
    tenor:string;
    interestRatePa:number;
    monthlyInstallment:number;
    lastPaymentAmount:number;
    lastPaymentDate:string;
    installmentRemaining:number;
    npaStatus:string;
    totalEligibleTax:string;
    totalValueAddedTax:string;
   
}