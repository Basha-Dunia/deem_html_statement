export class LoanCalculator{
    loanAmount:number;
    loanTenor:string;
    monthlyLoanInterestRate:number;
    emi:string;
    processingFees:number;
}