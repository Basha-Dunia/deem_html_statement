export class RewardDetails {

    amountSpent: number;
    rewardsEarned: number;
    openingBalance: number;
    earnedAed: number;
    purchasedAed: number;
    redeemedAed: number;
    availableBalance: number;
    airlinesSpentPer: number;
    hotelsSpentPer: number;
    othersSpentPer: number;
    entertainmentSpentPer: number;
}