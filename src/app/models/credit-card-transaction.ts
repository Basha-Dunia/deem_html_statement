import {CreditCardDetails} from './CreditCardDetails';
import{TransactionDetails} from './transaction-details'
export class CreditCardTransaction{
    name:string;
    address:string;
    timeStap:string;
    email:string;
    cardNumber:string;
    purchaseCashAdvance:string;
    interestCharges:string;
    paymentsCredit:string;
    totalPaymentDue:string;
    CurrentBalace:string;
    invoiceDate:string;
    serviceForVat:string;
    totalFeesForVat:string;
    totalVatForThePeriod:string;
    date:string;
    merchantName:string;
    originalAmount:string;
    outstandingAmount:string;
    instalmentsPending:number;
    tenor:number;
    error:string;

    transactions:TransactionDetails[]=[];
    constructor(){
        this.transactions.push(new TransactionDetails());
    }



}