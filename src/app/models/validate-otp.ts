export class ValidateOTP{
    target:string;
    validationStatus:string;
    validationAttempts:string;
    error:Error=new Error()
}

class Error{
    errorDesc:string;
}