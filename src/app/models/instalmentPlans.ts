export class InstalmentPlans{
    date:string;
    merchantName:string;
    originalAmount:string;
    outstandingAmount:string;
    instalmentsPending:number;
    tenor:string;
}