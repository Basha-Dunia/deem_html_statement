import {CreditCardDetails} from './CreditCardDetails';
import { EppTransaction } from './epp-transaction';
import { InstallmentDetail } from './installment-detail';
export class CreditCardTransaction{
    
    name:string;
    address:string;
    timeStap:string;
    email:string;
    cardNumber:string;
    purchaseCashAdvance:string;
    interestCharges:string;
    paymentsCredit:string;
    totalPaymentDue:string;
    CurrentBalace:string;
    invoiceDate:string;
    serviceForVat:string;
    totalFeesForVat:string;
    totalVatForThePeriod:string;
    date:string;
    merchantName:string;
    originalAmount:string;
    outstandingAmount:string;
    instalmentsPending:number;
    tenor:number;
    error:Error=new Error();
    // creditCardDetail:CreditCardDetails;
    // creditCardDetails:CreditCardDetails[]=[];
    transactions:CreditCardDetails[]=[];
    installmentDetails:InstallmentDetail[]=[];

}
class Error{
    errorDesc:string;
}

// class CreditCardDetails{
   
//     date: string;
//     postingDate: string;
//     description: string;
//     currencyType: string;
//     amount: number;
//     eppEligible:string
// }
