import { EppTransaction } from './epp-transaction';
export class CreditCardDetails{

    date: string;
    postingDate: string;
    description: string;
    currencyType: string;
    cardNo:string;
    txnReferenceId:string;
    amount: number;
    originalAmount:number;
    eppEligible:string;
    eppRequested:string;
    debitCreditFlag:string;
    eppTransaction:EppTransaction=new EppTransaction(); 
    eppTransactions:EppTransaction[]=[];

}