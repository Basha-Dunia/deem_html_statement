export class TransactionDetails{

    transactionDate:string;
    postingDate:string;
    description:string;
    amountOriginalCurrency:string;
    amount:string
}