import {CreditCardSummary} from './credit-card-summary-model';
import {AvailableLoanStatement} from './available-loan-statement';
import {AvailableCardStatement} from './available-card-statement';
import {PersonalLoanSummary} from './personal-loan-summary';
import { InsuranceDetails } from './insurance-details';
import { NoOfAvailableCards } from './No-of-cards-available';
export class ProfileDetails{
userDetails:UserDetails=new UserDetails();
statementDetils:StatementDetails=new StatementDetails();
creditCardDetail:CreditCardSummary=new CreditCardSummary();
availableCardStatement:AvailableCardStatement=new AvailableCardStatement();
availableLoanStatement:AvailableLoanStatement=new AvailableLoanStatement();
personalLoanSummary:PersonalLoanSummary=new PersonalLoanSummary();
creditCardDetails:CreditCardSummary[]=[];
availableCardStatements:AvailableCardStatement[]=[];
availableLoanStatements:AvailableLoanStatement[]=[];
error:Error=new Error();
insuranceDetails:InsuranceDetails=new InsuranceDetails();
noOfAvailableCards:NoOfAvailableCards[]=[];
}

class Error{
    errorDesc:string='';
}

class UserDetails{
   
    cifNumber:string;
        firstName:string;
        middleName: string;
        lastName:string;
        address1:string ;
        address2:string ;
        address3: string;
        address4: string;
        city: string;
        state: string;
        country: string;
        email: string;
        mobile:string;
        dob: string
       
}
class StatementDetails{
   
    statementDate: string;
    statementPeriod: string
}


