export class CreditCardSummary {


    cardType: string;
    primaryCardNumber: string;
    accountNumber: string;
    totalPaymentDue: string;
    minimumPaymentDue: number;
    paymentDueDate: string;
    totalCreditLimit: string;
    totalCashLimit: string;
    totalOutstandingBalance: string;
    previousBalance:string;
    interestCharges:string;
    cashAdvance:string;
    paymentCredit:string;
    currentBalance:string;
    statementPeriod:string;

}