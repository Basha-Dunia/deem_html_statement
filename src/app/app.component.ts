import { Component,OnInit } from '@angular/core';
import { GoogleAnalyticsService } from './helper-services/google-analytics/google-analytics.service';
import { BnNgIdleService } from 'bn-ng-idle';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
 constructor(private analyticsService:GoogleAnalyticsService,private bnIdle: BnNgIdleService){
  this.analyticsService.init();
 }
  title = 'deemglobal-deem-statements';

   ngOnInit(): void {
    this.bnIdle.startWatching(300).subscribe((isTimedOut: boolean) => {
      if (isTimedOut) {
       
      if(location.href.includes('statement'))
      {
          localStorage.clear();
          sessionStorage.clear();
          location.reload();
      }
         
       
      }
    });
  }
}
