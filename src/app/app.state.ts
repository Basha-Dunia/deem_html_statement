import {Offer} from './models/offer';

export interface AppState{
    readonly offer:Offer[];
}