import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormatDateService {

  constructor() { }

  dateFormat1(periodDate:string){
   
    var dateArray= periodDate.split('/');
   var date1=dateArray[0];
   var month1=dateArray[1];
   var year1=dateArray[2];
     var date2 = parseInt(date1) < 10 ? '0' + '' + parseInt(date1) + '' : '' + parseInt(date1) + '';
     var month2 = parseInt(month1) < 10 ? '0' + '' + parseInt(month1) + '' : '' + parseInt(month1) + '';
     var newDate = `${year1 + '-' + month2 + '-' + date2}`;
     return newDate;
     
   }
}
