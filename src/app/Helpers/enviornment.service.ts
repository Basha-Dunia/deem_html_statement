import { Injectable } from '@angular/core';
import{environment} from 'src/environments/environment';
import{prodEenvironment} from 'src/environments/environment.prod';
import{devEenvironment} from 'src/environments/environment.dev';
@Injectable({
  providedIn: 'root'
})
export class EnviornmentService {

  constructor() { }
  getEnviornment(){
    if(environment.production)
    {
      return prodEenvironment;
    }
    else
    {
      return devEenvironment;
    }

  }
}
