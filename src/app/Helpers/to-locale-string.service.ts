import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToLocaleStringService {

  constructor() { }

  fromNumberToLocaleString(value){
    if(value)
    {
     if(!isNaN(parseFloat(''+value+'')))
     {
      return value.toLocaleString();
     }
    }
    else
    {
      return '0.00';
    }

  }
}
