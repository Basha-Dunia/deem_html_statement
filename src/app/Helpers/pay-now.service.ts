import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PayNowService {

  constructor() { }
  formatNumber(number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }
  payNow(productType:string,productId:string,minimumAmountDue:string,totalAmountDue:string,cif:string) {
    var time = new Date();
    var dateTime = time.getFullYear() + '' + this.formatNumber(time.getMonth() + 1) + this.formatNumber(time.getDate()) + this.formatNumber(time.getHours()) + this.formatNumber(time.getMinutes()) + this.formatNumber(time.getSeconds());
    var reference = cif + '' + (time.getTime() + '').substring(4);
    var f = document.createElement('form');
    f.action = 'https://www.dunia.ae/pgs_prod/pgs.php';
    f.method = 'POST';
    f.target = '_blank';

    var pp_Version = document.createElement('input');
    pp_Version.type = 'hidden';
    pp_Version.name = 'pp_Version';
    pp_Version.id = 'pp_Version';
    pp_Version.value = '1.1';//+document.documentElement.outerHTML;
    f.appendChild(pp_Version);

    var pp_TxnType = document.createElement('input');
    pp_TxnType.type = 'hidden';
    pp_TxnType.name = 'pp_TxnType';
    pp_TxnType.id = 'pp_TxnType'
    pp_TxnType.value = 'DD';//+document.documentElement.outerHTML;
    f.appendChild(pp_TxnType);

    var pp_Language = document.createElement('input');
    pp_Language.type = 'hidden';
    pp_Language.name = 'pp_Language';
    pp_Language.id = 'pp_Language'
    pp_Language.value = 'EN';//+document.documentElement.outerHTML;
    f.appendChild(pp_Language);

    var pp_SubMerchantID = document.createElement('input');
    pp_SubMerchantID.type = 'hidden';
    pp_SubMerchantID.name = 'pp_SubMerchantID';
    pp_SubMerchantID.id = 'pp_SubMerchantID'
    pp_SubMerchantID.value = '';//+document.documentElement.outerHTML;
    f.appendChild(pp_SubMerchantID);

    var pp_BankID = document.createElement('input');
    pp_BankID.type = 'hidden';
    pp_BankID.name = 'pp_BankID';
    pp_BankID.id = 'pp_BankID'
    pp_BankID.value = '';//+document.documentElement.outerHTML;
    f.appendChild(pp_BankID);

    var pp_ProductID = document.createElement('input');
    pp_ProductID.type = 'hidden';
    pp_ProductID.name = 'pp_ProductID';
    pp_ProductID.id = 'pp_ProductID'
    pp_ProductID.value = 'RETL';//+document.documentElement.outerHTML;
    f.appendChild(pp_ProductID);

    var pp_TxnRefNo = document.createElement('input');
    pp_TxnRefNo.type = 'hidden';
    pp_TxnRefNo.name = 'pp_TxnRefNo';
    pp_TxnRefNo.id = 'pp_TxnRefNo'
    pp_TxnRefNo.value = reference;//+document.documentElement.outerHTML;
    f.appendChild(pp_TxnRefNo);

    var pp_Amount = document.createElement('input');
    pp_Amount.type = 'hidden';
    pp_Amount.name = 'pp_Amount';
    pp_Amount.id = 'pp_Amount';
    pp_Amount.value = totalAmountDue;//+document.documentElement.outerHTML;
    f.appendChild(pp_Amount);

    var pp_TxnCurrency = document.createElement('input');
    pp_TxnCurrency.type = 'hidden';
    pp_TxnCurrency.name = 'pp_TxnCurrency';
    pp_TxnCurrency.id = 'pp_TxnCurrency';
    pp_TxnCurrency.value = 'AED';//+document.documentElement.outerHTML;
    f.appendChild(pp_TxnCurrency);


    var pp_TxnDateTime = document.createElement('input');
    pp_TxnDateTime.type = 'hidden';
    pp_TxnDateTime.name = 'pp_TxnDateTime';
    pp_TxnDateTime.id = 'pp_TxnDateTime';
    pp_TxnDateTime.value = dateTime;//+document.documentElement.outerHTML;
    f.appendChild(pp_TxnDateTime);

    var pp_TxnExpiryDateTime = document.createElement('input');
    pp_TxnExpiryDateTime.type = 'hidden';
    pp_TxnExpiryDateTime.name = 'pp_TxnExpiryDateTime';
    pp_TxnExpiryDateTime.id = 'pp_TxnExpiryDateTime';
    pp_TxnExpiryDateTime.value = '-1';//+document.documentElement.outerHTML;
    f.appendChild(pp_TxnExpiryDateTime);

    var pp_BillReference = document.createElement('input');
    pp_BillReference.type = 'hidden';
    pp_BillReference.name = 'pp_BillReference';
    pp_BillReference.id = 'pp_BillReference';
    pp_BillReference.value =productType ;//+document.documentElement.outerHTML;
    f.appendChild(pp_BillReference);

    var pp_Description = document.createElement('input');
    pp_Description.type = 'hidden';
    pp_Description.name = 'pp_Description';
    pp_Description.id = 'pp_Description';
    pp_Description.value = 'Payment for Credit Card';//+document.documentElement.outerHTML;
    f.appendChild(pp_Description);

    var pp_minPaymentDue = document.createElement('input');
    pp_minPaymentDue.type = 'hidden';
    pp_minPaymentDue.name = 'pp_minPaymentDue';
    pp_minPaymentDue.id = 'pp_minPaymentDue';
    pp_minPaymentDue.value = minimumAmountDue;//+document.documentElement.outerHTML;
    f.appendChild(pp_minPaymentDue);

    var ppmpf_1 = document.createElement('input');
    ppmpf_1.type = 'hidden';
    ppmpf_1.name = 'ppmpf_1';
    ppmpf_1.id = 'ppmpf_1';
    ppmpf_1.value = cif;//+document.documentElement.outerHTML;
    f.appendChild(ppmpf_1);

    var ppmpf_2 = document.createElement('input');
    ppmpf_2.type = 'hidden';
    ppmpf_2.name = 'ppmpf_2';
    ppmpf_2.id = 'ppmpf_2';
    ppmpf_2.value = productId//+document.documentElement.outerHTML;
    f.appendChild(ppmpf_2);

    var ppmpf_3 = document.createElement('input');
    ppmpf_3.type = 'hidden';
    ppmpf_3.name = 'ppmpf_3';
    ppmpf_3.id = 'ppmpf_3';
    ppmpf_3.value = 'HTMLSTMT'//+document.documentElement.outerHTML;
    f.appendChild(ppmpf_3);

    var ppmpf_4 = document.createElement('input');
    ppmpf_4.type = 'hidden';
    ppmpf_4.name = 'ppmpf_4';
    ppmpf_4.id = 'ppmpf_4';
    ppmpf_4.value = '4'//+document.documentElement.outerHTML;
    f.appendChild(ppmpf_4);

    var ppmpf_5 = document.createElement('input');
    ppmpf_5.type = 'hidden';
    ppmpf_5.name = 'ppmpf_5';
    ppmpf_5.id = 'ppmpf_5';
    ppmpf_5.value = '5'//+document.documentElement.outerHTML;
    f.appendChild(ppmpf_5);

    var ppmbf_1 = document.createElement('input');
    ppmbf_1.type = 'hidden';
    ppmbf_1.name = 'ppmbf_1';
    ppmbf_1.id = 'ppmbf_1';
    ppmbf_1.value =cif;//+document.documentElement.outerHTML;
    f.appendChild(ppmbf_1);

    var ppmbf_2 = document.createElement('input');
    ppmbf_2.type = 'hidden';
    ppmbf_2.name = 'ppmbf_2';
    ppmbf_2.id = 'ppmbf_2';
    ppmbf_2.value = productId//+document.documentElement.outerHTML;
    f.appendChild(ppmbf_2);

    var ppmbf_3 = document.createElement('input');
    ppmbf_3.type = 'hidden';
    ppmbf_3.name = 'ppmbf_3';
    ppmbf_3.id = 'ppmbf_3';
    ppmbf_3.value = 'HTMLSTMT'//+document.documentElement.outerHTML;
    f.appendChild(ppmbf_3);

    var ppmbf_4 = document.createElement('input');
    ppmbf_4.type = 'hidden';
    ppmbf_4.name = 'ppmbf_4';
    ppmbf_4.id = 'ppmbf_4';
    ppmbf_4.value = '4'//+document.documentElement.outerHTML;
    f.appendChild(ppmbf_4);

    var ppmbf_5 = document.createElement('input');
    ppmbf_5.type = 'hidden';
    ppmbf_5.name = 'ppmbf_5';
    ppmbf_5.id = 'ppmbf_5';
    ppmbf_5.value = '5'//+document.documentElement.outerHTML;
    f.appendChild(ppmbf_5);

    document.body.appendChild(f);
    f.submit();
    //To get the value from the pgs portal
    //  $product_type = $_POST['product_type'];
    // $amount = $_POST['amount'];
  }
}