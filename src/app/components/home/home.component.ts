import { Component, OnInit, } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { PdfService } from 'src/app/services/pdf/pdf.service';
import pdfMake from "pdfmake/build/pdfmake";
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
//import pdfFonts from "pdfmake/build/vfs_fonts";
import pdfFonts from '../../../assets/Fonts/vfs_fonts';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { FormGroup, Validators } from '@angular/forms';
import { CreditCardSummaryService } from 'src/app/services/credit-card-summary/credit-card-summary.service';
import { LoginService } from 'src/app/services/login/login.service';
import { PersonalLoanService } from 'src/app/services/personal-loan/personal-loan.service';
import { CreditCardTransaction } from 'src/app/models/credit-card-transaction-model';
import { CreditCardTransactionService } from 'src/app/services/credit-card-transaction/credit-card-transaction.service';
import { VatInvoiceService } from 'src/app/services/vat-invoice/vat-invoice.service';
import { VatInvoice } from 'src/app/models/vat-invoice';
import { FormatDateService } from 'src/app/Helpers/format-date.service';
import { PayNowService } from 'src/app/Helpers/pay-now.service';
import { RewardDetailsService } from 'src/app/services/reward-details/reward-details.service';
import { RewardDetails } from 'src/app/models/reward-details';
import { InsuranceDetailsService } from 'src/app/services/insurance-details/insurance-details.service';
import { InsuranceDetails } from 'src/app/models/insurance-details';
import { ToLocaleStringService } from 'src/app/Helpers/to-locale-string.service';
import { CreditCardDetails } from 'src/app/models/CreditCardDetails';
import { ifStmt } from '@angular/compiler/src/output/output_ast';
import { StatementDownloadAuditService } from 'src/app/services/statement-download-audit/statement-download-audit.service';
import { PersonalLoanSummary } from 'src/app/models/personal-loan-summary'
//import * as TwitterCldrLoader from "twitter_cldr/twitter_cldr";

//const TwitterCldr = TwitterCldrLoader.load("en");
//import { Injectable } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
// @Injectable({
//   providedIn: 'root'
// })
export class HomeComponent implements OnInit {
  public selected;
  slectedStatementDate: any;
  statementPeriodOne: any;
  statementPeriodTwo: any;
  statementCategory: FormGroup;
  creditCardTransaction = new CreditCardTransaction();
  profileDetails: ProfileDetails = new ProfileDetails();
  transactionData: any = [];
  vatInvoice: VatInvoice;
  insuranceDetails: InsuranceDetails = new InsuranceDetails();
  cardTransactions = [new CreditCardDetails()];
  isRecord;
  isNoRecord;
  noRecordMessage: string;
  loanInsurance;
  cardInsurance;
  cardTypeImageUrl: string;
  isEnabled: boolean = false;
  isMobileScren;
  isRed;
  isBlack;

  constructor(private _router: Router, private statementService: StatementSummaryService,
    private cardSummaryService: CreditCardSummaryService, private loginService: LoginService,
    private plService: PersonalLoanService, private cardTransactionService: CreditCardTransactionService,
    private vatInvoiceService: VatInvoiceService, private creditCardTransactionService: CreditCardTransactionService,
    private datePipe: DatePipe, private formatDate: FormatDateService, private payNowService: PayNowService,
    private rsService: RewardDetailsService, private insuranceDetailsService: InsuranceDetailsService,
    public localeStringService: ToLocaleStringService, private breakpointObserver: BreakpointObserver,
    private statementDownloadLog: StatementDownloadAuditService) {

    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    pdfMake.fonts = {


      dejavu: {
        normal: 'dejavu-sans.TTF',
        bold: 'dejavu-sans.TTF',
        italics: 'dejavu-sans.TTF',
        bolditalics: 'dejavu-sans.TTF'
      }
    }

    breakpointObserver.observe([
      '(max-width: 768px)'
    ]).subscribe(result => {
      if (result.matches) {
        this.isMobileScren = true;
      } else {
        this.isMobileScren = false;
      }
    });

  }

  async ngOnInit() {

    this.noRecordMessage = 'Statement is not generated for this customer.';
    this.profileDetails.creditCardDetails.length = 0;
    this.profileDetails = this.statementService.profileDetails;

    this.cardInsurance = this.profileDetails.insuranceDetails.cardInsurance != undefined || this.profileDetails.insuranceDetails.cardInsurance != null ? this.profileDetails.insuranceDetails.cardInsurance : '0.00';
    this.loanInsurance = this.profileDetails.insuranceDetails.loanInsurance != undefined || this.profileDetails.insuranceDetails.loanInsurance != null ? this.profileDetails.insuranceDetails.loanInsurance : '0.00';
    var stmtPeriod;
 
    if (this.profileDetails.statementDetils.statementPeriod) {
      console.log('inside statement period3');
      stmtPeriod = this.profileDetails.statementDetils.statementPeriod;
    }
    // if (this.cardSummaryService.cardSummaryDetails.length > 0) {
    //   console.log('inside statement period1');
    //   stmtPeriod = this.cardSummaryService.cardSummaryDetails[0].statementPeriod
    // }
    // else if (this.plService.presonlaLoan.statementPeriod) {
    //   console.log('inside statement period2');
    //   stmtPeriod = this.plService.presonlaLoan.statementPeriod
    // }
    // else {
    //   if (this.profileDetails.statementDetils.statementPeriod) {
    //     console.log('inside statement period3');
    //     stmtPeriod = this.profileDetails.statementDetils.statementPeriod;
    //   }
    //   if (this.profileDetails.personalLoanSummary.statementPeriod) {
    //     console.log('inside statement period4');
    //     stmtPeriod = this.profileDetails.personalLoanSummary.statementPeriod;
    //   }
    // }
    if (stmtPeriod) {
      this.statementPeriodOne = this.formatDate.dateFormat1(stmtPeriod.slice(0, 10));
      this.statementPeriodTwo = this.formatDate.dateFormat1(stmtPeriod.slice(13, 25));
    }

    if (this.profileDetails.availableCardStatements.length > 0 && !this.cardSummaryService.selectedDate) {
      this.selected = this.profileDetails.availableCardStatements[0].statementDate;
    }
    if (this.profileDetails.availableCardStatements.length > 0 && this.cardSummaryService.selectedDate) {

      this.selected = this.cardSummaryService.selectedDate;
    }
    if (this.profileDetails.availableCardStatements.length < 0 && !this.cardSummaryService.selectedDate) {
      this.selected = "No Records";
    }

    if (this.profileDetails.personalLoanSummary.loanAccountNumber != undefined || this.profileDetails.creditCardDetails.length > 0) {
      this.isRecord = true;
      this.isNoRecord = false
    }
    else {
      this.isRecord = false;
      this.isNoRecord = true;

    }

    this.profileDetails.userDetails.firstName;
    this.profileDetails.userDetails.lastName;

  }

  async selectPeriod(stmtDate) {
   
    this.selected = '';
    var stmtPeriod = '';
    this.slectedStatementDate = stmtDate
    this.profileDetails.creditCardDetails.length = 0;
    this.plService.presonlaLoan = new PersonalLoanSummary();
    this.profileDetails.creditCardDetails = await this.cardSummaryService.getCreditCardSummery(this.loginService.getCif(), stmtDate)
    this.profileDetails.personalLoanSummary = await this.plService.getLoanDetails(this.loginService.getCif(),stmtDate);// this.statementService.profileDetails.statementDetils.statementDate
    this.creditCardTransactionService.creditCardTransaction.transactions.length = 0
    this.creditCardTransactionService.creditCardTransaction.transactions.length = 0
  
    if (this.profileDetails.creditCardDetails.length > 0) {
      stmtPeriod = this.profileDetails.creditCardDetails[0].statementPeriod;
      await this.creditCardTransactionService.creditCardTransactionData(this.statementService.profileDetails.creditCardDetails[0].primaryCardNumber.replace('XXXXXXXX', '00000000'), stmtDate, this.loginService.getCif());
    }
    else if(this.profileDetails.personalLoanSummary.statementPeriod){

      stmtPeriod=  this.profileDetails.personalLoanSummary.statementPeriod
    }
    // if(!this.profileDetails.personalLoanSummary)
    // {
    //   this.profileDetails.personalLoanSummary=new PersonalLoanSummary();
    // }

    this.insuranceDetails = await this.insuranceDetailsService.getInsuranceDetails(this.loginService.getCif(), stmtDate);
    this.cardInsurance = this.insuranceDetails.cardInsurance != undefined || this.insuranceDetails.cardInsurance != null ? this.insuranceDetails.cardInsurance : 0;
    this.loanInsurance = this.insuranceDetails.loanInsurance != undefined || this.insuranceDetails.loanInsurance != null ? this.insuranceDetails.loanInsurance : 0;
    // this.profileDetails.personalLoanSummary = await this.plService.getLoanDetails(this.loginService.getCif(), stmtDate);
    // if (this.profileDetails.personalLoanSummary.statementPeriod && this.profileDetails.creditCardDetails.length === 0) {
    //   stmtPeriod = this.profileDetails.personalLoanSummary.statementPeriod;

    // }
    if(stmtPeriod)
    {
    this.statementPeriodOne =this.formatDate.dateFormat1(stmtPeriod.slice(0, 10));
    this.statementPeriodTwo =this.formatDate.dateFormat1(stmtPeriod.slice(13, 25));
    }
  }

  redirectToCreditCard() {
    this._router.navigate(['/statement/credit-card']);
  }
  redirectToLoan() {
    this._router.navigate(['/statement/loan']);
  }
  public getBrowserName() {
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        return 'edge';
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        return 'opera';
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        return 'chrome';
      case agent.indexOf('trident') > -1:
        return 'ie';
      case agent.indexOf('firefox') > -1:
        return 'firefox';
      case agent.indexOf('safari') > -1:
        return 'safari';
      default:
        return 'other';
    }
  }
  async generatePDF(action = 'open') {
    var statementDate;
    this.isEnabled = true;
    this.cardTransactionService.creditCardTransaction.transactions.length = 0;
    this.cardTransactionService.creditCardTransaction.installmentDetails.length=0;
    var stmtPeriod;
    const numberWithCommas=(x)=> {
      if(x)
      {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
      return '0.00';
  }
    //this.cardSummaryService.selectedDate
    //this.statementService.profileDetails.statementDetils.statementDate
    if (this.cardSummaryService.cardSummaryDetails.length > 0) {
      stmtPeriod = this.cardSummaryService.cardSummaryDetails[0].statementPeriod
    }
    // else if (this.plService.presonlaLoan.statementPeriod) {
    //   stmtPeriod = this.plService.presonlaLoan.statementPeriod
    // }
    else {
      if (this.profileDetails.statementDetils.statementPeriod) {
        stmtPeriod = this.profileDetails.statementDetils.statementPeriod;
      }
      // if (this.profileDetails.personalLoanSummary.statementPeriod) {
      //   stmtPeriod = this.profileDetails.personalLoanSummary.statementPeriod;
      // }
    }
    // if (this.cardSummaryService.cardSummaryDetails.length > 0 || this.plService.presonlaLoan.loanAccountNumber != undefined) {
    //   statementDate = this.slectedStatementDate;

    // }
    // else {
    //   statementDate = this.statementService.profileDetails.availableCardStatements[0].statementDate;
    // }
    if (this.cardSummaryService.cardSummaryDetails.length > 0 ) {
      statementDate = this.cardSummaryService.selectedDate;

    }
    else {
      statementDate = this.statementService.profileDetails.availableCardStatements[0].statementDate;
    }


    for (const el of this.profileDetails.noOfAvailableCards) {
      await this.creditCardTransactionService.creditCardTransactionData(el.accountNumber.replace('XXXXXXXX', '00000000'), statementDate, this.loginService.getCif());
    }

    this.vatInvoice = await this.vatInvoiceService.getVatDetails(this.loginService.getCif(), statementDate);
    var rewardDetails = await this.rsService.getRewardDetails(this.loginService.getCif(), statementDate);
   if(this.profileDetails.creditCardDetails.length>0)
   {
    await this.statementDownloadLog.statementDownloadAudit(this.loginService.getCif(), statementDate, this.profileDetails.creditCardDetails[0].primaryCardNumber.replace('XXXXXXXX', '00000000'));
   }
    let ariLines; let hotels; let ent; let others;
    ariLines = rewardDetails.airlinesSpentPer > 0 ? rewardDetails.airlinesSpentPer : 0.00001;
    hotels = rewardDetails.hotelsSpentPer > 0 ? rewardDetails.hotelsSpentPer : 0.00001;
    ent = rewardDetails.entertainmentSpentPer > 0 ? rewardDetails.entertainmentSpentPer : 0.00001;
    others = rewardDetails.othersSpentPer > 0 ? rewardDetails.othersSpentPer : 0.00001;
    var profileDetails = this.profileDetails;
    var arabicText = this.getCardArabicText();
    // const rtl=this.maybeRtlize;
    let b = 100;
    var svg = `<svg width="90" height="90" viewBox="0 0 36 36" style="margin: 10px auto;max-width: 80%;max-height: 200px;stroke: #6E3DEA;">
    <path style="fill: none;stroke: #27272E;stroke-width: 2.0;"
      d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
    />
    <path style="fill: none;stroke-width: 2.0;stroke-linecap:square;"
      stroke-dasharray="`+ '' + ariLines + '' + ',' + '' + b + '' + `"
      d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
    />
    <text x="18" y="20.35" style="fill: #27272E;font-family: sans-serif;font-size: 0.5em; color:black;text-anchor: middle;">`+
      '' + rewardDetails.airlinesSpentPer + '' + `%</text></svg>`;

    var svgHotels = `<svg width="90" height="90" viewBox="0 0 36 36" style="margin: 10px auto;max-width: 80%;max-height: 200px;stroke: #6E3DEA;">
      <path style="fill: none;stroke: #27272E;stroke-width: 2.0;"
        d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path style="fill: none;stroke-width: 2.0;stroke-linecap:square;"
        stroke-dasharray="`+ '' + hotels + '' + ',' + '' + b + '' + `"
        d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" style="fill: #27272E;font-family: sans-serif;font-size: 0.5em;text-anchor: middle;">`+
      '' + rewardDetails.hotelsSpentPer + '' + `%</text></svg>`;

    var svgEnt = `<svg width="90" height="90" viewBox="0 0 36 36" style="margin: 10px auto;max-width: 80%;max-height: 200px;stroke: #6E3DEA;">
        <path style="fill: none;stroke: #27272E;stroke-width: 2.0;"
          d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
        />
        <path style="fill: none;stroke-width: 2.0;stroke-linecap:square;"
          stroke-dasharray="`+ '' + ent + '' + ',' + '' + b + '' + `"
          d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
        />
        <text x="18" y="20.35" style="fill: #27272E;font-family: sans-serif;font-size: 0.5em;text-anchor: middle;">`+
      '' + rewardDetails.entertainmentSpentPer + '' + `%</text></svg>`;

    var svgOthers = `<svg width="90" height="90" viewBox="0 0 36 36" style="margin: 10px auto;max-width: 80%;max-height: 200px;stroke: #6E3DEA;">
          <path style="fill: none;stroke: #27272E;stroke-width: 2.0;"
            d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
          />
          <path style="fill: none;stroke-width: 2.0;stroke-linecap:square;"
            stroke-dasharray="`+ '' + others + '' + ',' + '' + b + '' + `"
            d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
          />
          <text x="18" y="20.35" style="fill: #27272E;font-family: sans-serif;font-size: 0.5em;text-anchor: middle;">`+
      '' + rewardDetails.othersSpentPer + '' + `%</text></svg>`;
    let docDefinition;
    //Both loan & Card Product
    const reverseString = (str: string) => {
     
      return str.split(" ").reverse().join(" ");
    }
    //Loan & Credit Card
    if (profileDetails.personalLoanSummary.loanAccountNumber != undefined && profileDetails.creditCardDetails.length > 0) {
      var totalpaymentDueAmount;
      var towardsPrincipal;
      if(profileDetails.personalLoanSummary.npaStatus != "WRITEOFF"){
        towardsPrincipal = profileDetails.personalLoanSummary.towardsPrincipal;
        totalpaymentDueAmount= profileDetails.personalLoanSummary.paymentDue;
      }else{
        towardsPrincipal = profileDetails.personalLoanSummary.principalOutstanding;
        totalpaymentDueAmount = profileDetails.personalLoanSummary.totalpaymentDue;
      }
      docDefinition = {
        pageMargins: [25, 240, 25, 70],
        footer: function (currentPage, pageCount) {
          return  [

            {
              text: [{
                text: 'Page ' + '' + currentPage.toString() + '' + ' of ' + pageCount,
                alignment: 'center',
                fontSize: 8,
                marginTop: 30

              }],

            },
            {
              canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, color: '#6E3DEA' },


              ], marginBottom: 10
            },
            {
              text: [{
                text: 'Deem Finance LLC (Deem) is regulated by Central Bank of the UAE' + ' ' + reverseString('تخضع شركة ديم للتمويل ذ.م.م ديم لرقابة مصرف الإمارات العربية المتحدة المركزي'),
                fontSize: 8,
                alignment: 'center',

              },
              ],

            },
            {
              text: [{
                text: 'P.O.Box 44005 | Abu Dhabi, UAE  www.deem.io | 600 525550 | customercare@deem.io TRN: 100038451900003',
                fontSize: 8,
                alignment: 'center',

              },
              ],

            },

          ]

        },

        header: function (currentPage, pageCount, pageSize) {
          // you can apply any logic and return any valid pdfmake element
          if (currentPage === 1) {
            return [
              {

                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApsAAAEPCAYAAAANjQ+sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACXASURBVHhe7d2PleS2scXhF4JDUAgOwSEoBIegEByCQnAICsEhKASFoAz87l0111wMuptkF8gq8HfP+c57hmZnSDT/FEGQ/X///e9/AQAAgCG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsI7KH8JP+UX633MwAA4J66jcAryj/kX/Kb/CluXPyn928AAMA9dRuBhbIetfxd1oVlD8UmAAD4rtuI+1I8avmLeNTyD1kXkltQbAIAgO+6jbgHxaOWP4tHLf8j66LxKIpNAADwXbcRc1L+Lp+MWm5BsQkAAL7rNqI+5W/iUUs/yBM1arkFxSYAAPiu24h6lGXU8t8yatRyC4pNAADwXbcR+SkeuTx71HILik0AAPBdtxH5KX5qfF3kZUGxCQAAvus2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKzUKUv4s/s63+3vs9uC/lb6vtY6u/9X4XsFDWx6Z/yr8aP8vy3zkuHbDqv8VPvZ+bWbdxRv5wHx/yL+Id6N/yn41+k2XH8++4fId7LIf/n2xuWWwq/jyWA7W3F283f0jbP0f8Kevt0H/Hf49CYiKKT/o+sa+PT79Luz0ctRzPfpXlWHa7k95d+bMWHzv8+Xs78HGl3Ua2Wo5J3k59Tv1H72/egbLeb48e+72fL/vmt/6UqY7v3cbqFH/43qmWA/b6Q43kDcR/w3/r1IO2QrF5EcV9vxxYogrKo/z3lyL0tgf8apT1MSqyoDxiKRy8DfmkyUXMBPw5Pj5Pb2NnHaeW7WjaEVBlOf6PrC0W6xqj9H7ZbazGH8Ljw/CH8snV2qe8Q/vK5OfeckZSKDZPorgwOOvgEsHL6atjbnkloSyjSr4wuPIYtZVPcqccyxDLn5n4XNh+pmfzdu7lKH0cUpb6IsO+uxzbyxWe3cYK3NmybADrDyOLoTuaQrE5kHL2iMAoywUQhefJFBeYPjFcPXL5KR/LfJwtP7oyK38u4m0t6/HKy1WqSFJ8jr16AOsV75NlLga7jZkpHmXKvAH0+GTzz976HKVQbAZTvG25MKu0be2xFJ7M0xtEWU761QvMZ5aL6JJTNhTv4z52ptFbzq0Ub2++61LlmOXlTHsMUpZBrEqDDF7W0PpihG5jRop3zCq3MZ/xjuYDw8dXd4r7o/39GZQrNhUfXGYtDp7xvsQt0iCK90cXYW0/z6ziaFW6c0i7jFspPm5VvjAOORdG8HI8lqdyf6YuOruNmSg+iM9WCHiD/mijUCg2P6DMcHCJUOKqOCv33aMP2369E+9DJUbMlfLFpuLR2eoDLwtvO7/01vMs/vuP5WiXrSofj9Ldeeg2ZuDOkll2qGcObxT+d4/fkU3qYlNxkVltGsYZKDp3cF89+qztx7vzvpW26FRKF5uKL5C//I4JeEDp1Hnlis+hM+/DntOZ5q5Dt/FK7hy52+2o3RuFQrG5g8JI5jYUnS+4bx591PYb/sf7WJpbpGtKyWJT8fFr9sEXGz7Kqbgvsz5YHM37YorpUt3Gq7hTHp2z7qy72LVRKBSbGymz3SY5g09sPMH+oHh/u9u83k95n0t14aKUKzYV3za/0/Fr2Iic4v34jueCf/f640zdxrMpd7rSeOfXXh+1FIrNNxQKhM95Ll66EaqzeN3lbndaoqW5cHksS7t8l2qXcU2p/hDQUeG31RUfy9q/cyfu08uO5d3GMyl3vdJ45e1GoVBsPqG4QLj7gSWSbxvf7tuJlDvfaRnhX71+PpNSpthUXGh++fkb8b73ccGp3GUKwhYhfXpEt/Esim9vrjsC//Nyo1AoNjsU33JiTt0Ym0bdq1O40zKOL6Qve4BIKVFsKncvNBcfFUf+t8LdrR9dUnB2G8+gcGvqPW8U3XmcCsVmQ5n1Sc1MLi0WRlO40zLe0+PaaEr6YlOh0PzRoeLI/+bxb9vfh7/65dS7Vd3GkRSPGnClsc+XSfYKxeaDwm2Sc11WLIykcLFyrtNHypXUxabiAunLz2Bfwemfffyb9vfgf3b16ae6jaMoFJrH/VBwKhSbonDb/DqXvow5isJt8+u4+DvtoYXH32uX4VKrZfN36VMgPee+ebutKBSa251WcHYbR1AoND/3veBUbl9sKjzAcb3LX6nxCYXj0vXc/6cUnErmYpPt8L3fl/7qUbw/c07Yx/01fGpUtzGawgE9zrd5Fv6/q7ZMTik2FeY15eH515e9UuMohRGQPE4ZYVFSFpsKb8/Yrjv9QqHOOG74BV+3MZJX4LEi6xXDcd8OynLbYlOh0Mxn+MEqkkKhmc/wglNJV2xK1mN5Zl8eblF46PgzQ+9SdRujKBSaY3iOom8h9/7b1YYWmwqFZl4lCk6FQjOvoQWnkrHYZM75fu6z78cahdcoxhj24Ge3MYrCpPtxsp4shxWbCoVmfqfN2T1CodDMb1jBqWQsNnHMt9vpCg9WxXE/Dpm/2W2MoDAH5Z6GFBsKhWYdKR8aUig06/hh5CqKQrE5FxeafKaxxpzDe42fUigM7it8Q1XYnupJ9W1DCqMf9YRPy1AoTObCPj1G+O30buMnFEYP7i202FS8PbV/AzV8+TKCKyjMHa/rt95nepRCsQm8F35nodv4CYWD+r2FFZsKo1H1nfLC4FcU5o7X9q/e53qEQrEJbBO231m38Sgv3GpBcU8hxabCaNQchsy920rhmDSHkO9xVig2gW080BN27O42HqFwuxMWVWzyzrR5hN4K3UrJ+now7Bdy4lMoNoHt4u4q9BqPUBiFgn1cbCoUCfM59XvUFY+MMwVjLhHHFopNYLuw0c1u414Kt6qw+OiEoMw2T9Pr4hOcXwXm/cT8dL2/NcSWNvPcwlkv2twPw79/d6HcoahYti3fBbjLtvXRRYtCsQnsEzJQ0G3cQ+EhDqx9WmxWPxl4jqJP/h6dPVxcKS4W/K0YMz3cEjLF4h1l1m8T8XHW25YLyk+2LU95qrptfXTRolBsAvv80duX9uo27qEwtw5rhwsKpfL7NL0fhDzE0FJ8S9h9M8OJcujrkJQZL36/Xbz01vdTSsVt6/AcYIViE9jv4+NPt3ErhYeC0DpUbCoV59h5eX2L8rSnrRXvc5Uv8Nxnw/pLmWUk+Ipty4V6lW3r0IWdQrEJ7Pfxt8J1G7dS2HHROlpsVvt6Uy/vaYVAS3FhUHX/G/LtQoqnHrR/qyIXfFdvW9mL9kO39hTOWcAxHx2Tuo1bKLMc2BFrd7GpVBoh90MWl7+ofKF4P/Q80XY5swt/WEip2A9r3raGTMU4wssimft094MLyuzFpkfEvY7Lg2H+DNc8V3d5YKz6/nIm99WzfvU0FLf5InHmt/J8NAWq27iFMsNO6w3IO503FD/Q4Q3n6Unw8d/NO6xHtrhK/upIsVmlH1N93/dC8RSEarfWP74ts6ZUnu9rl45mPuNleixbu7wZ7J6Socx4zHaB43PS7otgxaPY/rczF0lHLf2668JYWeZBVzsmv/PR+5K7je8o3kDXC1GJi8vdG9ArigtVF59cKe4sNhUX7+3vyCjF93y/4mVcLW8FIfug4oN75X2vyrbl4q5d9qvteum0MlOx6XUJGwlXKs3ZHcl9EHls8mBWxn1nrz9767hVt/EdpdoG6RORC8zhIweKi6c777B7i83sB38fJNLcNn/Hy/pY5nY9MgoZ3VS8b7e/uwJ/TkOeMh9BybhteXk2H9eVGYrNodMtFH/Odxzp9LYRNgi1psxSdB7e7rqNryjutPUfz8w7zCWjBspdrxI3F5tK9lFNHxjKFJoLL/Nj2dv1yejjg7tScVSTbSvO5mO8Ur3YDPv6wHf8t1Z/d2benk+56FNcF1Qu5A9vf93GV5QKowg++aS4NaXcrejcU2xmfuK1ZDGw8LI/1qFdr2w+mgerVJ2rmeZBoL2UbNvW5ifTlarF5mkF0Zr/5uNvt8szCxd+px/nlWpvX1nsfiZj0W18Rck+inDpK2meUTyKV3le2VabNkYl+7zf9PPo3lFcFLTrlY1PZIf3V6XiPjXLtpWpCNlUiCkVi03382UXvv7bj2Vol6s6F5qX1QpKyQvldj226jY+o2Q+eXlnSD1aoHgKwuyjnFuLzcxXdqfdqhpNqXBAO1R8KVUeLlsLfQr/SkqmbWvTk7JKxWLz8jssSsV97ZVLC82FUrHgPLQ9dhufUbIWSik2nK2Uqrf+tthabGa9Uj58myArJfsFzu+95X5HqXbhdmg9M1MyXTS+nf+rVCs204yCK1UfxGv5bkiaekGpNjf20HSObuMzSsYCoeRIgTLrrYm3xZqStdj25zHkacQrKRVeDbSr35VKDyouys4BfkXJ8sDD25e8K5WKzXTnNiX7N0ttkW4/VCr166E7f93GHsUThdd/MIPSt6SUGQvOLcVm1h1rmtvnLSX7bbBdDwop1e4OzLxtZZle9XbkWKlSbKYafVt4maTyOWv3t06dQakwILA49HL3bmOPkm2O3RRzn5TZCs6XxaaSdUTq0HctV6Jkvu28q/+VLKNpW6QsHCIpWc4PL0etlCrFZtqHyJSqt9NTT2NRqsyLPTTVrNvYo2Squj/62qRslMwPXu31rtjMOiJV9lU0WynZRyU23d5Ssr/JoFX+6fN3lCzb1suRK6VCsXnoZH4mpcoo3Fr6Y7xS4Xb6oW8S6ja2lEwH91IPA22lzPLQ0LtiM+POlP7gHkXJPBl90y0u/9zq32Q3/Yj5Qsmwbb0cvVIqFJvpv1VKqXa+KnGMV0pcSLfLvUW3saVkObj7ynnKSfamzPBapKc7tZL1Fnr6g3sUJfPo5qbbXEqlyfTTj2oulCzb1tPBCCV7sVni4kTJfpekVebOlZK+DmiXeYtuY0vJcnCf+sCteAeueHti7VWxmfEhs9uMPC2UzAezt3ctmp/P7I7bVoa5m0/PE0r2YjPlAyw9SpXBkVKvHFMqzN3cXbx3G1tKhiuYW9zqVKq/PPdVsZnxRe5lDu5HKd6mfHfCJ4fsFzMvR5mVSvvHtE+gL5T1tpXloa2nD48q2YvNMq9eUzIOHvSUO8Yr2Y/T8cWmkmEOgYvd6d5/+IxS5Yqx51WxmfEJ4qnm/yp+2MzzqTKd/Pd4+QokpdILkKc6ZinLtuWLxszb1tMRZSVzsVlqBM6UCrfSy+2HSsaBmbUhxWaGicDTjxCsKdXmw6x1i03F69T+7NVKv9VA8YWgRxd8YKrw4MMWL+9g+L+vfjazcoXDmrJsWy7uK25b3YtIJfO67HrXbAZK9vnTJfdDJfsbaoYUm1dX2C66pnv6/B2l2ldYLZ4Vmxlvf5aZA6y4WHcfLif/qhcjb7Xrvtb+bGKV5t6tty0XDzNsW93pGErmYrPcg4pK9vNUuQJ+oWTeD4cUm1fvnLca1VwoVUc3nxWbGQ9KaS9ilPXJv/pDY3t13zjh9tXPZJf21p2yzLOcedvqnjeUzMVmxdu9GQcR1sq+aUTJvK0OKTavLHj8t283qrlQss/b6HlWbGa73ZLm9oriIirbQxZX6o44u331M5ll27Yqz+E9qjtFRsl6Aj/0ouyrKdnfC1l23rSSedQ4tthUrp5nN8VXUh6llHjBa+NZsZltBOWSEXPFn+nykEXmK9crPRuVyn7LbnHJrTtlxjm8R3UfElKy9svLucqZNeuRSruslSiZn/YPLzavHiKf9gXuWynVThrPis325662e2fZS/HF2vohixnmwp3h2TZUZV8YfutOuc0c3qPaPjOFYjOYknXEvPpDepmnKIQXm1fetrrdC5F7lCq3DhdfDppKxrl2odMz/PtkOfnfcZ5lpO5Jwu2rn8ks/Nadwra135fBCiVrsVn22QSFAn4A5eo7y6+EF5tX3rYq+xRZJCXzBtfTKzazXaF9fCGjeJ2YZzlI29/W/kxSH8+9U5jDG+PLCVGh2AymUGwO0qxPJuHFpg926z9wpuG3OatQsu7MPb1i88qLlp5d79dUlocsmAt3nh9GnpUq85d3neAU5vCO86WAUyg2gylZ+7T0e5StWZ9MwovNyzaidlnuTMlWrL1Sodh8emBXlocsvMze/pkLd40fDmb+36v/lhnbVh4UmydQ6NNBmvXJZJpis/zwdySlyonWesXmlSPkPd8e4FDaeZac/POoWmx+e22Twhze6315m4lCYRRMoU8HadYnk/Bic/3Lz1R+I4mkVJq32Ss2sx2MvDyc/HP74Rt4/L9X/y0zb1vMs8yhwrFoQbEZj2JznGmKzbJv/R9FqVIcVTrAI68fThT+36v/BmxBsXkChT4dpFmfTKYpNnevyOyUKgUbxSYiUGziUxSbJ1Do00Ga9clkjmKzXQ64W8qcbHsHeOZCYi+KTXzqy2uoFAqjYAp9OkizPpnEFZvKZa8aaZcF7pbSxWb7M8A7FJv42HobemxHFEbBFPp0kGZ9MgktNq96+rP0V0yNolT5JiGKTXzKD9h8e6p7tQ1RbGIPz3H/8p5FhcIomEKfDtKsTyZTFJtfihW4W8q8+oViE3t4ioVfDeRi8ukB7PHf238LLFzweBvxe0yffl3o4+faf5sBxWY8is1xKDZnpVBsYgYetfS35XikfvN3iCsUm1h41NLv7vXrsL58//krCoVRMIU+HaRZn0woNmelUGyimm+3MsVFwe6D05pCsXlf61HLH77GdK/H72p/fwYUm/EoNseh2JyVQrGJ7HzS8ajly1uZRygUm/fgke9Do5ZbKBRGwRT6dJBmfTKh2JyVUrnYzHowwnFho5ZbKBSb8/F8XR8b/Nn6+PbRqOUWj7/XLkcGFJvxKDbHCS02r/qKRIrNDoViE1daioLwUcstHn+7XSbUsoxaer5u+KjlFgqFUTCFPh2kWZ9M4opNa375Wf7oLcvdKWW+G7qz7BSbtRx+AGMUhWKzltNHLbd4LFO7rBlQbMaj2BxnimJTf7q/PHemVDnZUmzWsxQFHz+AMcpj2drlRh6H3jJwNoXCKJhCnw7SrE8mcxSbkvKEdyXFI01tP2XUKzZ9Emp/Dte4/FbmEUqVaSR3sOndqBkpFEbBFPp0kGZ9MgkvNq/6TutSB7AzKFl36Fav2OQW6DVS3so84rH87frhHN6G0o9abvFYl3b9MqDYjEexOU54sXnVRvRLb3nuTLmq8N+rV2z6JNX+HOKVHLXcSmnXF/FOfcvA2RQKo2AKfTpIsz6ZTFNs/ru3PHel/LTqm+x6xSajUvF+uJUp0089Udo+wOd8jB/ybtSMHuvb9kEGFJvxKDbHCS82r5pr93tvee5KqfRwRK/YrFQsZ1XiAYyRlKwntSqmHrXcQqEwCqbQp4M065NJeLF55Vw7HhJ6UCo9YPOl2HysQ/tzeK7sAxgjPfqk7Ss85yLA21Datwyc7dEnbT9lQLEZj2JznPBi88oRtX/2lumOFI9ItP2T1bNi0yNz7c/iLz5Y3+ZW5lHKlRe/2aV7N2pGCoVRMIU+HaRZn0zCi80r59oxb1OUv6/6pIJnxWaVVzeNdvtbmUcplaaTjOYTPKOWOz36re3LDCg241FsjhNbbFrzB870Z2957kapdAvdnhWbdx2V8oGYUcsA7j9p+/cOpn7LwJkUCqNgCn06SLM+mQwpNq+8hXv7W+lKpVvo9qzYvMMT6etbmYxaDqBUeQXYUdO8GzWjR9+2fZ4BxWY8is1xhhSbV25I3cLlLpSKBdqzYvNvq5+ZxVIUcCvzJI8+bz+Hyhi1PJFCYRRMoU8HadYnkyHF5tW3P297AFYqPn379AJBqfyQEA9gJKBUno7BWwYuplAYBVPo00Ga9clkSLF59aT8Wz4opFSdn/aq2Kwy/3R9K5NRy0SUSqP9t383ajYKhVEwhT4dpFmfTIYUmxmKntsdqJWq7xR8VWxefeHyjgsDRi2TW31eGXkEnFHLpBQKo2AKfTpIsz6ZxBebplz9kMqt5m4qlR+meVVsZp+3+VtvuZGLP6fVZ5aNR8UZCU9KoTAKptCngzTrk8mwYjPDOxJvM1qgVJ7b+PLCwP999bMZcbszOcXzZtvPLZPyJ7lZKRRGwRT6dJBmfTIZVmx6ztH6D13Bo6vTjxgo2U+k77wrNrOvH18mkJySfT4zo5tJKRRGwRT6dJBmfTIZVmxmObhPXQgo/rYgn6ja9a7kXbFZ4cGnW4yiK75jUfLpaCX76P8tRjcVXzy62CjxMN1jWdefUxYUm/EoNscZU2yakuXgPuWL3hXPZ6x8+3zxdn6tkn09365DdcqzecH+bNI/Qa1kHyH3RePUUzIUH7N6F8dpXxOmUBgFU+jTQZr1yWRosZnltTU+uE33xLCSYV5shC3FZoWpAr/0ln0WytaH/ry/pRv9VCqMkE990aLsOSekGP18LEe7bBlQbMaj2BxnaLHpW7zrP3alqUYNlCyFfIQtxWaFbxOadmRK8Um/Xd89Uox+KllPcmuz3on59HxwyeinQmEUTKFPB2nWJ5NxxaYpV78Cac0nvPKT8JUMD19F2jSao1QYyZ1uZEoZcdF4yeinUmHfme6iRRk15cdFiy9iPPo5pM8ef6P9uxlQbMaj2BxneLGZbQTOB7yyB3Klwu3kvbYWm1XeJfprb/krUp7NsRvhlNFP5az1+cQUF8YL5awLRQ9u+CLGx8mQixiFwiiYQp8O0qxPJsOLzYzzpHyyKTeHU5lljmZr82igkmmk/JXyt0KVqx9AGzL6+fh97d/KaIovDFCuHnD4aPTz8e/b35kBxWY8is1xxhabplx5wnrGJ7ISBYHik37WnTPCnmKz0hSCsgWncnWh+YyX6aMHsZQKDwotSr+6Tcm4vy6jn5tGjhUKo2AKfTpIsz6ZnFJsZi4QfMWb9naV4lvHFW77fWLXPEelyuimlSs4layF5uLjaQpKpbsEJQtOJfMI8h+9Ze5RKIyCKfTpIM36ZDK+2DQlc8Hk4iXsNl0ExSf8bPNdR9lbbFabt1qm4FQ86pe50LSPp8AolUY3rVTB6eVdLXtGmy9YFAqjYAp9OkizPpmcVmxWmCflHeDyh4cUF1Ozj2au7S02XYhXGt209A8NKZ7Tln272zwi9Y5SbQ60LwJSPzSkVLhYsc0XLAqFUTCFPh2kWZ9MTis2S82TklNHOhUXUC4yqxVREXYVm6ZUmru5SHEx01IqjaKHvThfqTa6ab4YSHUXZqFUuUj+vbf8zygURsEU+nSQZn0yOafYNKXiSIIPoCNfw+I5me6XO41ktnYXm6ZULMz9Oaf5piHFRXuVfnTfhY7sKVWeTG+lmWuu+D2sWYuHnl3TWhQKo2AKfTpIsz6ZnFpsVhxJWLjw/PgVLIr7wCf4uxeYa0eLzSrv3ezx9nTZCJX/tlQqECx83qLiUd2q++GlFy6Kj2XVBhB2T8NQKIyCKfTpIM36ZHJesWlKtYPTMy4WvMO4ADWPgPoEvvD8t+W/eZ39sxSXfYeKTVP8CpP291Xi7eLn3rpFU1xY+UKnwpy6niF3GJSKUzLWPDLt488pI52Kj29Vj+O7H9ZTKIyCKfTpIM36ZHJ6sVl5dBNjfFJsenuaoYh3weBbo+FfNqD4wqf6SPrQB6yUrCe/Pfz5+nMOv3hRfKvc22flOeWHHi5TKIyCKfTpIM36ZHJusWmKR/vWC4F7O1xsmuJRnfZ3VuYTuosGj7jt2kEVF9/LqPoMBZS5iBo6aqfMctGy8Lp8//Yl2dx//tnHv/F+5d8xS78cKsIVCqNgCn06SLM+mVxSbFaeJ4V4HxWb5t+x+n0z8v7idXym6q3xLU6Zl+i/s/qbs+ptO2u9fzODT+6eZO0Xis14FJvjnF9smuLRl/WC4L4iis3ZRqbwl4+3jT3891Z/G3PwceHwfF+FwiiYQp8O0qxPJtcUm6ZwYIeFFBQKFzBzcZEQPof1FYW7LvP59Lv0KYyCKfTpIM36ZHJpscloFCxs9Eqp+pQsvrrktT6K5yu2y4KaIu6aUBgFU+jTQZr1yeS6YtOUO8yTwmuht0qVmecv3sVvvc/2LAoPMdbngYyPHyxTKIyCKfTpIM36ZHJtsWlK9Xcl4jPRxSa3QmvzxcLl346jMEpe2+6TW49CYRRMoU8HadYnkxTFpouDyu9vw2dCi01T/F5ACs56/JmdOk/zGcXHJUbJa9r98vZnFAqjYAp9OkizPplcX2ya4uJgvWC4j/Bi0xTm3tWTotBcKBSc9YR+AYBCYRRMoU8HadYnkxzFpinVvzYOxwwpNk1hm6ojbDQqksK0jDpGfH8+hVEwhT4dpFmfTPIUm6b4K9HWC4g4PmFmnK4wrNg0hYIzv5SF5kJhWkZ+4YWmKRRGwRT6dJBmfTLJVWyawsT8Mfweyow7+dBi0xQKzrxSF5oLxQUnc8tzGlJomkJhFEyhTwdp1ieTfMWmKVk3xqq+ndCVWxabprjgZHQqlxKF5kJhDmc+wwpNUyiMgin06SDN+mSSttjkoB7n+8FYuW2xaQq3Q3PwZ5DqYaCtFI5NeQwvDhQKo2AKfTpIsz6Z5Cw2TeGg/rkfrvqVWxebprjgZLu6jvu+ZKG5pjDd5zq+WDllVFyhMAqm0KeDNOuTSd5i0xQXnBzUj/lye0m5fbFpircrvkzgfO7zy1/YHkXhG9DO53mzp12sKBRGwRT6dJBmfTLJXWwuFArOfbrzmBSKzRWFryU8zyXfdT6a4ve5MjXjHKdfrCgURsEU+nSQZn0yqVFsmsIowjZPby8pFJsNxcUCTxmPM8Vt81cUj5RnPYHOwMX8JRcrCoVRMIU+HaRZn0zqFJvmBRZGEfrcLz/3+m2hUGx2KNxWH8PvzZ3mtvk7ii+IOT7F8jHrp15/n+Hx99tlyoBiMx7F5ji1ik1TfhIe8PjRptEjhWLzBcXvImWU83PTj2Y+o/j4lPVkWomL9stfjaVQGAVT6NNBmvXJpF6xuVCYb/cXz2fdNHqkUGy+oXiUk23rmBQFQgYKFy7HpRkRVyiMgin06SDN+mRSt9g05c6vsXl727ylUGxupHiEilvr23hbdIF+m1vmW7g/Hv3CrfVtfHy67JZ5z2OZ2uXMgGIzHsXmOLWLzYVyt7lSm0cz1xSKzZ0UzxPOenC8mve5Q9vinSi+cOGNGs95/9p9MjrDY9na5c2AYjMexeY4cxSbpngUwbdf1is4G4/iHj4oKxSbBykUnf/DSOYBylJ0MtL5F/dFyiJzoVAYBVPo00Ga9clknmJzocw4iuC5Xx/PhVMoNj+keOrGXUepvm2HQpH5AfefuFi/45xOF9oeFEh1u/wZhcIomEKfDtKsTybzFZsLxUWnD2qVRxFCisyF4v7wjp7Jr71lzU5xweDpG7MXDMut8ls+XT6a4uL9DnODvY7lHh5TMh4zreyDeAp9OkizPpnsPn90GzNTXBT4gF7pQSIfmHc9/IPrKB7t9AF0lsLTBWbJ4qAqxRfHvniZ6YFHr4vXqcQoJoA8uo1VKJmLAg7ME1CWbaxa0eB9wiOYXORcTFkKz2ojnt8vUoTjGIDDuo0V+WAoywH9qlvt/tsUmJNSPKru9y1mLD69PC4uKQySU/xwmud4+nZU+zleaSkufQxjmgWAMN3GGfhg+Tho+gQ84qC+nNz9N1I/gYlx/Nk/toFl3tLoCx3/fv+d79ue8IBPYYqPVb5IWArQMy5k/HdcWPpv+gKKCxQAw3QbZ6V4ZMon52VkYbEUCmvLgXjhk4H/HQdlvKW4gPD2shQRi3Y761n//LLdMdJ0M4rv1vSOV8sF9Cs+pi0/v1yUGBcmAE7XbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAA4HP//b//B3B+JA7XYURBAAAAAElFTkSuQmCC',
                alignment: 'left',
                width: 100,
                height: 40,
                marginTop: 40,
                marginLeft: 25,
                marginBottom: 20,
              },

              {
                columns: [
                  [
                    {
                      text: `${profileDetails.userDetails.firstName + ' ' + profileDetails.userDetails.lastName}`,
                      fontSize: 10,
                      bold: true,

                    },
                    {
                      text: `${profileDetails.userDetails.address1 + ', ' + profileDetails.userDetails.address2
                        + ', ' + profileDetails.userDetails.city + ', ' + profileDetails.userDetails.country}`, fontSize: 10,
                    },
                    { text: profileDetails.userDetails.email, fontSize: 10, },
                    { text: `+971 ${profileDetails.userDetails.mobile.substring(3, profileDetails.userDetails.mobile.length)}`, fontSize: 10, },
                    { text: `CIF Number: ${profileDetails.userDetails.cifNumber}`, fontSize: 10, marginBottom: 10 }
                  ],
                  [
                    {
                      text: `Date: ${new Date().toLocaleString()}`,
                      alignment: 'right',
                      fontSize: 10,
                    },
                    // { 
                    //   text: `Card No : ${((Math.random() *1000).toFixed(0))}`,
                    //   alignment: 'right'
                    // }
                  ]
                ],
                marginLeft: 25,
                marginRight: 25,
                margingBottom: 20
              },
              {
                columns: [
                  [
                    {
                      text: '',
                      margingTop: 10,

                    },
                    {
                      text: 'Statement Date:',
                      fontSize: 8,
                      marginBottom: 10


                    },
                    { text: 'Statement Period:', fontSize: 8, },

                  ],
                  [
                    {
                      text: statementDate,
                      alignment: 'center',
                      fontSize: 8,
                      marginBottom: 10
                    },
                    {
                      text: stmtPeriod,
                      alignment: 'center',
                      fontSize: 8,

                    },


                    // { 
                    //   text: `Card No : ${((Math.random() *1000).toFixed(0))}`,
                    //   alignment: 'right'
                    // }
                  ],
                  [
                    {
                      text: 'الحساب' + ' كشف ' + ' تاريخ',
                      alignment: 'right',
                      fontSize: 8,
                      marginBottom: 10

                    },
                    {
                      text: 'الحساب' + ' كشف ' + ' مدة',
                      alignment: 'right',
                      fontSize: 8,

                    },
                  ]

                ],
                marginLeft: 25,
                marginRight: 25,

              },

              { canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, }] },

            ]

          }
          else {
            return [
              {

                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApsAAAEPCAYAAAANjQ+sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACXASURBVHhe7d2PleS2scXhF4JDUAgOwSEoBIegEByCQnAICsEhKASFoAz87l0111wMuptkF8gq8HfP+c57hmZnSDT/FEGQ/X///e9/AQAAgCG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsI7KH8JP+UX633MwAA4J66jcAryj/kX/Kb/CluXPyn928AAMA9dRuBhbIetfxd1oVlD8UmAAD4rtuI+1I8avmLeNTyD1kXkltQbAIAgO+6jbgHxaOWP4tHLf8j66LxKIpNAADwXbcRc1L+Lp+MWm5BsQkAAL7rNqI+5W/iUUs/yBM1arkFxSYAAPiu24h6lGXU8t8yatRyC4pNAADwXbcR+SkeuTx71HILik0AAPBdtxH5KX5qfF3kZUGxCQAAvus2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKzUKUv4s/s63+3vs9uC/lb6vtY6u/9X4XsFDWx6Z/yr8aP8vy3zkuHbDqv8VPvZ+bWbdxRv5wHx/yL+Id6N/yn41+k2XH8++4fId7LIf/n2xuWWwq/jyWA7W3F283f0jbP0f8Kevt0H/Hf49CYiKKT/o+sa+PT79Luz0ctRzPfpXlWHa7k95d+bMWHzv8+Xs78HGl3Ua2Wo5J3k59Tv1H72/egbLeb48e+72fL/vmt/6UqY7v3cbqFH/43qmWA/b6Q43kDcR/w3/r1IO2QrF5EcV9vxxYogrKo/z3lyL0tgf8apT1MSqyoDxiKRy8DfmkyUXMBPw5Pj5Pb2NnHaeW7WjaEVBlOf6PrC0W6xqj9H7ZbazGH8Ljw/CH8snV2qe8Q/vK5OfeckZSKDZPorgwOOvgEsHL6atjbnkloSyjSr4wuPIYtZVPcqccyxDLn5n4XNh+pmfzdu7lKH0cUpb6IsO+uxzbyxWe3cYK3NmybADrDyOLoTuaQrE5kHL2iMAoywUQhefJFBeYPjFcPXL5KR/LfJwtP7oyK38u4m0t6/HKy1WqSFJ8jr16AOsV75NlLga7jZkpHmXKvAH0+GTzz976HKVQbAZTvG25MKu0be2xFJ7M0xtEWU761QvMZ5aL6JJTNhTv4z52ptFbzq0Ub2++61LlmOXlTHsMUpZBrEqDDF7W0PpihG5jRop3zCq3MZ/xjuYDw8dXd4r7o/39GZQrNhUfXGYtDp7xvsQt0iCK90cXYW0/z6ziaFW6c0i7jFspPm5VvjAOORdG8HI8lqdyf6YuOruNmSg+iM9WCHiD/mijUCg2P6DMcHCJUOKqOCv33aMP2369E+9DJUbMlfLFpuLR2eoDLwtvO7/01vMs/vuP5WiXrSofj9Ldeeg2ZuDOkll2qGcObxT+d4/fkU3qYlNxkVltGsYZKDp3cF89+qztx7vzvpW26FRKF5uKL5C//I4JeEDp1Hnlis+hM+/DntOZ5q5Dt/FK7hy52+2o3RuFQrG5g8JI5jYUnS+4bx591PYb/sf7WJpbpGtKyWJT8fFr9sEXGz7Kqbgvsz5YHM37YorpUt3Gq7hTHp2z7qy72LVRKBSbGymz3SY5g09sPMH+oHh/u9u83k95n0t14aKUKzYV3za/0/Fr2Iic4v34jueCf/f640zdxrMpd7rSeOfXXh+1FIrNNxQKhM95Ll66EaqzeN3lbndaoqW5cHksS7t8l2qXcU2p/hDQUeG31RUfy9q/cyfu08uO5d3GMyl3vdJ45e1GoVBsPqG4QLj7gSWSbxvf7tuJlDvfaRnhX71+PpNSpthUXGh++fkb8b73ccGp3GUKwhYhfXpEt/Esim9vrjsC//Nyo1AoNjsU33JiTt0Ym0bdq1O40zKOL6Qve4BIKVFsKncvNBcfFUf+t8LdrR9dUnB2G8+gcGvqPW8U3XmcCsVmQ5n1Sc1MLi0WRlO40zLe0+PaaEr6YlOh0PzRoeLI/+bxb9vfh7/65dS7Vd3GkRSPGnClsc+XSfYKxeaDwm2Sc11WLIykcLFyrtNHypXUxabiAunLz2Bfwemfffyb9vfgf3b16ae6jaMoFJrH/VBwKhSbonDb/DqXvow5isJt8+u4+DvtoYXH32uX4VKrZfN36VMgPee+ebutKBSa251WcHYbR1AoND/3veBUbl9sKjzAcb3LX6nxCYXj0vXc/6cUnErmYpPt8L3fl/7qUbw/c07Yx/01fGpUtzGawgE9zrd5Fv6/q7ZMTik2FeY15eH515e9UuMohRGQPE4ZYVFSFpsKb8/Yrjv9QqHOOG74BV+3MZJX4LEi6xXDcd8OynLbYlOh0Mxn+MEqkkKhmc/wglNJV2xK1mN5Zl8eblF46PgzQ+9SdRujKBSaY3iOom8h9/7b1YYWmwqFZl4lCk6FQjOvoQWnkrHYZM75fu6z78cahdcoxhj24Ge3MYrCpPtxsp4shxWbCoVmfqfN2T1CodDMb1jBqWQsNnHMt9vpCg9WxXE/Dpm/2W2MoDAH5Z6GFBsKhWYdKR8aUig06/hh5CqKQrE5FxeafKaxxpzDe42fUigM7it8Q1XYnupJ9W1DCqMf9YRPy1AoTObCPj1G+O30buMnFEYP7i202FS8PbV/AzV8+TKCKyjMHa/rt95nepRCsQm8F35nodv4CYWD+r2FFZsKo1H1nfLC4FcU5o7X9q/e53qEQrEJbBO231m38Sgv3GpBcU8hxabCaNQchsy920rhmDSHkO9xVig2gW080BN27O42HqFwuxMWVWzyzrR5hN4K3UrJ+now7Bdy4lMoNoHt4u4q9BqPUBiFgn1cbCoUCfM59XvUFY+MMwVjLhHHFopNYLuw0c1u414Kt6qw+OiEoMw2T9Pr4hOcXwXm/cT8dL2/NcSWNvPcwlkv2twPw79/d6HcoahYti3fBbjLtvXRRYtCsQnsEzJQ0G3cQ+EhDqx9WmxWPxl4jqJP/h6dPVxcKS4W/K0YMz3cEjLF4h1l1m8T8XHW25YLyk+2LU95qrptfXTRolBsAvv80duX9uo27qEwtw5rhwsKpfL7NL0fhDzE0FJ8S9h9M8OJcujrkJQZL36/Xbz01vdTSsVt6/AcYIViE9jv4+NPt3ErhYeC0DpUbCoV59h5eX2L8rSnrRXvc5Uv8Nxnw/pLmWUk+Ipty4V6lW3r0IWdQrEJ7Pfxt8J1G7dS2HHROlpsVvt6Uy/vaYVAS3FhUHX/G/LtQoqnHrR/qyIXfFdvW9mL9kO39hTOWcAxHx2Tuo1bKLMc2BFrd7GpVBoh90MWl7+ofKF4P/Q80XY5swt/WEip2A9r3raGTMU4wssimft094MLyuzFpkfEvY7Lg2H+DNc8V3d5YKz6/nIm99WzfvU0FLf5InHmt/J8NAWq27iFMsNO6w3IO503FD/Q4Q3n6Unw8d/NO6xHtrhK/upIsVmlH1N93/dC8RSEarfWP74ts6ZUnu9rl45mPuNleixbu7wZ7J6Socx4zHaB43PS7otgxaPY/rczF0lHLf2668JYWeZBVzsmv/PR+5K7je8o3kDXC1GJi8vdG9ArigtVF59cKe4sNhUX7+3vyCjF93y/4mVcLW8FIfug4oN75X2vyrbl4q5d9qvteum0MlOx6XUJGwlXKs3ZHcl9EHls8mBWxn1nrz9767hVt/EdpdoG6RORC8zhIweKi6c777B7i83sB38fJNLcNn/Hy/pY5nY9MgoZ3VS8b7e/uwJ/TkOeMh9BybhteXk2H9eVGYrNodMtFH/Odxzp9LYRNgi1psxSdB7e7rqNryjutPUfz8w7zCWjBspdrxI3F5tK9lFNHxjKFJoLL/Nj2dv1yejjg7tScVSTbSvO5mO8Ur3YDPv6wHf8t1Z/d2benk+56FNcF1Qu5A9vf93GV5QKowg++aS4NaXcrejcU2xmfuK1ZDGw8LI/1qFdr2w+mgerVJ2rmeZBoL2UbNvW5ifTlarF5mkF0Zr/5uNvt8szCxd+px/nlWpvX1nsfiZj0W18Rck+inDpK2meUTyKV3le2VabNkYl+7zf9PPo3lFcFLTrlY1PZIf3V6XiPjXLtpWpCNlUiCkVi03382UXvv7bj2Vol6s6F5qX1QpKyQvldj226jY+o2Q+eXlnSD1aoHgKwuyjnFuLzcxXdqfdqhpNqXBAO1R8KVUeLlsLfQr/SkqmbWvTk7JKxWLz8jssSsV97ZVLC82FUrHgPLQ9dhufUbIWSik2nK2Uqrf+tthabGa9Uj58myArJfsFzu+95X5HqXbhdmg9M1MyXTS+nf+rVCs204yCK1UfxGv5bkiaekGpNjf20HSObuMzSsYCoeRIgTLrrYm3xZqStdj25zHkacQrKRVeDbSr35VKDyouys4BfkXJ8sDD25e8K5WKzXTnNiX7N0ttkW4/VCr166E7f93GHsUThdd/MIPSt6SUGQvOLcVm1h1rmtvnLSX7bbBdDwop1e4OzLxtZZle9XbkWKlSbKYafVt4maTyOWv3t06dQakwILA49HL3bmOPkm2O3RRzn5TZCs6XxaaSdUTq0HctV6Jkvu28q/+VLKNpW6QsHCIpWc4PL0etlCrFZtqHyJSqt9NTT2NRqsyLPTTVrNvYo2Squj/62qRslMwPXu31rtjMOiJV9lU0WynZRyU23d5Ssr/JoFX+6fN3lCzb1suRK6VCsXnoZH4mpcoo3Fr6Y7xS4Xb6oW8S6ja2lEwH91IPA22lzPLQ0LtiM+POlP7gHkXJPBl90y0u/9zq32Q3/Yj5Qsmwbb0cvVIqFJvpv1VKqXa+KnGMV0pcSLfLvUW3saVkObj7ynnKSfamzPBapKc7tZL1Fnr6g3sUJfPo5qbbXEqlyfTTj2oulCzb1tPBCCV7sVni4kTJfpekVebOlZK+DmiXeYtuY0vJcnCf+sCteAeueHti7VWxmfEhs9uMPC2UzAezt3ctmp/P7I7bVoa5m0/PE0r2YjPlAyw9SpXBkVKvHFMqzN3cXbx3G1tKhiuYW9zqVKq/PPdVsZnxRe5lDu5HKd6mfHfCJ4fsFzMvR5mVSvvHtE+gL5T1tpXloa2nD48q2YvNMq9eUzIOHvSUO8Yr2Y/T8cWmkmEOgYvd6d5/+IxS5Yqx51WxmfEJ4qnm/yp+2MzzqTKd/Pd4+QokpdILkKc6ZinLtuWLxszb1tMRZSVzsVlqBM6UCrfSy+2HSsaBmbUhxWaGicDTjxCsKdXmw6x1i03F69T+7NVKv9VA8YWgRxd8YKrw4MMWL+9g+L+vfjazcoXDmrJsWy7uK25b3YtIJfO67HrXbAZK9vnTJfdDJfsbaoYUm1dX2C66pnv6/B2l2ldYLZ4Vmxlvf5aZA6y4WHcfLif/qhcjb7Xrvtb+bGKV5t6tty0XDzNsW93pGErmYrPcg4pK9vNUuQJ+oWTeD4cUm1fvnLca1VwoVUc3nxWbGQ9KaS9ilPXJv/pDY3t13zjh9tXPZJf21p2yzLOcedvqnjeUzMVmxdu9GQcR1sq+aUTJvK0OKTavLHj8t283qrlQss/b6HlWbGa73ZLm9oriIirbQxZX6o44u331M5ll27Yqz+E9qjtFRsl6Aj/0ouyrKdnfC1l23rSSedQ4tthUrp5nN8VXUh6llHjBa+NZsZltBOWSEXPFn+nykEXmK9crPRuVyn7LbnHJrTtlxjm8R3UfElKy9svLucqZNeuRSruslSiZn/YPLzavHiKf9gXuWynVThrPis325662e2fZS/HF2vohixnmwp3h2TZUZV8YfutOuc0c3qPaPjOFYjOYknXEvPpDepmnKIQXm1fetrrdC5F7lCq3DhdfDppKxrl2odMz/PtkOfnfcZ5lpO5Jwu2rn8ks/Nadwra135fBCiVrsVn22QSFAn4A5eo7y6+EF5tX3rYq+xRZJCXzBtfTKzazXaF9fCGjeJ2YZzlI29/W/kxSH8+9U5jDG+PLCVGh2AymUGwO0qxPJuHFpg926z9wpuG3OatQsu7MPb1i88qLlp5d79dUlocsmAt3nh9GnpUq85d3neAU5vCO86WAUyg2gylZ+7T0e5StWZ9MwovNyzaidlnuTMlWrL1Sodh8emBXlocsvMze/pkLd40fDmb+36v/lhnbVh4UmydQ6NNBmvXJZJpis/zwdySlyonWesXmlSPkPd8e4FDaeZac/POoWmx+e22Twhze6315m4lCYRRMoU8HadYnk/Bic/3Lz1R+I4mkVJq32Ss2sx2MvDyc/HP74Rt4/L9X/y0zb1vMs8yhwrFoQbEZj2JznGmKzbJv/R9FqVIcVTrAI68fThT+36v/BmxBsXkChT4dpFmfTKYpNnevyOyUKgUbxSYiUGziUxSbJ1Do00Ga9clkjmKzXQ64W8qcbHsHeOZCYi+KTXzqy2uoFAqjYAp9OkizPpnEFZvKZa8aaZcF7pbSxWb7M8A7FJv42HobemxHFEbBFPp0kGZ9MgktNq96+rP0V0yNolT5JiGKTXzKD9h8e6p7tQ1RbGIPz3H/8p5FhcIomEKfDtKsTyZTFJtfihW4W8q8+oViE3t4ioVfDeRi8ukB7PHf238LLFzweBvxe0yffl3o4+faf5sBxWY8is1xKDZnpVBsYgYetfS35XikfvN3iCsUm1h41NLv7vXrsL58//krCoVRMIU+HaRZn0woNmelUGyimm+3MsVFwe6D05pCsXlf61HLH77GdK/H72p/fwYUm/EoNseh2JyVQrGJ7HzS8ajly1uZRygUm/fgke9Do5ZbKBRGwRT6dJBmfTKh2JyVUrnYzHowwnFho5ZbKBSb8/F8XR8b/Nn6+PbRqOUWj7/XLkcGFJvxKDbHCS02r/qKRIrNDoViE1daioLwUcstHn+7XSbUsoxaer5u+KjlFgqFUTCFPh2kWZ9M4opNa375Wf7oLcvdKWW+G7qz7BSbtRx+AGMUhWKzltNHLbd4LFO7rBlQbMaj2BxnimJTf7q/PHemVDnZUmzWsxQFHz+AMcpj2drlRh6H3jJwNoXCKJhCnw7SrE8mcxSbkvKEdyXFI01tP2XUKzZ9Emp/Dte4/FbmEUqVaSR3sOndqBkpFEbBFPp0kGZ9MgkvNq/6TutSB7AzKFl36Fav2OQW6DVS3so84rH87frhHN6G0o9abvFYl3b9MqDYjEexOU54sXnVRvRLb3nuTLmq8N+rV2z6JNX+HOKVHLXcSmnXF/FOfcvA2RQKo2AKfTpIsz6ZTFNs/ru3PHel/LTqm+x6xSajUvF+uJUp0089Udo+wOd8jB/ybtSMHuvb9kEGFJvxKDbHCS82r5pr93tvee5KqfRwRK/YrFQsZ1XiAYyRlKwntSqmHrXcQqEwCqbQp4M065NJeLF55Vw7HhJ6UCo9YPOl2HysQ/tzeK7sAxgjPfqk7Ss85yLA21Datwyc7dEnbT9lQLEZj2JznPBi88oRtX/2lumOFI9ItP2T1bNi0yNz7c/iLz5Y3+ZW5lHKlRe/2aV7N2pGCoVRMIU+HaRZn0zCi80r59oxb1OUv6/6pIJnxWaVVzeNdvtbmUcplaaTjOYTPKOWOz36re3LDCg241FsjhNbbFrzB870Z2957kapdAvdnhWbdx2V8oGYUcsA7j9p+/cOpn7LwJkUCqNgCn06SLM+mQwpNq+8hXv7W+lKpVvo9qzYvMMT6etbmYxaDqBUeQXYUdO8GzWjR9+2fZ4BxWY8is1xhhSbV25I3cLlLpSKBdqzYvNvq5+ZxVIUcCvzJI8+bz+Hyhi1PJFCYRRMoU8HadYnkyHF5tW3P297AFYqPn379AJBqfyQEA9gJKBUno7BWwYuplAYBVPo00Ga9clkSLF59aT8Wz4opFSdn/aq2Kwy/3R9K5NRy0SUSqP9t383ajYKhVEwhT4dpFmfTIYUmxmKntsdqJWq7xR8VWxefeHyjgsDRi2TW31eGXkEnFHLpBQKo2AKfTpIsz6ZxBebplz9kMqt5m4qlR+meVVsZp+3+VtvuZGLP6fVZ5aNR8UZCU9KoTAKptCngzTrk8mwYjPDOxJvM1qgVJ7b+PLCwP999bMZcbszOcXzZtvPLZPyJ7lZKRRGwRT6dJBmfTIZVmx6ztH6D13Bo6vTjxgo2U+k77wrNrOvH18mkJySfT4zo5tJKRRGwRT6dJBmfTIZVmxmObhPXQgo/rYgn6ja9a7kXbFZ4cGnW4yiK75jUfLpaCX76P8tRjcVXzy62CjxMN1jWdefUxYUm/EoNscZU2yakuXgPuWL3hXPZ6x8+3zxdn6tkn09365DdcqzecH+bNI/Qa1kHyH3RePUUzIUH7N6F8dpXxOmUBgFU+jTQZr1yWRosZnltTU+uE33xLCSYV5shC3FZoWpAr/0ln0WytaH/ry/pRv9VCqMkE990aLsOSekGP18LEe7bBlQbMaj2BxnaLHpW7zrP3alqUYNlCyFfIQtxWaFbxOadmRK8Um/Xd89Uox+KllPcmuz3on59HxwyeinQmEUTKFPB2nWJ5NxxaYpV78Cac0nvPKT8JUMD19F2jSao1QYyZ1uZEoZcdF4yeinUmHfme6iRRk15cdFiy9iPPo5pM8ef6P9uxlQbMaj2BxneLGZbQTOB7yyB3Klwu3kvbYWm1XeJfprb/krUp7NsRvhlNFP5az1+cQUF8YL5awLRQ9u+CLGx8mQixiFwiiYQp8O0qxPJsOLzYzzpHyyKTeHU5lljmZr82igkmmk/JXyt0KVqx9AGzL6+fh97d/KaIovDFCuHnD4aPTz8e/b35kBxWY8is1xxhabplx5wnrGJ7ISBYHik37WnTPCnmKz0hSCsgWncnWh+YyX6aMHsZQKDwotSr+6Tcm4vy6jn5tGjhUKo2AKfTpIsz6ZnFJsZi4QfMWb9naV4lvHFW77fWLXPEelyuimlSs4layF5uLjaQpKpbsEJQtOJfMI8h+9Ze5RKIyCKfTpIM36ZDK+2DQlc8Hk4iXsNl0ExSf8bPNdR9lbbFabt1qm4FQ86pe50LSPp8AolUY3rVTB6eVdLXtGmy9YFAqjYAp9OkizPpmcVmxWmCflHeDyh4cUF1Ozj2au7S02XYhXGt209A8NKZ7Tln272zwi9Y5SbQ60LwJSPzSkVLhYsc0XLAqFUTCFPh2kWZ9MTis2S82TklNHOhUXUC4yqxVREXYVm6ZUmru5SHEx01IqjaKHvThfqTa6ab4YSHUXZqFUuUj+vbf8zygURsEU+nSQZn0yOafYNKXiSIIPoCNfw+I5me6XO41ktnYXm6ZULMz9Oaf5piHFRXuVfnTfhY7sKVWeTG+lmWuu+D2sWYuHnl3TWhQKo2AKfTpIsz6ZnFpsVhxJWLjw/PgVLIr7wCf4uxeYa0eLzSrv3ezx9nTZCJX/tlQqECx83qLiUd2q++GlFy6Kj2XVBhB2T8NQKIyCKfTpIM36ZHJesWlKtYPTMy4WvMO4ADWPgPoEvvD8t+W/eZ39sxSXfYeKTVP8CpP291Xi7eLn3rpFU1xY+UKnwpy6niF3GJSKUzLWPDLt488pI52Kj29Vj+O7H9ZTKIyCKfTpIM36ZHJ6sVl5dBNjfFJsenuaoYh3weBbo+FfNqD4wqf6SPrQB6yUrCe/Pfz5+nMOv3hRfKvc22flOeWHHi5TKIyCKfTpIM36ZHJusWmKR/vWC4F7O1xsmuJRnfZ3VuYTuosGj7jt2kEVF9/LqPoMBZS5iBo6aqfMctGy8Lp8//Yl2dx//tnHv/F+5d8xS78cKsIVCqNgCn06SLM+mVxSbFaeJ4V4HxWb5t+x+n0z8v7idXym6q3xLU6Zl+i/s/qbs+ptO2u9fzODT+6eZO0Xis14FJvjnF9smuLRl/WC4L4iis3ZRqbwl4+3jT3891Z/G3PwceHwfF+FwiiYQp8O0qxPJtcUm6ZwYIeFFBQKFzBzcZEQPof1FYW7LvP59Lv0KYyCKfTpIM36ZHJpscloFCxs9Eqp+pQsvrrktT6K5yu2y4KaIu6aUBgFU+jTQZr1yeS6YtOUO8yTwmuht0qVmecv3sVvvc/2LAoPMdbngYyPHyxTKIyCKfTpIM36ZHJtsWlK9Xcl4jPRxSa3QmvzxcLl346jMEpe2+6TW49CYRRMoU8HadYnkxTFpouDyu9vw2dCi01T/F5ACs56/JmdOk/zGcXHJUbJa9r98vZnFAqjYAp9OkizPplcX2ya4uJgvWC4j/Bi0xTm3tWTotBcKBSc9YR+AYBCYRRMoU8HadYnkxzFpinVvzYOxwwpNk1hm6ojbDQqksK0jDpGfH8+hVEwhT4dpFmfTPIUm6b4K9HWC4g4PmFmnK4wrNg0hYIzv5SF5kJhWkZ+4YWmKRRGwRT6dJBmfTLJVWyawsT8Mfweyow7+dBi0xQKzrxSF5oLxQUnc8tzGlJomkJhFEyhTwdp1ieTfMWmKVk3xqq+ndCVWxabprjgZHQqlxKF5kJhDmc+wwpNUyiMgin06SDN+mSSttjkoB7n+8FYuW2xaQq3Q3PwZ5DqYaCtFI5NeQwvDhQKo2AKfTpIsz6Z5Cw2TeGg/rkfrvqVWxebprjgZLu6jvu+ZKG5pjDd5zq+WDllVFyhMAqm0KeDNOuTSd5i0xQXnBzUj/lye0m5fbFpircrvkzgfO7zy1/YHkXhG9DO53mzp12sKBRGwRT6dJBmfTLJXWwuFArOfbrzmBSKzRWFryU8zyXfdT6a4ve5MjXjHKdfrCgURsEU+nSQZn0yqVFsmsIowjZPby8pFJsNxcUCTxmPM8Vt81cUj5RnPYHOwMX8JRcrCoVRMIU+HaRZn0zqFJvmBRZGEfrcLz/3+m2hUGx2KNxWH8PvzZ3mtvk7ii+IOT7F8jHrp15/n+Hx99tlyoBiMx7F5ji1ik1TfhIe8PjRptEjhWLzBcXvImWU83PTj2Y+o/j4lPVkWomL9stfjaVQGAVT6NNBmvXJpF6xuVCYb/cXz2fdNHqkUGy+oXiUk23rmBQFQgYKFy7HpRkRVyiMgin06SDN+mRSt9g05c6vsXl727ylUGxupHiEilvr23hbdIF+m1vmW7g/Hv3CrfVtfHy67JZ5z2OZ2uXMgGIzHsXmOLWLzYVyt7lSm0cz1xSKzZ0UzxPOenC8mve5Q9vinSi+cOGNGs95/9p9MjrDY9na5c2AYjMexeY4cxSbpngUwbdf1is4G4/iHj4oKxSbBykUnf/DSOYBylJ0MtL5F/dFyiJzoVAYBVPo00Ga9clknmJzocw4iuC5Xx/PhVMoNj+keOrGXUepvm2HQpH5AfefuFi/45xOF9oeFEh1u/wZhcIomEKfDtKsTybzFZsLxUWnD2qVRxFCisyF4v7wjp7Jr71lzU5xweDpG7MXDMut8ls+XT6a4uL9DnODvY7lHh5TMh4zreyDeAp9OkizPpnsPn90GzNTXBT4gF7pQSIfmHc9/IPrKB7t9AF0lsLTBWbJ4qAqxRfHvniZ6YFHr4vXqcQoJoA8uo1VKJmLAg7ME1CWbaxa0eB9wiOYXORcTFkKz2ojnt8vUoTjGIDDuo0V+WAoywH9qlvt/tsUmJNSPKru9y1mLD69PC4uKQySU/xwmud4+nZU+zleaSkufQxjmgWAMN3GGfhg+Tho+gQ84qC+nNz9N1I/gYlx/Nk/toFl3tLoCx3/fv+d79ue8IBPYYqPVb5IWArQMy5k/HdcWPpv+gKKCxQAw3QbZ6V4ZMon52VkYbEUCmvLgXjhk4H/HQdlvKW4gPD2shQRi3Y761n//LLdMdJ0M4rv1vSOV8sF9Cs+pi0/v1yUGBcmAE7XbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAA4HP//b//B3B+JA7XYURBAAAAAElFTkSuQmCC',
                alignment: 'left',
                width: 100,
                height: 40,
                marginTop: 40,
                marginLeft: 25,
                marginBottom: 20,
              },
              {
                columns: [
                  [
                    // {
                    //   text: '',
                    //   margingTop: 10
                    // },
                    {
                      text: 'Statement Date:',
                      fontSize: 8,
                      marginBottom: 10


                    },
                    { text: 'Statement Period:', fontSize: 8, },

                  ],
                  [
                    {
                      text: statementDate,
                      alignment: 'center',
                      fontSize: 8,
                      marginBottom: 10
                    },
                    {
                      text: stmtPeriod,
                      alignment: 'center',
                      fontSize: 8,
                    },

                  ],
                  [
                    {
                      text: 'الحساب' + ' كشف ' + ' تاريخ',
                      alignment: 'right',
                      fontSize: 8,
                      marginBottom: 10

                    },
                    {
                      text: 'الحساب' + ' كشف ' + ' مدة',
                      alignment: 'right',
                      fontSize: 8,

                    },
                  ]


                ],
                marginLeft: 25,
                marginRight: 25,


              },
              { canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, }] },
            ]
          }
        },

        content: [

          //Summary
         

          {
            columns: [
              [{
                text: 'Summary of Accounts',
                fontSize: 12,
                marginBottom: 10,
                color: '#6E3DEA',
              }
              ],
              [
                {
                  text: reverseString('ملخص الحسابات'),
                  fontSize: 12,
                  marginBottom: 10,
                  color: '#6E3DEA',
                  alignment: 'right'
                }
              ]
            ]
          },
          {
            
            columns: [[{
              text: `Deem ${profileDetails.creditCardDetails[0].cardType}`,
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA'
            }
            ],
            [
              {
                text: reverseString(arabicText),
                fontSize: 12,
                marginBottom: 10,
                //color: '#6E3DEA',
                alignment: 'right'
              }
            ]
          ]

          },
          {
            columns: [
              [{ text: 'Card Number', marginBottom: 10, },
              { text: 'Total Payment Due (AED)', marginBottom: 10, },
              { text: 'Minimum Payment Due (AED)', marginBottom: 10, },
              { text: 'Payment Due Date', marginBottom: 10, },
              { text: 'Total Credit Limit (AED)', marginBottom: 10, },
              { text: 'Total Cash Limit (AED)', marginBottom: 10, },
                // { text: 'Total Outstanding Balance (AED)', marginBottom: 10, },

              ],
              [
                { text: profileDetails.creditCardDetails[0].primaryCardNumber, marginBottom: 10, alignment: 'center', },
                { text: numberWithCommas(profileDetails.creditCardDetails[0].totalPaymentDue), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.creditCardDetails[0].minimumPaymentDue), marginBottom: 10, alignment: 'center' },
                // { text: 'Sunday, 24th January 2021', marginBottom: 10 },
                { text: profileDetails.creditCardDetails[0].paymentDueDate, marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.creditCardDetails[0].totalCreditLimit), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.creditCardDetails[0].totalCashLimit), marginBottom: 10, alignment: 'center' },
                // { text: profileDetails.creditCardDetails[0].totalOutstandingBalance, marginBottom: 10, alignment: 'center' },
              ],
              [{ text: reverseString(' رقم البطاقة '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' إجمالي الدفعة المستحقة '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' الحد الأدنى للدفع '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' التاريخ المحدد للدفع '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' إجمالي حد الإئتمان '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' إجمالي الحد النقدي '), marginBottom: 10, alignment: 'right', },

              ],

            ],
            fontSize: 8,
            marginBottom: 10
          },
          //Everyday Account
          // {
          //   columns: [{
          //     text: 'Deem Everyday Account',
          //     fontSize: 12,
          //     marginBottom: 10,
          //     color: '#6E3DEA'
          //   }
          //   ]

          // },

          // {
          //   columns: [
          //     [{ text: 'Linked Card Number', marginBottom: 10 },
          //     { text: 'Total Balance (AED)', marginBottom: 10 },
          //     { text: 'Main Wallet Balance (AED)', marginBottom: 10 },
          //     { text: 'Savings Wallet Balance (AED)', marginBottom: 10 },],
          //     [
          //       { text: '4582xxxxxxxx1479', marginBottom: 10 },
          //       { text: '17,946.10', marginBottom: 10 },
          //       { text: '13,984.06', marginBottom: 10 },
          //       { text: '3,962.04', marginBottom: 10 },

          //     ]
          //   ],
          //   fontSize: 8,
          //   marginBottom: 10
          // },
          //Personal Loan
          {
            columns: [[{
              text: 'Deem Personal Loan',
              fontSize: 12,
              marginBottom: 10,
             // color: '#6E3DEA'
            }],
            [{
              text: reverseString('قرض ديم الشخصي'),
              fontSize: 12,
              marginBottom: 10,
             // color: '#6E3DEA',
              alignment: 'right'
            }],
            ]

          },
          //
          {
            columns: [
              [
                { text: 'Loan Account Number', marginBottom: 10 },
                { text: 'Principal Outstanding (AED)', marginBottom: 10 },
                { text: 'Payment Due (AED)', marginBottom: 10 },
                { text: 'Towards Principal (AED)', marginBottom: 10, marginLeft: 10 },
                { text: 'Towards Interest (AED)', marginBottom: 10, marginLeft: 10 },
                { text: 'Late Payment Fee / Other Charges (AED)', marginBottom: 10, marginLeft: 10, },
                { text: 'Payment Due Date', marginBottom: 10, },
              ],

              [
                { text: profileDetails.personalLoanSummary.loanAccountNumber, marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.personalLoanSummary.principalOutstanding), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(totalpaymentDueAmount), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(towardsPrincipal), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.personalLoanSummary.towardsInterest), marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.lateFeeOtherCharges, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.paymentDueDate, marginBottom: 10, alignment: 'center' },
                //{ text: 'Friday, 1st January 2021', marginBottom: 10 },

              ],
              [{ text: reverseString('رقم حساب القرض'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('المبلغ  الأساسي المستحق'), marginBottom: 10, alignment: 'right' },
              { text:  reverseString('المبلغ  المستحق'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('المبلغ  الأساسي'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
              { text: reverseString('الفائدة'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
              { text: reverseString('رسوم  السداد المتأخر /  الرسوم الأخرى'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
              { text: reverseString('تاريخ  السداد'), marginBottom: 10, alignment: 'right' },
              ]
            ],
            fontSize: 8,
           
          },
          {text: '', pageBreak: 'after'},
          //Double Secure
          {
            columns: [[{
              text: 'Deem Double Secure',
              fontSize: 12,
              marginBottom: 10,
             // color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString(' ديم دبل سيكيور '),
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          },
          {
            columns: [
              [
                { text: 'Monthly Billing Amount', marginBottom: 10 },
                { text: 'For more details and Terms & Conditions, please visit', marginBottom: 10 },
                { text: 'www.deem.io/cards/cash-up and ', marginBottom: 10 },
                { text: 'www.deem.io/cards/miles-up', marginBottom: 20 },


              ],

              [
                { text: `AED ${numberWithCommas(this.cardInsurance)}`, marginBottom: 10, alignment: 'center' },
                { text: '', marginBottom: 10, alignment: 'center' },
                { text: '', marginBottom: 10, alignment: 'center' },
                { text: '', marginBottom: 20, alignment: 'center' },
                { text: '', marginBottom: 30, alignment: 'center' },



              ],
              [
                { text: reverseString(' مبلغ الفاتورة الشهرية '), marginBottom: 10, alignment: 'right' },
                { text: reverseString(' لمزيد من التفاصيل و الشروط و الأحكام, يرجى زيارة '), alignment: 'right', marginBottom: 20 },
                { text: 'www.deem.io/cards/cash-up and ', marginBottom: 10, alignment: 'right' },
                { text: 'www.deem.io/cards/miles-up', marginBottom: 20, alignment: 'right' },

              ]



            ],
            fontSize: 8,
            marginBottom: 10
          },
          //Credit Life Plus
          {
            columns: [[{
              text: 'Deem Credit Life Plus',
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA'
            }],
            [{
              text: reverseString('ديم كريديت لايف بلس'),
              fontSize: 12,
              marginBottom: 10,
             // color: '#6E3DEA',
              alignment: 'right'
            }],
            ]
          },
          {
            columns: [
              [
                { text: 'Monthly Billing Amount (AED)', marginBottom: 10 },
                { text: 'For more details and Terms & Conditions, please visit', marginBottom: 10 },
                { text: 'www.deem.io/loans/personal', marginBottom: 10 },
                

              ],

              [
                { text: '', marginBottom: 50, alignment: 'center' },

                { text: numberWithCommas(this.loanInsurance), marginBottom: 10, alignment: 'center' },


              ],
              [
                { text: reverseString('مبلغ الفاتورة الشهرية'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('لمزيد من التفاصيل و الشروط و الأحكام, يرجى زيارة'), marginBottom: 10, alignment: 'right' },
                { text: 'www.deem.io/loans/personal', marginBottom: 20, alignment: 'right' },
                

              ],
            ],
            fontSize: 8,
            marginBottom: 10
          },
          //Credit Card Summary
          {
            columns: [
              [
                {
                  text: 'Deem Credit Card',
                  fontSize: 12,
                  marginBottom: 10,
                  color: '#6E3DEA',
                }],
              [{
                text: reverseString(' بطاقة ديم الائتمانية '),
                fontSize: 12,
                marginBottom: 10,
                color: '#6E3DEA',
                alignment: 'right'
              }]
            ]
          },
          {
            columns: [[{
              text: 'Summary of Accounts',
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString(' ملخص الحسابات '),
              fontSize: 12,
              marginBottom: 20,
              //color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          },
          {
            columns: [
              [{ text: 'Card Number', marginBottom: 10 },
              { text: 'Previous Balance', marginBottom: 10 },
              { text: 'Purchases/Cash Advance', marginBottom: 10 },
              { text: 'Interest/Charges', marginBottom: 10 },
              { text: 'Payments/Credits', marginBottom: 10 },
              { text: 'Total Payment Due', marginBottom: 10 },

              ],
              [
                { text: profileDetails.creditCardDetails[0].primaryCardNumber, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].previousBalance)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].cashAdvance)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].interestCharges)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].paymentCredit)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].totalPaymentDue)}`, marginBottom: 10, alignment: 'center' },
              ],
              [{ text: reverseString(' رقم البطاقة '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' الرصيد السابق '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' المشتريات/ السلفة النقدية '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' الفوائد/ الرسوم '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' المدفوعات/ الإئتمان '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' إجمالي الدفع المستحق '), marginBottom: 10, alignment: 'right' },

              ],
            ],
            fontSize: 8,
          
          },
          {text: '', pageBreak: 'after'},
          //Transaction
          {
            columns: [[{
              text: 'Credit Card Transaction Details',
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString(' تفاصيل معاملات بطاقة الإئتمان '),
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA',
              alignment: 'right'
            }],
            ]
          },


          {
            table: {
              headerRows: 1,
              widths: ['auto', '*', '*', 'auto', '*'],
              marginTop: 100,
              dontBreakRows: true,
              body: [
                [

                  { text: [{ text: reverseString('تاريخ التحويل') + '\n', fontSize: 8, marginBottom: 10 }, { text: 'Transaction date', fontSize: 9, marginBottom: 10 }] },

                  {
                    text: [{ text: reverseString('تاريخ تخصيص و إيداع المبلغ المسدد') + '\n', fontSize: 8, marginBottom: 10 }, {
                      text: 'Posting date', fontSize: 9, marginBottom: 10

                    }],

                  }, {
                    text: [{ text: reverseString('الوصف') + '\n', fontSize: 8, marginBottom: 10 }, {
                      text: 'Description', fontSize: 9, marginBottom: 10

                    }],

                  }, {
                    text: [{ text: reverseString('المبلغ الأصلي') + '\n', marginBottom: 10, fontSize: 8, alignment: 'right' }, {
                      text: 'Amount - Original Currency', marginBottom: 10, fontSize: 9, alignment: 'right'

                    }],

                  }, {
                    text: [{ text: reverseString('المبلغ درهم إماراتي') + '\n', fontSize: 8, marginBottom: 10, alignment: 'right' }, {
                      text: 'Amount(AED)', fontSize: 9, marginBottom: 10, alignment: 'right'

                    }],

                  },

                ],
                // [{

                //   text: 'Transaction date', fontSize: 9, marginBottom: 10
                // }, {
                //   text: 'Posting date', fontSize: 9, marginBottom: 10

                // }, {
                //   text: 'Description', fontSize: 9, marginBottom: 10

                // }, {
                //   text: 'Amount - Original Currency', marginBottom: 10, fontSize: 9, alignment: 'right'

                // }, {
                //   text: 'Amount(AED)', fontSize: 9, marginBottom: 10, alignment: 'right'

                // },

                // ],
                ...this.cardTransactionService.creditCardTransaction.transactions.map(t => ([{ text: t.date, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.postingDate, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.description, fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(t.currencyType), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: numberWithCommas(t.amount), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }])),
                //...this.cardTransactions.map(t => ([{ text: t.date, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.postingDate, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.description, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.amount, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: t.amount, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }])),
              ]
            },

            //layout: 'lightHorizontalLines',
            layout: {
              hLineColor: function (i, node) {
                return '#D8D8D8';
              },
              hLineWidth: function (i, node) {

                return i === 0 ? 0 : 0.20;
              },
              //   hLineWidth: function (i, node) {
              //     if (i === 0 || i === node.table.body.length) {
              //         return 1;
              //     }
              //     return (i === node.table.headerRows) ? 1 : 0;
              // },
              vLineWidth: function (i, node) {
                return 0;
              }

            }

          },
         

          // page break after text
          { text: '', pageBreak: 'after' },


          // }
          {
            columns: [{
              text: 'Summary of Instalment Plans',
              fontSize: 12,
              marginBottom: 10,
              marginTop: 20,
              color: '#6E3DEA'
            },
            {
              text: reverseString('ملخص خطة السداد'),
              fontSize: 12,
              marginBottom: 10,
              marginTop: 20,
              color: '#6E3DEA',
              alignment: 'right'
            }
            ]

          },
          {
            table: {
              headerRows: 1,
              widths: ['*', '*', '*', '*'],
              marginTop: 100,
              dontBreakRows: true,
              body: [

                [{
                  text: [{ text: reverseString('التاريخ') + '\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Date', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }],

                }, {
                  text: [{ text: reverseString('المبلغ الإجمالي المتبقي درهم')+'\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Outstanding Amount (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }],

                }, {
                  text: [{ text: reverseString('الأقساط المتبقية')+'\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Instalments Pending', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'
                  }]
                },
                {
                  text: [{ text: reverseString('المدة الزمنية')+'\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Tenor', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },

                ],
                // [{
                //   text: 'Date', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Outstanding Amount (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Instalments Pending', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'
                // },
                // {
                //   text: 'Tenor', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },

                // ],
                //this.creditCardTransactionService.creditCardTransaction.installmentDetails
                ...this.cardTransactionService.creditCardTransaction.installmentDetails.map(t => ([{ text: t.statementDate, fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(t.outstanding), fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(t.pendingInstallments), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: t.tenor, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }])),

              ]
            },

            // layout: 'noBorders'
            layout: {
              hLineColor: function (i, node) {
                return '#D8D8D8';
              },
              hLineWidth: function (i, node) {

                return i === 0 ? 0 : 0.20;
              },

              vLineWidth: function (i, node) {
                return 0;
              }

            }
          },

          //Tax Invoice
     
          {
            columns:[
              [{text: 'Tax Invoice',
             fontSize: 12,
             marginBottom: 10,
             marginTop: 20,
             color: '#6E3DEA',
           }],
           [{
             text:reverseString(' فاتورة ضريبية '),
             fontSize: 12,
             marginBottom: 10,
             marginTop: 20,
             alignment: 'right',
             color: '#6E3DEA',
           }]
         ]
           },
           {
            columns: [
              [{
                text: 'Deem Credit Card',
                fontSize: 12,
                marginBottom: 10,
               // color: '#6E3DEA'
              }],
              [{
                text: reverseString(' بطاقة ديم الإئتمانية '),
                fontSize: 12,
                marginBottom: 10,
               // color: '#6E3DEA',
                alignment: 'right'
              }],

            ]

          },
          {
            columns: [
              [{ text: 'Invoice Date:', marginBottom: 10 },
              { text: 'Services Eligible for Value Added Tax:', marginBottom: 10 },
              { text: 'Total Fees Eligible for Value Added Tax:', marginBottom: 10 },
              { text: 'Total Value Added Tax for the Period', marginBottom: 10 },


              ],
              [
                { text: statementDate, marginBottom: 10, alignment: 'center' },
                { text: 'As described in the list of transactions', marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(parseFloat(this.vatInvoice.creditCardTaxDetail.totalEligibleTax).toFixed(2))}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(parseFloat(this.vatInvoice.creditCardTaxDetail.totalValueAddedTax).toFixed(2))}`, marginBottom: 10, alignment: 'center' },

              ],
              [{ text: reverseString(' تاريخ الفاتورة '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' الخدمات المؤهلة لقيمة الضريبة المضافة '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' إجمالي الرسوم المؤهلة لقيمة الضريبة المضافة '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' إجمالي ضريبة القيمة المضافة للفترة '), marginBottom: 10, alignment: 'right' },


              ],

            ],
            fontSize: 8,
            marginBottom: 20
            //pageBreak: "after"
          },
          //
          {
            text: '',
            style: 'sectionHeader',
          },

          // {
          //   canvas: [
          //     {
          //       type: 'path',
          //       d: 'M 0,20 L 100,160 Q 130,200 150,120 C 190,-40 200,200 300,150 L 400,90',
          //       dash: {length: 5},
          //       // lineWidth: 10,
          //       lineColor: 'blue',
          //     },
          //    ]
          // },
          //Rewards and Spend Analyzer
          {
            columns:[
              [{text: 'Credit Card - Rewards / Spend Analyzer',
            fontSize: 12,
            marginBottom: 20,
            marginTop: 20,
            color: '#6E3DEA',}],
           
            [{
              text: reverseString('مكافآت بطاقة الائتمان ') + ' \\ ' + reverseString('محلل الإنفاق'),
            fontSize: 12,
            marginBottom: 20,
            marginTop: 20,
            alignment: 'right',
            color: '#6E3DEA',
            }]
          ]
          },
          // {
          //   columns: [[{
          //     text: 'Spend Categories',
          //     fontSize: 12,
          //     marginBottom: 20,
          //     //color: '#6E3DEA'
          //   }
          //   ],
          //   [{
          //     text: reverseString(' فئات الإنفاق '),
          //     fontSize: 12,
          //     marginBottom: 20,
          //     //color: '#6E3DEA',
          //     alignment: 'right'
          //   }
          //   ],
          //   ]

          // },
          {
            columns: [[{
              text: 'Spend Categories',
              fontSize: 12,
              marginBottom: 20,
              //color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString(' فئات الإنفاق '),
              fontSize: 12,
              marginBottom: 20,
              //color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          },
          {
            columns: [
              [{
                svg: svg,


              },
           {text:[ 
            {
              text: reverseString(' الرحلات الجوية')+'\n',
              marginTop: 10,
              fontSize: 8,
              marginLeft: 20
            },
               {text: 'Airlines',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20
              },
              
            ],
          }
              ],
              [{
                svg: svgHotels,


              },
              {
               text:[ {text: 'الفنادق'+'\n',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20},
                {text: 'Hotels',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20},
              ]
              }
              ],
              [{
                svg: svgEnt,


              },
              {
               text:[ 
                 {text: 'الترفيه'+'\n',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20},
                {text: 'Entertainment',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20}
              ]
              }
              ],
              [{
                svg: svgOthers,


              },
              {
               text:[ {text: reverseString(' النفقات الأخرى')+'\n',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20
              },
              {text: 'Other Spends',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20
              },
              ]
              }
              ],
            ]
          },
          { text: '', pageBreak: 'after' },
          {
            columns: [
              [{
                text: 'Rewards Earned',
                fontSize: 12,
                marginBottom: 10,
                marginTop: 20,
               // color: '#6E3DEA'
              }
              ],
              [{
                text: reverseString(' المكافآت المكتسبة '),
                fontSize: 12,
                marginBottom: 10,
                marginTop: 20,
                //color: '#6E3DEA',
                alignment: 'right'
              }
              ],
            ]
          },
          {
            table: {
              headerRows: 1,
              widths: ['*', '*', '*'],
              marginTop: 100,
              dontBreakRows: true,
              body: [

                [{
                  text: [{ text: reverseString('فئات الإنفاق') + '\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Spend Categories', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }]

                }, {
                  text: [{ text: reverseString('مبلغ الإنفاق') + '\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true], alignment: 'center' }, {
                    text: 'Amount Spent', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'center'

                  }]

                }, {
                  text: [{ text: reverseString('المكافآت المكتسبة') + '\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Rewards Earned', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },

                ],
                // [{
                //   text: 'Spend Categories', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Amount Spent', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'center'

                // }, {
                //   text: 'Rewards Earned', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },

                // ],
                [{ text: 'Airlines, Hotels, Entertainment, Other Spends', fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(rewardDetails.amountSpent.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'center' }, { text: rewardDetails.rewardsEarned, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }],
                [{ text: 'Bonus Rewards', fontSize: 8, margin: [0, 5, 0, 5] }, { text: '---', fontSize: 8, margin: [0, 5, 0, 5], alignment: 'center' }, { text: rewardDetails.rewardsEarned, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }],
              ]

            },

            // layout: 'noBorders'
            layout: {
              hLineColor: function (i, node) {
                return '#D8D8D8';
              },
              hLineWidth: function (i, node) {

                return i === 0 ? 0 : 0.20;
              },

              vLineWidth: function (i, node) {
                return 0;
              }

            }
          },

          //Summary of Reward Points

          {
            columns: [[{
              text: 'Summary of Reward Points',
              fontSize: 12,
              marginBottom: 10,
              marginTop: 20,
             // color: '#6E3DEA'
            }],
            [{
              text: reverseString('ملخص   نقاط المكافآت'),
              fontSize: 12,
              marginBottom: 10,
              marginTop: 20,
              //color: '#6E3DEA',
              alignment: 'right'
            }]
            ]


          },
          {
            table: {
              headerRows: 1,
              widths: ['*', '*', '*', '*', '*'],
              marginTop: 100,
              dontBreakRows: true,
              body: [

                [{
                  text: [{ text: reverseString('رصيد الأشهر السابقة درهم') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Previous Months Balance (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }]

                }, {
                  text: [{ text: reverseString('المكتسب درهم') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Earned (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }]

                }, {
                  text: [{ text: reverseString('تم شراؤها درهم') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Purchased (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },
                {
                  text: [{ text: reverseString('النقاط  المستبدلة') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Redeemed (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },
                {
                  text: [{ text: reverseString('الرصيد المتاح درهم') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Available Balance (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },

                ],
                // [{
                //   text: 'Previous Months Balance (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Earned (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Purchased (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },
                // {
                //   text: 'Redeemed (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },
                // {
                //   text: 'Available Balance (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },

                // ],
                [{ text: numberWithCommas(rewardDetails.openingBalance), fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(rewardDetails.earnedAed.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(rewardDetails.purchasedAed.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: numberWithCommas(rewardDetails.redeemedAed.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: numberWithCommas(rewardDetails.availableBalance.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }],

              ],

            },

            // layout: 'noBorders'
            layout: {
              hLineColor: function (i, node) {
                return '#D8D8D8';
              },
              hLineWidth: function (i, node) {

                return i === 0 ? 0 : 0.20;
              },

              vLineWidth: function (i, node) {
                return 0;
              }

            },

          },
          {text: '', pageBreak: 'after'},
          //Personal Loan





          {
           columns:[
             [ {text: 'Deem Personal Loan',
            fontSize: 12,
            marginBottom: 10,
            marginTop: 10,
            color: '#6E3DEA',
          }
          ],
          [{
            text: reverseString('قرض ديم الشخصي'),
            fontSize: 12,
            marginBottom: 10,
            marginTop: 10,
            alignment:'right',
            color: '#6E3DEA',
          }]
          ]
          },
          {
            columns: [[{
              text: 'Summary Of Accounts ',
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString('ملخص الحسابات'),
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          }, {

            columns: [
              [
                { text: 'Loan Account Number', marginBottom: 10 },
                { text: 'Principal Outstanding (AED)', marginBottom: 10 },
                { text: 'Payment Due (AED)', marginBottom: 10 },
                { text: 'Towards Principal (AED)', marginLeft: 10, marginBottom: 10 },
                { text: 'Towards Interest (AED)', marginLeft: 10, marginBottom: 10 },
                { text: 'Late Payment Fee / Other Charges (AED)', marginLeft: 10, marginBottom: 10 },
                { text: 'Payment Due Date', marginBottom: 10 },
                { text: 'Amount Borrowed (AED)', marginBottom: 10 },
                { text: 'Tenor (Months)', marginBottom: 10 },
                { text: 'Interest Rate p.a. (%)', marginBottom: 10 },
                { text: 'Monthly Instalment (AED)', marginBottom: 10 },
                { text: 'Last Payment Amount (AED)', marginBottom: 10 },
                { text: 'Last Payment Date', marginBottom: 10 },
                { text: 'Instalments Remaining', marginBottom: 10 },
              ],
              [
                { text: profileDetails.personalLoanSummary.loanAccountNumber, marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.personalLoanSummary.principalOutstanding), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(totalpaymentDueAmount), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(towardsPrincipal), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.personalLoanSummary.towardsPrincipal), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.personalLoanSummary.lateFeeOtherCharges), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.personalLoanSummary.paymentDueDate), marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.personalLoanSummary.amountBorrowed), marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.tenor, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.interestRatePa, marginBottom: 10, alignment: 'center' },
                { text: numberWithCommas(profileDetails.personalLoanSummary.monthlyInstallment), marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.lastPaymentAmount, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.lastPaymentDate, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.installmentRemaining, marginBottom: 10, alignment: 'center' },
              ],
              [
                { text: reverseString('رقم حساب القرض'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('المبلغ  الأساسي المستحق'), marginBottom: 10, alignment: 'right' },
                { text:  reverseString('المبلغ  المستحق'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('المبلغ  الأساسي'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
                { text: reverseString('الفائدة'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
                { text: reverseString('رسوم  السداد المتأخر /  الرسوم الأخرى'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
                { text: reverseString('تاريخ  السداد'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('المبلغ  المقترض'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('المدة  أشهر'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('نسبة الفائدة سنويا'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('الأقساط الشهرية'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('مبلغ الدفعة الأخيرة'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('تاريخ الدفع الأخير'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('الأقساط المتبقية'), marginBottom: 10, alignment: 'right' },
              ],
            ],
            fontSize: 8,
          },




          {
           columns:[ 
             [{text: 'Tax Invoice',
            fontSize: 12,
            marginBottom: 10,
            marginTop: 10,
            color: '#6E3DEA',
           }],
           [{text:reverseString(' فاتورة ضريبية '),
           fontSize: 12,
           marginBottom: 10,
           marginTop: 10,
           alignment:'right',
           color: '#6E3DEA',
          }],

          ]
          },
          {
            columns: [[{
              text: 'Deem Personal Loan',
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA'
            }
            ],
            [{
              text:reverseString('قرض ديم الشخصي'),
              fontSize: 12,
              marginBottom: 10,
             // color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]
          },
          {
            columns: [
              [
                { text: 'InvoiceDate', marginBottom: 10 },
                { text: 'Services Eligible for Value Added Tax:', marginBottom: 10 },
                { text: 'Total Fees Eligible for Value Added Tax:', marginBottom: 10 },
                { text: 'Total Value Added Tax for the Period', marginBottom: 10 },

              ],
              [
                { text: statementDate, marginBottom: 10,alignment: 'center' },
                
                // { text: 'All Deem fees and charges billed during the statement period*', marginBottom: 10 },
                { text: 'As described in the list of transactions',marginBottom: 10,alignment: 'center' },
                { text: numberWithCommas(parseFloat(this.vatInvoice.loanTaxDetail.totalEligibleTax).toFixed(2)), marginBottom: 10,alignment: 'center' },
                { text: numberWithCommas(parseFloat(this.vatInvoice.loanTaxDetail.totalValueAddedTax).toFixed(2)), marginBottom: 10,alignment: 'center' }

              ],
              [{ text: reverseString('تاريخ الفاتورة'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('الخدمات المؤهلة لقيمة الضريبة المضافة'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('إجمالي الرسوم المؤهلة لقيمة الضريبة المضافة'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('إجمالي ضريبة القيمة المضافة للفترة'), marginBottom: 10, alignment: 'right' },


              ],
            ],
            fontSize: 8,
            marginBottom: 20,

          },
          {text: '', pageBreak: 'after'},
          //Terms & Conditions


          {
           columns:[ 
             [{text: 'Terms & Conditions',
            fontSize: 12,
            marginBottom: 30,
            marginTop: 10,
            color: '#6E3DEA',
          }],
          [{text: 'الشروط و الأحكام',
          fontSize: 12,
          marginBottom: 30,
          marginTop: 10,
          alignment:'right',
          color: '#6E3DEA',
        }],
          ]
          },
          {
            columnGap: 3,
            alignment: 'justify',
            width: '50%',
            fontSize: 8,
            columns: [

              [
                {

                  text: 'Important notes:',
                  bold: true,
                  marginBottom: 10
                },
                {
                  text: `1. Minimum payment due: You may pay your card balance in full or in partial payments. If you make a partial payment, you must pay at least the minimum amount due each month by the payment due date. Interest will be applicable if payments are not made in full.

                  2. Total cash limit: This limit reflects the amount that can be withdrawn by you in cash.
                  
                  3. Total credit limit: This is the limit available for you to make purchases.
                  
                  4. Pursuant to united states government sanctions against certain countries, MasterCard® has advised that it will refuse to authorize card transactions originating in sanctioned countries. While the sanctions are in effect, you will not be able to use your Deem MasterCard® credit card in sanctioned countries.
                  
                  5. In line with central bank regulations, Deem is required to ensure that we keep updated and valid records of all our customers, it is therefore your responsibility to provide Deem with your renewed ID documents including, but not limited to passport, residence visa, Emirates ID and ensure that you keep your personal data including contact information updated at all times. You are required to immediately update us of any changes in your license where applicable, contact details, including, but not limited to, e-mail address(es), telephone number(s), employment status, etc. Please send your updated information by the registered email to customercare@deem.io or call 600 525550.
                  
                  6. Terms and conditions apply. Visit www.deem.io to view T&Cs and applicable fees and charges.
                  
                  7. Deem will never ask for any personal Information such as your complete card number/account number, passwords, or CVV numbers. This is in line with general banking practice and as per the relevant UAE authority directives. Additionally, please be informed that as per Deem process and internal policies, Deem shall not ask for any monetary compensation for granting credit facilities in advance. If you are approached by anyone, please contact us immediately on Deem Customer Care Centre number 600 525550 or write to us on customercare@deem.io.
                  
                  8. Warning: If you make only the minimum repayment / payment each period, you will pay more in interest / profit / fees and it will take you longer to pay off your outstanding balance.
                  
                  9. For feedback, complaints or queries you may contact our 24-hour Deem Customer Care Centre on 600 525550 or email us at customercare@deem.io.
                  
                  10. To view the Deem Customer Charter, please visit www.deem.io/charter`,
                  marginBottom: 10
                },
              ],
              [
                {
                  text: reverseString('ملاحظات هامة:'),
                  bold: true,
                  marginBottom: 10,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .1 الحد الأدنى المستحق للدفع: يمكنك دفع رصيد بطاقتك بالكامل أوعلى `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` دفعات. و إذا قمت بالسداد جزئيا يجب أن تدفع الحد الأدنى للمبلغ `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` المستحق شهريا بحلول تاريخ استحقاق الدفع سيتم احتساب الفائدة إذا لم يتم `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` السداد بالكامل. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .2 إجمالي النقد المتاح: ينطبق هذا الحد على المبلغ الذي يمكن سحبه نقداً. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .3 إجمالي الرصيد المتاح: هذا هو الحد المتاح لك لسداد مشترياتك. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(`.4 وفقا للعقوبات التي تفرضها حكومة الولايات المتحدة ضد دول معينة, `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` أفادت ماستر كارد بأنها سترفض السماح بإجراءات معاملات البطاقة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الناشئة في البلدان الخاضعة للعقوبات أثناء سريان العقوبات و لن يتمكن `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` العميل من استخدام البطاقة الإئتمانية الخاصة به في البلدان الخاضعة `),

                  alignment: 'right',

                },
                {
                  text: reverseString(` للعقوبات. `),

                  alignment: 'right',
                  marginBottom: 20
                },
                {
                  text: reverseString(` .5 بالتوافق مع قواعد المصرف المركزي يجب على ديم التأكد من الاحتفاظ `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` بسجلات محدثة و صالحة لجميع العملاء. و لذلك يرجى منك أن تزود `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` ديم بوثائق هويتك المحدثة بما في ذلك جواز السفر, تأشيرة الإقامة,  `),

                  alignment: 'right'
                },
                {
                  text: reverseString(``),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الهوية الإماراتية. و التأكد من بقاء بياناتك الشخصية بما في ذلك بيانات `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الاتصال و التي تشمل عناوين البريد الالكتروني و رقم الهاتف و الحالة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الوظيفية. يرجى إرسال معلوماتك المحدثة على عنوان البريد المسجل `),

                  alignment: 'right',

                },
                {
                  text: reverseString(` customercare@deem.io أو الاتصال على 525550 600  `),

                  alignment: 'right',
                  marginBottom: 30,

                },

                {
                  text: reverseString(`.6 تطبق الشروط و الأحكام قم بزيارة www.deem.io  لمطالعة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الشروط و الأحكام و الرسوم و النفقات `),
                  marginBottom: 10,
                  alignment: 'right'
                },
                {
                  text:reverseString(`.7 لن نطلب منك أبدًا أن تزودنا بمعلوماتك الشخصية كرقم البطاقة أو`),
                  alignment:'right'
                },
                {
                  text:reverseString(`رقم الحساب أو كلمة المرور و الرقم التعريفي للبطاقة .و ذلك`),
                  alignment:'right'
                },
               
                {
                  text:reverseString(`تماشيا مع الممارسات المصرفية العامة و وفقًا لتوجيهات السلطات`),
                  alignment:'right'
                },
                {
                  text:reverseString(`الإماراتية ذات الصلة. بالإضافة إلى ذلك، يرجى العلم بأن ديم لن`),
                  alignment:'right'
                },
                {
                  text:reverseString(`تطلب أي تعويض نقدي لمنح الستهيلات الائتمانية مقدمًا وفقًا`),
                  alignment:'right'
                },
                {
                  text:reverseString(`لسياسة الشركة. في حال تلقيت اتصال من شخص يطلب منك`),
                  alignment:'right'
                },
                {
                  text:reverseString(`معلوماتك الشخصية يرجى التواصل على الفور مع خدمة العملاء`),
                  alignment:'right'
                },
                {
                  text:reverseString(`525550 600 أو إرسال بريد الكتروني على`),
                  alignment:'right'
                },
                {
                  text:'customercare@deem.io',
                  alignment:'right'
                },
                {
                  text:reverseString(` .8  تحذير: إذا قمت بسداد الحد الأدنى فقط من الرصيد المستحق،`),
                  alignment:'right'
                },
                {
                  text:reverseString(`فستدفع المزيد من الفوائد / الأرباح  / الرسوم، كما سيستغرق`),
                  alignment:'right'
                },
                {
                  text:reverseString(`الأمر وقتًا أطول لسداد رصيدك المستحق`),
                  alignment:'right',
                  marginBottom: 10,
                },
                {
                  text:reverseString(` .9  للملاحظات أو للشكاوى أو الاستفسارات، يمكنك الاتصال بخدمة`),
                  alignment:'right'
                },
                {
                  text:reverseString(`عملاء ديم المتواجد على مدار الساعة على 525550 600 أو مراسلتنا`),
                  alignment:'right'
                },
                {
                  text:reverseString(` customercare@deem.io. عبر البريد الإلكتروني على`),
                  alignment:'right',
                  marginBottom: 10,
                },
                // {
                //   text:reverseString(`10.  لعرض ميثاق خدمة العملاء لديم ، يرجى زيارة `),
                //   alignment:'right',
                //marginBottom: 10,
                // },
                {
                  text:reverseString(` .10  لعرض ميثاق خدمة العملاء لديم ، يرجى زيارة`),
                  alignment:'right'
                },
                {
                  text:`www.deem.io/charter`,
                  alignment:'right'
                },
              ],
            ]
          },
          // {
          //   columns: [
          //     [{ qr: `${'Premananda Routray'}`, fit: '50' }],
          //     // [{ text: 'Signature', alignment: 'right', italics: true }],
          //   ]
          // },


        ],
        styles: {
          langA: {
            font: 'Moderat',
          },
          langB: {
            font: 'Moderat',
          },
          sectionHeader: {
            bold: true,
            //decoration: 'underline',
            fontSize: 12,
            margin: [0, 10, 0, 10]
          },
          spacer: {
            margin: [0, 0, 0, 40]
          },
          tableSectionHeader: {
            bold: true,
            //decoration: 'underline',
            fontSize: 10,
            margin: [0, 10, 0, 10]
          },
          tableSectionHeader2: {
            bold: true,
            //decoration: 'underline',
            fontSize: 10,
            margin: [0, 10, 0, 10]
          },
          header: {
            fontSize: 18,
            bold: true,
            margin: [0, 0, 0, 10]
          },
          subheader: {
            fontSize: 16,
            bold: true,
            margin: [0, 10, 0, 5]
          },
          tableExample: {
            margin: [0, 0, 0, 10]
          },
          tableExample2: {
            margin: [0, 0, 0, 0]
          },
          tableHeader: {
            bold: true,
            fontSize: 13,
            color: 'black'
          },

        },
        defaultStyle: {
          font: 'dejavu'
        }
      };
    }
    //Only Credit Card 
    if (profileDetails.creditCardDetails.length > 0 && profileDetails.personalLoanSummary.loanAccountNumber === undefined) {
      docDefinition = {
        pageMargins: [25, 240, 25, 70],
  //       pageMargins : function(cp) {
  //         if ( cp === 1 ){
  //                return {left:25,top:240, right:25,bottom:70};
  //         } else {
  //                return {left:25,top:200, right:25,bottom:70};
  //         }
  //  },
        footer: function (currentPage, pageCount) {
          return [

            {
              text: [{
                text: 'Page ' + '' + currentPage.toString() + '' + ' of ' + pageCount,
                alignment: 'center',
                fontSize: 8,
                marginTop: 30

              }],

            },
            {
              canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, color: '#6E3DEA' },


              ], marginBottom: 10
            },
            {
              text: [{
                text: 'Deem Finance LLC (Deem) is regulated by Central Bank of the UAE' + ' ' + reverseString('تخضع شركة ديم للتمويل ذ.م.م ديم لرقابة مصرف الإمارات العربية المتحدة المركزي'),
                fontSize: 8,
                alignment: 'center',

              },
              ],

            },
            {
              text: [{
                text: 'P.O.Box 44005 | Abu Dhabi, UAE  www.deem.io | 600 525550 | customercare@deem.io TRN: 100038451900003',
                fontSize: 8,
                alignment: 'center',

              },
              ],

            },

          ]

        },

        header: function (currentPage, pageCount, pageSize) {
          // you can apply any logic and return any valid pdfmake element
          if (currentPage === 1) {
            return [
              {

                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApsAAAEPCAYAAAANjQ+sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACXASURBVHhe7d2PleS2scXhF4JDUAgOwSEoBIegEByCQnAICsEhKASFoAz87l0111wMuptkF8gq8HfP+c57hmZnSDT/FEGQ/X///e9/AQAAgCG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsI7KH8JP+UX633MwAA4J66jcAryj/kX/Kb/CluXPyn928AAMA9dRuBhbIetfxd1oVlD8UmAAD4rtuI+1I8avmLeNTyD1kXkltQbAIAgO+6jbgHxaOWP4tHLf8j66LxKIpNAADwXbcRc1L+Lp+MWm5BsQkAAL7rNqI+5W/iUUs/yBM1arkFxSYAAPiu24h6lGXU8t8yatRyC4pNAADwXbcR+SkeuTx71HILik0AAPBdtxH5KX5qfF3kZUGxCQAAvus2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKzUKUv4s/s63+3vs9uC/lb6vtY6u/9X4XsFDWx6Z/yr8aP8vy3zkuHbDqv8VPvZ+bWbdxRv5wHx/yL+Id6N/yn41+k2XH8++4fId7LIf/n2xuWWwq/jyWA7W3F283f0jbP0f8Kevt0H/Hf49CYiKKT/o+sa+PT79Luz0ctRzPfpXlWHa7k95d+bMWHzv8+Xs78HGl3Ua2Wo5J3k59Tv1H72/egbLeb48e+72fL/vmt/6UqY7v3cbqFH/43qmWA/b6Q43kDcR/w3/r1IO2QrF5EcV9vxxYogrKo/z3lyL0tgf8apT1MSqyoDxiKRy8DfmkyUXMBPw5Pj5Pb2NnHaeW7WjaEVBlOf6PrC0W6xqj9H7ZbazGH8Ljw/CH8snV2qe8Q/vK5OfeckZSKDZPorgwOOvgEsHL6atjbnkloSyjSr4wuPIYtZVPcqccyxDLn5n4XNh+pmfzdu7lKH0cUpb6IsO+uxzbyxWe3cYK3NmybADrDyOLoTuaQrE5kHL2iMAoywUQhefJFBeYPjFcPXL5KR/LfJwtP7oyK38u4m0t6/HKy1WqSFJ8jr16AOsV75NlLga7jZkpHmXKvAH0+GTzz976HKVQbAZTvG25MKu0be2xFJ7M0xtEWU761QvMZ5aL6JJTNhTv4z52ptFbzq0Ub2++61LlmOXlTHsMUpZBrEqDDF7W0PpihG5jRop3zCq3MZ/xjuYDw8dXd4r7o/39GZQrNhUfXGYtDp7xvsQt0iCK90cXYW0/z6ziaFW6c0i7jFspPm5VvjAOORdG8HI8lqdyf6YuOruNmSg+iM9WCHiD/mijUCg2P6DMcHCJUOKqOCv33aMP2369E+9DJUbMlfLFpuLR2eoDLwtvO7/01vMs/vuP5WiXrSofj9Ldeeg2ZuDOkll2qGcObxT+d4/fkU3qYlNxkVltGsYZKDp3cF89+qztx7vzvpW26FRKF5uKL5C//I4JeEDp1Hnlis+hM+/DntOZ5q5Dt/FK7hy52+2o3RuFQrG5g8JI5jYUnS+4bx591PYb/sf7WJpbpGtKyWJT8fFr9sEXGz7Kqbgvsz5YHM37YorpUt3Gq7hTHp2z7qy72LVRKBSbGymz3SY5g09sPMH+oHh/u9u83k95n0t14aKUKzYV3za/0/Fr2Iic4v34jueCf/f640zdxrMpd7rSeOfXXh+1FIrNNxQKhM95Ll66EaqzeN3lbndaoqW5cHksS7t8l2qXcU2p/hDQUeG31RUfy9q/cyfu08uO5d3GMyl3vdJ45e1GoVBsPqG4QLj7gSWSbxvf7tuJlDvfaRnhX71+PpNSpthUXGh++fkb8b73ccGp3GUKwhYhfXpEt/Esim9vrjsC//Nyo1AoNjsU33JiTt0Ym0bdq1O40zKOL6Qve4BIKVFsKncvNBcfFUf+t8LdrR9dUnB2G8+gcGvqPW8U3XmcCsVmQ5n1Sc1MLi0WRlO40zLe0+PaaEr6YlOh0PzRoeLI/+bxb9vfh7/65dS7Vd3GkRSPGnClsc+XSfYKxeaDwm2Sc11WLIykcLFyrtNHypXUxabiAunLz2Bfwemfffyb9vfgf3b16ae6jaMoFJrH/VBwKhSbonDb/DqXvow5isJt8+u4+DvtoYXH32uX4VKrZfN36VMgPee+ebutKBSa251WcHYbR1AoND/3veBUbl9sKjzAcb3LX6nxCYXj0vXc/6cUnErmYpPt8L3fl/7qUbw/c07Yx/01fGpUtzGawgE9zrd5Fv6/q7ZMTik2FeY15eH515e9UuMohRGQPE4ZYVFSFpsKb8/Yrjv9QqHOOG74BV+3MZJX4LEi6xXDcd8OynLbYlOh0Mxn+MEqkkKhmc/wglNJV2xK1mN5Zl8eblF46PgzQ+9SdRujKBSaY3iOom8h9/7b1YYWmwqFZl4lCk6FQjOvoQWnkrHYZM75fu6z78cahdcoxhj24Ge3MYrCpPtxsp4shxWbCoVmfqfN2T1CodDMb1jBqWQsNnHMt9vpCg9WxXE/Dpm/2W2MoDAH5Z6GFBsKhWYdKR8aUig06/hh5CqKQrE5FxeafKaxxpzDe42fUigM7it8Q1XYnupJ9W1DCqMf9YRPy1AoTObCPj1G+O30buMnFEYP7i202FS8PbV/AzV8+TKCKyjMHa/rt95nepRCsQm8F35nodv4CYWD+r2FFZsKo1H1nfLC4FcU5o7X9q/e53qEQrEJbBO231m38Sgv3GpBcU8hxabCaNQchsy920rhmDSHkO9xVig2gW080BN27O42HqFwuxMWVWzyzrR5hN4K3UrJ+now7Bdy4lMoNoHt4u4q9BqPUBiFgn1cbCoUCfM59XvUFY+MMwVjLhHHFopNYLuw0c1u414Kt6qw+OiEoMw2T9Pr4hOcXwXm/cT8dL2/NcSWNvPcwlkv2twPw79/d6HcoahYti3fBbjLtvXRRYtCsQnsEzJQ0G3cQ+EhDqx9WmxWPxl4jqJP/h6dPVxcKS4W/K0YMz3cEjLF4h1l1m8T8XHW25YLyk+2LU95qrptfXTRolBsAvv80duX9uo27qEwtw5rhwsKpfL7NL0fhDzE0FJ8S9h9M8OJcujrkJQZL36/Xbz01vdTSsVt6/AcYIViE9jv4+NPt3ErhYeC0DpUbCoV59h5eX2L8rSnrRXvc5Uv8Nxnw/pLmWUk+Ipty4V6lW3r0IWdQrEJ7Pfxt8J1G7dS2HHROlpsVvt6Uy/vaYVAS3FhUHX/G/LtQoqnHrR/qyIXfFdvW9mL9kO39hTOWcAxHx2Tuo1bKLMc2BFrd7GpVBoh90MWl7+ofKF4P/Q80XY5swt/WEip2A9r3raGTMU4wssimft094MLyuzFpkfEvY7Lg2H+DNc8V3d5YKz6/nIm99WzfvU0FLf5InHmt/J8NAWq27iFMsNO6w3IO503FD/Q4Q3n6Unw8d/NO6xHtrhK/upIsVmlH1N93/dC8RSEarfWP74ts6ZUnu9rl45mPuNleixbu7wZ7J6Socx4zHaB43PS7otgxaPY/rczF0lHLf2668JYWeZBVzsmv/PR+5K7je8o3kDXC1GJi8vdG9ArigtVF59cKe4sNhUX7+3vyCjF93y/4mVcLW8FIfug4oN75X2vyrbl4q5d9qvteum0MlOx6XUJGwlXKs3ZHcl9EHls8mBWxn1nrz9767hVt/EdpdoG6RORC8zhIweKi6c777B7i83sB38fJNLcNn/Hy/pY5nY9MgoZ3VS8b7e/uwJ/TkOeMh9BybhteXk2H9eVGYrNodMtFH/Odxzp9LYRNgi1psxSdB7e7rqNryjutPUfz8w7zCWjBspdrxI3F5tK9lFNHxjKFJoLL/Nj2dv1yejjg7tScVSTbSvO5mO8Ur3YDPv6wHf8t1Z/d2benk+56FNcF1Qu5A9vf93GV5QKowg++aS4NaXcrejcU2xmfuK1ZDGw8LI/1qFdr2w+mgerVJ2rmeZBoL2UbNvW5ifTlarF5mkF0Zr/5uNvt8szCxd+px/nlWpvX1nsfiZj0W18Rck+inDpK2meUTyKV3le2VabNkYl+7zf9PPo3lFcFLTrlY1PZIf3V6XiPjXLtpWpCNlUiCkVi03382UXvv7bj2Vol6s6F5qX1QpKyQvldj226jY+o2Q+eXlnSD1aoHgKwuyjnFuLzcxXdqfdqhpNqXBAO1R8KVUeLlsLfQr/SkqmbWvTk7JKxWLz8jssSsV97ZVLC82FUrHgPLQ9dhufUbIWSik2nK2Uqrf+tthabGa9Uj58myArJfsFzu+95X5HqXbhdmg9M1MyXTS+nf+rVCs204yCK1UfxGv5bkiaekGpNjf20HSObuMzSsYCoeRIgTLrrYm3xZqStdj25zHkacQrKRVeDbSr35VKDyouys4BfkXJ8sDD25e8K5WKzXTnNiX7N0ttkW4/VCr166E7f93GHsUThdd/MIPSt6SUGQvOLcVm1h1rmtvnLSX7bbBdDwop1e4OzLxtZZle9XbkWKlSbKYafVt4maTyOWv3t06dQakwILA49HL3bmOPkm2O3RRzn5TZCs6XxaaSdUTq0HctV6Jkvu28q/+VLKNpW6QsHCIpWc4PL0etlCrFZtqHyJSqt9NTT2NRqsyLPTTVrNvYo2Squj/62qRslMwPXu31rtjMOiJV9lU0WynZRyU23d5Ssr/JoFX+6fN3lCzb1suRK6VCsXnoZH4mpcoo3Fr6Y7xS4Xb6oW8S6ja2lEwH91IPA22lzPLQ0LtiM+POlP7gHkXJPBl90y0u/9zq32Q3/Yj5Qsmwbb0cvVIqFJvpv1VKqXa+KnGMV0pcSLfLvUW3saVkObj7ynnKSfamzPBapKc7tZL1Fnr6g3sUJfPo5qbbXEqlyfTTj2oulCzb1tPBCCV7sVni4kTJfpekVebOlZK+DmiXeYtuY0vJcnCf+sCteAeueHti7VWxmfEhs9uMPC2UzAezt3ctmp/P7I7bVoa5m0/PE0r2YjPlAyw9SpXBkVKvHFMqzN3cXbx3G1tKhiuYW9zqVKq/PPdVsZnxRe5lDu5HKd6mfHfCJ4fsFzMvR5mVSvvHtE+gL5T1tpXloa2nD48q2YvNMq9eUzIOHvSUO8Yr2Y/T8cWmkmEOgYvd6d5/+IxS5Yqx51WxmfEJ4qnm/yp+2MzzqTKd/Pd4+QokpdILkKc6ZinLtuWLxszb1tMRZSVzsVlqBM6UCrfSy+2HSsaBmbUhxWaGicDTjxCsKdXmw6x1i03F69T+7NVKv9VA8YWgRxd8YKrw4MMWL+9g+L+vfjazcoXDmrJsWy7uK25b3YtIJfO67HrXbAZK9vnTJfdDJfsbaoYUm1dX2C66pnv6/B2l2ldYLZ4Vmxlvf5aZA6y4WHcfLif/qhcjb7Xrvtb+bGKV5t6tty0XDzNsW93pGErmYrPcg4pK9vNUuQJ+oWTeD4cUm1fvnLca1VwoVUc3nxWbGQ9KaS9ilPXJv/pDY3t13zjh9tXPZJf21p2yzLOcedvqnjeUzMVmxdu9GQcR1sq+aUTJvK0OKTavLHj8t283qrlQss/b6HlWbGa73ZLm9oriIirbQxZX6o44u331M5ll27Yqz+E9qjtFRsl6Aj/0ouyrKdnfC1l23rSSedQ4tthUrp5nN8VXUh6llHjBa+NZsZltBOWSEXPFn+nykEXmK9crPRuVyn7LbnHJrTtlxjm8R3UfElKy9svLucqZNeuRSruslSiZn/YPLzavHiKf9gXuWynVThrPis325662e2fZS/HF2vohixnmwp3h2TZUZV8YfutOuc0c3qPaPjOFYjOYknXEvPpDepmnKIQXm1fetrrdC5F7lCq3DhdfDppKxrl2odMz/PtkOfnfcZ5lpO5Jwu2rn8ks/Nadwra135fBCiVrsVn22QSFAn4A5eo7y6+EF5tX3rYq+xRZJCXzBtfTKzazXaF9fCGjeJ2YZzlI29/W/kxSH8+9U5jDG+PLCVGh2AymUGwO0qxPJuHFpg926z9wpuG3OatQsu7MPb1i88qLlp5d79dUlocsmAt3nh9GnpUq85d3neAU5vCO86WAUyg2gylZ+7T0e5StWZ9MwovNyzaidlnuTMlWrL1Sodh8emBXlocsvMze/pkLd40fDmb+36v/lhnbVh4UmydQ6NNBmvXJZJpis/zwdySlyonWesXmlSPkPd8e4FDaeZac/POoWmx+e22Twhze6315m4lCYRRMoU8HadYnk/Bic/3Lz1R+I4mkVJq32Ss2sx2MvDyc/HP74Rt4/L9X/y0zb1vMs8yhwrFoQbEZj2JznGmKzbJv/R9FqVIcVTrAI68fThT+36v/BmxBsXkChT4dpFmfTKYpNnevyOyUKgUbxSYiUGziUxSbJ1Do00Ga9clkjmKzXQ64W8qcbHsHeOZCYi+KTXzqy2uoFAqjYAp9OkizPpnEFZvKZa8aaZcF7pbSxWb7M8A7FJv42HobemxHFEbBFPp0kGZ9MgktNq96+rP0V0yNolT5JiGKTXzKD9h8e6p7tQ1RbGIPz3H/8p5FhcIomEKfDtKsTyZTFJtfihW4W8q8+oViE3t4ioVfDeRi8ukB7PHf238LLFzweBvxe0yffl3o4+faf5sBxWY8is1xKDZnpVBsYgYetfS35XikfvN3iCsUm1h41NLv7vXrsL58//krCoVRMIU+HaRZn0woNmelUGyimm+3MsVFwe6D05pCsXlf61HLH77GdK/H72p/fwYUm/EoNseh2JyVQrGJ7HzS8ajly1uZRygUm/fgke9Do5ZbKBRGwRT6dJBmfTKh2JyVUrnYzHowwnFho5ZbKBSb8/F8XR8b/Nn6+PbRqOUWj7/XLkcGFJvxKDbHCS02r/qKRIrNDoViE1daioLwUcstHn+7XSbUsoxaer5u+KjlFgqFUTCFPh2kWZ9M4opNa375Wf7oLcvdKWW+G7qz7BSbtRx+AGMUhWKzltNHLbd4LFO7rBlQbMaj2BxnimJTf7q/PHemVDnZUmzWsxQFHz+AMcpj2drlRh6H3jJwNoXCKJhCnw7SrE8mcxSbkvKEdyXFI01tP2XUKzZ9Emp/Dte4/FbmEUqVaSR3sOndqBkpFEbBFPp0kGZ9MgkvNq/6TutSB7AzKFl36Fav2OQW6DVS3so84rH87frhHN6G0o9abvFYl3b9MqDYjEexOU54sXnVRvRLb3nuTLmq8N+rV2z6JNX+HOKVHLXcSmnXF/FOfcvA2RQKo2AKfTpIsz6ZTFNs/ru3PHel/LTqm+x6xSajUvF+uJUp0089Udo+wOd8jB/ybtSMHuvb9kEGFJvxKDbHCS82r5pr93tvee5KqfRwRK/YrFQsZ1XiAYyRlKwntSqmHrXcQqEwCqbQp4M065NJeLF55Vw7HhJ6UCo9YPOl2HysQ/tzeK7sAxgjPfqk7Ss85yLA21Datwyc7dEnbT9lQLEZj2JznPBi88oRtX/2lumOFI9ItP2T1bNi0yNz7c/iLz5Y3+ZW5lHKlRe/2aV7N2pGCoVRMIU+HaRZn0zCi80r59oxb1OUv6/6pIJnxWaVVzeNdvtbmUcplaaTjOYTPKOWOz36re3LDCg241FsjhNbbFrzB870Z2957kapdAvdnhWbdx2V8oGYUcsA7j9p+/cOpn7LwJkUCqNgCn06SLM+mQwpNq+8hXv7W+lKpVvo9qzYvMMT6etbmYxaDqBUeQXYUdO8GzWjR9+2fZ4BxWY8is1xhhSbV25I3cLlLpSKBdqzYvNvq5+ZxVIUcCvzJI8+bz+Hyhi1PJFCYRRMoU8HadYnkyHF5tW3P297AFYqPn379AJBqfyQEA9gJKBUno7BWwYuplAYBVPo00Ga9clkSLF59aT8Wz4opFSdn/aq2Kwy/3R9K5NRy0SUSqP9t383ajYKhVEwhT4dpFmfTIYUmxmKntsdqJWq7xR8VWxefeHyjgsDRi2TW31eGXkEnFHLpBQKo2AKfTpIsz6ZxBebplz9kMqt5m4qlR+meVVsZp+3+VtvuZGLP6fVZ5aNR8UZCU9KoTAKptCngzTrk8mwYjPDOxJvM1qgVJ7b+PLCwP999bMZcbszOcXzZtvPLZPyJ7lZKRRGwRT6dJBmfTIZVmx6ztH6D13Bo6vTjxgo2U+k77wrNrOvH18mkJySfT4zo5tJKRRGwRT6dJBmfTIZVmxmObhPXQgo/rYgn6ja9a7kXbFZ4cGnW4yiK75jUfLpaCX76P8tRjcVXzy62CjxMN1jWdefUxYUm/EoNscZU2yakuXgPuWL3hXPZ6x8+3zxdn6tkn09365DdcqzecH+bNI/Qa1kHyH3RePUUzIUH7N6F8dpXxOmUBgFU+jTQZr1yWRosZnltTU+uE33xLCSYV5shC3FZoWpAr/0ln0WytaH/ry/pRv9VCqMkE990aLsOSekGP18LEe7bBlQbMaj2BxnaLHpW7zrP3alqUYNlCyFfIQtxWaFbxOadmRK8Um/Xd89Uox+KllPcmuz3on59HxwyeinQmEUTKFPB2nWJ5NxxaYpV78Cac0nvPKT8JUMD19F2jSao1QYyZ1uZEoZcdF4yeinUmHfme6iRRk15cdFiy9iPPo5pM8ef6P9uxlQbMaj2BxneLGZbQTOB7yyB3Klwu3kvbYWm1XeJfprb/krUp7NsRvhlNFP5az1+cQUF8YL5awLRQ9u+CLGx8mQixiFwiiYQp8O0qxPJsOLzYzzpHyyKTeHU5lljmZr82igkmmk/JXyt0KVqx9AGzL6+fh97d/KaIovDFCuHnD4aPTz8e/b35kBxWY8is1xxhabplx5wnrGJ7ISBYHik37WnTPCnmKz0hSCsgWncnWh+YyX6aMHsZQKDwotSr+6Tcm4vy6jn5tGjhUKo2AKfTpIsz6ZnFJsZi4QfMWb9naV4lvHFW77fWLXPEelyuimlSs4layF5uLjaQpKpbsEJQtOJfMI8h+9Ze5RKIyCKfTpIM36ZDK+2DQlc8Hk4iXsNl0ExSf8bPNdR9lbbFabt1qm4FQ86pe50LSPp8AolUY3rVTB6eVdLXtGmy9YFAqjYAp9OkizPpmcVmxWmCflHeDyh4cUF1Ozj2au7S02XYhXGt209A8NKZ7Tln272zwi9Y5SbQ60LwJSPzSkVLhYsc0XLAqFUTCFPh2kWZ9MTis2S82TklNHOhUXUC4yqxVREXYVm6ZUmru5SHEx01IqjaKHvThfqTa6ab4YSHUXZqFUuUj+vbf8zygURsEU+nSQZn0yOafYNKXiSIIPoCNfw+I5me6XO41ktnYXm6ZULMz9Oaf5piHFRXuVfnTfhY7sKVWeTG+lmWuu+D2sWYuHnl3TWhQKo2AKfTpIsz6ZnFpsVhxJWLjw/PgVLIr7wCf4uxeYa0eLzSrv3ezx9nTZCJX/tlQqECx83qLiUd2q++GlFy6Kj2XVBhB2T8NQKIyCKfTpIM36ZHJesWlKtYPTMy4WvMO4ADWPgPoEvvD8t+W/eZ39sxSXfYeKTVP8CpP291Xi7eLn3rpFU1xY+UKnwpy6niF3GJSKUzLWPDLt488pI52Kj29Vj+O7H9ZTKIyCKfTpIM36ZHJ6sVl5dBNjfFJsenuaoYh3weBbo+FfNqD4wqf6SPrQB6yUrCe/Pfz5+nMOv3hRfKvc22flOeWHHi5TKIyCKfTpIM36ZHJusWmKR/vWC4F7O1xsmuJRnfZ3VuYTuosGj7jt2kEVF9/LqPoMBZS5iBo6aqfMctGy8Lp8//Yl2dx//tnHv/F+5d8xS78cKsIVCqNgCn06SLM+mVxSbFaeJ4V4HxWb5t+x+n0z8v7idXym6q3xLU6Zl+i/s/qbs+ptO2u9fzODT+6eZO0Xis14FJvjnF9smuLRl/WC4L4iis3ZRqbwl4+3jT3891Z/G3PwceHwfF+FwiiYQp8O0qxPJtcUm6ZwYIeFFBQKFzBzcZEQPof1FYW7LvP59Lv0KYyCKfTpIM36ZHJpscloFCxs9Eqp+pQsvrrktT6K5yu2y4KaIu6aUBgFU+jTQZr1yeS6YtOUO8yTwmuht0qVmecv3sVvvc/2LAoPMdbngYyPHyxTKIyCKfTpIM36ZHJtsWlK9Xcl4jPRxSa3QmvzxcLl346jMEpe2+6TW49CYRRMoU8HadYnkxTFpouDyu9vw2dCi01T/F5ACs56/JmdOk/zGcXHJUbJa9r98vZnFAqjYAp9OkizPplcX2ya4uJgvWC4j/Bi0xTm3tWTotBcKBSc9YR+AYBCYRRMoU8HadYnkxzFpinVvzYOxwwpNk1hm6ojbDQqksK0jDpGfH8+hVEwhT4dpFmfTPIUm6b4K9HWC4g4PmFmnK4wrNg0hYIzv5SF5kJhWkZ+4YWmKRRGwRT6dJBmfTLJVWyawsT8Mfweyow7+dBi0xQKzrxSF5oLxQUnc8tzGlJomkJhFEyhTwdp1ieTfMWmKVk3xqq+ndCVWxabprjgZHQqlxKF5kJhDmc+wwpNUyiMgin06SDN+mSSttjkoB7n+8FYuW2xaQq3Q3PwZ5DqYaCtFI5NeQwvDhQKo2AKfTpIsz6Z5Cw2TeGg/rkfrvqVWxebprjgZLu6jvu+ZKG5pjDd5zq+WDllVFyhMAqm0KeDNOuTSd5i0xQXnBzUj/lye0m5fbFpircrvkzgfO7zy1/YHkXhG9DO53mzp12sKBRGwRT6dJBmfTLJXWwuFArOfbrzmBSKzRWFryU8zyXfdT6a4ve5MjXjHKdfrCgURsEU+nSQZn0yqVFsmsIowjZPby8pFJsNxcUCTxmPM8Vt81cUj5RnPYHOwMX8JRcrCoVRMIU+HaRZn0zqFJvmBRZGEfrcLz/3+m2hUGx2KNxWH8PvzZ3mtvk7ii+IOT7F8jHrp15/n+Hx99tlyoBiMx7F5ji1ik1TfhIe8PjRptEjhWLzBcXvImWU83PTj2Y+o/j4lPVkWomL9stfjaVQGAVT6NNBmvXJpF6xuVCYb/cXz2fdNHqkUGy+oXiUk23rmBQFQgYKFy7HpRkRVyiMgin06SDN+mRSt9g05c6vsXl727ylUGxupHiEilvr23hbdIF+m1vmW7g/Hv3CrfVtfHy67JZ5z2OZ2uXMgGIzHsXmOLWLzYVyt7lSm0cz1xSKzZ0UzxPOenC8mve5Q9vinSi+cOGNGs95/9p9MjrDY9na5c2AYjMexeY4cxSbpngUwbdf1is4G4/iHj4oKxSbBykUnf/DSOYBylJ0MtL5F/dFyiJzoVAYBVPo00Ga9clknmJzocw4iuC5Xx/PhVMoNj+keOrGXUepvm2HQpH5AfefuFi/45xOF9oeFEh1u/wZhcIomEKfDtKsTybzFZsLxUWnD2qVRxFCisyF4v7wjp7Jr71lzU5xweDpG7MXDMut8ls+XT6a4uL9DnODvY7lHh5TMh4zreyDeAp9OkizPpnsPn90GzNTXBT4gF7pQSIfmHc9/IPrKB7t9AF0lsLTBWbJ4qAqxRfHvniZ6YFHr4vXqcQoJoA8uo1VKJmLAg7ME1CWbaxa0eB9wiOYXORcTFkKz2ojnt8vUoTjGIDDuo0V+WAoywH9qlvt/tsUmJNSPKru9y1mLD69PC4uKQySU/xwmud4+nZU+zleaSkufQxjmgWAMN3GGfhg+Tho+gQ84qC+nNz9N1I/gYlx/Nk/toFl3tLoCx3/fv+d79ue8IBPYYqPVb5IWArQMy5k/HdcWPpv+gKKCxQAw3QbZ6V4ZMon52VkYbEUCmvLgXjhk4H/HQdlvKW4gPD2shQRi3Y761n//LLdMdJ0M4rv1vSOV8sF9Cs+pi0/v1yUGBcmAE7XbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAA4HP//b//B3B+JA7XYURBAAAAAElFTkSuQmCC',
                alignment: 'left',
                width: 100,
                height: 40,
                marginTop: 40,
                marginLeft: 25,
                marginBottom: 20,
              },

              {
                columns: [
                  [
                    {
                      text: `${profileDetails.userDetails.firstName + ' ' + profileDetails.userDetails.lastName}`,
                      fontSize: 10,
                      bold: true,

                    },
                    {
                      text: `${profileDetails.userDetails.address1 + ', ' + profileDetails.userDetails.address2
                        + ', ' + profileDetails.userDetails.city + ', ' + profileDetails.userDetails.country}`, fontSize: 10,
                    },
                    { text: profileDetails.userDetails.email, fontSize: 10, },
                    { text: `+971 ${profileDetails.userDetails.mobile.substring(3, profileDetails.userDetails.mobile.length)}`, fontSize: 10, },
                    { text: `CIF Number: ${profileDetails.userDetails.cifNumber}`, fontSize: 10, marginBottom: 10 }
                  ],
                  [
                    {
                      text: `Date: ${new Date().toLocaleString()}`,
                      alignment: 'right',
                      fontSize: 10,
                    },
                  ]
                ],
                marginLeft: 25,
                marginRight: 25,
                margingBottom: 20
              },
              {
                columns: [
                  [
                    {
                      text: '',
                      margingTop: 10,

                    },
                    {
                      text: 'Statement Date:',
                      fontSize: 8,
                      marginBottom: 10


                    },
                    { text: 'Statement Period:', fontSize: 8, },

                  ],
                  [
                    {
                      text: statementDate,
                      alignment: 'center',
                      fontSize: 8,
                      marginBottom: 10
                    },
                    {
                      text: stmtPeriod,
                      alignment: 'center',
                      fontSize: 8,
                    },

                  ],
                  [
                    {
                      text: 'الحساب' + ' كشف ' + ' تاريخ',
                      alignment: 'right',
                      fontSize: 8,
                      marginBottom: 10

                    },
                    {
                      text: 'الحساب' + ' كشف ' + ' مدة',
                      alignment: 'right',
                      fontSize: 8,

                    },
                  ]
                ],
                marginLeft: 25,
                marginRight: 25,

              },

              { canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, }] },

            ]

          }
            if (currentPage != 1)  {
            return [
              {

                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApsAAAEPCAYAAAANjQ+sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACXASURBVHhe7d2PleS2scXhF4JDUAgOwSEoBIegEByCQnAICsEhKASFoAz87l0111wMuptkF8gq8HfP+c57hmZnSDT/FEGQ/X///e9/AQAAgCG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsI7KH8JP+UX633MwAA4J66jcAryj/kX/Kb/CluXPyn928AAMA9dRuBhbIetfxd1oVlD8UmAAD4rtuI+1I8avmLeNTyD1kXkltQbAIAgO+6jbgHxaOWP4tHLf8j66LxKIpNAADwXbcRc1L+Lp+MWm5BsQkAAL7rNqI+5W/iUUs/yBM1arkFxSYAAPiu24h6lGXU8t8yatRyC4pNAADwXbcR+SkeuTx71HILik0AAPBdtxH5KX5qfF3kZUGxCQAAvus2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKzUKUv4s/s63+3vs9uC/lb6vtY6u/9X4XsFDWx6Z/yr8aP8vy3zkuHbDqv8VPvZ+bWbdxRv5wHx/yL+Id6N/yn41+k2XH8++4fId7LIf/n2xuWWwq/jyWA7W3F283f0jbP0f8Kevt0H/Hf49CYiKKT/o+sa+PT79Luz0ctRzPfpXlWHa7k95d+bMWHzv8+Xs78HGl3Ua2Wo5J3k59Tv1H72/egbLeb48e+72fL/vmt/6UqY7v3cbqFH/43qmWA/b6Q43kDcR/w3/r1IO2QrF5EcV9vxxYogrKo/z3lyL0tgf8apT1MSqyoDxiKRy8DfmkyUXMBPw5Pj5Pb2NnHaeW7WjaEVBlOf6PrC0W6xqj9H7ZbazGH8Ljw/CH8snV2qe8Q/vK5OfeckZSKDZPorgwOOvgEsHL6atjbnkloSyjSr4wuPIYtZVPcqccyxDLn5n4XNh+pmfzdu7lKH0cUpb6IsO+uxzbyxWe3cYK3NmybADrDyOLoTuaQrE5kHL2iMAoywUQhefJFBeYPjFcPXL5KR/LfJwtP7oyK38u4m0t6/HKy1WqSFJ8jr16AOsV75NlLga7jZkpHmXKvAH0+GTzz976HKVQbAZTvG25MKu0be2xFJ7M0xtEWU761QvMZ5aL6JJTNhTv4z52ptFbzq0Ub2++61LlmOXlTHsMUpZBrEqDDF7W0PpihG5jRop3zCq3MZ/xjuYDw8dXd4r7o/39GZQrNhUfXGYtDp7xvsQt0iCK90cXYW0/z6ziaFW6c0i7jFspPm5VvjAOORdG8HI8lqdyf6YuOruNmSg+iM9WCHiD/mijUCg2P6DMcHCJUOKqOCv33aMP2369E+9DJUbMlfLFpuLR2eoDLwtvO7/01vMs/vuP5WiXrSofj9Ldeeg2ZuDOkll2qGcObxT+d4/fkU3qYlNxkVltGsYZKDp3cF89+qztx7vzvpW26FRKF5uKL5C//I4JeEDp1Hnlis+hM+/DntOZ5q5Dt/FK7hy52+2o3RuFQrG5g8JI5jYUnS+4bx591PYb/sf7WJpbpGtKyWJT8fFr9sEXGz7Kqbgvsz5YHM37YorpUt3Gq7hTHp2z7qy72LVRKBSbGymz3SY5g09sPMH+oHh/u9u83k95n0t14aKUKzYV3za/0/Fr2Iic4v34jueCf/f640zdxrMpd7rSeOfXXh+1FIrNNxQKhM95Ll66EaqzeN3lbndaoqW5cHksS7t8l2qXcU2p/hDQUeG31RUfy9q/cyfu08uO5d3GMyl3vdJ45e1GoVBsPqG4QLj7gSWSbxvf7tuJlDvfaRnhX71+PpNSpthUXGh++fkb8b73ccGp3GUKwhYhfXpEt/Esim9vrjsC//Nyo1AoNjsU33JiTt0Ym0bdq1O40zKOL6Qve4BIKVFsKncvNBcfFUf+t8LdrR9dUnB2G8+gcGvqPW8U3XmcCsVmQ5n1Sc1MLi0WRlO40zLe0+PaaEr6YlOh0PzRoeLI/+bxb9vfh7/65dS7Vd3GkRSPGnClsc+XSfYKxeaDwm2Sc11WLIykcLFyrtNHypXUxabiAunLz2Bfwemfffyb9vfgf3b16ae6jaMoFJrH/VBwKhSbonDb/DqXvow5isJt8+u4+DvtoYXH32uX4VKrZfN36VMgPee+ebutKBSa251WcHYbR1AoND/3veBUbl9sKjzAcb3LX6nxCYXj0vXc/6cUnErmYpPt8L3fl/7qUbw/c07Yx/01fGpUtzGawgE9zrd5Fv6/q7ZMTik2FeY15eH515e9UuMohRGQPE4ZYVFSFpsKb8/Yrjv9QqHOOG74BV+3MZJX4LEi6xXDcd8OynLbYlOh0Mxn+MEqkkKhmc/wglNJV2xK1mN5Zl8eblF46PgzQ+9SdRujKBSaY3iOom8h9/7b1YYWmwqFZl4lCk6FQjOvoQWnkrHYZM75fu6z78cahdcoxhj24Ge3MYrCpPtxsp4shxWbCoVmfqfN2T1CodDMb1jBqWQsNnHMt9vpCg9WxXE/Dpm/2W2MoDAH5Z6GFBsKhWYdKR8aUig06/hh5CqKQrE5FxeafKaxxpzDe42fUigM7it8Q1XYnupJ9W1DCqMf9YRPy1AoTObCPj1G+O30buMnFEYP7i202FS8PbV/AzV8+TKCKyjMHa/rt95nepRCsQm8F35nodv4CYWD+r2FFZsKo1H1nfLC4FcU5o7X9q/e53qEQrEJbBO231m38Sgv3GpBcU8hxabCaNQchsy920rhmDSHkO9xVig2gW080BN27O42HqFwuxMWVWzyzrR5hN4K3UrJ+now7Bdy4lMoNoHt4u4q9BqPUBiFgn1cbCoUCfM59XvUFY+MMwVjLhHHFopNYLuw0c1u414Kt6qw+OiEoMw2T9Pr4hOcXwXm/cT8dL2/NcSWNvPcwlkv2twPw79/d6HcoahYti3fBbjLtvXRRYtCsQnsEzJQ0G3cQ+EhDqx9WmxWPxl4jqJP/h6dPVxcKS4W/K0YMz3cEjLF4h1l1m8T8XHW25YLyk+2LU95qrptfXTRolBsAvv80duX9uo27qEwtw5rhwsKpfL7NL0fhDzE0FJ8S9h9M8OJcujrkJQZL36/Xbz01vdTSsVt6/AcYIViE9jv4+NPt3ErhYeC0DpUbCoV59h5eX2L8rSnrRXvc5Uv8Nxnw/pLmWUk+Ipty4V6lW3r0IWdQrEJ7Pfxt8J1G7dS2HHROlpsVvt6Uy/vaYVAS3FhUHX/G/LtQoqnHrR/qyIXfFdvW9mL9kO39hTOWcAxHx2Tuo1bKLMc2BFrd7GpVBoh90MWl7+ofKF4P/Q80XY5swt/WEip2A9r3raGTMU4wssimft094MLyuzFpkfEvY7Lg2H+DNc8V3d5YKz6/nIm99WzfvU0FLf5InHmt/J8NAWq27iFMsNO6w3IO503FD/Q4Q3n6Unw8d/NO6xHtrhK/upIsVmlH1N93/dC8RSEarfWP74ts6ZUnu9rl45mPuNleixbu7wZ7J6Socx4zHaB43PS7otgxaPY/rczF0lHLf2668JYWeZBVzsmv/PR+5K7je8o3kDXC1GJi8vdG9ArigtVF59cKe4sNhUX7+3vyCjF93y/4mVcLW8FIfug4oN75X2vyrbl4q5d9qvteum0MlOx6XUJGwlXKs3ZHcl9EHls8mBWxn1nrz9767hVt/EdpdoG6RORC8zhIweKi6c777B7i83sB38fJNLcNn/Hy/pY5nY9MgoZ3VS8b7e/uwJ/TkOeMh9BybhteXk2H9eVGYrNodMtFH/Odxzp9LYRNgi1psxSdB7e7rqNryjutPUfz8w7zCWjBspdrxI3F5tK9lFNHxjKFJoLL/Nj2dv1yejjg7tScVSTbSvO5mO8Ur3YDPv6wHf8t1Z/d2benk+56FNcF1Qu5A9vf93GV5QKowg++aS4NaXcrejcU2xmfuK1ZDGw8LI/1qFdr2w+mgerVJ2rmeZBoL2UbNvW5ifTlarF5mkF0Zr/5uNvt8szCxd+px/nlWpvX1nsfiZj0W18Rck+inDpK2meUTyKV3le2VabNkYl+7zf9PPo3lFcFLTrlY1PZIf3V6XiPjXLtpWpCNlUiCkVi03382UXvv7bj2Vol6s6F5qX1QpKyQvldj226jY+o2Q+eXlnSD1aoHgKwuyjnFuLzcxXdqfdqhpNqXBAO1R8KVUeLlsLfQr/SkqmbWvTk7JKxWLz8jssSsV97ZVLC82FUrHgPLQ9dhufUbIWSik2nK2Uqrf+tthabGa9Uj58myArJfsFzu+95X5HqXbhdmg9M1MyXTS+nf+rVCs204yCK1UfxGv5bkiaekGpNjf20HSObuMzSsYCoeRIgTLrrYm3xZqStdj25zHkacQrKRVeDbSr35VKDyouys4BfkXJ8sDD25e8K5WKzXTnNiX7N0ttkW4/VCr166E7f93GHsUThdd/MIPSt6SUGQvOLcVm1h1rmtvnLSX7bbBdDwop1e4OzLxtZZle9XbkWKlSbKYafVt4maTyOWv3t06dQakwILA49HL3bmOPkm2O3RRzn5TZCs6XxaaSdUTq0HctV6Jkvu28q/+VLKNpW6QsHCIpWc4PL0etlCrFZtqHyJSqt9NTT2NRqsyLPTTVrNvYo2Squj/62qRslMwPXu31rtjMOiJV9lU0WynZRyU23d5Ssr/JoFX+6fN3lCzb1suRK6VCsXnoZH4mpcoo3Fr6Y7xS4Xb6oW8S6ja2lEwH91IPA22lzPLQ0LtiM+POlP7gHkXJPBl90y0u/9zq32Q3/Yj5Qsmwbb0cvVIqFJvpv1VKqXa+KnGMV0pcSLfLvUW3saVkObj7ynnKSfamzPBapKc7tZL1Fnr6g3sUJfPo5qbbXEqlyfTTj2oulCzb1tPBCCV7sVni4kTJfpekVebOlZK+DmiXeYtuY0vJcnCf+sCteAeueHti7VWxmfEhs9uMPC2UzAezt3ctmp/P7I7bVoa5m0/PE0r2YjPlAyw9SpXBkVKvHFMqzN3cXbx3G1tKhiuYW9zqVKq/PPdVsZnxRe5lDu5HKd6mfHfCJ4fsFzMvR5mVSvvHtE+gL5T1tpXloa2nD48q2YvNMq9eUzIOHvSUO8Yr2Y/T8cWmkmEOgYvd6d5/+IxS5Yqx51WxmfEJ4qnm/yp+2MzzqTKd/Pd4+QokpdILkKc6ZinLtuWLxszb1tMRZSVzsVlqBM6UCrfSy+2HSsaBmbUhxWaGicDTjxCsKdXmw6x1i03F69T+7NVKv9VA8YWgRxd8YKrw4MMWL+9g+L+vfjazcoXDmrJsWy7uK25b3YtIJfO67HrXbAZK9vnTJfdDJfsbaoYUm1dX2C66pnv6/B2l2ldYLZ4Vmxlvf5aZA6y4WHcfLif/qhcjb7Xrvtb+bGKV5t6tty0XDzNsW93pGErmYrPcg4pK9vNUuQJ+oWTeD4cUm1fvnLca1VwoVUc3nxWbGQ9KaS9ilPXJv/pDY3t13zjh9tXPZJf21p2yzLOcedvqnjeUzMVmxdu9GQcR1sq+aUTJvK0OKTavLHj8t283qrlQss/b6HlWbGa73ZLm9oriIirbQxZX6o44u331M5ll27Yqz+E9qjtFRsl6Aj/0ouyrKdnfC1l23rSSedQ4tthUrp5nN8VXUh6llHjBa+NZsZltBOWSEXPFn+nykEXmK9crPRuVyn7LbnHJrTtlxjm8R3UfElKy9svLucqZNeuRSruslSiZn/YPLzavHiKf9gXuWynVThrPis325662e2fZS/HF2vohixnmwp3h2TZUZV8YfutOuc0c3qPaPjOFYjOYknXEvPpDepmnKIQXm1fetrrdC5F7lCq3DhdfDppKxrl2odMz/PtkOfnfcZ5lpO5Jwu2rn8ks/Nadwra135fBCiVrsVn22QSFAn4A5eo7y6+EF5tX3rYq+xRZJCXzBtfTKzazXaF9fCGjeJ2YZzlI29/W/kxSH8+9U5jDG+PLCVGh2AymUGwO0qxPJuHFpg926z9wpuG3OatQsu7MPb1i88qLlp5d79dUlocsmAt3nh9GnpUq85d3neAU5vCO86WAUyg2gylZ+7T0e5StWZ9MwovNyzaidlnuTMlWrL1Sodh8emBXlocsvMze/pkLd40fDmb+36v/lhnbVh4UmydQ6NNBmvXJZJpis/zwdySlyonWesXmlSPkPd8e4FDaeZac/POoWmx+e22Twhze6315m4lCYRRMoU8HadYnk/Bic/3Lz1R+I4mkVJq32Ss2sx2MvDyc/HP74Rt4/L9X/y0zb1vMs8yhwrFoQbEZj2JznGmKzbJv/R9FqVIcVTrAI68fThT+36v/BmxBsXkChT4dpFmfTKYpNnevyOyUKgUbxSYiUGziUxSbJ1Do00Ga9clkjmKzXQ64W8qcbHsHeOZCYi+KTXzqy2uoFAqjYAp9OkizPpnEFZvKZa8aaZcF7pbSxWb7M8A7FJv42HobemxHFEbBFPp0kGZ9MgktNq96+rP0V0yNolT5JiGKTXzKD9h8e6p7tQ1RbGIPz3H/8p5FhcIomEKfDtKsTyZTFJtfihW4W8q8+oViE3t4ioVfDeRi8ukB7PHf238LLFzweBvxe0yffl3o4+faf5sBxWY8is1xKDZnpVBsYgYetfS35XikfvN3iCsUm1h41NLv7vXrsL58//krCoVRMIU+HaRZn0woNmelUGyimm+3MsVFwe6D05pCsXlf61HLH77GdK/H72p/fwYUm/EoNseh2JyVQrGJ7HzS8ajly1uZRygUm/fgke9Do5ZbKBRGwRT6dJBmfTKh2JyVUrnYzHowwnFho5ZbKBSb8/F8XR8b/Nn6+PbRqOUWj7/XLkcGFJvxKDbHCS02r/qKRIrNDoViE1daioLwUcstHn+7XSbUsoxaer5u+KjlFgqFUTCFPh2kWZ9M4opNa375Wf7oLcvdKWW+G7qz7BSbtRx+AGMUhWKzltNHLbd4LFO7rBlQbMaj2BxnimJTf7q/PHemVDnZUmzWsxQFHz+AMcpj2drlRh6H3jJwNoXCKJhCnw7SrE8mcxSbkvKEdyXFI01tP2XUKzZ9Emp/Dte4/FbmEUqVaSR3sOndqBkpFEbBFPp0kGZ9MgkvNq/6TutSB7AzKFl36Fav2OQW6DVS3so84rH87frhHN6G0o9abvFYl3b9MqDYjEexOU54sXnVRvRLb3nuTLmq8N+rV2z6JNX+HOKVHLXcSmnXF/FOfcvA2RQKo2AKfTpIsz6ZTFNs/ru3PHel/LTqm+x6xSajUvF+uJUp0089Udo+wOd8jB/ybtSMHuvb9kEGFJvxKDbHCS82r5pr93tvee5KqfRwRK/YrFQsZ1XiAYyRlKwntSqmHrXcQqEwCqbQp4M065NJeLF55Vw7HhJ6UCo9YPOl2HysQ/tzeK7sAxgjPfqk7Ss85yLA21Datwyc7dEnbT9lQLEZj2JznPBi88oRtX/2lumOFI9ItP2T1bNi0yNz7c/iLz5Y3+ZW5lHKlRe/2aV7N2pGCoVRMIU+HaRZn0zCi80r59oxb1OUv6/6pIJnxWaVVzeNdvtbmUcplaaTjOYTPKOWOz36re3LDCg241FsjhNbbFrzB870Z2957kapdAvdnhWbdx2V8oGYUcsA7j9p+/cOpn7LwJkUCqNgCn06SLM+mQwpNq+8hXv7W+lKpVvo9qzYvMMT6etbmYxaDqBUeQXYUdO8GzWjR9+2fZ4BxWY8is1xhhSbV25I3cLlLpSKBdqzYvNvq5+ZxVIUcCvzJI8+bz+Hyhi1PJFCYRRMoU8HadYnkyHF5tW3P297AFYqPn379AJBqfyQEA9gJKBUno7BWwYuplAYBVPo00Ga9clkSLF59aT8Wz4opFSdn/aq2Kwy/3R9K5NRy0SUSqP9t383ajYKhVEwhT4dpFmfTIYUmxmKntsdqJWq7xR8VWxefeHyjgsDRi2TW31eGXkEnFHLpBQKo2AKfTpIsz6ZxBebplz9kMqt5m4qlR+meVVsZp+3+VtvuZGLP6fVZ5aNR8UZCU9KoTAKptCngzTrk8mwYjPDOxJvM1qgVJ7b+PLCwP999bMZcbszOcXzZtvPLZPyJ7lZKRRGwRT6dJBmfTIZVmx6ztH6D13Bo6vTjxgo2U+k77wrNrOvH18mkJySfT4zo5tJKRRGwRT6dJBmfTIZVmxmObhPXQgo/rYgn6ja9a7kXbFZ4cGnW4yiK75jUfLpaCX76P8tRjcVXzy62CjxMN1jWdefUxYUm/EoNscZU2yakuXgPuWL3hXPZ6x8+3zxdn6tkn09365DdcqzecH+bNI/Qa1kHyH3RePUUzIUH7N6F8dpXxOmUBgFU+jTQZr1yWRosZnltTU+uE33xLCSYV5shC3FZoWpAr/0ln0WytaH/ry/pRv9VCqMkE990aLsOSekGP18LEe7bBlQbMaj2BxnaLHpW7zrP3alqUYNlCyFfIQtxWaFbxOadmRK8Um/Xd89Uox+KllPcmuz3on59HxwyeinQmEUTKFPB2nWJ5NxxaYpV78Cac0nvPKT8JUMD19F2jSao1QYyZ1uZEoZcdF4yeinUmHfme6iRRk15cdFiy9iPPo5pM8ef6P9uxlQbMaj2BxneLGZbQTOB7yyB3Klwu3kvbYWm1XeJfprb/krUp7NsRvhlNFP5az1+cQUF8YL5awLRQ9u+CLGx8mQixiFwiiYQp8O0qxPJsOLzYzzpHyyKTeHU5lljmZr82igkmmk/JXyt0KVqx9AGzL6+fh97d/KaIovDFCuHnD4aPTz8e/b35kBxWY8is1xxhabplx5wnrGJ7ISBYHik37WnTPCnmKz0hSCsgWncnWh+YyX6aMHsZQKDwotSr+6Tcm4vy6jn5tGjhUKo2AKfTpIsz6ZnFJsZi4QfMWb9naV4lvHFW77fWLXPEelyuimlSs4layF5uLjaQpKpbsEJQtOJfMI8h+9Ze5RKIyCKfTpIM36ZDK+2DQlc8Hk4iXsNl0ExSf8bPNdR9lbbFabt1qm4FQ86pe50LSPp8AolUY3rVTB6eVdLXtGmy9YFAqjYAp9OkizPpmcVmxWmCflHeDyh4cUF1Ozj2au7S02XYhXGt209A8NKZ7Tln272zwi9Y5SbQ60LwJSPzSkVLhYsc0XLAqFUTCFPh2kWZ9MTis2S82TklNHOhUXUC4yqxVREXYVm6ZUmru5SHEx01IqjaKHvThfqTa6ab4YSHUXZqFUuUj+vbf8zygURsEU+nSQZn0yOafYNKXiSIIPoCNfw+I5me6XO41ktnYXm6ZULMz9Oaf5piHFRXuVfnTfhY7sKVWeTG+lmWuu+D2sWYuHnl3TWhQKo2AKfTpIsz6ZnFpsVhxJWLjw/PgVLIr7wCf4uxeYa0eLzSrv3ezx9nTZCJX/tlQqECx83qLiUd2q++GlFy6Kj2XVBhB2T8NQKIyCKfTpIM36ZHJesWlKtYPTMy4WvMO4ADWPgPoEvvD8t+W/eZ39sxSXfYeKTVP8CpP291Xi7eLn3rpFU1xY+UKnwpy6niF3GJSKUzLWPDLt488pI52Kj29Vj+O7H9ZTKIyCKfTpIM36ZHJ6sVl5dBNjfFJsenuaoYh3weBbo+FfNqD4wqf6SPrQB6yUrCe/Pfz5+nMOv3hRfKvc22flOeWHHi5TKIyCKfTpIM36ZHJusWmKR/vWC4F7O1xsmuJRnfZ3VuYTuosGj7jt2kEVF9/LqPoMBZS5iBo6aqfMctGy8Lp8//Yl2dx//tnHv/F+5d8xS78cKsIVCqNgCn06SLM+mVxSbFaeJ4V4HxWb5t+x+n0z8v7idXym6q3xLU6Zl+i/s/qbs+ptO2u9fzODT+6eZO0Xis14FJvjnF9smuLRl/WC4L4iis3ZRqbwl4+3jT3891Z/G3PwceHwfF+FwiiYQp8O0qxPJtcUm6ZwYIeFFBQKFzBzcZEQPof1FYW7LvP59Lv0KYyCKfTpIM36ZHJpscloFCxs9Eqp+pQsvrrktT6K5yu2y4KaIu6aUBgFU+jTQZr1yeS6YtOUO8yTwmuht0qVmecv3sVvvc/2LAoPMdbngYyPHyxTKIyCKfTpIM36ZHJtsWlK9Xcl4jPRxSa3QmvzxcLl346jMEpe2+6TW49CYRRMoU8HadYnkxTFpouDyu9vw2dCi01T/F5ACs56/JmdOk/zGcXHJUbJa9r98vZnFAqjYAp9OkizPplcX2ya4uJgvWC4j/Bi0xTm3tWTotBcKBSc9YR+AYBCYRRMoU8HadYnkxzFpinVvzYOxwwpNk1hm6ojbDQqksK0jDpGfH8+hVEwhT4dpFmfTPIUm6b4K9HWC4g4PmFmnK4wrNg0hYIzv5SF5kJhWkZ+4YWmKRRGwRT6dJBmfTLJVWyawsT8Mfweyow7+dBi0xQKzrxSF5oLxQUnc8tzGlJomkJhFEyhTwdp1ieTfMWmKVk3xqq+ndCVWxabprjgZHQqlxKF5kJhDmc+wwpNUyiMgin06SDN+mSSttjkoB7n+8FYuW2xaQq3Q3PwZ5DqYaCtFI5NeQwvDhQKo2AKfTpIsz6Z5Cw2TeGg/rkfrvqVWxebprjgZLu6jvu+ZKG5pjDd5zq+WDllVFyhMAqm0KeDNOuTSd5i0xQXnBzUj/lye0m5fbFpircrvkzgfO7zy1/YHkXhG9DO53mzp12sKBRGwRT6dJBmfTLJXWwuFArOfbrzmBSKzRWFryU8zyXfdT6a4ve5MjXjHKdfrCgURsEU+nSQZn0yqVFsmsIowjZPby8pFJsNxcUCTxmPM8Vt81cUj5RnPYHOwMX8JRcrCoVRMIU+HaRZn0zqFJvmBRZGEfrcLz/3+m2hUGx2KNxWH8PvzZ3mtvk7ii+IOT7F8jHrp15/n+Hx99tlyoBiMx7F5ji1ik1TfhIe8PjRptEjhWLzBcXvImWU83PTj2Y+o/j4lPVkWomL9stfjaVQGAVT6NNBmvXJpF6xuVCYb/cXz2fdNHqkUGy+oXiUk23rmBQFQgYKFy7HpRkRVyiMgin06SDN+mRSt9g05c6vsXl727ylUGxupHiEilvr23hbdIF+m1vmW7g/Hv3CrfVtfHy67JZ5z2OZ2uXMgGIzHsXmOLWLzYVyt7lSm0cz1xSKzZ0UzxPOenC8mve5Q9vinSi+cOGNGs95/9p9MjrDY9na5c2AYjMexeY4cxSbpngUwbdf1is4G4/iHj4oKxSbBykUnf/DSOYBylJ0MtL5F/dFyiJzoVAYBVPo00Ga9clknmJzocw4iuC5Xx/PhVMoNj+keOrGXUepvm2HQpH5AfefuFi/45xOF9oeFEh1u/wZhcIomEKfDtKsTybzFZsLxUWnD2qVRxFCisyF4v7wjp7Jr71lzU5xweDpG7MXDMut8ls+XT6a4uL9DnODvY7lHh5TMh4zreyDeAp9OkizPpnsPn90GzNTXBT4gF7pQSIfmHc9/IPrKB7t9AF0lsLTBWbJ4qAqxRfHvniZ6YFHr4vXqcQoJoA8uo1VKJmLAg7ME1CWbaxa0eB9wiOYXORcTFkKz2ojnt8vUoTjGIDDuo0V+WAoywH9qlvt/tsUmJNSPKru9y1mLD69PC4uKQySU/xwmud4+nZU+zleaSkufQxjmgWAMN3GGfhg+Tho+gQ84qC+nNz9N1I/gYlx/Nk/toFl3tLoCx3/fv+d79ue8IBPYYqPVb5IWArQMy5k/HdcWPpv+gKKCxQAw3QbZ6V4ZMon52VkYbEUCmvLgXjhk4H/HQdlvKW4gPD2shQRi3Y761n//LLdMdJ0M4rv1vSOV8sF9Cs+pi0/v1yUGBcmAE7XbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAA4HP//b//B3B+JA7XYURBAAAAAElFTkSuQmCC',
                alignment: 'left',
                width: 100,
                height: 40,
                marginTop: 40,
                marginLeft: 25,
                marginBottom: 40
              },
              {
                columns: [
                  [
                    {
                      text: '',
                      margingTop: 10
                    },
                    {
                      text: 'Statement Date:',
                      fontSize: 8,
                      marginBottom: 10


                    },
                    { text: 'Statement Period:', fontSize: 8, },

                  ],
                  [
                    {
                      text: statementDate,
                      alignment: 'center',
                      fontSize: 8,
                      marginBottom: 10
                    },
                    {
                      text: stmtPeriod,
                      alignment: 'center',
                      fontSize: 8,
                    },

                  ],
                  [
                    {
                      text: 'الحساب' + ' كشف ' + ' تاريخ',
                      alignment: 'right',
                      fontSize: 8,
                      marginBottom: 10
                      // style:'langA'

                    },
                    {
                      text: 'الحساب' + ' كشف ' + ' مدة',
                      alignment: 'right',
                      fontSize: 8,
                      // style:'langA'


                    },
                  ]

                ],
                marginLeft: 25,
                marginRight: 25,


              },
              { canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, }] },
            ]
          }
        },

        content: [

          // //Summary
          // {
          //   text: '',
          //   style: 'tableSectionHeader'
          // },

          {
            columns: [
              [{
                text: 'Summary of Accounts',
                fontSize: 12,
                marginBottom: 10,
                color: '#6E3DEA',
              }
              ],
              [
                {
                  text: reverseString('ملخص الحسابات'),
                  fontSize: 12,
                  marginBottom: 10,
                  color: '#6E3DEA',
                  alignment: 'right'
                }
              ]
            ]
          },
          {
            columns: [[{
              text: `Deem ${profileDetails.creditCardDetails[0].cardType}`,
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA'
            }],
            [
              {
                text: reverseString(arabicText),
                fontSize: 12,
                marginBottom: 10,
                //color: '#6E3DEA',
                alignment: 'right'
              }
            ]
            ]

          },
          {
            columns: [
              [{ text: 'Card Number', marginBottom: 10 },
              { text: 'Total Payment Due', marginBottom: 10 },
              { text: 'Minimum Payment Due', marginBottom: 10 },
              { text: 'Payment Due Date', marginBottom: 10 },
              { text: 'Total Credit Limit', marginBottom: 10 },
              { text: 'Total Cash Limit', marginBottom: 10 },

              ],
              [
                { text: profileDetails.creditCardDetails[0].primaryCardNumber, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].totalPaymentDue)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].minimumPaymentDue)}`, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.creditCardDetails[0].paymentDueDate, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].totalCreditLimit)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].totalCashLimit)}`, marginBottom: 10, alignment: 'center' },

              ],
              [{ text: reverseString(' رقم البطاقة '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' إجمالي الدفعة المستحقة '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' الحد الأدنى للدفع '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' التاريخ المحدد للدفع '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' إجمالي حد الإئتمان '), marginBottom: 10, alignment: 'right', },
              { text: reverseString(' إجمالي الحد النقدي '), marginBottom: 10, alignment: 'right', },

              ],

            ],
            fontSize: 8,
            marginBottom: 10
          },

          {
            columns: [[{
              text: 'Deem Double Secure',
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString(' ديم دبل سيكيور '),
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          },
          {
            columns: [
              [
                { text: 'Monthly Billing Amount', marginBottom: 10 },
                { text: 'For more details and Terms & Conditions, please visit', marginBottom: 10 },
                { text: 'www.deem.io/cards/cash-up and ', marginBottom: 10 },
                { text: 'www.deem.io/cards/miles-up', marginBottom: 20 },


              ],

              [
                { text: `AED ${numberWithCommas(this.cardInsurance)}`, marginBottom: 10, alignment: 'center' },
                { text: '', marginBottom: 10, alignment: 'center' },
                { text: '', marginBottom: 10, alignment: 'center' },
                { text: '', marginBottom: 20, alignment: 'center' },
                { text: '', marginBottom: 30, alignment: 'center' },



              ],
              [
                { text: reverseString(' مبلغ الفاتورة الشهرية '), marginBottom: 10, alignment: 'right' },
                { text: reverseString(' لمزيد من التفاصيل و الشروط و الأحكام, يرجى زيارة '), alignment: 'right', marginBottom: 20 },
                { text: 'www.deem.io/cards/cash-up and ', marginBottom: 10, alignment: 'right' },
                { text: 'www.deem.io/cards/miles-up', marginBottom: 20, alignment: 'right' },

              ]



            ],
            fontSize: 8,
            marginBottom: 10
          },
          //Credit Life Plus
          // {
          //   columns: [{
          //     text: 'Deem Credit Life Plus',
          //     fontSize: 12,
          //     marginBottom: 10,
          //     color: '#6E3DEA'
          //   }
          //   ]
          // },
          // {
          //   columns: [
          //     [
          //       { text: 'For more details and Terms & Conditions, please visit', marginBottom: 10 },
          //       { text: 'www.deem.io/loans/personal', marginBottom: 20 },
          //       { text: 'Monthly Billing Amount (AED)', marginBottom: 10 },

          //     ],

          //     [
          //       { text: '', marginBottom: 50 },

          //       { text: '1,384.00', marginBottom: 10 },


          //     ]
          //   ],
          //   fontSize: 8,
          //   marginBottom: 10
          // },
          //Credit Card Summary
          {
            columns: [
              [
                {
                  text: 'Deem Credit Card',
                  fontSize: 12,
                  marginBottom: 10,
                  color: '#6E3DEA',
                }],
              [{
                text: reverseString(' بطاقة ديم الائتمانية '),
                fontSize: 12,
                marginBottom: 10,
                color: '#6E3DEA',
                alignment: 'right'
              }]
            ]
          },
          {
            columns: [[{
              text: 'Summary of Accounts',
              fontSize: 12,
              marginBottom: 10,
              //color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString(' ملخص الحسابات '),
              fontSize: 12,
              marginBottom: 20,
              //color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          },
          {
            columns: [
              [{ text: 'Card Number', marginBottom: 10 },
              { text: 'Previous Balance', marginBottom: 10 },
              { text: 'Purchases/Cash Advance', marginBottom: 10 },
              { text: 'Interest/Charges', marginBottom: 10 },
              { text: 'Payments/Credits', marginBottom: 10 },
              { text: 'Total Payment Due', marginBottom: 10 },

              ],
              [
                { text: profileDetails.creditCardDetails[0].primaryCardNumber, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].previousBalance)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].cashAdvance)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].interestCharges)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].paymentCredit)}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(profileDetails.creditCardDetails[0].totalPaymentDue)}`, marginBottom: 10, alignment: 'center' },
              ],
              [{ text: reverseString(' رقم البطاقة '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' الرصيد السابق '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' المشتريات/ السلفة النقدية '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' الفوائد/ الرسوم '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' المدفوعات/ الإئتمان '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' إجمالي الدفع المستحق '), marginBottom: 10, alignment: 'right' },

              ],
            ],
            fontSize: 8,
            marginBottom: 40
          },
          //Transaction
          {
            columns: [[{
              text: 'Credit Card Transaction Details',
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString(' تفاصيل معاملات بطاقة الإئتمان '),
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA',
              alignment: 'right'
            }],
            ]
          },

          {
            table: {
              headerRows: 1,
              widths: ['auto', '*', '*', 'auto', '*'],
              marginTop: 100,
              dontBreakRows: true,
              body: [
                [

                  { text: [{ text: reverseString('تاريخ التحويل') + '\n', fontSize: 8, marginBottom: 10 }, { text: 'Transaction date', fontSize: 9, marginBottom: 10 }] },

                  {
                    text: [{ text: reverseString('تاريخ تخصيص و إيداع المبلغ المسدد') + '\n', fontSize: 8, marginBottom: 10 }, {
                      text: 'Posting date', fontSize: 9, marginBottom: 10

                    }],

                  }, {
                    text: [{ text: reverseString('الوصف') + '\n', fontSize: 8, marginBottom: 10, }, {
                      text: 'Description', fontSize: 9, marginBottom: 10

                    }],

                  }, {
                    text: [{ text: reverseString('المبلغ الأصلي') + '\n', marginBottom: 10, fontSize: 8, alignment: 'right' }, {
                      text: 'Amount - Original Currency', marginBottom: 10, fontSize: 9, alignment: 'right'

                    }],

                  }, {
                    text: [{ text: reverseString('المبلغ درهم إماراتي') + '\n', fontSize: 8, marginBottom: 10, alignment: 'right' }, {
                      text: 'Amount(AED)', fontSize: 9, marginBottom: 10, alignment: 'right'

                    }],

                  },

                ],
                // [{

                //   text: 'Transaction date', fontSize: 9, marginBottom: 10
                // }, {
                //   text: 'Posting date', fontSize: 9, marginBottom: 10

                // }, {
                //   text: 'Description', fontSize: 9, marginBottom: 10

                // }, {
                //   text: 'Amount - Original Currency', marginBottom: 10, fontSize: 9, alignment: 'right'

                // }, {
                //   text: 'Amount(AED)', fontSize: 9, marginBottom: 10, alignment: 'right'

                // },

                // ],
                ...this.cardTransactionService.creditCardTransaction.transactions.map(t => ([{ text: t.date, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.postingDate, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.description, fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(t.currencyType), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: numberWithCommas(t.amount), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }])),
                //...this.cardTransactions.map(t => ([{ text: t.date, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.postingDate, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.description, fontSize: 8, margin: [0, 5, 0, 5] }, { text: t.amount, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: t.amount, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }])),
              ]
            },

            //layout: 'lightHorizontalLines',
            layout: {
              hLineColor: function (i, node) {
                return '#D8D8D8';
              },
              hLineWidth: function (i, node) {

                return i === 0 ? 0 : 0.20;
              },
              //   hLineWidth: function (i, node) {
              //     if (i === 0 || i === node.table.body.length) {
              //         return 1;
              //     }
              //     return (i === node.table.headerRows) ? 1 : 0;
              // },
              vLineWidth: function (i, node) {
                return 0;
              }

            }

          },
          //Tax Invoice
          // { text: 'Text on the next page', pageBreak: 'before' },

          // 'Another paragraph, this time a little bit longer to make sure, this line will be divided into at least two lines',

          // page break after text
          { text: '', pageBreak: 'after' },
          {
            columns: [{
              text: 'Summary of Instalment Plans',
              fontSize: 12,
              marginBottom: 10,
              marginTop: 20,
              color: '#6E3DEA'
            },
            {
              text: reverseString('ملخص خطة السداد'),
              fontSize: 12,
              marginBottom: 10,
              marginTop: 20,
              color: '#6E3DEA',
              alignment: 'right'
            }
            ]

          },
          {
            table: {
              headerRows: 1,
              widths: ['*', '*', '*', '*'],
              marginTop: 100,
              dontBreakRows: true,
              body: [

                [{
                  text: [{ text: reverseString('التاريخ') + '\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Date', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }],

                }, {
                  text: [{ text: reverseString('المبلغ الإجمالي المتبقي درهم')+'\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Outstanding Amount (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }],

                }, {
                  text: [{ text: reverseString('الأقساط المتبقية')+'\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Instalments Pending', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'
                  }]
                },
                {
                  text: [{ text: reverseString('المدة الزمنية')+'\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Tenor', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },

                ],
                // [{
                //   text: 'Date', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Outstanding Amount (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Instalments Pending', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'
                // },
                // {
                //   text: 'Tenor', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },

                // ],
                //this.creditCardTransactionService.creditCardTransaction.installmentDetails
                ...this.cardTransactionService.creditCardTransaction.installmentDetails.map(t => ([{ text: t.statementDate, fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(t.outstanding), fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(t.pendingInstallments), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: t.tenor, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }])),

              ]
            },

            // layout: 'noBorders'
            layout: {
              hLineColor: function (i, node) {
                return '#D8D8D8';
              },
              hLineWidth: function (i, node) {

                return i === 0 ? 0 : 0.20;
              },

              vLineWidth: function (i, node) {
                return 0;
              }

            }
          },
//Tax Invoice
          {
           columns:[
             [{text: 'Tax Invoice',
            fontSize: 12,
            marginBottom: 10,
            marginTop: 20,
            color: '#6E3DEA',
          }],
          [{
            text:reverseString(' فاتورة ضريبية '),
            fontSize: 12,
            marginBottom: 10,
            marginTop: 20,
            alignment: 'right',
            color: '#6E3DEA',
          }]
        ]
          },
          {
            columns: [
              [{
                text: 'Deem Credit Card',
                fontSize: 12,
                marginBottom: 10,
                //color: '#6E3DEA'
              }],
              [{
                text: reverseString(' بطاقة ديم الإئتمانية '),
                fontSize: 12,
                marginBottom: 10,
                //color: '#6E3DEA',
                alignment: 'right'
              }],

            ]

          },
          {
            columns: [
              [{ text: 'Invoice Date:', marginBottom: 10 },
              { text: 'Services Eligible for Value Added Tax:', marginBottom: 10 },
              { text: 'Total Fees Eligible for Value Added Tax:', marginBottom: 10 },
              { text: 'Total Value Added Tax for the Period', marginBottom: 10 },


              ],
              [
                { text: statementDate, marginBottom: 10, alignment: 'center' },
                { text: 'As described in the list of transactions', marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(parseFloat(this.vatInvoice.creditCardTaxDetail.totalEligibleTax).toFixed(2))}`, marginBottom: 10, alignment: 'center' },
                { text: `AED ${numberWithCommas(parseFloat(this.vatInvoice.creditCardTaxDetail.totalValueAddedTax).toFixed(2))}`, marginBottom: 10, alignment: 'center' },

              ],
              [{ text: reverseString(' تاريخ الفاتورة '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' الخدمات المؤهلة لقيمة الضريبة المضافة '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' إجمالي الرسوم المؤهلة لقيمة الضريبة المضافة '), marginBottom: 10, alignment: 'right' },
              { text: reverseString(' إجمالي ضريبة القيمة المضافة للفترة '), marginBottom: 10, alignment: 'right' },


              ],

            ],
            fontSize: 8,
            marginBottom: 20
            //pageBreak: "after"
          },
          //
          {
            text: '',
            style: 'sectionHeader',
          },

          // {
          //   canvas: [
          //     {
          //       type: 'path',
          //       d: 'M 0,20 L 100,160 Q 130,200 150,120 C 190,-40 200,200 300,150 L 400,90',
          //       dash: {length: 5},
          //       // lineWidth: 10,
          //       lineColor: 'blue',
          //     },
          //    ]
          // },
          {
            columns:[
              [{text: 'Credit Card - Rewards / Spend Analyzer',
            fontSize: 12,
            marginBottom: 20,
            marginTop: 20,
            color: '#6E3DEA',
          }],
            [{
              text: reverseString('مكافآت بطاقة الائتمان ') + ' \\ ' + reverseString('محلل الإنفاق'),
            fontSize: 12,
            marginBottom: 20,
            marginTop: 20,
            alignment: 'right',
            color: '#6E3DEA',
            }]
          ]
          },
          {
            columns: [[{
              text: 'Spend Categories',
              fontSize: 12,
              marginBottom: 20,
              //color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString(' فئات الإنفاق '),
              fontSize: 12,
              marginBottom: 20,
              //color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          },
          {
            columns: [
              [{
                svg: svg,


              },
           {text:[ 
            {
              text: reverseString(' الرحلات الجوية')+'\n',
              marginTop: 10,
              fontSize: 8,
              marginLeft: 20
            },
               {text: 'Airlines',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20
              },
              
            ],
          }
              ],
              [{
                svg: svgHotels,


              },
              {
               text:[ {text: reverseString('الفنادق')+'\n',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20},
                {text: 'Hotels',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20},
              ]
              }
              ],
              [{
                svg: svgEnt,


              },
              {
               text:[ 
                 {text: reverseString('الترفيه')+'\n',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20},
                {text: 'Entertainment',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20}
              ]
              }
              ],
              [{
                svg: svgOthers,


              },
              {
               text:[ {text: reverseString(' النفقات الأخرى')+'\n',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20
              },
              {text: 'Other Spends',
                marginTop: 10,
                fontSize: 8,
                marginLeft: 20
              },
              ]
              }
              ],
            ]
          },
          { text: '', pageBreak: 'after' },
          {
            columns: [
              [{
                text: 'Rewards Earned',
                fontSize: 12,
                marginBottom: 10,
                marginTop: 20,
                //color: '#6E3DEA'
              }
              ],
              [{
                text: reverseString(' المكافآت المكتسبة '),
                fontSize: 12,
                marginBottom: 10,
                marginTop: 20,
                //color: '#6E3DEA',
                alignment: 'right'
              }
              ],
            ]
          },
          {
            table: {
              headerRows: 1,
              widths: ['*', '*', '*'],
              marginTop: 100,
              dontBreakRows: true,
              body: [

                [{
                  text: [{ text: reverseString('فئات الإنفاق') + '\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Spend Categories', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }]

                }, {
                  text: [{ text: reverseString('مبلغ الإنفاق') + '\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true], alignment: 'center' }, {
                    text: 'Amount Spent', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'center'

                  }]

                }, {
                  text: [{ text: reverseString('المكافآت المكتسبة') + '\n', fontSize: 8, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Rewards Earned', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },

                ],
                // [{
                //   text: 'Spend Categories', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Amount Spent', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'center'

                // }, {
                //   text: 'Rewards Earned', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },

                // ],
                [{ text: 'Airlines, Hotels, Entertainment, Other Spends', fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(rewardDetails.amountSpent.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'center' }, { text: rewardDetails.rewardsEarned, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }],
                [{ text: 'Bonus Rewards', fontSize: 8, margin: [0, 5, 0, 5] }, { text: '---', fontSize: 8, margin: [0, 5, 0, 5], alignment: 'center' }, { text: rewardDetails.rewardsEarned, fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }],
              ]

            },

            // layout: 'noBorders'
            layout: {
              hLineColor: function (i, node) {
                return '#D8D8D8';
              },
              hLineWidth: function (i, node) {

                return i === 0 ? 0 : 0.20;
              },

              vLineWidth: function (i, node) {
                return 0;
              }

            }
          },
        
          //Summary of Reward Points


          {
            columns: [[{
              text: 'Summary of Reward Points',
              fontSize: 12,
              marginBottom: 10,
              marginTop: 20,
              //color: '#6E3DEA'
            }],
            [{
              text: reverseString('ملخص   نقاط المكافآت'),
              fontSize: 12,
              marginBottom: 10,
              marginTop: 20,
              //color: '#6E3DEA',
              alignment: 'right'
            }]
            ]


          },
          {
            table: {
              headerRows: 1,
              widths: ['*', '*', '*', '*', '*'],
              marginTop: 100,
              dontBreakRows: true,
              body: [

                [{
                  text: [{ text: reverseString('رصيد الأشهر السابقة درهم') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Previous Months Balance (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }]

                }, {
                  text: [{ text: reverseString('المكتسب درهم') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true] }, {
                    text: 'Earned (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                  }]

                }, {
                  text: [{ text: reverseString('تم شراؤها درهم') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Purchased (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },
                {
                  text: [{ text: reverseString('النقاط  المستبدلة') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Redeemed (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },
                {
                  text: [{ text: reverseString('الرصيد المتاح درهم') + '\n', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right' }, {
                    text: 'Available Balance (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                  }]

                },

                ],
                // [{
                //   text: 'Previous Months Balance (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Earned (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true],

                // }, {
                //   text: 'Purchased (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },
                // {
                //   text: 'Redeemed (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },
                // {
                //   text: 'Available Balance (AED)', fontSize: 9, marginBottom: 10, border: [true, true, true, true], alignment: 'right'

                // },

                // ],
                [{ text: numberWithCommas(rewardDetails.openingBalance.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(rewardDetails.earnedAed.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5] }, { text: numberWithCommas(rewardDetails.purchasedAed.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: numberWithCommas(rewardDetails.redeemedAed.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }, { text: numberWithCommas(rewardDetails.availableBalance.toFixed(2)), fontSize: 8, margin: [0, 5, 0, 5], alignment: 'right' }],

              ],

            },

            // layout: 'noBorders'
            layout: {
              hLineColor: function (i, node) {
                return '#D8D8D8';
              },
              hLineWidth: function (i, node) {

                return i === 0 ? 0 : 0.20;
              },

              vLineWidth: function (i, node) {
                return 0;
              }

            },

          },
          {text: '', pageBreak: 'after'},
          //Personal Loan


          {
           columns:[ 
             [{text: 'Terms & Conditions',
            fontSize: 12,
            marginBottom: 30,
            marginTop: 10,
            color: '#6E3DEA',
          }],
          [{text: 'الشروط و الأحكام',
          fontSize: 12,
          marginBottom: 30,
          marginTop: 10,
          alignment:'right',
          color: '#6E3DEA',
        }],
          ]
          },
          {
            columnGap: 3,
            alignment: 'justify',
            width: '50%',
            fontSize: 8,
            columns: [

              [
                {

                  text: 'Important notes:',
                  bold: true,
                  marginBottom: 10
                },
                {
                  text: `1. Minimum payment due: You may pay your card balance in full or in partial payments. If you make a partial payment, you must pay at least the minimum amount due each month by the payment due date. Interest will be applicable if payments are not made in full.

                  2. Total cash limit: This limit reflects the amount that can be withdrawn by you in cash.
                  
                  3. Total credit limit: This is the limit available for you to make purchases.
                  
                  4. Pursuant to united states government sanctions against certain countries, MasterCard® has advised that it will refuse to authorize card transactions originating in sanctioned countries. While the sanctions are in effect, you will not be able to use your Deem MasterCard® credit card in sanctioned countries.
                  
                  5. In line with central bank regulations, Deem is required to ensure that we keep updated and valid records of all our customers, it is therefore your responsibility to provide Deem with your renewed ID documents including, but not limited to passport, residence visa, Emirates ID and ensure that you keep your personal data including contact information updated at all times. You are required to immediately update us of any changes in your license where applicable, contact details, including, but not limited to, e-mail address(es), telephone number(s), employment status, etc. Please send your updated information by the registered email to customercare@deem.io or call 600 525550.
                  
                  6. Terms and conditions apply. Visit www.deem.io to view T&Cs and applicable fees and charges.
                  
                  7. Deem will never ask for any personal Information such as your complete card number/account number, passwords, or CVV numbers. This is in line with general banking practice and as per the relevant UAE authority directives. Additionally, please be informed that as per Deem process and internal policies, Deem shall not ask for any monetary compensation for granting credit facilities in advance. If you are approached by anyone, please contact us immediately on Deem Customer Care Centre number 600 525550 or write to us on customercare@deem.io.
                  
                  8. Warning: If you make only the minimum repayment / payment each period, you will pay more in interest / profit / fees and it will take you longer to pay off your outstanding balance.
                  
                  9. For feedback, complaints or queries you may contact our 24-hour Deem Customer Care Centre on 600 525550 or email us at customercare@deem.io.
                  
                  10. To view the Deem Customer Charter, please visit www.deem.io/charter`,
                  marginBottom: 10
                },
              ],
              [
                {
                  text: reverseString('ملاحظات مهمة:'),
                  bold: true,
                  marginBottom: 10,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .1  الحد الأدنى للدفع المستحق: يمكنك دفع رصيد بطاقتك بالكامل أوعلى `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` دفعات. و إذا قمت بالسداد جزئيا يجب أن تدفع الحد الأدنى للمبلغ `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` المستحق شهريا بحلول تاريخ استحقاق الدفع سيتم تطبيق الفائدة إذا لم يتم `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` السداد بالكامل. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .2  إجمالي النقد المتاح: ينطبق هذا الحد على المبلغ الذي يمكن سحبه نقداً. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .3  إجمالي الرصيد المتاح: هذا هو الحد المتاح لك لسداد مشترياتك. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .4  وفقا للعقوبات التي تفرضها حكومة الولايات المتحدة ضد دول معينة, `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` أفادت ماستر كارد بأنها سترفض السماح بإجراءات معاملات البطاقة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الناشئة في البلدان الخاضعة للعقوبات أثناء سريان العقوبات و لن يتمكن `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` العميل من استخدام البطاقة الإئتمانية الخاصة به في البلدان الخاضعة `),

                  alignment: 'right',

                },
                {
                  text: reverseString(` للعقوبات. `),

                  alignment: 'right',
                  marginBottom: 20
                },
                {
                  text: reverseString(` .5  التوافق مع لوائح البنك المركزي يتعين على ديم التأكد من الاحتفاظ `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` بسجلات محدثة و صالحة لجميع العملاء. و لذلك يرجى منك أن تزود `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` ديم بوثائق هويتك المحدثة بما في ذلك جواز السفر, تأشيرة الإقامة, رقم `),

                  alignment: 'right'
                },
                {
                  text: reverseString(``),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الهوية الإماراتية. و التأكد من بقاء بياناتك الشخصية بما في ذلك بيانات `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الاتصال و التي تشمل عناوين البريد الالكتروني و رقم الهاتف و الحالة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الوظيفية. يرجى إرسال معلوماتك المحدثة على عنوان البريد المسجل `),

                  alignment: 'right',

                },
                {
                  text: reverseString(` customercare@deem.io أو الاتصال على 525550 600  `),

                  alignment: 'right',
                  marginBottom: 30,

                },

                {
                  text: reverseString(` .6  تطبق شروط و أحكام. قم بزيارة www.deem.io  لمطالعة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الشروط و الأحكام و الرسوم و النفقات `),
                  marginBottom: 10,
                  alignment: 'right'
                },
                {
                  text:reverseString(` .7  لن نطلب منك أبدًا أن تزودنا بمعلوماتك الشخصية كرقم البطاقة أو`),
                  alignment:'right'
                },
                {
                  text:reverseString(`رقم الحساب أو كلمة المرور و الرقم التعريفي للبطاقة .و ذلك`),
                  alignment:'right'
                },
               
                {
                  text:reverseString(`تماشيا مع الممارسات المصرفية العامة و وفقًا لتوجيهات السلطات`),
                  alignment:'right'
                },
                {
                  text:reverseString(`الإماراتية ذات الصلة. بالإضافة إلى ذلك، يرجى العلم بأن ديم لن`),
                  alignment:'right'
                },
                {
                  text:reverseString(`تطلب أي تعويض نقدي لمنح الستهيلات الائتمانية مقدمًا وفقًا`),
                  alignment:'right'
                },
                {
                  text:reverseString(`لسياسة الشركة. في حال تلقيت اتصال من شخص يطلب منك`),
                  alignment:'right'
                },
                {
                  text:reverseString(`معلوماتك الشخصية يرجى التواصل على الفور مع خدمة العملاء`),
                  alignment:'right'
                },
                {
                  text:reverseString(` أو إرسال بريد الكتروني على `)+ reverseString(' 525550 600 '),
                  alignment:'right'
                },
                {
                  text:'customercare@deem.io',
                  alignment:'right'
                },
                {
                  text:reverseString(` .8  تحذير: إذا قمت بسداد الحد الأدنى فقط من الرصيد المستحق،`),
                  alignment:'right'
                },
                {
                  text:reverseString(`فستدفع المزيد من الفوائد / الأرباح  / الرسوم، كما سيستغرق`),
                  alignment:'right'
                },
                {
                  text:reverseString(`الأمر وقتًا أطول لسداد رصيدك المستحق`),
                  alignment:'right',
                  marginBottom: 10,
                },
                {
                  text:reverseString(` .9  للملاحظات أو للشكاوى أو الاستفسارات، يمكنك الاتصال بخدمة`),
                  alignment:'right'
                },
                {
                  text:reverseString(`عملاء ديم المتواجد على مدار الساعة على 525550 600 أو مراسلتنا`),
                  alignment:'right'
                },
                {
                  text:reverseString(` customercare@deem.io. عبر البريد الإلكتروني على`),
                  alignment:'right',
                  marginBottom: 10,
                },
                // {
                //   text:reverseString(`10.  لعرض ميثاق خدمة العملاء لديم ، يرجى زيارة `),
                //   alignment:'right',
                //marginBottom: 10,
                // },
                {
                  text:reverseString(` .10  لعرض ميثاق خدمة العملاء لديم ، يرجى زيارة`),
                  alignment:'right'
                },
                {
                  text:`www.deem.io/charter`,
                  alignment:'right'
                },
              ],
            ]
          },

        ],
        styles: {
          langA: {
            font: 'Moderat',
          },
          langB: {
            font: 'Moderat',
          },
          sectionHeader: {
            bold: true,
            fontSize: 12,
            margin: [0, 10, 0, 10]
          },
          spacer: {
            margin: [0, 0, 0, 40]
          },
          tableSectionHeader: {
            bold: true,
            fontSize: 10,
            margin: [0, 10, 0, 10]
          },
          tableSectionHeader2: {
            bold: true,
            fontSize: 10,
            margin: [0, 10, 0, 10]
          },
          header: {
            fontSize: 18,
            bold: true,
            margin: [0, 0, 0, 10]
          },
          subheader: {
            fontSize: 16,
            bold: true,
            margin: [0, 10, 0, 5]
          },
          tableExample: {
            margin: [0, 0, 0, 10]
          },
          tableExample2: {
            margin: [0, 0, 0, 0]
          },
          tableHeader: {
            bold: true,
            fontSize: 13,
            color: 'black'
          },

        },
        defaultStyle: {
          font: 'dejavu'
        }
      };
    }
    //Only Loan
    if (profileDetails.personalLoanSummary.loanAccountNumber != undefined && profileDetails.creditCardDetails.length === 0) {
      var totalpaymentDueAmount;

        var towardsPrincipal;

        if(profileDetails.personalLoanSummary.npaStatus != "WRITEOFF"){

          towardsPrincipal = profileDetails.personalLoanSummary.towardsPrincipal;

          totalpaymentDueAmount= profileDetails.personalLoanSummary.paymentDue;

        }else{

          towardsPrincipal = profileDetails.personalLoanSummary.principalOutstanding;

          totalpaymentDueAmount= profileDetails.personalLoanSummary.totalpaymentDue;

        }
      docDefinition = {
        pageMargins: [25, 240, 25, 70],
        footer: function (currentPage, pageCount) {
          return [

            {
              text: [{
                text: 'Page ' + '' + currentPage.toString() + '' + ' of ' + pageCount,
                alignment: 'center',
                fontSize: 8,
                marginTop: 30

              }],

            },
            {
              canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, color: '#6E3DEA' },


              ], marginBottom: 10
            },
            {
              text: [{
                text: 'Deem Finance LLC (Deem) is regulated by Central Bank of the UAE' + ' ' + reverseString('تخضع شركة ديم للتمويل ذ.م.م ديم لرقابة مصرف الإمارات العربية المتحدة المركزي'),
                fontSize: 8,
                alignment: 'center',

              },
              ],

            },
            {
              text: [{
                text: 'P.O.Box 44005 | Abu Dhabi, UAE  www.deem.io | 600 525550 | customercare@deem.io TRN: 100038451900003',
                fontSize: 8,
                alignment: 'center',

              },
              ],

            },



          ]

        },

        header: function (currentPage, pageCount, pageSize) {
          // you can apply any logic and return any valid pdfmake element
          if (currentPage === 1) {
            return [
              {

                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApsAAAEPCAYAAAANjQ+sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACXASURBVHhe7d2PleS2scXhF4JDUAgOwSEoBIegEByCQnAICsEhKASFoAz87l0111wMuptkF8gq8HfP+c57hmZnSDT/FEGQ/X///e9/AQAAgCG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsI7KH8JP+UX633MwAA4J66jcAryj/kX/Kb/CluXPyn928AAMA9dRuBhbIetfxd1oVlD8UmAAD4rtuI+1I8avmLeNTyD1kXkltQbAIAgO+6jbgHxaOWP4tHLf8j66LxKIpNAADwXbcRc1L+Lp+MWm5BsQkAAL7rNqI+5W/iUUs/yBM1arkFxSYAAPiu24h6lGXU8t8yatRyC4pNAADwXbcR+SkeuTx71HILik0AAPBdtxH5KX5qfF3kZUGxCQAAvus2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKzUKUv4s/s63+3vs9uC/lb6vtY6u/9X4XsFDWx6Z/yr8aP8vy3zkuHbDqv8VPvZ+bWbdxRv5wHx/yL+Id6N/yn41+k2XH8++4fId7LIf/n2xuWWwq/jyWA7W3F283f0jbP0f8Kevt0H/Hf49CYiKKT/o+sa+PT79Luz0ctRzPfpXlWHa7k95d+bMWHzv8+Xs78HGl3Ua2Wo5J3k59Tv1H72/egbLeb48e+72fL/vmt/6UqY7v3cbqFH/43qmWA/b6Q43kDcR/w3/r1IO2QrF5EcV9vxxYogrKo/z3lyL0tgf8apT1MSqyoDxiKRy8DfmkyUXMBPw5Pj5Pb2NnHaeW7WjaEVBlOf6PrC0W6xqj9H7ZbazGH8Ljw/CH8snV2qe8Q/vK5OfeckZSKDZPorgwOOvgEsHL6atjbnkloSyjSr4wuPIYtZVPcqccyxDLn5n4XNh+pmfzdu7lKH0cUpb6IsO+uxzbyxWe3cYK3NmybADrDyOLoTuaQrE5kHL2iMAoywUQhefJFBeYPjFcPXL5KR/LfJwtP7oyK38u4m0t6/HKy1WqSFJ8jr16AOsV75NlLga7jZkpHmXKvAH0+GTzz976HKVQbAZTvG25MKu0be2xFJ7M0xtEWU761QvMZ5aL6JJTNhTv4z52ptFbzq0Ub2++61LlmOXlTHsMUpZBrEqDDF7W0PpihG5jRop3zCq3MZ/xjuYDw8dXd4r7o/39GZQrNhUfXGYtDp7xvsQt0iCK90cXYW0/z6ziaFW6c0i7jFspPm5VvjAOORdG8HI8lqdyf6YuOruNmSg+iM9WCHiD/mijUCg2P6DMcHCJUOKqOCv33aMP2369E+9DJUbMlfLFpuLR2eoDLwtvO7/01vMs/vuP5WiXrSofj9Ldeeg2ZuDOkll2qGcObxT+d4/fkU3qYlNxkVltGsYZKDp3cF89+qztx7vzvpW26FRKF5uKL5C//I4JeEDp1Hnlis+hM+/DntOZ5q5Dt/FK7hy52+2o3RuFQrG5g8JI5jYUnS+4bx591PYb/sf7WJpbpGtKyWJT8fFr9sEXGz7Kqbgvsz5YHM37YorpUt3Gq7hTHp2z7qy72LVRKBSbGymz3SY5g09sPMH+oHh/u9u83k95n0t14aKUKzYV3za/0/Fr2Iic4v34jueCf/f640zdxrMpd7rSeOfXXh+1FIrNNxQKhM95Ll66EaqzeN3lbndaoqW5cHksS7t8l2qXcU2p/hDQUeG31RUfy9q/cyfu08uO5d3GMyl3vdJ45e1GoVBsPqG4QLj7gSWSbxvf7tuJlDvfaRnhX71+PpNSpthUXGh++fkb8b73ccGp3GUKwhYhfXpEt/Esim9vrjsC//Nyo1AoNjsU33JiTt0Ym0bdq1O40zKOL6Qve4BIKVFsKncvNBcfFUf+t8LdrR9dUnB2G8+gcGvqPW8U3XmcCsVmQ5n1Sc1MLi0WRlO40zLe0+PaaEr6YlOh0PzRoeLI/+bxb9vfh7/65dS7Vd3GkRSPGnClsc+XSfYKxeaDwm2Sc11WLIykcLFyrtNHypXUxabiAunLz2Bfwemfffyb9vfgf3b16ae6jaMoFJrH/VBwKhSbonDb/DqXvow5isJt8+u4+DvtoYXH32uX4VKrZfN36VMgPee+ebutKBSa251WcHYbR1AoND/3veBUbl9sKjzAcb3LX6nxCYXj0vXc/6cUnErmYpPt8L3fl/7qUbw/c07Yx/01fGpUtzGawgE9zrd5Fv6/q7ZMTik2FeY15eH515e9UuMohRGQPE4ZYVFSFpsKb8/Yrjv9QqHOOG74BV+3MZJX4LEi6xXDcd8OynLbYlOh0Mxn+MEqkkKhmc/wglNJV2xK1mN5Zl8eblF46PgzQ+9SdRujKBSaY3iOom8h9/7b1YYWmwqFZl4lCk6FQjOvoQWnkrHYZM75fu6z78cahdcoxhj24Ge3MYrCpPtxsp4shxWbCoVmfqfN2T1CodDMb1jBqWQsNnHMt9vpCg9WxXE/Dpm/2W2MoDAH5Z6GFBsKhWYdKR8aUig06/hh5CqKQrE5FxeafKaxxpzDe42fUigM7it8Q1XYnupJ9W1DCqMf9YRPy1AoTObCPj1G+O30buMnFEYP7i202FS8PbV/AzV8+TKCKyjMHa/rt95nepRCsQm8F35nodv4CYWD+r2FFZsKo1H1nfLC4FcU5o7X9q/e53qEQrEJbBO231m38Sgv3GpBcU8hxabCaNQchsy920rhmDSHkO9xVig2gW080BN27O42HqFwuxMWVWzyzrR5hN4K3UrJ+now7Bdy4lMoNoHt4u4q9BqPUBiFgn1cbCoUCfM59XvUFY+MMwVjLhHHFopNYLuw0c1u414Kt6qw+OiEoMw2T9Pr4hOcXwXm/cT8dL2/NcSWNvPcwlkv2twPw79/d6HcoahYti3fBbjLtvXRRYtCsQnsEzJQ0G3cQ+EhDqx9WmxWPxl4jqJP/h6dPVxcKS4W/K0YMz3cEjLF4h1l1m8T8XHW25YLyk+2LU95qrptfXTRolBsAvv80duX9uo27qEwtw5rhwsKpfL7NL0fhDzE0FJ8S9h9M8OJcujrkJQZL36/Xbz01vdTSsVt6/AcYIViE9jv4+NPt3ErhYeC0DpUbCoV59h5eX2L8rSnrRXvc5Uv8Nxnw/pLmWUk+Ipty4V6lW3r0IWdQrEJ7Pfxt8J1G7dS2HHROlpsVvt6Uy/vaYVAS3FhUHX/G/LtQoqnHrR/qyIXfFdvW9mL9kO39hTOWcAxHx2Tuo1bKLMc2BFrd7GpVBoh90MWl7+ofKF4P/Q80XY5swt/WEip2A9r3raGTMU4wssimft094MLyuzFpkfEvY7Lg2H+DNc8V3d5YKz6/nIm99WzfvU0FLf5InHmt/J8NAWq27iFMsNO6w3IO503FD/Q4Q3n6Unw8d/NO6xHtrhK/upIsVmlH1N93/dC8RSEarfWP74ts6ZUnu9rl45mPuNleixbu7wZ7J6Socx4zHaB43PS7otgxaPY/rczF0lHLf2668JYWeZBVzsmv/PR+5K7je8o3kDXC1GJi8vdG9ArigtVF59cKe4sNhUX7+3vyCjF93y/4mVcLW8FIfug4oN75X2vyrbl4q5d9qvteum0MlOx6XUJGwlXKs3ZHcl9EHls8mBWxn1nrz9767hVt/EdpdoG6RORC8zhIweKi6c777B7i83sB38fJNLcNn/Hy/pY5nY9MgoZ3VS8b7e/uwJ/TkOeMh9BybhteXk2H9eVGYrNodMtFH/Odxzp9LYRNgi1psxSdB7e7rqNryjutPUfz8w7zCWjBspdrxI3F5tK9lFNHxjKFJoLL/Nj2dv1yejjg7tScVSTbSvO5mO8Ur3YDPv6wHf8t1Z/d2benk+56FNcF1Qu5A9vf93GV5QKowg++aS4NaXcrejcU2xmfuK1ZDGw8LI/1qFdr2w+mgerVJ2rmeZBoL2UbNvW5ifTlarF5mkF0Zr/5uNvt8szCxd+px/nlWpvX1nsfiZj0W18Rck+inDpK2meUTyKV3le2VabNkYl+7zf9PPo3lFcFLTrlY1PZIf3V6XiPjXLtpWpCNlUiCkVi03382UXvv7bj2Vol6s6F5qX1QpKyQvldj226jY+o2Q+eXlnSD1aoHgKwuyjnFuLzcxXdqfdqhpNqXBAO1R8KVUeLlsLfQr/SkqmbWvTk7JKxWLz8jssSsV97ZVLC82FUrHgPLQ9dhufUbIWSik2nK2Uqrf+tthabGa9Uj58myArJfsFzu+95X5HqXbhdmg9M1MyXTS+nf+rVCs204yCK1UfxGv5bkiaekGpNjf20HSObuMzSsYCoeRIgTLrrYm3xZqStdj25zHkacQrKRVeDbSr35VKDyouys4BfkXJ8sDD25e8K5WKzXTnNiX7N0ttkW4/VCr166E7f93GHsUThdd/MIPSt6SUGQvOLcVm1h1rmtvnLSX7bbBdDwop1e4OzLxtZZle9XbkWKlSbKYafVt4maTyOWv3t06dQakwILA49HL3bmOPkm2O3RRzn5TZCs6XxaaSdUTq0HctV6Jkvu28q/+VLKNpW6QsHCIpWc4PL0etlCrFZtqHyJSqt9NTT2NRqsyLPTTVrNvYo2Squj/62qRslMwPXu31rtjMOiJV9lU0WynZRyU23d5Ssr/JoFX+6fN3lCzb1suRK6VCsXnoZH4mpcoo3Fr6Y7xS4Xb6oW8S6ja2lEwH91IPA22lzPLQ0LtiM+POlP7gHkXJPBl90y0u/9zq32Q3/Yj5Qsmwbb0cvVIqFJvpv1VKqXa+KnGMV0pcSLfLvUW3saVkObj7ynnKSfamzPBapKc7tZL1Fnr6g3sUJfPo5qbbXEqlyfTTj2oulCzb1tPBCCV7sVni4kTJfpekVebOlZK+DmiXeYtuY0vJcnCf+sCteAeueHti7VWxmfEhs9uMPC2UzAezt3ctmp/P7I7bVoa5m0/PE0r2YjPlAyw9SpXBkVKvHFMqzN3cXbx3G1tKhiuYW9zqVKq/PPdVsZnxRe5lDu5HKd6mfHfCJ4fsFzMvR5mVSvvHtE+gL5T1tpXloa2nD48q2YvNMq9eUzIOHvSUO8Yr2Y/T8cWmkmEOgYvd6d5/+IxS5Yqx51WxmfEJ4qnm/yp+2MzzqTKd/Pd4+QokpdILkKc6ZinLtuWLxszb1tMRZSVzsVlqBM6UCrfSy+2HSsaBmbUhxWaGicDTjxCsKdXmw6x1i03F69T+7NVKv9VA8YWgRxd8YKrw4MMWL+9g+L+vfjazcoXDmrJsWy7uK25b3YtIJfO67HrXbAZK9vnTJfdDJfsbaoYUm1dX2C66pnv6/B2l2ldYLZ4Vmxlvf5aZA6y4WHcfLif/qhcjb7Xrvtb+bGKV5t6tty0XDzNsW93pGErmYrPcg4pK9vNUuQJ+oWTeD4cUm1fvnLca1VwoVUc3nxWbGQ9KaS9ilPXJv/pDY3t13zjh9tXPZJf21p2yzLOcedvqnjeUzMVmxdu9GQcR1sq+aUTJvK0OKTavLHj8t283qrlQss/b6HlWbGa73ZLm9oriIirbQxZX6o44u331M5ll27Yqz+E9qjtFRsl6Aj/0ouyrKdnfC1l23rSSedQ4tthUrp5nN8VXUh6llHjBa+NZsZltBOWSEXPFn+nykEXmK9crPRuVyn7LbnHJrTtlxjm8R3UfElKy9svLucqZNeuRSruslSiZn/YPLzavHiKf9gXuWynVThrPis325662e2fZS/HF2vohixnmwp3h2TZUZV8YfutOuc0c3qPaPjOFYjOYknXEvPpDepmnKIQXm1fetrrdC5F7lCq3DhdfDppKxrl2odMz/PtkOfnfcZ5lpO5Jwu2rn8ks/Nadwra135fBCiVrsVn22QSFAn4A5eo7y6+EF5tX3rYq+xRZJCXzBtfTKzazXaF9fCGjeJ2YZzlI29/W/kxSH8+9U5jDG+PLCVGh2AymUGwO0qxPJuHFpg926z9wpuG3OatQsu7MPb1i88qLlp5d79dUlocsmAt3nh9GnpUq85d3neAU5vCO86WAUyg2gylZ+7T0e5StWZ9MwovNyzaidlnuTMlWrL1Sodh8emBXlocsvMze/pkLd40fDmb+36v/lhnbVh4UmydQ6NNBmvXJZJpis/zwdySlyonWesXmlSPkPd8e4FDaeZac/POoWmx+e22Twhze6315m4lCYRRMoU8HadYnk/Bic/3Lz1R+I4mkVJq32Ss2sx2MvDyc/HP74Rt4/L9X/y0zb1vMs8yhwrFoQbEZj2JznGmKzbJv/R9FqVIcVTrAI68fThT+36v/BmxBsXkChT4dpFmfTKYpNnevyOyUKgUbxSYiUGziUxSbJ1Do00Ga9clkjmKzXQ64W8qcbHsHeOZCYi+KTXzqy2uoFAqjYAp9OkizPpnEFZvKZa8aaZcF7pbSxWb7M8A7FJv42HobemxHFEbBFPp0kGZ9MgktNq96+rP0V0yNolT5JiGKTXzKD9h8e6p7tQ1RbGIPz3H/8p5FhcIomEKfDtKsTyZTFJtfihW4W8q8+oViE3t4ioVfDeRi8ukB7PHf238LLFzweBvxe0yffl3o4+faf5sBxWY8is1xKDZnpVBsYgYetfS35XikfvN3iCsUm1h41NLv7vXrsL58//krCoVRMIU+HaRZn0woNmelUGyimm+3MsVFwe6D05pCsXlf61HLH77GdK/H72p/fwYUm/EoNseh2JyVQrGJ7HzS8ajly1uZRygUm/fgke9Do5ZbKBRGwRT6dJBmfTKh2JyVUrnYzHowwnFho5ZbKBSb8/F8XR8b/Nn6+PbRqOUWj7/XLkcGFJvxKDbHCS02r/qKRIrNDoViE1daioLwUcstHn+7XSbUsoxaer5u+KjlFgqFUTCFPh2kWZ9M4opNa375Wf7oLcvdKWW+G7qz7BSbtRx+AGMUhWKzltNHLbd4LFO7rBlQbMaj2BxnimJTf7q/PHemVDnZUmzWsxQFHz+AMcpj2drlRh6H3jJwNoXCKJhCnw7SrE8mcxSbkvKEdyXFI01tP2XUKzZ9Emp/Dte4/FbmEUqVaSR3sOndqBkpFEbBFPp0kGZ9MgkvNq/6TutSB7AzKFl36Fav2OQW6DVS3so84rH87frhHN6G0o9abvFYl3b9MqDYjEexOU54sXnVRvRLb3nuTLmq8N+rV2z6JNX+HOKVHLXcSmnXF/FOfcvA2RQKo2AKfTpIsz6ZTFNs/ru3PHel/LTqm+x6xSajUvF+uJUp0089Udo+wOd8jB/ybtSMHuvb9kEGFJvxKDbHCS82r5pr93tvee5KqfRwRK/YrFQsZ1XiAYyRlKwntSqmHrXcQqEwCqbQp4M065NJeLF55Vw7HhJ6UCo9YPOl2HysQ/tzeK7sAxgjPfqk7Ss85yLA21Datwyc7dEnbT9lQLEZj2JznPBi88oRtX/2lumOFI9ItP2T1bNi0yNz7c/iLz5Y3+ZW5lHKlRe/2aV7N2pGCoVRMIU+HaRZn0zCi80r59oxb1OUv6/6pIJnxWaVVzeNdvtbmUcplaaTjOYTPKOWOz36re3LDCg241FsjhNbbFrzB870Z2957kapdAvdnhWbdx2V8oGYUcsA7j9p+/cOpn7LwJkUCqNgCn06SLM+mQwpNq+8hXv7W+lKpVvo9qzYvMMT6etbmYxaDqBUeQXYUdO8GzWjR9+2fZ4BxWY8is1xhhSbV25I3cLlLpSKBdqzYvNvq5+ZxVIUcCvzJI8+bz+Hyhi1PJFCYRRMoU8HadYnkyHF5tW3P297AFYqPn379AJBqfyQEA9gJKBUno7BWwYuplAYBVPo00Ga9clkSLF59aT8Wz4opFSdn/aq2Kwy/3R9K5NRy0SUSqP9t383ajYKhVEwhT4dpFmfTIYUmxmKntsdqJWq7xR8VWxefeHyjgsDRi2TW31eGXkEnFHLpBQKo2AKfTpIsz6ZxBebplz9kMqt5m4qlR+meVVsZp+3+VtvuZGLP6fVZ5aNR8UZCU9KoTAKptCngzTrk8mwYjPDOxJvM1qgVJ7b+PLCwP999bMZcbszOcXzZtvPLZPyJ7lZKRRGwRT6dJBmfTIZVmx6ztH6D13Bo6vTjxgo2U+k77wrNrOvH18mkJySfT4zo5tJKRRGwRT6dJBmfTIZVmxmObhPXQgo/rYgn6ja9a7kXbFZ4cGnW4yiK75jUfLpaCX76P8tRjcVXzy62CjxMN1jWdefUxYUm/EoNscZU2yakuXgPuWL3hXPZ6x8+3zxdn6tkn09365DdcqzecH+bNI/Qa1kHyH3RePUUzIUH7N6F8dpXxOmUBgFU+jTQZr1yWRosZnltTU+uE33xLCSYV5shC3FZoWpAr/0ln0WytaH/ry/pRv9VCqMkE990aLsOSekGP18LEe7bBlQbMaj2BxnaLHpW7zrP3alqUYNlCyFfIQtxWaFbxOadmRK8Um/Xd89Uox+KllPcmuz3on59HxwyeinQmEUTKFPB2nWJ5NxxaYpV78Cac0nvPKT8JUMD19F2jSao1QYyZ1uZEoZcdF4yeinUmHfme6iRRk15cdFiy9iPPo5pM8ef6P9uxlQbMaj2BxneLGZbQTOB7yyB3Klwu3kvbYWm1XeJfprb/krUp7NsRvhlNFP5az1+cQUF8YL5awLRQ9u+CLGx8mQixiFwiiYQp8O0qxPJsOLzYzzpHyyKTeHU5lljmZr82igkmmk/JXyt0KVqx9AGzL6+fh97d/KaIovDFCuHnD4aPTz8e/b35kBxWY8is1xxhabplx5wnrGJ7ISBYHik37WnTPCnmKz0hSCsgWncnWh+YyX6aMHsZQKDwotSr+6Tcm4vy6jn5tGjhUKo2AKfTpIsz6ZnFJsZi4QfMWb9naV4lvHFW77fWLXPEelyuimlSs4layF5uLjaQpKpbsEJQtOJfMI8h+9Ze5RKIyCKfTpIM36ZDK+2DQlc8Hk4iXsNl0ExSf8bPNdR9lbbFabt1qm4FQ86pe50LSPp8AolUY3rVTB6eVdLXtGmy9YFAqjYAp9OkizPpmcVmxWmCflHeDyh4cUF1Ozj2au7S02XYhXGt209A8NKZ7Tln272zwi9Y5SbQ60LwJSPzSkVLhYsc0XLAqFUTCFPh2kWZ9MTis2S82TklNHOhUXUC4yqxVREXYVm6ZUmru5SHEx01IqjaKHvThfqTa6ab4YSHUXZqFUuUj+vbf8zygURsEU+nSQZn0yOafYNKXiSIIPoCNfw+I5me6XO41ktnYXm6ZULMz9Oaf5piHFRXuVfnTfhY7sKVWeTG+lmWuu+D2sWYuHnl3TWhQKo2AKfTpIsz6ZnFpsVhxJWLjw/PgVLIr7wCf4uxeYa0eLzSrv3ezx9nTZCJX/tlQqECx83qLiUd2q++GlFy6Kj2XVBhB2T8NQKIyCKfTpIM36ZHJesWlKtYPTMy4WvMO4ADWPgPoEvvD8t+W/eZ39sxSXfYeKTVP8CpP291Xi7eLn3rpFU1xY+UKnwpy6niF3GJSKUzLWPDLt488pI52Kj29Vj+O7H9ZTKIyCKfTpIM36ZHJ6sVl5dBNjfFJsenuaoYh3weBbo+FfNqD4wqf6SPrQB6yUrCe/Pfz5+nMOv3hRfKvc22flOeWHHi5TKIyCKfTpIM36ZHJusWmKR/vWC4F7O1xsmuJRnfZ3VuYTuosGj7jt2kEVF9/LqPoMBZS5iBo6aqfMctGy8Lp8//Yl2dx//tnHv/F+5d8xS78cKsIVCqNgCn06SLM+mVxSbFaeJ4V4HxWb5t+x+n0z8v7idXym6q3xLU6Zl+i/s/qbs+ptO2u9fzODT+6eZO0Xis14FJvjnF9smuLRl/WC4L4iis3ZRqbwl4+3jT3891Z/G3PwceHwfF+FwiiYQp8O0qxPJtcUm6ZwYIeFFBQKFzBzcZEQPof1FYW7LvP59Lv0KYyCKfTpIM36ZHJpscloFCxs9Eqp+pQsvrrktT6K5yu2y4KaIu6aUBgFU+jTQZr1yeS6YtOUO8yTwmuht0qVmecv3sVvvc/2LAoPMdbngYyPHyxTKIyCKfTpIM36ZHJtsWlK9Xcl4jPRxSa3QmvzxcLl346jMEpe2+6TW49CYRRMoU8HadYnkxTFpouDyu9vw2dCi01T/F5ACs56/JmdOk/zGcXHJUbJa9r98vZnFAqjYAp9OkizPplcX2ya4uJgvWC4j/Bi0xTm3tWTotBcKBSc9YR+AYBCYRRMoU8HadYnkxzFpinVvzYOxwwpNk1hm6ojbDQqksK0jDpGfH8+hVEwhT4dpFmfTPIUm6b4K9HWC4g4PmFmnK4wrNg0hYIzv5SF5kJhWkZ+4YWmKRRGwRT6dJBmfTLJVWyawsT8Mfweyow7+dBi0xQKzrxSF5oLxQUnc8tzGlJomkJhFEyhTwdp1ieTfMWmKVk3xqq+ndCVWxabprjgZHQqlxKF5kJhDmc+wwpNUyiMgin06SDN+mSSttjkoB7n+8FYuW2xaQq3Q3PwZ5DqYaCtFI5NeQwvDhQKo2AKfTpIsz6Z5Cw2TeGg/rkfrvqVWxebprjgZLu6jvu+ZKG5pjDd5zq+WDllVFyhMAqm0KeDNOuTSd5i0xQXnBzUj/lye0m5fbFpircrvkzgfO7zy1/YHkXhG9DO53mzp12sKBRGwRT6dJBmfTLJXWwuFArOfbrzmBSKzRWFryU8zyXfdT6a4ve5MjXjHKdfrCgURsEU+nSQZn0yqVFsmsIowjZPby8pFJsNxcUCTxmPM8Vt81cUj5RnPYHOwMX8JRcrCoVRMIU+HaRZn0zqFJvmBRZGEfrcLz/3+m2hUGx2KNxWH8PvzZ3mtvk7ii+IOT7F8jHrp15/n+Hx99tlyoBiMx7F5ji1ik1TfhIe8PjRptEjhWLzBcXvImWU83PTj2Y+o/j4lPVkWomL9stfjaVQGAVT6NNBmvXJpF6xuVCYb/cXz2fdNHqkUGy+oXiUk23rmBQFQgYKFy7HpRkRVyiMgin06SDN+mRSt9g05c6vsXl727ylUGxupHiEilvr23hbdIF+m1vmW7g/Hv3CrfVtfHy67JZ5z2OZ2uXMgGIzHsXmOLWLzYVyt7lSm0cz1xSKzZ0UzxPOenC8mve5Q9vinSi+cOGNGs95/9p9MjrDY9na5c2AYjMexeY4cxSbpngUwbdf1is4G4/iHj4oKxSbBykUnf/DSOYBylJ0MtL5F/dFyiJzoVAYBVPo00Ga9clknmJzocw4iuC5Xx/PhVMoNj+keOrGXUepvm2HQpH5AfefuFi/45xOF9oeFEh1u/wZhcIomEKfDtKsTybzFZsLxUWnD2qVRxFCisyF4v7wjp7Jr71lzU5xweDpG7MXDMut8ls+XT6a4uL9DnODvY7lHh5TMh4zreyDeAp9OkizPpnsPn90GzNTXBT4gF7pQSIfmHc9/IPrKB7t9AF0lsLTBWbJ4qAqxRfHvniZ6YFHr4vXqcQoJoA8uo1VKJmLAg7ME1CWbaxa0eB9wiOYXORcTFkKz2ojnt8vUoTjGIDDuo0V+WAoywH9qlvt/tsUmJNSPKru9y1mLD69PC4uKQySU/xwmud4+nZU+zleaSkufQxjmgWAMN3GGfhg+Tho+gQ84qC+nNz9N1I/gYlx/Nk/toFl3tLoCx3/fv+d79ue8IBPYYqPVb5IWArQMy5k/HdcWPpv+gKKCxQAw3QbZ6V4ZMon52VkYbEUCmvLgXjhk4H/HQdlvKW4gPD2shQRi3Y761n//LLdMdJ0M4rv1vSOV8sF9Cs+pi0/v1yUGBcmAE7XbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAA4HP//b//B3B+JA7XYURBAAAAAElFTkSuQmCC',
                alignment: 'left',
                width: 100,
                height: 40,
                marginTop: 40,
                marginLeft: 25,
                marginBottom: 20,
              },

              {
                columns: [
                  [
                    {
                      text: `${profileDetails.userDetails.firstName + ' ' + profileDetails.userDetails.lastName}`,
                      fontSize: 10,
                      bold: true,

                    },
                    {
                      text: `${profileDetails.userDetails.address1 + ', ' + profileDetails.userDetails.address2
                        + ', ' + profileDetails.userDetails.city + ', ' + profileDetails.userDetails.country}`, fontSize: 10,
                    },
                    { text: profileDetails.userDetails.email, fontSize: 10, },
                    { text: `+971 ${profileDetails.userDetails.mobile.substring(3, profileDetails.userDetails.mobile.length)}`, fontSize: 10, },
                    { text: `CIF Number: ${profileDetails.userDetails.cifNumber}`, fontSize: 10, marginBottom: 10 }
                  ],
                  [
                    {
                      text: `Date: ${new Date().toLocaleString()}`,
                      alignment: 'right',
                      fontSize: 10,
                    },
                  ]
                ],
                marginLeft: 25,
                marginRight: 25,
                margingBottom: 20
              },
              {
                columns: [
                  [
                    {
                      text: '',
                      margingTop: 10,

                    },
                    {
                      text: 'Statement Date:',
                      fontSize: 8,
                      marginBottom: 10


                    },
                    { text: 'Statement Period:', fontSize: 8, },

                  ],
                  [
                    {
                      text: statementDate,
                      alignment: 'center',
                      fontSize: 8,
                      marginBottom: 10
                    },
                    {
                      text: stmtPeriod,
                      alignment: 'center',
                      fontSize: 8,
                    },

                  ],
                  [
                    {
                      text: 'الحساب' + ' كشف ' + ' تاريخ',
                      alignment: 'right',
                      fontSize: 8,
                      marginBottom: 10

                    },
                    {
                      text: 'الحساب' + ' كشف ' + ' مدة',
                      alignment: 'right',
                      fontSize: 8,

                    },
                  ]
                ],
                marginLeft: 25,
                marginRight: 25,

              },

              { canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, }] },

            ]

          }
            if (currentPage != 1)  {
            return [
              {

                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApsAAAEPCAYAAAANjQ+sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACXASURBVHhe7d2PleS2scXhF4JDUAgOwSEoBIegEByCQnAICsEhKASFoAz87l0111wMuptkF8gq8HfP+c57hmZnSDT/FEGQ/X///e9/AQAAgCG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsI7KH8JP+UX633MwAA4J66jcAryj/kX/Kb/CluXPyn928AAMA9dRuBhbIetfxd1oVlD8UmAAD4rtuI+1I8avmLeNTyD1kXkltQbAIAgO+6jbgHxaOWP4tHLf8j66LxKIpNAADwXbcRc1L+Lp+MWm5BsQkAAL7rNqI+5W/iUUs/yBM1arkFxSYAAPiu24h6lGXU8t8yatRyC4pNAADwXbcR+SkeuTx71HILik0AAPBdtxH5KX5qfF3kZUGxCQAAvus2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKzUKUv4s/s63+3vs9uC/lb6vtY6u/9X4XsFDWx6Z/yr8aP8vy3zkuHbDqv8VPvZ+bWbdxRv5wHx/yL+Id6N/yn41+k2XH8++4fId7LIf/n2xuWWwq/jyWA7W3F283f0jbP0f8Kevt0H/Hf49CYiKKT/o+sa+PT79Luz0ctRzPfpXlWHa7k95d+bMWHzv8+Xs78HGl3Ua2Wo5J3k59Tv1H72/egbLeb48e+72fL/vmt/6UqY7v3cbqFH/43qmWA/b6Q43kDcR/w3/r1IO2QrF5EcV9vxxYogrKo/z3lyL0tgf8apT1MSqyoDxiKRy8DfmkyUXMBPw5Pj5Pb2NnHaeW7WjaEVBlOf6PrC0W6xqj9H7ZbazGH8Ljw/CH8snV2qe8Q/vK5OfeckZSKDZPorgwOOvgEsHL6atjbnkloSyjSr4wuPIYtZVPcqccyxDLn5n4XNh+pmfzdu7lKH0cUpb6IsO+uxzbyxWe3cYK3NmybADrDyOLoTuaQrE5kHL2iMAoywUQhefJFBeYPjFcPXL5KR/LfJwtP7oyK38u4m0t6/HKy1WqSFJ8jr16AOsV75NlLga7jZkpHmXKvAH0+GTzz976HKVQbAZTvG25MKu0be2xFJ7M0xtEWU761QvMZ5aL6JJTNhTv4z52ptFbzq0Ub2++61LlmOXlTHsMUpZBrEqDDF7W0PpihG5jRop3zCq3MZ/xjuYDw8dXd4r7o/39GZQrNhUfXGYtDp7xvsQt0iCK90cXYW0/z6ziaFW6c0i7jFspPm5VvjAOORdG8HI8lqdyf6YuOruNmSg+iM9WCHiD/mijUCg2P6DMcHCJUOKqOCv33aMP2369E+9DJUbMlfLFpuLR2eoDLwtvO7/01vMs/vuP5WiXrSofj9Ldeeg2ZuDOkll2qGcObxT+d4/fkU3qYlNxkVltGsYZKDp3cF89+qztx7vzvpW26FRKF5uKL5C//I4JeEDp1Hnlis+hM+/DntOZ5q5Dt/FK7hy52+2o3RuFQrG5g8JI5jYUnS+4bx591PYb/sf7WJpbpGtKyWJT8fFr9sEXGz7Kqbgvsz5YHM37YorpUt3Gq7hTHp2z7qy72LVRKBSbGymz3SY5g09sPMH+oHh/u9u83k95n0t14aKUKzYV3za/0/Fr2Iic4v34jueCf/f640zdxrMpd7rSeOfXXh+1FIrNNxQKhM95Ll66EaqzeN3lbndaoqW5cHksS7t8l2qXcU2p/hDQUeG31RUfy9q/cyfu08uO5d3GMyl3vdJ45e1GoVBsPqG4QLj7gSWSbxvf7tuJlDvfaRnhX71+PpNSpthUXGh++fkb8b73ccGp3GUKwhYhfXpEt/Esim9vrjsC//Nyo1AoNjsU33JiTt0Ym0bdq1O40zKOL6Qve4BIKVFsKncvNBcfFUf+t8LdrR9dUnB2G8+gcGvqPW8U3XmcCsVmQ5n1Sc1MLi0WRlO40zLe0+PaaEr6YlOh0PzRoeLI/+bxb9vfh7/65dS7Vd3GkRSPGnClsc+XSfYKxeaDwm2Sc11WLIykcLFyrtNHypXUxabiAunLz2Bfwemfffyb9vfgf3b16ae6jaMoFJrH/VBwKhSbonDb/DqXvow5isJt8+u4+DvtoYXH32uX4VKrZfN36VMgPee+ebutKBSa251WcHYbR1AoND/3veBUbl9sKjzAcb3LX6nxCYXj0vXc/6cUnErmYpPt8L3fl/7qUbw/c07Yx/01fGpUtzGawgE9zrd5Fv6/q7ZMTik2FeY15eH515e9UuMohRGQPE4ZYVFSFpsKb8/Yrjv9QqHOOG74BV+3MZJX4LEi6xXDcd8OynLbYlOh0Mxn+MEqkkKhmc/wglNJV2xK1mN5Zl8eblF46PgzQ+9SdRujKBSaY3iOom8h9/7b1YYWmwqFZl4lCk6FQjOvoQWnkrHYZM75fu6z78cahdcoxhj24Ge3MYrCpPtxsp4shxWbCoVmfqfN2T1CodDMb1jBqWQsNnHMt9vpCg9WxXE/Dpm/2W2MoDAH5Z6GFBsKhWYdKR8aUig06/hh5CqKQrE5FxeafKaxxpzDe42fUigM7it8Q1XYnupJ9W1DCqMf9YRPy1AoTObCPj1G+O30buMnFEYP7i202FS8PbV/AzV8+TKCKyjMHa/rt95nepRCsQm8F35nodv4CYWD+r2FFZsKo1H1nfLC4FcU5o7X9q/e53qEQrEJbBO231m38Sgv3GpBcU8hxabCaNQchsy920rhmDSHkO9xVig2gW080BN27O42HqFwuxMWVWzyzrR5hN4K3UrJ+now7Bdy4lMoNoHt4u4q9BqPUBiFgn1cbCoUCfM59XvUFY+MMwVjLhHHFopNYLuw0c1u414Kt6qw+OiEoMw2T9Pr4hOcXwXm/cT8dL2/NcSWNvPcwlkv2twPw79/d6HcoahYti3fBbjLtvXRRYtCsQnsEzJQ0G3cQ+EhDqx9WmxWPxl4jqJP/h6dPVxcKS4W/K0YMz3cEjLF4h1l1m8T8XHW25YLyk+2LU95qrptfXTRolBsAvv80duX9uo27qEwtw5rhwsKpfL7NL0fhDzE0FJ8S9h9M8OJcujrkJQZL36/Xbz01vdTSsVt6/AcYIViE9jv4+NPt3ErhYeC0DpUbCoV59h5eX2L8rSnrRXvc5Uv8Nxnw/pLmWUk+Ipty4V6lW3r0IWdQrEJ7Pfxt8J1G7dS2HHROlpsVvt6Uy/vaYVAS3FhUHX/G/LtQoqnHrR/qyIXfFdvW9mL9kO39hTOWcAxHx2Tuo1bKLMc2BFrd7GpVBoh90MWl7+ofKF4P/Q80XY5swt/WEip2A9r3raGTMU4wssimft094MLyuzFpkfEvY7Lg2H+DNc8V3d5YKz6/nIm99WzfvU0FLf5InHmt/J8NAWq27iFMsNO6w3IO503FD/Q4Q3n6Unw8d/NO6xHtrhK/upIsVmlH1N93/dC8RSEarfWP74ts6ZUnu9rl45mPuNleixbu7wZ7J6Socx4zHaB43PS7otgxaPY/rczF0lHLf2668JYWeZBVzsmv/PR+5K7je8o3kDXC1GJi8vdG9ArigtVF59cKe4sNhUX7+3vyCjF93y/4mVcLW8FIfug4oN75X2vyrbl4q5d9qvteum0MlOx6XUJGwlXKs3ZHcl9EHls8mBWxn1nrz9767hVt/EdpdoG6RORC8zhIweKi6c777B7i83sB38fJNLcNn/Hy/pY5nY9MgoZ3VS8b7e/uwJ/TkOeMh9BybhteXk2H9eVGYrNodMtFH/Odxzp9LYRNgi1psxSdB7e7rqNryjutPUfz8w7zCWjBspdrxI3F5tK9lFNHxjKFJoLL/Nj2dv1yejjg7tScVSTbSvO5mO8Ur3YDPv6wHf8t1Z/d2benk+56FNcF1Qu5A9vf93GV5QKowg++aS4NaXcrejcU2xmfuK1ZDGw8LI/1qFdr2w+mgerVJ2rmeZBoL2UbNvW5ifTlarF5mkF0Zr/5uNvt8szCxd+px/nlWpvX1nsfiZj0W18Rck+inDpK2meUTyKV3le2VabNkYl+7zf9PPo3lFcFLTrlY1PZIf3V6XiPjXLtpWpCNlUiCkVi03382UXvv7bj2Vol6s6F5qX1QpKyQvldj226jY+o2Q+eXlnSD1aoHgKwuyjnFuLzcxXdqfdqhpNqXBAO1R8KVUeLlsLfQr/SkqmbWvTk7JKxWLz8jssSsV97ZVLC82FUrHgPLQ9dhufUbIWSik2nK2Uqrf+tthabGa9Uj58myArJfsFzu+95X5HqXbhdmg9M1MyXTS+nf+rVCs204yCK1UfxGv5bkiaekGpNjf20HSObuMzSsYCoeRIgTLrrYm3xZqStdj25zHkacQrKRVeDbSr35VKDyouys4BfkXJ8sDD25e8K5WKzXTnNiX7N0ttkW4/VCr166E7f93GHsUThdd/MIPSt6SUGQvOLcVm1h1rmtvnLSX7bbBdDwop1e4OzLxtZZle9XbkWKlSbKYafVt4maTyOWv3t06dQakwILA49HL3bmOPkm2O3RRzn5TZCs6XxaaSdUTq0HctV6Jkvu28q/+VLKNpW6QsHCIpWc4PL0etlCrFZtqHyJSqt9NTT2NRqsyLPTTVrNvYo2Squj/62qRslMwPXu31rtjMOiJV9lU0WynZRyU23d5Ssr/JoFX+6fN3lCzb1suRK6VCsXnoZH4mpcoo3Fr6Y7xS4Xb6oW8S6ja2lEwH91IPA22lzPLQ0LtiM+POlP7gHkXJPBl90y0u/9zq32Q3/Yj5Qsmwbb0cvVIqFJvpv1VKqXa+KnGMV0pcSLfLvUW3saVkObj7ynnKSfamzPBapKc7tZL1Fnr6g3sUJfPo5qbbXEqlyfTTj2oulCzb1tPBCCV7sVni4kTJfpekVebOlZK+DmiXeYtuY0vJcnCf+sCteAeueHti7VWxmfEhs9uMPC2UzAezt3ctmp/P7I7bVoa5m0/PE0r2YjPlAyw9SpXBkVKvHFMqzN3cXbx3G1tKhiuYW9zqVKq/PPdVsZnxRe5lDu5HKd6mfHfCJ4fsFzMvR5mVSvvHtE+gL5T1tpXloa2nD48q2YvNMq9eUzIOHvSUO8Yr2Y/T8cWmkmEOgYvd6d5/+IxS5Yqx51WxmfEJ4qnm/yp+2MzzqTKd/Pd4+QokpdILkKc6ZinLtuWLxszb1tMRZSVzsVlqBM6UCrfSy+2HSsaBmbUhxWaGicDTjxCsKdXmw6x1i03F69T+7NVKv9VA8YWgRxd8YKrw4MMWL+9g+L+vfjazcoXDmrJsWy7uK25b3YtIJfO67HrXbAZK9vnTJfdDJfsbaoYUm1dX2C66pnv6/B2l2ldYLZ4Vmxlvf5aZA6y4WHcfLif/qhcjb7Xrvtb+bGKV5t6tty0XDzNsW93pGErmYrPcg4pK9vNUuQJ+oWTeD4cUm1fvnLca1VwoVUc3nxWbGQ9KaS9ilPXJv/pDY3t13zjh9tXPZJf21p2yzLOcedvqnjeUzMVmxdu9GQcR1sq+aUTJvK0OKTavLHj8t283qrlQss/b6HlWbGa73ZLm9oriIirbQxZX6o44u331M5ll27Yqz+E9qjtFRsl6Aj/0ouyrKdnfC1l23rSSedQ4tthUrp5nN8VXUh6llHjBa+NZsZltBOWSEXPFn+nykEXmK9crPRuVyn7LbnHJrTtlxjm8R3UfElKy9svLucqZNeuRSruslSiZn/YPLzavHiKf9gXuWynVThrPis325662e2fZS/HF2vohixnmwp3h2TZUZV8YfutOuc0c3qPaPjOFYjOYknXEvPpDepmnKIQXm1fetrrdC5F7lCq3DhdfDppKxrl2odMz/PtkOfnfcZ5lpO5Jwu2rn8ks/Nadwra135fBCiVrsVn22QSFAn4A5eo7y6+EF5tX3rYq+xRZJCXzBtfTKzazXaF9fCGjeJ2YZzlI29/W/kxSH8+9U5jDG+PLCVGh2AymUGwO0qxPJuHFpg926z9wpuG3OatQsu7MPb1i88qLlp5d79dUlocsmAt3nh9GnpUq85d3neAU5vCO86WAUyg2gylZ+7T0e5StWZ9MwovNyzaidlnuTMlWrL1Sodh8emBXlocsvMze/pkLd40fDmb+36v/lhnbVh4UmydQ6NNBmvXJZJpis/zwdySlyonWesXmlSPkPd8e4FDaeZac/POoWmx+e22Twhze6315m4lCYRRMoU8HadYnk/Bic/3Lz1R+I4mkVJq32Ss2sx2MvDyc/HP74Rt4/L9X/y0zb1vMs8yhwrFoQbEZj2JznGmKzbJv/R9FqVIcVTrAI68fThT+36v/BmxBsXkChT4dpFmfTKYpNnevyOyUKgUbxSYiUGziUxSbJ1Do00Ga9clkjmKzXQ64W8qcbHsHeOZCYi+KTXzqy2uoFAqjYAp9OkizPpnEFZvKZa8aaZcF7pbSxWb7M8A7FJv42HobemxHFEbBFPp0kGZ9MgktNq96+rP0V0yNolT5JiGKTXzKD9h8e6p7tQ1RbGIPz3H/8p5FhcIomEKfDtKsTyZTFJtfihW4W8q8+oViE3t4ioVfDeRi8ukB7PHf238LLFzweBvxe0yffl3o4+faf5sBxWY8is1xKDZnpVBsYgYetfS35XikfvN3iCsUm1h41NLv7vXrsL58//krCoVRMIU+HaRZn0woNmelUGyimm+3MsVFwe6D05pCsXlf61HLH77GdK/H72p/fwYUm/EoNseh2JyVQrGJ7HzS8ajly1uZRygUm/fgke9Do5ZbKBRGwRT6dJBmfTKh2JyVUrnYzHowwnFho5ZbKBSb8/F8XR8b/Nn6+PbRqOUWj7/XLkcGFJvxKDbHCS02r/qKRIrNDoViE1daioLwUcstHn+7XSbUsoxaer5u+KjlFgqFUTCFPh2kWZ9M4opNa375Wf7oLcvdKWW+G7qz7BSbtRx+AGMUhWKzltNHLbd4LFO7rBlQbMaj2BxnimJTf7q/PHemVDnZUmzWsxQFHz+AMcpj2drlRh6H3jJwNoXCKJhCnw7SrE8mcxSbkvKEdyXFI01tP2XUKzZ9Emp/Dte4/FbmEUqVaSR3sOndqBkpFEbBFPp0kGZ9MgkvNq/6TutSB7AzKFl36Fav2OQW6DVS3so84rH87frhHN6G0o9abvFYl3b9MqDYjEexOU54sXnVRvRLb3nuTLmq8N+rV2z6JNX+HOKVHLXcSmnXF/FOfcvA2RQKo2AKfTpIsz6ZTFNs/ru3PHel/LTqm+x6xSajUvF+uJUp0089Udo+wOd8jB/ybtSMHuvb9kEGFJvxKDbHCS82r5pr93tvee5KqfRwRK/YrFQsZ1XiAYyRlKwntSqmHrXcQqEwCqbQp4M065NJeLF55Vw7HhJ6UCo9YPOl2HysQ/tzeK7sAxgjPfqk7Ss85yLA21Datwyc7dEnbT9lQLEZj2JznPBi88oRtX/2lumOFI9ItP2T1bNi0yNz7c/iLz5Y3+ZW5lHKlRe/2aV7N2pGCoVRMIU+HaRZn0zCi80r59oxb1OUv6/6pIJnxWaVVzeNdvtbmUcplaaTjOYTPKOWOz36re3LDCg241FsjhNbbFrzB870Z2957kapdAvdnhWbdx2V8oGYUcsA7j9p+/cOpn7LwJkUCqNgCn06SLM+mQwpNq+8hXv7W+lKpVvo9qzYvMMT6etbmYxaDqBUeQXYUdO8GzWjR9+2fZ4BxWY8is1xhhSbV25I3cLlLpSKBdqzYvNvq5+ZxVIUcCvzJI8+bz+Hyhi1PJFCYRRMoU8HadYnkyHF5tW3P297AFYqPn379AJBqfyQEA9gJKBUno7BWwYuplAYBVPo00Ga9clkSLF59aT8Wz4opFSdn/aq2Kwy/3R9K5NRy0SUSqP9t383ajYKhVEwhT4dpFmfTIYUmxmKntsdqJWq7xR8VWxefeHyjgsDRi2TW31eGXkEnFHLpBQKo2AKfTpIsz6ZxBebplz9kMqt5m4qlR+meVVsZp+3+VtvuZGLP6fVZ5aNR8UZCU9KoTAKptCngzTrk8mwYjPDOxJvM1qgVJ7b+PLCwP999bMZcbszOcXzZtvPLZPyJ7lZKRRGwRT6dJBmfTIZVmx6ztH6D13Bo6vTjxgo2U+k77wrNrOvH18mkJySfT4zo5tJKRRGwRT6dJBmfTIZVmxmObhPXQgo/rYgn6ja9a7kXbFZ4cGnW4yiK75jUfLpaCX76P8tRjcVXzy62CjxMN1jWdefUxYUm/EoNscZU2yakuXgPuWL3hXPZ6x8+3zxdn6tkn09365DdcqzecH+bNI/Qa1kHyH3RePUUzIUH7N6F8dpXxOmUBgFU+jTQZr1yWRosZnltTU+uE33xLCSYV5shC3FZoWpAr/0ln0WytaH/ry/pRv9VCqMkE990aLsOSekGP18LEe7bBlQbMaj2BxnaLHpW7zrP3alqUYNlCyFfIQtxWaFbxOadmRK8Um/Xd89Uox+KllPcmuz3on59HxwyeinQmEUTKFPB2nWJ5NxxaYpV78Cac0nvPKT8JUMD19F2jSao1QYyZ1uZEoZcdF4yeinUmHfme6iRRk15cdFiy9iPPo5pM8ef6P9uxlQbMaj2BxneLGZbQTOB7yyB3Klwu3kvbYWm1XeJfprb/krUp7NsRvhlNFP5az1+cQUF8YL5awLRQ9u+CLGx8mQixiFwiiYQp8O0qxPJsOLzYzzpHyyKTeHU5lljmZr82igkmmk/JXyt0KVqx9AGzL6+fh97d/KaIovDFCuHnD4aPTz8e/b35kBxWY8is1xxhabplx5wnrGJ7ISBYHik37WnTPCnmKz0hSCsgWncnWh+YyX6aMHsZQKDwotSr+6Tcm4vy6jn5tGjhUKo2AKfTpIsz6ZnFJsZi4QfMWb9naV4lvHFW77fWLXPEelyuimlSs4layF5uLjaQpKpbsEJQtOJfMI8h+9Ze5RKIyCKfTpIM36ZDK+2DQlc8Hk4iXsNl0ExSf8bPNdR9lbbFabt1qm4FQ86pe50LSPp8AolUY3rVTB6eVdLXtGmy9YFAqjYAp9OkizPpmcVmxWmCflHeDyh4cUF1Ozj2au7S02XYhXGt209A8NKZ7Tln272zwi9Y5SbQ60LwJSPzSkVLhYsc0XLAqFUTCFPh2kWZ9MTis2S82TklNHOhUXUC4yqxVREXYVm6ZUmru5SHEx01IqjaKHvThfqTa6ab4YSHUXZqFUuUj+vbf8zygURsEU+nSQZn0yOafYNKXiSIIPoCNfw+I5me6XO41ktnYXm6ZULMz9Oaf5piHFRXuVfnTfhY7sKVWeTG+lmWuu+D2sWYuHnl3TWhQKo2AKfTpIsz6ZnFpsVhxJWLjw/PgVLIr7wCf4uxeYa0eLzSrv3ezx9nTZCJX/tlQqECx83qLiUd2q++GlFy6Kj2XVBhB2T8NQKIyCKfTpIM36ZHJesWlKtYPTMy4WvMO4ADWPgPoEvvD8t+W/eZ39sxSXfYeKTVP8CpP291Xi7eLn3rpFU1xY+UKnwpy6niF3GJSKUzLWPDLt488pI52Kj29Vj+O7H9ZTKIyCKfTpIM36ZHJ6sVl5dBNjfFJsenuaoYh3weBbo+FfNqD4wqf6SPrQB6yUrCe/Pfz5+nMOv3hRfKvc22flOeWHHi5TKIyCKfTpIM36ZHJusWmKR/vWC4F7O1xsmuJRnfZ3VuYTuosGj7jt2kEVF9/LqPoMBZS5iBo6aqfMctGy8Lp8//Yl2dx//tnHv/F+5d8xS78cKsIVCqNgCn06SLM+mVxSbFaeJ4V4HxWb5t+x+n0z8v7idXym6q3xLU6Zl+i/s/qbs+ptO2u9fzODT+6eZO0Xis14FJvjnF9smuLRl/WC4L4iis3ZRqbwl4+3jT3891Z/G3PwceHwfF+FwiiYQp8O0qxPJtcUm6ZwYIeFFBQKFzBzcZEQPof1FYW7LvP59Lv0KYyCKfTpIM36ZHJpscloFCxs9Eqp+pQsvrrktT6K5yu2y4KaIu6aUBgFU+jTQZr1yeS6YtOUO8yTwmuht0qVmecv3sVvvc/2LAoPMdbngYyPHyxTKIyCKfTpIM36ZHJtsWlK9Xcl4jPRxSa3QmvzxcLl346jMEpe2+6TW49CYRRMoU8HadYnkxTFpouDyu9vw2dCi01T/F5ACs56/JmdOk/zGcXHJUbJa9r98vZnFAqjYAp9OkizPplcX2ya4uJgvWC4j/Bi0xTm3tWTotBcKBSc9YR+AYBCYRRMoU8HadYnkxzFpinVvzYOxwwpNk1hm6ojbDQqksK0jDpGfH8+hVEwhT4dpFmfTPIUm6b4K9HWC4g4PmFmnK4wrNg0hYIzv5SF5kJhWkZ+4YWmKRRGwRT6dJBmfTLJVWyawsT8Mfweyow7+dBi0xQKzrxSF5oLxQUnc8tzGlJomkJhFEyhTwdp1ieTfMWmKVk3xqq+ndCVWxabprjgZHQqlxKF5kJhDmc+wwpNUyiMgin06SDN+mSSttjkoB7n+8FYuW2xaQq3Q3PwZ5DqYaCtFI5NeQwvDhQKo2AKfTpIsz6Z5Cw2TeGg/rkfrvqVWxebprjgZLu6jvu+ZKG5pjDd5zq+WDllVFyhMAqm0KeDNOuTSd5i0xQXnBzUj/lye0m5fbFpircrvkzgfO7zy1/YHkXhG9DO53mzp12sKBRGwRT6dJBmfTLJXWwuFArOfbrzmBSKzRWFryU8zyXfdT6a4ve5MjXjHKdfrCgURsEU+nSQZn0yqVFsmsIowjZPby8pFJsNxcUCTxmPM8Vt81cUj5RnPYHOwMX8JRcrCoVRMIU+HaRZn0zqFJvmBRZGEfrcLz/3+m2hUGx2KNxWH8PvzZ3mtvk7ii+IOT7F8jHrp15/n+Hx99tlyoBiMx7F5ji1ik1TfhIe8PjRptEjhWLzBcXvImWU83PTj2Y+o/j4lPVkWomL9stfjaVQGAVT6NNBmvXJpF6xuVCYb/cXz2fdNHqkUGy+oXiUk23rmBQFQgYKFy7HpRkRVyiMgin06SDN+mRSt9g05c6vsXl727ylUGxupHiEilvr23hbdIF+m1vmW7g/Hv3CrfVtfHy67JZ5z2OZ2uXMgGIzHsXmOLWLzYVyt7lSm0cz1xSKzZ0UzxPOenC8mve5Q9vinSi+cOGNGs95/9p9MjrDY9na5c2AYjMexeY4cxSbpngUwbdf1is4G4/iHj4oKxSbBykUnf/DSOYBylJ0MtL5F/dFyiJzoVAYBVPo00Ga9clknmJzocw4iuC5Xx/PhVMoNj+keOrGXUepvm2HQpH5AfefuFi/45xOF9oeFEh1u/wZhcIomEKfDtKsTybzFZsLxUWnD2qVRxFCisyF4v7wjp7Jr71lzU5xweDpG7MXDMut8ls+XT6a4uL9DnODvY7lHh5TMh4zreyDeAp9OkizPpnsPn90GzNTXBT4gF7pQSIfmHc9/IPrKB7t9AF0lsLTBWbJ4qAqxRfHvniZ6YFHr4vXqcQoJoA8uo1VKJmLAg7ME1CWbaxa0eB9wiOYXORcTFkKz2ojnt8vUoTjGIDDuo0V+WAoywH9qlvt/tsUmJNSPKru9y1mLD69PC4uKQySU/xwmud4+nZU+zleaSkufQxjmgWAMN3GGfhg+Tho+gQ84qC+nNz9N1I/gYlx/Nk/toFl3tLoCx3/fv+d79ue8IBPYYqPVb5IWArQMy5k/HdcWPpv+gKKCxQAw3QbZ6V4ZMon52VkYbEUCmvLgXjhk4H/HQdlvKW4gPD2shQRi3Y761n//LLdMdJ0M4rv1vSOV8sF9Cs+pi0/v1yUGBcmAE7XbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAA4HP//b//B3B+JA7XYURBAAAAAElFTkSuQmCC',
                alignment: 'left',
                width: 100,
                height: 40,
                marginTop: 40,
                marginLeft: 25,
                marginBottom: 40
              },
              {
                columns: [
                  [
                    {
                      text: '',
                      margingTop: 10
                    },
                    {
                      text: 'Statement Date:',
                      fontSize: 8,
                      marginBottom: 10


                    },
                    { text: 'Statement Period:', fontSize: 8, },

                  ],
                  [
                    {
                      text: statementDate,
                      alignment: 'center',
                      fontSize: 8,
                      marginBottom: 10
                    },
                    {
                      text: stmtPeriod,
                      alignment: 'center',
                      fontSize: 8,
                    },

                  ],
                  [
                    {
                      text: 'الحساب' + ' كشف ' + ' تاريخ',
                      alignment: 'right',
                      fontSize: 8,
                      marginBottom: 10
                      // style:'langA'

                    },
                    {
                      text: 'الحساب' + ' كشف ' + ' مدة',
                      alignment: 'right',
                      fontSize: 8,
                      // style:'langA'


                    },
                  ]

                ],
                marginLeft: 25,
                marginRight: 25,


              },
              { canvas: [{ type: 'line', x1: 25, y1: 10, x2: 575, y2: 10, lineWidth: 0.1, }] },
            ]
          }
        },

        content: [

          //Summary
          {
            text: '',
            style: 'tableSectionHeader'
          },


          //Personal Loan
          {
            columns: [[{
              text: 'Deem Personal Loan',
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString('قرض ديم الشخصي'),
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA',
              alignment: 'right'
            }],
            ]


          },
          //
          {
            columns: [
              [
                { text: 'Personal Loan Statement Date', marginBottom: 10 },
                { text: 'Loan Account Number', marginBottom: 10 },
                { text: 'Principal Outstanding (AED)', marginBottom: 10 },
                { text: 'Payment Due (AED)', marginBottom: 10 },
                { text: 'Towards Principal (AED)', marginBottom: 10, marginLeft: 10 },
                { text: 'Towards Interest (AED)', marginBottom: 10, marginLeft: 10 },
                { text: 'Late Payment Fee / Other Charges (AED)', marginBottom: 10, marginLeft: 10 },
                { text: 'Payment Due Date', marginBottom: 10, },
                 { text: 'Interest Rate P.A.(%)', marginBottom: 10, },
              
              ],

              [
                { text: profileDetails.personalLoanSummary.statementDate, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.loanAccountNumber, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.principalOutstanding, marginBottom: 10, alignment: 'center' },
                { text: totalpaymentDueAmount, marginBottom: 10, alignment: 'center' },
                { text: towardsPrincipal, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.towardsInterest, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.lateFeeOtherCharges, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.paymentDueDate, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.interestRatePa, marginBottom: 10, alignment: 'center' },
               
                //{ text: 'Friday, 1st January 2021', marginBottom: 10 },

              ],
              [
                { text: reverseString('تاريخ  كشف حساب القرض الشخصي'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('رقم حساب القرض'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('المبلغ  الأساسي المستحق'), marginBottom: 10, alignment: 'right' },
                { text:  reverseString('المبلغ  المستحق'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('المبلغ  الأساسي'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
                { text: reverseString('الفائدة'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
                { text: reverseString('رسوم  السداد المتأخر /  الرسوم الأخرى'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
                { text: reverseString('تاريخ  السداد'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('معدل الفائدة السنوي'), marginBottom: 10, alignment: 'right' },
             
              ]
            ],
            fontSize: 8,
            marginBottom: 20
          },
          //Double Secure
          // { 
          //   columns: [{
          //     text: 'Double Secure',
          //     fontSize: 12,
          //     marginBottom: 10,
          //     color: '#6E3DEA'
          //   }
          //   ]

          // },
          // {
          //   columns: [
          //     [
          //       { text: 'For more details and Terms & Conditions, please visit', marginBottom: 10 },
          //       { text: 'www.deem.io/cards/cash-up and www.deem.io/-', marginBottom: 10 },
          //       { text: 'cards/miles-up', marginBottom: 20 },
          //       { text: 'Monthly Billing Amount (AED)', marginBottom: 10 },

          //     ],

          //     [
          //       { text: '', marginBottom: 10 },
          //       { text: '', marginBottom: 10 },
          //       { text: '', marginBottom: 20 },
          //       { text: '', marginBottom: 30 },
          //       { text: '1,384.00', marginBottom: 10 },


          //     ]
          //   ],
          //   fontSize: 8,
          //   marginBottom: 10
          // },
          //Credit Life Plus
          {
            columns: [[{
              text: 'Deem Credit Life Plus',
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString('ديم كريديت لايف بلس'),
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA',
              alignment: 'right'
            }],
            ]
          },
          {
            columns: [
              [
                { text: 'Monthly Billing Amount (AED)', marginBottom: 10 },
                { text: 'For more details and Terms & Conditions, please visit', marginBottom: 10 },
                { text: 'www.deem.io/loans/personal', marginBottom: 10 },
                
              ],

              [

                { text: '', marginBottom: 50, alignment: 'center' },

                { text: numberWithCommas(this.loanInsurance), marginBottom: 10, alignment: 'center' },
              
                // { text: '', marginBottom: 60, alignment: 'center' },

               


              ],
              [
                { text: reverseString('مبلغ الفاتورة الشهرية'), marginBottom: 20, alignment: 'right' },
                { text: reverseString('لمزيد من التفاصيل و الشروط و الأحكام, يرجى زيارة'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('www.deem.io/loans/personal'), marginBottom: 10, alignment: 'right' },
                // { text: reverseString('www.deem.io/loans/personal'), marginBottom: 10, alignment: 'right' },

              ],
            ],
            fontSize: 8,
            marginBottom: 20
          },
          //Credit Card Summary

          //Transaction


          //Tax Invoice


          //


          // {
          //   canvas: [
          //     {
          //       type: 'path',
          //       d: 'M 0,20 L 100,160 Q 130,200 150,120 C 190,-40 200,200 300,150 L 400,90',
          //       dash: {length: 5},
          //       // lineWidth: 10,
          //       lineColor: 'blue',
          //     },
          //    ]
          // },


          //Summary of Reward Points

          //Personal Loan

          
           {columns: [[{text: 'Deem Personal Loan',
            fontSize: 12,
            marginBottom: 10,
           
            }],
          [{
            text: reverseString('قرض  ديم الشخصي'),
            fontSize: 12,
            marginBottom: 10,
           
            alignment: 'right'
          }]
        ]},
          {
            columns: [[{
              text: 'Summary Of Accounts ',
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString('ملخص الحسابات'),
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          }, {

            columns: [
              [
                { text: 'Loan Account Number', marginBottom: 10 },
                { text: 'Principal Outstanding (AED)', marginBottom: 10 },
                { text: 'Payment Due (AED)', marginBottom: 10 },
                { text: 'Towards Principal (AED)', marginLeft: 10, marginBottom: 10 },
                { text: 'Towards Interest (AED)', marginLeft: 10, marginBottom: 10 },
                { text: 'Late Payment Fee / Other Charges (AED)', marginLeft: 10, marginBottom: 10 },
                { text: 'Payment Due Date', marginBottom: 10 },
                { text: 'Amount Borrowed (AED)', marginBottom: 10 },
                { text: 'Tenor (Months)', marginBottom: 10 },
                { text: 'Interest Rate p.a. (%)', marginBottom: 10 },
                { text: 'Monthly Instalment (AED)', marginBottom: 10 },
                { text: 'Last Payment Amount (AED)', marginBottom: 10 },
                { text: 'Last Payment Date', marginBottom: 10 },
                { text: 'Instalments Remaining', marginBottom: 10 },
              
              ],
              [
                { text: profileDetails.personalLoanSummary.loanAccountNumber, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.principalOutstanding, marginBottom: 10, alignment: 'center' },
                { text: totalpaymentDueAmount, marginBottom: 10, alignment: 'center' },
                { text: towardsPrincipal, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.towardsInterest, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.lateFeeOtherCharges, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.paymentDueDate, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.amountBorrowed, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.tenor, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.interestRatePa, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.monthlyInstallment, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.lastPaymentAmount, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.lastPaymentDate, marginBottom: 10, alignment: 'center' },
                { text: profileDetails.personalLoanSummary.installmentRemaining, marginBottom: 10, alignment: 'center' },
               
              ],
              [
                { text: reverseString('رقم حساب القرض'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('المبلغ  الأساسي المستحق'), marginBottom: 10, alignment: 'right' },
              { text:  reverseString('المبلغ  المستحق'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('المبلغ  الأساسي'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
              { text: reverseString('الفائدة'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
              { text: reverseString('رسوم  السداد المتأخر /  الرسوم الأخرى'), marginBottom: 10, marginLeft: 10, alignment: 'right' },
              { text: reverseString('تاريخ  السداد'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('المبلغ المقترض'), marginBottom: 10, alignment: 'right' },
               // { text: reverseString('المدة  (أشهر)'), marginBottom: 10, alignment: 'right' },
                 { text: reverseString('المدة -  أشهر'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('نسبة الفائدة سنويا'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('الأقساط الشهرية'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('مبلغ الدفعة الأخيرة'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('تاريخ الدفع الأخير'), marginBottom: 10, alignment: 'right' },
                { text: reverseString('الأقساط المتبقية'), marginBottom: 10, alignment: 'right' },
               
              ],
            ],
            fontSize: 8,
          },




         {columns:[ [{
            text: 'Tax Invoice',
            fontSize: 12,
            marginBottom: 10,
          }],
          [{
            text: reverseString('فاتورة  ضريبية'),
            fontSize: 12,
            marginBottom: 10,
            alignment:'right'
          }]
        ]},
          {
            columns: [[{
              text: 'Deem Personal Loan',
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA'
            }
            ],
            [{
              text: reverseString('قرض ديم الشخصي'),
              fontSize: 12,
              marginBottom: 10,
              color: '#6E3DEA',
              alignment: 'right'
            }
            ],
            ]

          },
          {
            columns: [
              [
                { text: 'InvoiceDate', marginBottom: 10 },
                { text: 'Services Eligible for Value Added Tax:', marginBottom: 10 },
                { text: 'Total Fees Eligible for Value Added Tax:', marginBottom: 10 },
                { text: 'Total Value Added Tax for the Period', marginBottom: 10 },

              ],
              [
                { text: profileDetails.statementDetils.statementDate, marginBottom: 10 ,alignment: 'center'},
                { text: 'As described in the list of transactions',marginBottom: 10,alignment: 'center' },
                // { text: 'Loan Rescheduling Fee, Fee for Liability Letter ,', },
                // { text: 'and No Liability Certiﬁcates, Partial Payment' },
                // { text: 'Charges, Loan Top-Up Fee, Delayed Payment' },
                // { text: 'Penal Interest Charges, Loan Cancellation Fee' },
                // { text: 'and Deem Credit Life Plus.', marginBottom: 10 },
                { text: this.vatInvoice.loanTaxDetail.totalEligibleTax, marginBottom: 10,alignment: 'center' },
                { text: this.vatInvoice.loanTaxDetail.totalValueAddedTax,alignment: 'center' }

              ],
              [{ text: reverseString('تاريخ الفاتورة'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('الخدمات المؤهلة لقيمة الضريبة المضافة'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('إجمالي الرسوم المؤهلة لقيمة الضريبة المضافة'), marginBottom: 10, alignment: 'right' },
              { text: reverseString('إجمالي ضريبة القيمة المضافة للفترة'), marginBottom: 10, alignment: 'right' },


              ],
            ],
            fontSize: 8,
            marginBottom: 20,

          },
          {text: '', pageBreak: 'after'},
          //Personal Loan


          {
           columns:[ 
             [{text: 'Terms & Conditions',
            fontSize: 12,
            marginBottom: 30,
            marginTop: 10,
            color: '#6E3DEA',
          }],
          [{text: 'الشروط و الأحكام',
          fontSize: 12,
          marginBottom: 30,
          marginTop: 10,
          alignment:'right',
          color: '#6E3DEA',
        }],
          ]
          },
          {
            columnGap: 3,
            alignment: 'justify',
            width: '50%',
            fontSize: 8,
            columns: [

              [
                {

                  text: 'Important notes:',
                  bold: true,
                  marginBottom: 10
                },
                {
                  text: `1. Minimum payment due: You may pay your card balance in full or in partial payments. If you make a partial payment, you must pay at least the minimum amount due each month by the payment due date. Interest will be applicable if payments are not made in full.

                  2. Total cash limit: This limit reflects the amount that can be withdrawn by you in cash.
                  
                  3. Total credit limit: This is the limit available for you to make purchases.
                  
                  4. Pursuant to united states government sanctions against certain countries, MasterCard® has advised that it will refuse to authorize card transactions originating in sanctioned countries. While the sanctions are in effect, you will not be able to use your Deem MasterCard® credit card in sanctioned countries.
                  
                  5. In line with central bank regulations, Deem is required to ensure that we keep updated and valid records of all our customers, it is therefore your responsibility to provide Deem with your renewed ID documents including, but not limited to passport, residence visa, Emirates ID and ensure that you keep your personal data including contact information updated at all times. You are required to immediately update us of any changes in your license where applicable, contact details, including, but not limited to, e-mail address(es), telephone number(s), employment status, etc. Please send your updated information by the registered email to customercare@deem.io or call 600 525550.
                  
                  6. Terms and conditions apply. Visit www.deem.io to view T&Cs and applicable fees and charges.
                  
                  7. Deem will never ask for any personal Information such as your complete card number/account number, passwords, or CVV numbers. This is in line with general banking practice and as per the relevant UAE authority directives. Additionally, please be informed that as per Deem process and internal policies, Deem shall not ask for any monetary compensation for granting credit facilities in advance. If you are approached by anyone, please contact us immediately on Deem Customer Care Centre number 600 525550 or write to us on customercare@deem.io.
                  
                  8. Warning: If you make only the minimum repayment / payment each period, you will pay more in interest / profit / fees and it will take you longer to pay off your outstanding balance.
                  
                  9. For feedback, complaints or queries you may contact our 24-hour Deem Customer Care Centre on 600 525550 or email us at customercare@deem.io.
                  
                  10. To view the Deem Customer Charter, please visit www.deem.io/charter`,

                  

                  
                  marginBottom: 10
                },
              ],
              [
                {
                  text: reverseString('ملاحظات هامة:'),
                  bold: true,
                  marginBottom: 10,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .1  الحد الأدنى المستحق للدفع: يمكنك دفع رصيد بطاقتك بالكامل أوعلى `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` دفعات. و إذا قمت بالسداد جزئيا يجب أن تدفع الحد الأدنى للمبلغ `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` المستحق شهريا بحلول تاريخ استحقاق الدفع سيتم احتساب الفائدة إذا لم يتم `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` السداد بالكامل. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .2  إجمالي النقد المتاح: ينطبق هذا الحد على المبلغ الذي يمكن سحبه نقداً. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .3  إجمالي الرصيد المتاح: هذا هو الحد المتاح لك لسداد مشترياتك. `),
                  marginBottom: 20,
                  alignment: 'right'
                },
                {
                  text: reverseString(` .4  وفقا للعقوبات التي تفرضها حكومة الولايات المتحدة ضد دول معينة, `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` أفادت ماستر كارد بأنها سترفض السماح بإجراءات معاملات البطاقة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الناشئة في البلدان الخاضعة للعقوبات أثناء سريان العقوبات و لن يتمكن `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` العميل من استخدام البطاقة الإئتمانية الخاصة به في البلدان الخاضعة `),

                  alignment: 'right',

                },
                {
                  text: reverseString(` للعقوبات. `),

                  alignment: 'right',
                  marginBottom: 20
                },
                {
                  text: reverseString(` .5  بالتوافق مع قواعد المصرف المركزي يجب على ديم التأكد من الاحتفاظ `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` بسجلات محدثة و صالحة لجميع العملاء. و لذلك يرجى منك أن تزود `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` ديم بوثائق هويتك المحدثة بما في ذلك جواز السفر, تأشيرة الإقامة,  `),

                  alignment: 'right'
                },
                {
                  text: reverseString(``),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الهوية الإماراتية. و التأكد من بقاء بياناتك الشخصية بما في ذلك بيانات `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الاتصال و التي تشمل عناوين البريد الالكتروني و رقم الهاتف و الحالة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الوظيفية. يرجى إرسال معلوماتك المحدثة على عنوان البريد المسجل `),

                  alignment: 'right',

                },
                {
                  text: reverseString(` customercare@deem.io أو الاتصال على 525550 600  `),

                  alignment: 'right',
                  marginBottom: 30,

                },

                {
                  text: reverseString(` .6  تطبق  الشروط و الأحكام قم بزيارة www.deem.io  لمطالعة `),

                  alignment: 'right'
                },
                {
                  text: reverseString(` الشروط و الأحكام و الرسوم و النفقات `),
                  marginBottom: 10,
                  alignment: 'right'
                },
                {
                  text:reverseString(` .7  لن نطلب منك أبدًا أن تزودنا بمعلوماتك الشخصية كرقم البطاقة أو`),
                  alignment:'right'
                },
                {
                  text:reverseString(`رقم الحساب أو كلمة المرور و الرقم التعريفي للبطاقة .و ذلك`),
                  alignment:'right'
                },
               
                {
                  text:reverseString(`تماشيا مع الممارسات المصرفية العامة و وفقًا لتوجيهات السلطات`),
                  alignment:'right'
                },
                {
                  text:reverseString(`الإماراتية ذات الصلة. بالإضافة إلى ذلك، يرجى العلم بأن ديم لن`),
                  alignment:'right'
                },
                {
                  text:reverseString(`تطلب أي تعويض نقدي لمنح الستهيلات الائتمانية مقدمًا وفقًا`),
                  alignment:'right'
                },
                {
                  text:reverseString(`لسياسة الشركة. في حال تلقيت اتصال من شخص يطلب منك`),
                  alignment:'right'
                },
                {
                  text:reverseString(`معلوماتك الشخصية يرجى التواصل على الفور مع خدمة العملاء`),
                  alignment:'right'
                },
                {
                  text:reverseString(`525550 600 أو إرسال بريد الكتروني على`),
                  alignment:'right'
                },
                {
                  text:'customercare@deem.io',
                  alignment:'right',
                  marginBottom: 10,
                },
                {
                  text:reverseString(` .8  تحذير: إذا قمت بسداد الحد الأدنى فقط من الرصيد المستحق،`),
                  alignment:'right'
                },
                {
                  text:reverseString(`فستدفع المزيد من الفوائد / الأرباح  / الرسوم، كما سيستغرق`),
                  alignment:'right'
                },
                {
                  text:reverseString(`الأمر وقتًا أطول لسداد رصيدك المستحق`),
                  alignment:'right',
                  marginBottom: 10,
                },
                {
                  text:reverseString(` .9  للملاحظات أو للشكاوى أو الاستفسارات، يمكنك الاتصال بخدمة`),
                  alignment:'right'
                },
                {
                  text:reverseString(`عملاء ديم المتواجد على مدار الساعة على 525550 600 أو مراسلتنا`),
                  alignment:'right'
                },
                {
                  text:reverseString(` customercare@deem.io. عبر البريد الإلكتروني على`),
                  alignment:'right',
                  marginBottom: 10,
                },
                // {
                //   text:reverseString(`10.  لعرض ميثاق خدمة العملاء لديم ، يرجى زيارة `),
                //   alignment:'right',
                //marginBottom: 10,
                // },
                {
                  text:reverseString(` .10  لعرض ميثاق خدمة العملاء لديم ، يرجى زيارة`),
                  alignment:'right'
                },
                {
                  text:`www.deem.io/charter`,
                  alignment:'right'
                },
              ],
            ]
          },


        ],
        styles: {
          langA: {
            font: 'Moderat',
          },
          langB: {
            font: 'Moderat',
          },
          sectionHeader: {
            bold: true,
            fontSize: 12,
            margin: [0, 10, 0, 10]
          },
          spacer: {
            margin: [0, 0, 0, 40]
          },
          tableSectionHeader: {
            bold: true,
            fontSize: 10,
            margin: [0, 10, 0, 10]
          },
          tableSectionHeader2: {
            bold: true,
            fontSize: 10,
            margin: [0, 10, 0, 10]
          },
          header: {
            fontSize: 18,
            bold: true,
            margin: [0, 0, 0, 10]
          },
          subheader: {
            fontSize: 16,
            bold: true,
            margin: [0, 10, 0, 5]
          },
          tableExample: {
            margin: [0, 0, 0, 10]
          },
          tableExample2: {
            margin: [0, 0, 0, 0]
          },
          tableHeader: {
            bold: true,
            fontSize: 13,
            color: 'black'
          },

        },
        defaultStyle: {
          font: 'dejavu'
        }
      };
    }


    if (action === 'download') {

      pdfMake.createPdf(docDefinition).download(`${'Deem Statement_' + new Date().toLocaleString()}.pdf`, (err, success) => {
        this.isEnabled = false;
      });

    } else if (action === 'print') {
      pdfMake.createPdf(docDefinition).print();
    } else {
      if (this.getBrowserName() === 'edge' || this.getBrowserName() === 'ie') {
        const pdf = pdfMake.createPdf(docDefinition);
        if (window.navigator ) {
          return new Promise((resolve) => {
            pdf.getBlob(blob => {
              //window.navigator.msSaveOrOpenBlob(blob, `${new Date().toLocaleString()}.pdf`);
              //  resolve();
            });
          });
        } else {
          return new Promise((resolve) => {
            pdf.download(`${new Date().toLocaleString()}.pdf`, resolve);
          });
        }
      }
      else {
        const pdf = pdfMake.createPdf(docDefinition).open();
      }

    }

  }

  sliceString(str: string) {
    return str.slice(14, 10);

  }


  payNow() {

    var productId = this.profileDetails.creditCardDetails[0].primaryCardNumber;
    var minimumDueAmount = this.profileDetails.creditCardDetails[0].minimumPaymentDue.toLocaleString();
    var totalDueAmount = this.profileDetails.creditCardDetails[0].totalPaymentDue.toLocaleString();
    var cif = this.loginService.getCif();
    this.payNowService.payNow('CreditCard', productId, minimumDueAmount, totalDueAmount, cif)
  }

  getDueDateColor() {

    var dueDate = new Date(this.profileDetails.creditCardDetails[0].paymentDueDate);
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    return dueDate < today ? '#FF0000' : ''

  }
  async getDetails() {
    var statementDate
    if (this.cardSummaryService.selectedDate) {
      statementDate = this.cardSummaryService.selectedDate;

    }
    else {
      statementDate = this.statementService.profileDetails.statementDetils.statementDate;
    }

    await this.creditCardTransactionService.creditCardTransactionData(this.statementService.profileDetails.creditCardDetails[0].primaryCardNumber.replace('XXXXXXXX', '00000000'), statementDate, this.loginService.getCif());
  }
  getCardImageUrl() {


    if (this.statementService.cardTypeImage) {
      if (this.statementService.cardTypeImage.toLowerCase().includes('platinum')) {
        return '/assets/images/platinum.png'
      }
      else if (this.statementService.cardTypeImage.toLowerCase().includes('titanium')) {
        return '/assets/images/titanium.png'
      }
      else if (this.statementService.cardTypeImage.toLowerCase().includes('world')) {
        return '/assets/images/world.png'
      }
    }
    else {
      return '/assets/images/platinum.png';
    }
  }
  formatNumber(number) {
    return number != null ? parseFloat('' + number + '').toLocaleString() : '0.00';
  }
  getCardArabicText() {

    //console.log('cardTypeImage='+this.statementService.cardTypeImage)
    if (this.statementService.cardTypeImage) {
      if (this.statementService.cardTypeImage.toLowerCase().includes('platinum cash up')) {
        return 'ديم بلاتينيوم كاش أب';
      }
      else if (this.statementService.cardTypeImage.toLowerCase().includes('titanium cash up')) {
        return 'ديم تايتينوم كاش أب';
      }
      else if (this.statementService.cardTypeImage.toLowerCase().includes('world cash up')) {
        return 'ديم وورلد كاش أب';
      }
      else if (this.statementService.cardTypeImage.toLowerCase().includes('platinum miles up')) {
        return 'ديم بلاتينوم مايلز أب';
      }
      else if (this.statementService.cardTypeImage.toLowerCase().includes('titanium miles up')) {
        return 'ديم تايتينوم مايلز أب';
      }
      else if (this.statementService.cardTypeImage.toLowerCase().includes('world miles up')) {
        return 'ديم وورلد مايلز أب';
      }
    }
    else
    {
      return 'بطاقة ديم الئتمانية';
    }
  }
  isHebrew(text: string) {
    var position = text.search(/[\u0590-\u05FF]/);
    return position >= 0;
  }

}
