import { Component, OnInit } from '@angular/core';
import { CmsPaymentOptionsService } from 'src/app/services/cms-payment-options/cms-payment-options.service';



@Component({
  selector: 'app-payment-options',
  templateUrl: './payment-options.component.html',
  styleUrls: ['./payment-options.component.css'],
  
})
export class PaymentOptionsComponent implements OnInit {
 english:any;
 arabic:any;
 public markdownArabicString: string;
 public markdownEnglishString: string;
  public isLoadingPayment = false;
  constructor(private cmsPaymentOptions: CmsPaymentOptionsService, ) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.cmsPaymentOptions.getPaymentOptions().subscribe((data: any) => {
       this.markdownEnglishString = data.english;
       this.markdownArabicString = data.arabic;
      });
    }, 0);
  }
  ngAfterViewInit() {

  }

}
