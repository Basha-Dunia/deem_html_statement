import { Component, OnInit, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EppTransactionService } from 'src/app/services/epp-transaction/epp-transaction.service';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';
import { PayNowService } from 'src/app/Helpers/pay-now.service';
//import { CreditCardEppTransactionInteractionServiceService } from 'src/app/services/credit-card-epp-transaction-interaction-service.service/credit-card-epp-transaction-interaction-service.service';

@Component({
  selector: 'app-epp-transaction',
  templateUrl: './epp-transaction.component.html',
  styleUrls: ['./epp-transaction.component.css']
})
export class EppTransactionComponent implements OnInit {

  eppData: any;
  optionSelected: any;
  checkBoxSelected: any = false; 
  isChecked: any = false;
  eppResponse: any;
  options = [];
  parsedData: any;
  eppTransactions: any;
  fixedMonthyInterest: string;
  processinfFee: string;
  foreClosureFee: string;
  isDropDownValueSelected: boolean
  eppError: string;
  enableEppError: boolean = false;
  instalmentAmount:any;
  amount:any;
  profileDetails: ProfileDetails = new ProfileDetails();
  //private creditCardEppTransactionInteractionServiceServic: CreditCardEppTransactionInteractionServiceService
  constructor(public service: EppTransactionService,private eppTransactionService: EppTransactionService,
     private _router: Router,private location: Location,private statementService: StatementSummaryService
     ,private payNowService:PayNowService) {
    this.eppData = localStorage.getItem('eppData');
    this.parsedData = JSON.parse(this.eppData);
    this.eppTransactions = this.parsedData.eppTransactions;
  }
  
  async ngOnInit() {
    
    this.getDropDownValues();
    this.profileDetails = this.statementService.profileDetails;
  }
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  onClear() {
    
  }
  exitClick() {
    // this.dialogRef.close();
  }
  onOptionsSelected(event) {
   
    this.isDropDownValueSelected = false;
    this.amount=this.parsedData.amount;
    this.processinfFee = this.optionSelected.processingFee;
    this.fixedMonthyInterest = this.optionSelected.interestRate;
    this.foreClosureFee = this.optionSelected.foreClosureFee;
   // this.instalmentAmount=((parseInt(this.amount)/parseInt(this.optionSelected.tenor)) + ((parseInt(this.amount)*parseInt(this.fixedMonthyInterest))/100)).toFixed(2);
   this.instalmentAmount=((this.amount/parseInt(this.optionSelected.tenor)) + ((this.amount*parseFloat(this.fixedMonthyInterest))/100)).toFixed(2);
    
  }

  getDropDownValues() {
    for (var i = 0; i < this.eppTransactions.length; i++) {
      this.options.push(this.eppTransactions[i].tenor)
    }
  }

  onCheck(event) {
    this.isChecked = event.target.checked
    this.checkBoxSelected=false;
  }

  async acceptOffer() {
    this.checkBoxSelected = this.isChecked;
    if (this.optionSelected == undefined && this.checkBoxSelected == false) {
      this.isDropDownValueSelected = true;
      this.checkBoxSelected = true;
    }
    else if (this.checkBoxSelected == false && this.optionSelected != undefined) {
      this.isDropDownValueSelected = false;
      this.checkBoxSelected = true;
    }
    
    else if (this.checkBoxSelected == true && this.optionSelected == undefined) {
      this.isDropDownValueSelected = true;
      this.checkBoxSelected = false;
    }
    else{
      this.checkBoxSelected = false;
      this.eppResponse = await this.eppTransactionService.eppService(this.optionSelected, this.parsedData,this.instalmentAmount);
     
      if (this.eppResponse.status == 'success') {
        this._router.navigate(['/statement/epp-thank-you']);
      }
      else if(this.eppResponse.status == 'failure') {
        this.eppError = "Unable to register EPP request";
        this.enableEppError = true;
      }
      else{
        this.eppError = "Something went wrong while processing your request";
        this.enableEppError = true;
      }

    }

  }
 async back() {
    this.location.back();
    
  }
  formatNumber(number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }
  payNow() {
   
    var productId = this.statementService.profileDetails.creditCardDetails[0].primaryCardNumber//this.profileDetails.creditCardDetails[0].primaryCardNumber;
    var minimumDueAmount = this.statementService.profileDetails.creditCardDetails[0].minimumPaymentDue.toLocaleString();
    var totalDueAmount = this.statementService.profileDetails.creditCardDetails[0].totalPaymentDue.toLocaleString();
    var cif=this.profileDetails.userDetails.cifNumber;
     this.payNowService.payNow('CreditCard',productId,minimumDueAmount,totalDueAmount,cif)
  }
 

}
