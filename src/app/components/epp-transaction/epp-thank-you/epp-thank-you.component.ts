import { Component, OnInit } from '@angular/core';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { PayNowService } from 'src/app/Helpers/pay-now.service';

@Component({
  selector: 'app-epp-thank-you',
  templateUrl: './epp-thank-you.component.html',
  styleUrls: ['./epp-thank-you.component.css']
})
export class EppThankYouComponent implements OnInit {

  profileDetails: ProfileDetails = new ProfileDetails();
  constructor(private statementService: StatementSummaryService,private payNowService:PayNowService) { }

  ngOnInit(): void {
    this.profileDetails = this.statementService.profileDetails;
  }
  formatNumber(number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }
  payNow() {
    
    var productId = this.statementService.profileDetails.creditCardDetails[0].primaryCardNumber//this.profileDetails.creditCardDetails[0].primaryCardNumber;
    var minimumDueAmount = this.statementService.profileDetails.creditCardDetails[0].minimumPaymentDue.toLocaleString();
    var totalDueAmount = this.statementService.profileDetails.creditCardDetails[0].totalPaymentDue.toLocaleString();
   var cif=this.profileDetails.userDetails.cifNumber;
    this.payNowService.payNow('CreditCard',productId,minimumDueAmount,totalDueAmount,cif)
  }
}
