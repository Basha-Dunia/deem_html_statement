import { Component, OnInit } from '@angular/core';
import { FormatDateService } from 'src/app/Helpers/format-date.service';
import { PayNowService } from 'src/app/Helpers/pay-now.service';
import { ToLocaleStringService } from 'src/app/Helpers/to-locale-string.service';
import { PersonalLoanDetails } from 'src/app/models/personal-loan-details-model';
import { PersonalLoanSummary } from 'src/app/models/personal-loan-summary';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { CreditCardSummaryService } from 'src/app/services/credit-card-summary/credit-card-summary.service';
import { LoginService } from 'src/app/services/login/login.service';
import { PersonalLoanService } from 'src/app/services/personal-loan/personal-loan.service';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
})
export class LoanComponent implements OnInit {
  public selected;
  profileDetails: ProfileDetails = new ProfileDetails();
  plDetails: PersonalLoanDetails;
  plSummary: PersonalLoanSummary;
  statementPeriodOne: any;
  statementPeriodTwo: any;
   
  constructor(private statementService: StatementSummaryService,
    private plService: PersonalLoanService, private loginService: LoginService,
    private formatDate: FormatDateService, private payNowService: PayNowService, 
    private cardSummaryService: CreditCardSummaryService,public localeStringService: ToLocaleStringService) { 
      
    }

  async ngOnInit() {
    this.profileDetails = this.statementService.profileDetails;
    var stmtPeriod;
    if (this.plService.presonlaLoan.statementPeriod) {
      stmtPeriod = this.plService.presonlaLoan.statementPeriod
    }
    else {
      stmtPeriod = this.profileDetails.personalLoanSummary.statementPeriod;
    }
    if (stmtPeriod) {
      this.statementPeriodOne = this.formatDate.dateFormat1(stmtPeriod.slice(0, 10));
      this.statementPeriodTwo = this.formatDate.dateFormat1(stmtPeriod.slice(13, 25));
    }
    if (this.profileDetails.availableLoanStatements.length > 0) {
      var index = this.profileDetails.availableLoanStatements.length - 1;
      this.selected = this.profileDetails.availableLoanStatements[index].statementDate;
    }
    else {
      this.selected = "No Records"
    }
    this.plSummary = this.statementService.profileDetails.personalLoanSummary;

  }
  async selectPeriod(stmtDate) {
    this.selected = '';
    this.statementPeriodOne = '';
    this.plSummary = await this.plService.getLoanDetails(this.loginService.getCif(), stmtDate);

  }
  formatNumber(number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }
  payNow() {
    var productId = this.statementService.profileDetails.personalLoanSummary.loanAccountNumber;
    var minimumDueAmount = ''+this.statementService.profileDetails.personalLoanSummary.paymentDue+'';
    var totalDueAmount = ''+this.statementService.profileDetails.personalLoanSummary.paymentDue+'';
    var cif = this.loginService.getCif();
    this.payNowService.payNow('PersonalLoan', productId, minimumDueAmount, totalDueAmount, cif)
  }
  formatNumberLoan(number)
  {
    return number != null ? parseFloat('' + number + '').toLocaleString() : '0.00';
  }
}
