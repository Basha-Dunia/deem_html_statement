import { Component, OnInit } from '@angular/core';
import { TermsService } from 'src/app/services/cms-terms/cms-terms.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css'],
 
})
export class TermsComponent implements OnInit {
  isLoading=false
  english:any=null;
  arabic:any=null;
  public markdownArabicString: string;
  public markdownEnglishString: string;
  constructor(private cmsTermsSevice:TermsService) {
     
     this.cmsTermsSevice.getTerms().subscribe((data:any)=>{
      this.markdownEnglishString = data.English;
      this.markdownArabicString = data.Arabic;
      
      // this.english=  this.md.convertMarkedDownToHtml(data.English) ;
      // this.arabic=this.md.convertMarkedDownToHtml(data.Arabic) ;
    
    });
   }

  async ngOnInit() {
   
  }
 
}
