import { Component, OnInit,  } from '@angular/core';

import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import { LoaderService } from 'src/app/helper-services/loader/loader.service';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.css','./home-layout.component.scss'],
 
})
export class HomeLayoutComponent implements OnInit {
 
  // showLoadingIndicator = true;
  options: FormGroup;
  isMobileScren;
  constructor(   public loaderService: LoaderService,private breakpointObserver:BreakpointObserver) {

    breakpointObserver.observe([
      '(max-width: 768px)'
        ]).subscribe(result => {
          if (result.matches) {
            this.isMobileScren=true;
          } else {
            this.isMobileScren=false;
          }
        });
  }
  ngOnInit():void{
    
  }
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));
 

}
