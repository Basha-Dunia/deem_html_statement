import { Component, OnInit } from '@angular/core';
import { CreditCardTransaction } from '../../models/credit-card-transaction';
import { TransactionDetails } from '../../models/transaction-details';
import pdfMake from "pdfmake/build/pdfmake";
//import pdfFonts from "pdfmake/build/vfs_fonts";
import pdfFonts from '../../../assets/Fonts/vfs_fonts';
import { from } from 'rxjs';
import { blue } from '@angular-devkit/core/src/terminal';



// pdfMake.fonts = {

//   // download default Roboto font from cdnjs.com
//   Moderat: {
//     normal: '../../../assets/Fonts/Moderat-Regular.ttf',
//     bold: '../../../assets/Fonts/Moderat-Regular.ttf',
//     italics: '../../../assets/Fonts/Moderat-Regular.ttf',
//     bolditalics: '../../../assets/Fonts//Moderat-Regular.ttf'
//   },

// }

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css', './test.component.scss']
})
export class TestComponent implements OnInit {
  creditCardTransaction = new CreditCardTransaction();
  transactions: TransactionDetails[] = [];

  constructor() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    pdfMake.fonts = {
    
   
      // Roboto: {
      //   normal: 'Roboto-Regular.ttf',
      //   bold: 'Roboto-Medium.ttf',
      //   italics: 'Roboto-Italic.ttf',
      //   bolditalics: 'Roboto-MediumItalic.ttf'
      // },
      // Arial:{
      //   normal: 'Arial-Unicode-Regular.ttf',
      //   bold: 'Arial-Unicode-Regular.ttf',
      //    italics: 'Arial-Unicode-Regular.ttf',
      //   bolditalics: 'Arial-Unicode-Regular.ttf'
      // },
      Harmattan:{
        normal: 'Harmattan-Regular.ttf',
        bold: 'Harmattan-Regular.ttf',
         italics: 'Harmattan-Regular.ttf',
        bolditalics: 'Harmattan-Regular.ttf'

      }
      //   Moderat: {
      //   normal: 'Moderat-Regular.ttf',
      //   bold: 'Moderat-Regular.ttf',
      //    italics: 'Moderat-Regular.ttf',
      //   bolditalics: 'Moderat-Regular.ttf'
       
      // },
      // NotoSans: {
      //   normal:'NotoSans-Regular.ttf',
      //   bold: 'NotoSans-Bold.ttf',
      //    italics: 'NotoSans-Italic.ttf',
      //   bolditalics: 'NotoSans-BoldItalic.ttf'
      // }

    // },
    //   Courier: {
    //     normal: 'Courier',
    //     bold: 'Courier-Bold',
    //     italics: 'Courier-Oblique',
    //     bolditalics: 'Courier-BoldOblique'
    //   },
    //   Helvetica: {
    //     normal: 'Helvetica-Regular.ttf',
    //     bold: 'Helvetica-Bold.ttf',
    //     italics: 'Helvetica-Oblique.ttf',
    //     bolditalics: 'Helvetica-BoldOblique.ttf'
    //   },
    //   Times: {
    //     normal: 'Times-Roman',
    //     bold: 'Times-Bold',
    //     italics: 'Times-Italic',
    //     bolditalics: 'Times-BoldItalic'
    //   },
    //   Symbol: {
    //     normal: 'Symbol'
    //   },
    //   ZapfDingbats: {
    //     normal: 'ZapfDingbats'
    //   }
    };
   }

  ngOnInit(): void {



    ///
    this.transactions = [{
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 600.00",
      amount: "600.00 C"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 14.00",
      amount: "	14.00 D"


    },
    {
      transactionDate: "04/08/2020",
      postingDate: "04/08/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "12/08/2020",
      postingDate: "13/08/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 4130.00",
      amount: "4130.00 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "BILLED FINANCE CHARGES",
      amountOriginalCurrency: "AED 393.26",
      amount: "393.26 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 88.36",
      amount: "88.36 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "VAT ON DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 4.42",
      amount: "4.42 D"


    },
    {
      transactionDate: "16/07/2020",
      postingDate: "16/07/2020",
      description: "RETAIL PURCHASE NATIONAL TAXI",
      amountOriginalCurrency: "AED 16.00",
      amount: "16.00 D"


    },
    {
      transactionDate: "18/07/2020",
      postingDate: "18/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 16.50",
      amount: "16.50 D"


    },
    {
      transactionDate: "19/07/2020",
      postingDate: "19/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 1900.00",
      amount: "1900.00 C"


    },
    {
      transactionDate: "25/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE GOVT OF SHARJAH",
      amountOriginalCurrency: "AED 740.98",
      amount: "740.98 D"


    },
    {
      transactionDate: "26/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE NEEL WA FURAT.COM",
      amountOriginalCurrency: "USD 18.00",
      amount: "68.10 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE ARABIA TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "VAT ON SERVICE CHARGE DEBIT NEEL WA FURAT.COM",
      amountOriginalCurrency: "AED 0.10",
      amount: "0.10 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.50",
      amount: "12.50 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE Etisalat Telecom Corp",
      amountOriginalCurrency: "AED 340.39",
      amount: "340.39 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 3494.75",
      amount: "3494.75 C"


    },
    {
      transactionDate: "30/07/2020",
      postingDate: "31/07/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 500.00",
      amount: "500.00 D"


    },
    {
      transactionDate: "31/07/2020",
      postingDate: "01/08/2020",
      description: "RETAIL PURCHASE WARDA AL MADINA SMKT",
      amountOriginalCurrency: "AED 63.80",
      amount: "63.80 D"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 600.00",
      amount: "600.00 C"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 14.00",
      amount: "	14.00 D"


    },
    {
      transactionDate: "04/08/2020",
      postingDate: "04/08/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "12/08/2020",
      postingDate: "13/08/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 4130.00",
      amount: "4130.00 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "BILLED FINANCE CHARGES",
      amountOriginalCurrency: "AED 393.26",
      amount: "393.26 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 88.36",
      amount: "88.36 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "VAT ON DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 4.42",
      amount: "4.42 D"


    },
    {
      transactionDate: "16/07/2020",
      postingDate: "16/07/2020",
      description: "RETAIL PURCHASE NATIONAL TAXI",
      amountOriginalCurrency: "AED 16.00",
      amount: "16.00 D"


    },
    {
      transactionDate: "18/07/2020",
      postingDate: "18/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 16.50",
      amount: "16.50 D"


    },
    {
      transactionDate: "19/07/2020",
      postingDate: "19/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 1900.00",
      amount: "1900.00 C"


    },
    {
      transactionDate: "25/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE GOVT OF SHARJAH",
      amountOriginalCurrency: "AED 740.98",
      amount: "740.98 D"


    },
    {
      transactionDate: "26/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE NEEL WA FURAT.COM",
      amountOriginalCurrency: "USD 18.00",
      amount: "68.10 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE ARABIA TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "VAT ON SERVICE CHARGE DEBIT NEEL WA FURAT.COM",
      amountOriginalCurrency: "AED 0.10",
      amount: "0.10 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.50",
      amount: "12.50 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE Etisalat Telecom Corp",
      amountOriginalCurrency: "AED 340.39",
      amount: "340.39 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 3494.75",
      amount: "3494.75 C"


    },
    {
      transactionDate: "30/07/2020",
      postingDate: "31/07/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 500.00",
      amount: "500.00 D"


    },
    {
      transactionDate: "31/07/2020",
      postingDate: "01/08/2020",
      description: "RETAIL PURCHASE WARDA AL MADINA SMKT",
      amountOriginalCurrency: "AED 63.80",
      amount: "63.80 D"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 600.00",
      amount: "600.00 C"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 14.00",
      amount: "	14.00 D"


    },
    {
      transactionDate: "04/08/2020",
      postingDate: "04/08/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "12/08/2020",
      postingDate: "13/08/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 4130.00",
      amount: "4130.00 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "BILLED FINANCE CHARGES",
      amountOriginalCurrency: "AED 393.26",
      amount: "393.26 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 88.36",
      amount: "88.36 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "VAT ON DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 4.42",
      amount: "4.42 D"


    },
    {
      transactionDate: "16/07/2020",
      postingDate: "16/07/2020",
      description: "RETAIL PURCHASE NATIONAL TAXI",
      amountOriginalCurrency: "AED 16.00",
      amount: "16.00 D"


    },
    {
      transactionDate: "18/07/2020",
      postingDate: "18/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 16.50",
      amount: "16.50 D"


    },
    {
      transactionDate: "19/07/2020",
      postingDate: "19/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 1900.00",
      amount: "1900.00 C"


    },
    {
      transactionDate: "25/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE GOVT OF SHARJAH",
      amountOriginalCurrency: "AED 740.98",
      amount: "740.98 D"


    },
    {
      transactionDate: "26/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE NEEL WA FURAT.COM",
      amountOriginalCurrency: "USD 18.00",
      amount: "68.10 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE ARABIA TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "VAT ON SERVICE CHARGE DEBIT NEEL WA FURAT.COM",
      amountOriginalCurrency: "AED 0.10",
      amount: "0.10 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.50",
      amount: "12.50 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE Etisalat Telecom Corp",
      amountOriginalCurrency: "AED 340.39",
      amount: "340.39 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 3494.75",
      amount: "3494.75 C"


    },
    {
      transactionDate: "30/07/2020",
      postingDate: "31/07/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 500.00",
      amount: "500.00 D"


    },
    {
      transactionDate: "31/07/2020",
      postingDate: "01/08/2020",
      description: "RETAIL PURCHASE WARDA AL MADINA SMKT",
      amountOriginalCurrency: "AED 63.80",
      amount: "63.80 D"


    },
    ]
    this.creditCardTransaction.transactions = this.transactions;

  }
  public getBrowserName() {
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        return 'edge';
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        return 'opera';
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        return 'chrome';
      case agent.indexOf('trident') > -1:
        return 'ie';
      case agent.indexOf('firefox') > -1:
        return 'firefox';
      case agent.indexOf('safari') > -1:
        return 'safari';
      default:
        return 'other';
    }
  }
  generatePDF(action = 'open') {
    
    let ariLines = 11; let hotels=9; let ent=28; let others=52;
    let b = 100;
    var svg = `<svg width="85" height="85" viewBox="0 0 36 36" style="margin: 10px auto;max-width: 80%;max-height: 200px;stroke: #6E3DEA;">
    <path style="fill: none;stroke: #27272E;stroke-width: 2.0;"
      d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
    />
    <path style="fill: none;stroke-width: 2.0;stroke-linecap:square;"
      stroke-dasharray="`+ '' + ariLines + '' + ',' + '' + b + '' + `"
      d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
    />
    <text x="18" y="20.35" style="fill: #27272E;font-family: sans-serif;font-size: 0.5em;text-anchor: middle;">`+
      '' + ariLines + '' + `%</text></svg>`;

      var svgHotels = `<svg width="85" height="85" viewBox="0 0 36 36" style="margin: 10px auto;max-width: 80%;max-height: 200px;stroke: #6E3DEA;">
      <path style="fill: none;stroke: #27272E;stroke-width: 2.0;"
        d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path style="fill: none;stroke-width: 2.0;stroke-linecap:square;"
        stroke-dasharray="`+ '' + hotels + '' + ',' + '' + b + '' + `"
        d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" style="fill: #27272E;font-family: sans-serif;font-size: 0.5em;text-anchor: middle;">`+
        '' + hotels + '' + `%</text></svg>`;

        var svgEnt = `<svg width="85" height="85" viewBox="0 0 36 36" style="margin: 10px auto;max-width: 80%;max-height: 200px;stroke: #6E3DEA;">
        <path style="fill: none;stroke: #27272E;stroke-width: 2.0;"
          d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
        />
        <path style="fill: none;stroke-width: 2.0;stroke-linecap:square;"
          stroke-dasharray="`+ '' + ent + '' + ',' + '' + b + '' + `"
          d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
        />
        <text x="18" y="20.35" style="fill: #27272E;font-family: sans-serif;font-size: 0.5em;text-anchor: middle;">`+
          '' + ent + '' + `%</text></svg>`;

          var svgOthers = `<svg width="85" height="85" viewBox="0 0 36 36" style="margin: 10px auto;max-width: 80%;max-height: 200px;stroke: #6E3DEA;">
          <path style="fill: none;stroke: #27272E;stroke-width: 2.0;"
            d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
          />
          <path style="fill: none;stroke-width: 2.0;stroke-linecap:square;"
            stroke-dasharray="`+ '' + others + '' + ',' + '' + b + '' + `"
            d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
          />
          <text x="18" y="20.35" style="fill: #27272E;font-family: sans-serif;font-size: 0.5em;text-anchor: middle;">`+
            '' + others + '' + `%</text></svg>`;
    let docDefinition = {
      pageMargins: [40, 220, 40, 70],
      footer: function (currentPage, pageCount) {
        return [

          {
            text: [{
              text: 'Page ' + '' + currentPage.toString() + '' + ' of ' + pageCount,
              alignment: 'center',
              fontSize: 8,
             marginTop:20

            }],

          },
          {
            canvas: [{ type: 'line', x1: 40, y1: 10, x2: 560, y2: 10, lineWidth: 0.1, color: '#6E3DEA' },


            ], marginBottom: 10
          },
          {
            text: [{
              text: 'Deem Fiance L.L.C | P.O.Box 44005 | Abu Dhabi, UAE  www.deem.io | 800 3366 | customercare@deem.io TRN: 100038451900003',
              fontSize: 8,
              alignment: 'center',

            }]
          },


        ]
       
      },

      header: function (currentPage, pageCount, pageSize) {
        // you can apply any logic and return any valid pdfmake element
        if (currentPage === 1) {
          return [
            {
             
              image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApsAAAEPCAYAAAANjQ+sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACXASURBVHhe7d2PleS2scXhF4JDUAgOwSEoBIegEByCQnAICsEhKASFoAz87l0111wMuptkF8gq8HfP+c57hmZnSDT/FEGQ/X///e9/AQAAgCG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsI7KH8JP+UX633MwAA4J66jcAryj/kX/Kb/CluXPyn928AAMA9dRuBhbIetfxd1oVlD8UmAAD4rtuI+1I8avmLeNTyD1kXkltQbAIAgO+6jbgHxaOWP4tHLf8j66LxKIpNAADwXbcRc1L+Lp+MWm5BsQkAAL7rNqI+5W/iUUs/yBM1arkFxSYAAPiu24h6lGXU8t8yatRyC4pNAADwXbcR+SkeuTx71HILik0AAPBdtxH5KX5qfF3kZUGxCQAAvus2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKzUKUv4s/s63+3vs9uC/lb6vtY6u/9X4XsFDWx6Z/yr8aP8vy3zkuHbDqv8VPvZ+bWbdxRv5wHx/yL+Id6N/yn41+k2XH8++4fId7LIf/n2xuWWwq/jyWA7W3F283f0jbP0f8Kevt0H/Hf49CYiKKT/o+sa+PT79Luz0ctRzPfpXlWHa7k95d+bMWHzv8+Xs78HGl3Ua2Wo5J3k59Tv1H72/egbLeb48e+72fL/vmt/6UqY7v3cbqFH/43qmWA/b6Q43kDcR/w3/r1IO2QrF5EcV9vxxYogrKo/z3lyL0tgf8apT1MSqyoDxiKRy8DfmkyUXMBPw5Pj5Pb2NnHaeW7WjaEVBlOf6PrC0W6xqj9H7ZbazGH8Ljw/CH8snV2qe8Q/vK5OfeckZSKDZPorgwOOvgEsHL6atjbnkloSyjSr4wuPIYtZVPcqccyxDLn5n4XNh+pmfzdu7lKH0cUpb6IsO+uxzbyxWe3cYK3NmybADrDyOLoTuaQrE5kHL2iMAoywUQhefJFBeYPjFcPXL5KR/LfJwtP7oyK38u4m0t6/HKy1WqSFJ8jr16AOsV75NlLga7jZkpHmXKvAH0+GTzz976HKVQbAZTvG25MKu0be2xFJ7M0xtEWU761QvMZ5aL6JJTNhTv4z52ptFbzq0Ub2++61LlmOXlTHsMUpZBrEqDDF7W0PpihG5jRop3zCq3MZ/xjuYDw8dXd4r7o/39GZQrNhUfXGYtDp7xvsQt0iCK90cXYW0/z6ziaFW6c0i7jFspPm5VvjAOORdG8HI8lqdyf6YuOruNmSg+iM9WCHiD/mijUCg2P6DMcHCJUOKqOCv33aMP2369E+9DJUbMlfLFpuLR2eoDLwtvO7/01vMs/vuP5WiXrSofj9Ldeeg2ZuDOkll2qGcObxT+d4/fkU3qYlNxkVltGsYZKDp3cF89+qztx7vzvpW26FRKF5uKL5C//I4JeEDp1Hnlis+hM+/DntOZ5q5Dt/FK7hy52+2o3RuFQrG5g8JI5jYUnS+4bx591PYb/sf7WJpbpGtKyWJT8fFr9sEXGz7Kqbgvsz5YHM37YorpUt3Gq7hTHp2z7qy72LVRKBSbGymz3SY5g09sPMH+oHh/u9u83k95n0t14aKUKzYV3za/0/Fr2Iic4v34jueCf/f640zdxrMpd7rSeOfXXh+1FIrNNxQKhM95Ll66EaqzeN3lbndaoqW5cHksS7t8l2qXcU2p/hDQUeG31RUfy9q/cyfu08uO5d3GMyl3vdJ45e1GoVBsPqG4QLj7gSWSbxvf7tuJlDvfaRnhX71+PpNSpthUXGh++fkb8b73ccGp3GUKwhYhfXpEt/Esim9vrjsC//Nyo1AoNjsU33JiTt0Ym0bdq1O40zKOL6Qve4BIKVFsKncvNBcfFUf+t8LdrR9dUnB2G8+gcGvqPW8U3XmcCsVmQ5n1Sc1MLi0WRlO40zLe0+PaaEr6YlOh0PzRoeLI/+bxb9vfh7/65dS7Vd3GkRSPGnClsc+XSfYKxeaDwm2Sc11WLIykcLFyrtNHypXUxabiAunLz2Bfwemfffyb9vfgf3b16ae6jaMoFJrH/VBwKhSbonDb/DqXvow5isJt8+u4+DvtoYXH32uX4VKrZfN36VMgPee+ebutKBSa251WcHYbR1AoND/3veBUbl9sKjzAcb3LX6nxCYXj0vXc/6cUnErmYpPt8L3fl/7qUbw/c07Yx/01fGpUtzGawgE9zrd5Fv6/q7ZMTik2FeY15eH515e9UuMohRGQPE4ZYVFSFpsKb8/Yrjv9QqHOOG74BV+3MZJX4LEi6xXDcd8OynLbYlOh0Mxn+MEqkkKhmc/wglNJV2xK1mN5Zl8eblF46PgzQ+9SdRujKBSaY3iOom8h9/7b1YYWmwqFZl4lCk6FQjOvoQWnkrHYZM75fu6z78cahdcoxhj24Ge3MYrCpPtxsp4shxWbCoVmfqfN2T1CodDMb1jBqWQsNnHMt9vpCg9WxXE/Dpm/2W2MoDAH5Z6GFBsKhWYdKR8aUig06/hh5CqKQrE5FxeafKaxxpzDe42fUigM7it8Q1XYnupJ9W1DCqMf9YRPy1AoTObCPj1G+O30buMnFEYP7i202FS8PbV/AzV8+TKCKyjMHa/rt95nepRCsQm8F35nodv4CYWD+r2FFZsKo1H1nfLC4FcU5o7X9q/e53qEQrEJbBO231m38Sgv3GpBcU8hxabCaNQchsy920rhmDSHkO9xVig2gW080BN27O42HqFwuxMWVWzyzrR5hN4K3UrJ+now7Bdy4lMoNoHt4u4q9BqPUBiFgn1cbCoUCfM59XvUFY+MMwVjLhHHFopNYLuw0c1u414Kt6qw+OiEoMw2T9Pr4hOcXwXm/cT8dL2/NcSWNvPcwlkv2twPw79/d6HcoahYti3fBbjLtvXRRYtCsQnsEzJQ0G3cQ+EhDqx9WmxWPxl4jqJP/h6dPVxcKS4W/K0YMz3cEjLF4h1l1m8T8XHW25YLyk+2LU95qrptfXTRolBsAvv80duX9uo27qEwtw5rhwsKpfL7NL0fhDzE0FJ8S9h9M8OJcujrkJQZL36/Xbz01vdTSsVt6/AcYIViE9jv4+NPt3ErhYeC0DpUbCoV59h5eX2L8rSnrRXvc5Uv8Nxnw/pLmWUk+Ipty4V6lW3r0IWdQrEJ7Pfxt8J1G7dS2HHROlpsVvt6Uy/vaYVAS3FhUHX/G/LtQoqnHrR/qyIXfFdvW9mL9kO39hTOWcAxHx2Tuo1bKLMc2BFrd7GpVBoh90MWl7+ofKF4P/Q80XY5swt/WEip2A9r3raGTMU4wssimft094MLyuzFpkfEvY7Lg2H+DNc8V3d5YKz6/nIm99WzfvU0FLf5InHmt/J8NAWq27iFMsNO6w3IO503FD/Q4Q3n6Unw8d/NO6xHtrhK/upIsVmlH1N93/dC8RSEarfWP74ts6ZUnu9rl45mPuNleixbu7wZ7J6Socx4zHaB43PS7otgxaPY/rczF0lHLf2668JYWeZBVzsmv/PR+5K7je8o3kDXC1GJi8vdG9ArigtVF59cKe4sNhUX7+3vyCjF93y/4mVcLW8FIfug4oN75X2vyrbl4q5d9qvteum0MlOx6XUJGwlXKs3ZHcl9EHls8mBWxn1nrz9767hVt/EdpdoG6RORC8zhIweKi6c777B7i83sB38fJNLcNn/Hy/pY5nY9MgoZ3VS8b7e/uwJ/TkOeMh9BybhteXk2H9eVGYrNodMtFH/Odxzp9LYRNgi1psxSdB7e7rqNryjutPUfz8w7zCWjBspdrxI3F5tK9lFNHxjKFJoLL/Nj2dv1yejjg7tScVSTbSvO5mO8Ur3YDPv6wHf8t1Z/d2benk+56FNcF1Qu5A9vf93GV5QKowg++aS4NaXcrejcU2xmfuK1ZDGw8LI/1qFdr2w+mgerVJ2rmeZBoL2UbNvW5ifTlarF5mkF0Zr/5uNvt8szCxd+px/nlWpvX1nsfiZj0W18Rck+inDpK2meUTyKV3le2VabNkYl+7zf9PPo3lFcFLTrlY1PZIf3V6XiPjXLtpWpCNlUiCkVi03382UXvv7bj2Vol6s6F5qX1QpKyQvldj226jY+o2Q+eXlnSD1aoHgKwuyjnFuLzcxXdqfdqhpNqXBAO1R8KVUeLlsLfQr/SkqmbWvTk7JKxWLz8jssSsV97ZVLC82FUrHgPLQ9dhufUbIWSik2nK2Uqrf+tthabGa9Uj58myArJfsFzu+95X5HqXbhdmg9M1MyXTS+nf+rVCs204yCK1UfxGv5bkiaekGpNjf20HSObuMzSsYCoeRIgTLrrYm3xZqStdj25zHkacQrKRVeDbSr35VKDyouys4BfkXJ8sDD25e8K5WKzXTnNiX7N0ttkW4/VCr166E7f93GHsUThdd/MIPSt6SUGQvOLcVm1h1rmtvnLSX7bbBdDwop1e4OzLxtZZle9XbkWKlSbKYafVt4maTyOWv3t06dQakwILA49HL3bmOPkm2O3RRzn5TZCs6XxaaSdUTq0HctV6Jkvu28q/+VLKNpW6QsHCIpWc4PL0etlCrFZtqHyJSqt9NTT2NRqsyLPTTVrNvYo2Squj/62qRslMwPXu31rtjMOiJV9lU0WynZRyU23d5Ssr/JoFX+6fN3lCzb1suRK6VCsXnoZH4mpcoo3Fr6Y7xS4Xb6oW8S6ja2lEwH91IPA22lzPLQ0LtiM+POlP7gHkXJPBl90y0u/9zq32Q3/Yj5Qsmwbb0cvVIqFJvpv1VKqXa+KnGMV0pcSLfLvUW3saVkObj7ynnKSfamzPBapKc7tZL1Fnr6g3sUJfPo5qbbXEqlyfTTj2oulCzb1tPBCCV7sVni4kTJfpekVebOlZK+DmiXeYtuY0vJcnCf+sCteAeueHti7VWxmfEhs9uMPC2UzAezt3ctmp/P7I7bVoa5m0/PE0r2YjPlAyw9SpXBkVKvHFMqzN3cXbx3G1tKhiuYW9zqVKq/PPdVsZnxRe5lDu5HKd6mfHfCJ4fsFzMvR5mVSvvHtE+gL5T1tpXloa2nD48q2YvNMq9eUzIOHvSUO8Yr2Y/T8cWmkmEOgYvd6d5/+IxS5Yqx51WxmfEJ4qnm/yp+2MzzqTKd/Pd4+QokpdILkKc6ZinLtuWLxszb1tMRZSVzsVlqBM6UCrfSy+2HSsaBmbUhxWaGicDTjxCsKdXmw6x1i03F69T+7NVKv9VA8YWgRxd8YKrw4MMWL+9g+L+vfjazcoXDmrJsWy7uK25b3YtIJfO67HrXbAZK9vnTJfdDJfsbaoYUm1dX2C66pnv6/B2l2ldYLZ4Vmxlvf5aZA6y4WHcfLif/qhcjb7Xrvtb+bGKV5t6tty0XDzNsW93pGErmYrPcg4pK9vNUuQJ+oWTeD4cUm1fvnLca1VwoVUc3nxWbGQ9KaS9ilPXJv/pDY3t13zjh9tXPZJf21p2yzLOcedvqnjeUzMVmxdu9GQcR1sq+aUTJvK0OKTavLHj8t283qrlQss/b6HlWbGa73ZLm9oriIirbQxZX6o44u331M5ll27Yqz+E9qjtFRsl6Aj/0ouyrKdnfC1l23rSSedQ4tthUrp5nN8VXUh6llHjBa+NZsZltBOWSEXPFn+nykEXmK9crPRuVyn7LbnHJrTtlxjm8R3UfElKy9svLucqZNeuRSruslSiZn/YPLzavHiKf9gXuWynVThrPis325662e2fZS/HF2vohixnmwp3h2TZUZV8YfutOuc0c3qPaPjOFYjOYknXEvPpDepmnKIQXm1fetrrdC5F7lCq3DhdfDppKxrl2odMz/PtkOfnfcZ5lpO5Jwu2rn8ks/Nadwra135fBCiVrsVn22QSFAn4A5eo7y6+EF5tX3rYq+xRZJCXzBtfTKzazXaF9fCGjeJ2YZzlI29/W/kxSH8+9U5jDG+PLCVGh2AymUGwO0qxPJuHFpg926z9wpuG3OatQsu7MPb1i88qLlp5d79dUlocsmAt3nh9GnpUq85d3neAU5vCO86WAUyg2gylZ+7T0e5StWZ9MwovNyzaidlnuTMlWrL1Sodh8emBXlocsvMze/pkLd40fDmb+36v/lhnbVh4UmydQ6NNBmvXJZJpis/zwdySlyonWesXmlSPkPd8e4FDaeZac/POoWmx+e22Twhze6315m4lCYRRMoU8HadYnk/Bic/3Lz1R+I4mkVJq32Ss2sx2MvDyc/HP74Rt4/L9X/y0zb1vMs8yhwrFoQbEZj2JznGmKzbJv/R9FqVIcVTrAI68fThT+36v/BmxBsXkChT4dpFmfTKYpNnevyOyUKgUbxSYiUGziUxSbJ1Do00Ga9clkjmKzXQ64W8qcbHsHeOZCYi+KTXzqy2uoFAqjYAp9OkizPpnEFZvKZa8aaZcF7pbSxWb7M8A7FJv42HobemxHFEbBFPp0kGZ9MgktNq96+rP0V0yNolT5JiGKTXzKD9h8e6p7tQ1RbGIPz3H/8p5FhcIomEKfDtKsTyZTFJtfihW4W8q8+oViE3t4ioVfDeRi8ukB7PHf238LLFzweBvxe0yffl3o4+faf5sBxWY8is1xKDZnpVBsYgYetfS35XikfvN3iCsUm1h41NLv7vXrsL58//krCoVRMIU+HaRZn0woNmelUGyimm+3MsVFwe6D05pCsXlf61HLH77GdK/H72p/fwYUm/EoNseh2JyVQrGJ7HzS8ajly1uZRygUm/fgke9Do5ZbKBRGwRT6dJBmfTKh2JyVUrnYzHowwnFho5ZbKBSb8/F8XR8b/Nn6+PbRqOUWj7/XLkcGFJvxKDbHCS02r/qKRIrNDoViE1daioLwUcstHn+7XSbUsoxaer5u+KjlFgqFUTCFPh2kWZ9M4opNa375Wf7oLcvdKWW+G7qz7BSbtRx+AGMUhWKzltNHLbd4LFO7rBlQbMaj2BxnimJTf7q/PHemVDnZUmzWsxQFHz+AMcpj2drlRh6H3jJwNoXCKJhCnw7SrE8mcxSbkvKEdyXFI01tP2XUKzZ9Emp/Dte4/FbmEUqVaSR3sOndqBkpFEbBFPp0kGZ9MgkvNq/6TutSB7AzKFl36Fav2OQW6DVS3so84rH87frhHN6G0o9abvFYl3b9MqDYjEexOU54sXnVRvRLb3nuTLmq8N+rV2z6JNX+HOKVHLXcSmnXF/FOfcvA2RQKo2AKfTpIsz6ZTFNs/ru3PHel/LTqm+x6xSajUvF+uJUp0089Udo+wOd8jB/ybtSMHuvb9kEGFJvxKDbHCS82r5pr93tvee5KqfRwRK/YrFQsZ1XiAYyRlKwntSqmHrXcQqEwCqbQp4M065NJeLF55Vw7HhJ6UCo9YPOl2HysQ/tzeK7sAxgjPfqk7Ss85yLA21Datwyc7dEnbT9lQLEZj2JznPBi88oRtX/2lumOFI9ItP2T1bNi0yNz7c/iLz5Y3+ZW5lHKlRe/2aV7N2pGCoVRMIU+HaRZn0zCi80r59oxb1OUv6/6pIJnxWaVVzeNdvtbmUcplaaTjOYTPKOWOz36re3LDCg241FsjhNbbFrzB870Z2957kapdAvdnhWbdx2V8oGYUcsA7j9p+/cOpn7LwJkUCqNgCn06SLM+mQwpNq+8hXv7W+lKpVvo9qzYvMMT6etbmYxaDqBUeQXYUdO8GzWjR9+2fZ4BxWY8is1xhhSbV25I3cLlLpSKBdqzYvNvq5+ZxVIUcCvzJI8+bz+Hyhi1PJFCYRRMoU8HadYnkyHF5tW3P297AFYqPn379AJBqfyQEA9gJKBUno7BWwYuplAYBVPo00Ga9clkSLF59aT8Wz4opFSdn/aq2Kwy/3R9K5NRy0SUSqP9t383ajYKhVEwhT4dpFmfTIYUmxmKntsdqJWq7xR8VWxefeHyjgsDRi2TW31eGXkEnFHLpBQKo2AKfTpIsz6ZxBebplz9kMqt5m4qlR+meVVsZp+3+VtvuZGLP6fVZ5aNR8UZCU9KoTAKptCngzTrk8mwYjPDOxJvM1qgVJ7b+PLCwP999bMZcbszOcXzZtvPLZPyJ7lZKRRGwRT6dJBmfTIZVmx6ztH6D13Bo6vTjxgo2U+k77wrNrOvH18mkJySfT4zo5tJKRRGwRT6dJBmfTIZVmxmObhPXQgo/rYgn6ja9a7kXbFZ4cGnW4yiK75jUfLpaCX76P8tRjcVXzy62CjxMN1jWdefUxYUm/EoNscZU2yakuXgPuWL3hXPZ6x8+3zxdn6tkn09365DdcqzecH+bNI/Qa1kHyH3RePUUzIUH7N6F8dpXxOmUBgFU+jTQZr1yWRosZnltTU+uE33xLCSYV5shC3FZoWpAr/0ln0WytaH/ry/pRv9VCqMkE990aLsOSekGP18LEe7bBlQbMaj2BxnaLHpW7zrP3alqUYNlCyFfIQtxWaFbxOadmRK8Um/Xd89Uox+KllPcmuz3on59HxwyeinQmEUTKFPB2nWJ5NxxaYpV78Cac0nvPKT8JUMD19F2jSao1QYyZ1uZEoZcdF4yeinUmHfme6iRRk15cdFiy9iPPo5pM8ef6P9uxlQbMaj2BxneLGZbQTOB7yyB3Klwu3kvbYWm1XeJfprb/krUp7NsRvhlNFP5az1+cQUF8YL5awLRQ9u+CLGx8mQixiFwiiYQp8O0qxPJsOLzYzzpHyyKTeHU5lljmZr82igkmmk/JXyt0KVqx9AGzL6+fh97d/KaIovDFCuHnD4aPTz8e/b35kBxWY8is1xxhabplx5wnrGJ7ISBYHik37WnTPCnmKz0hSCsgWncnWh+YyX6aMHsZQKDwotSr+6Tcm4vy6jn5tGjhUKo2AKfTpIsz6ZnFJsZi4QfMWb9naV4lvHFW77fWLXPEelyuimlSs4layF5uLjaQpKpbsEJQtOJfMI8h+9Ze5RKIyCKfTpIM36ZDK+2DQlc8Hk4iXsNl0ExSf8bPNdR9lbbFabt1qm4FQ86pe50LSPp8AolUY3rVTB6eVdLXtGmy9YFAqjYAp9OkizPpmcVmxWmCflHeDyh4cUF1Ozj2au7S02XYhXGt209A8NKZ7Tln272zwi9Y5SbQ60LwJSPzSkVLhYsc0XLAqFUTCFPh2kWZ9MTis2S82TklNHOhUXUC4yqxVREXYVm6ZUmru5SHEx01IqjaKHvThfqTa6ab4YSHUXZqFUuUj+vbf8zygURsEU+nSQZn0yOafYNKXiSIIPoCNfw+I5me6XO41ktnYXm6ZULMz9Oaf5piHFRXuVfnTfhY7sKVWeTG+lmWuu+D2sWYuHnl3TWhQKo2AKfTpIsz6ZnFpsVhxJWLjw/PgVLIr7wCf4uxeYa0eLzSrv3ezx9nTZCJX/tlQqECx83qLiUd2q++GlFy6Kj2XVBhB2T8NQKIyCKfTpIM36ZHJesWlKtYPTMy4WvMO4ADWPgPoEvvD8t+W/eZ39sxSXfYeKTVP8CpP291Xi7eLn3rpFU1xY+UKnwpy6niF3GJSKUzLWPDLt488pI52Kj29Vj+O7H9ZTKIyCKfTpIM36ZHJ6sVl5dBNjfFJsenuaoYh3weBbo+FfNqD4wqf6SPrQB6yUrCe/Pfz5+nMOv3hRfKvc22flOeWHHi5TKIyCKfTpIM36ZHJusWmKR/vWC4F7O1xsmuJRnfZ3VuYTuosGj7jt2kEVF9/LqPoMBZS5iBo6aqfMctGy8Lp8//Yl2dx//tnHv/F+5d8xS78cKsIVCqNgCn06SLM+mVxSbFaeJ4V4HxWb5t+x+n0z8v7idXym6q3xLU6Zl+i/s/qbs+ptO2u9fzODT+6eZO0Xis14FJvjnF9smuLRl/WC4L4iis3ZRqbwl4+3jT3891Z/G3PwceHwfF+FwiiYQp8O0qxPJtcUm6ZwYIeFFBQKFzBzcZEQPof1FYW7LvP59Lv0KYyCKfTpIM36ZHJpscloFCxs9Eqp+pQsvrrktT6K5yu2y4KaIu6aUBgFU+jTQZr1yeS6YtOUO8yTwmuht0qVmecv3sVvvc/2LAoPMdbngYyPHyxTKIyCKfTpIM36ZHJtsWlK9Xcl4jPRxSa3QmvzxcLl346jMEpe2+6TW49CYRRMoU8HadYnkxTFpouDyu9vw2dCi01T/F5ACs56/JmdOk/zGcXHJUbJa9r98vZnFAqjYAp9OkizPplcX2ya4uJgvWC4j/Bi0xTm3tWTotBcKBSc9YR+AYBCYRRMoU8HadYnkxzFpinVvzYOxwwpNk1hm6ojbDQqksK0jDpGfH8+hVEwhT4dpFmfTPIUm6b4K9HWC4g4PmFmnK4wrNg0hYIzv5SF5kJhWkZ+4YWmKRRGwRT6dJBmfTLJVWyawsT8Mfweyow7+dBi0xQKzrxSF5oLxQUnc8tzGlJomkJhFEyhTwdp1ieTfMWmKVk3xqq+ndCVWxabprjgZHQqlxKF5kJhDmc+wwpNUyiMgin06SDN+mSSttjkoB7n+8FYuW2xaQq3Q3PwZ5DqYaCtFI5NeQwvDhQKo2AKfTpIsz6Z5Cw2TeGg/rkfrvqVWxebprjgZLu6jvu+ZKG5pjDd5zq+WDllVFyhMAqm0KeDNOuTSd5i0xQXnBzUj/lye0m5fbFpircrvkzgfO7zy1/YHkXhG9DO53mzp12sKBRGwRT6dJBmfTLJXWwuFArOfbrzmBSKzRWFryU8zyXfdT6a4ve5MjXjHKdfrCgURsEU+nSQZn0yqVFsmsIowjZPby8pFJsNxcUCTxmPM8Vt81cUj5RnPYHOwMX8JRcrCoVRMIU+HaRZn0zqFJvmBRZGEfrcLz/3+m2hUGx2KNxWH8PvzZ3mtvk7ii+IOT7F8jHrp15/n+Hx99tlyoBiMx7F5ji1ik1TfhIe8PjRptEjhWLzBcXvImWU83PTj2Y+o/j4lPVkWomL9stfjaVQGAVT6NNBmvXJpF6xuVCYb/cXz2fdNHqkUGy+oXiUk23rmBQFQgYKFy7HpRkRVyiMgin06SDN+mRSt9g05c6vsXl727ylUGxupHiEilvr23hbdIF+m1vmW7g/Hv3CrfVtfHy67JZ5z2OZ2uXMgGIzHsXmOLWLzYVyt7lSm0cz1xSKzZ0UzxPOenC8mve5Q9vinSi+cOGNGs95/9p9MjrDY9na5c2AYjMexeY4cxSbpngUwbdf1is4G4/iHj4oKxSbBykUnf/DSOYBylJ0MtL5F/dFyiJzoVAYBVPo00Ga9clknmJzocw4iuC5Xx/PhVMoNj+keOrGXUepvm2HQpH5AfefuFi/45xOF9oeFEh1u/wZhcIomEKfDtKsTybzFZsLxUWnD2qVRxFCisyF4v7wjp7Jr71lzU5xweDpG7MXDMut8ls+XT6a4uL9DnODvY7lHh5TMh4zreyDeAp9OkizPpnsPn90GzNTXBT4gF7pQSIfmHc9/IPrKB7t9AF0lsLTBWbJ4qAqxRfHvniZ6YFHr4vXqcQoJoA8uo1VKJmLAg7ME1CWbaxa0eB9wiOYXORcTFkKz2ojnt8vUoTjGIDDuo0V+WAoywH9qlvt/tsUmJNSPKru9y1mLD69PC4uKQySU/xwmud4+nZU+zleaSkufQxjmgWAMN3GGfhg+Tho+gQ84qC+nNz9N1I/gYlx/Nk/toFl3tLoCx3/fv+d79ue8IBPYYqPVb5IWArQMy5k/HdcWPpv+gKKCxQAw3QbZ6V4ZMon52VkYbEUCmvLgXjhk4H/HQdlvKW4gPD2shQRi3Y761n//LLdMdJ0M4rv1vSOV8sF9Cs+pi0/v1yUGBcmAE7XbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAA4HP//b//B3B+JA7XYURBAAAAAElFTkSuQmCC',
              alignment: 'left',
              width: 100,
              height: 40,
              marginTop: 40,
              marginLeft: 40,
              marginBottom: 20,
            },

            {
              columns: [
                [
                  {
                    text: 'Matheus Leite',
                    fontSize: 12,
                    bold: true,

                  },
                  { text: 'JLT - Cluster L, Icon Tower 2, 1601', fontSize: 10, },
                  { text: 'matheusleite@gmail.com', fontSize: 10, },
                  { text: '+971 55 545 216', fontSize: 10, },
                  { text: 'CIF Number: 1234567890',fontSize: 10, marginBottom: 10 }
                ],
                [
                  {
                    text: `Date: ${new Date().toLocaleString()}`,
                    alignment: 'right',
                    fontSize: 10,
                  },
                  // { 
                  //   text: `Card No : ${((Math.random() *1000).toFixed(0))}`,
                  //   alignment: 'right'
                  // }
                ]
              ],
              marginLeft: 40,
              marginRight: 40,
              margingBottom: 20
            },
            {
              columns: [
                [
                  {
                    text: '',
                    margingTop: 10,

                  },
                  {
                    text: 'Statement Date:',
                    fontSize: 10,
                    marginBottom: 10


                  },
                  { text: 'Statement Period:', fontSize: 8, marginBottom: 10 },

                ],
                [
                  {
                    text: `15/12/2020`,
                    alignment: 'center',
                    fontSize: 10,
                    marginBottom: 10
                  },
                  {
                    text: `16/12/2020 - 15/12/2020`,
                    alignment: 'center',
                    fontSize: 10,
                    marginBottom: 10
                  },


                  // { 
                  //   text: `Card No : ${((Math.random() *1000).toFixed(0))}`,
                  //   alignment: 'right'
                  // }
                ],
                [
                  {
                    text: 'باسحلا فشك خيرات',
                    alignment: 'right',
                    fontSize: 10,
                    marginBottom:10,
                    // style:'langA'

                  },
                  {
                    text: 'باسحلا فشك خيرات',
                    alignment: 'right',
                    fontSize: 10,
                    marginBottom: 10
                    // style:'langA'

                  },
                ]

              ],
              marginLeft: 40,
              marginRight: 40,

            },
            
            { canvas: [{ type: 'line', x1: 40, y1: 10, x2: 560, y2: 10, lineWidth: 0.1, }] },

          ]

        }
        else {
          return [
            {
              
              image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApsAAAEPCAYAAAANjQ+sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACXASURBVHhe7d2PleS2scXhF4JDUAgOwSEoBIegEByCQnAICsEhKASFoAz87l0111wMuptkF8gq8HfP+c57hmZnSDT/FEGQ/X///e9/AQAAgCG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsIAAAAROg2AgAAABG6jQAAAECEbiMAAAAQodsI7KH8JP+UX633MwAA4J66jcAryj/kX/Kb/CluXPyn928AAMA9dRuBhbIetfxd1oVlD8UmAAD4rtuI+1I8avmLeNTyD1kXkltQbAIAgO+6jbgHxaOWP4tHLf8j66LxKIpNAADwXbcRc1L+Lp+MWm5BsQkAAL7rNqI+5W/iUUs/yBM1arkFxSYAAPiu24h6lGXU8t8yatRyC4pNAADwXbcR+SkeuTx71HILik0AAPBdtxH5KX5qfF3kZUGxCQAAvus2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKTQAAkF63EfkpFJsAACC9biPyUyg2AQBAet1G5KdQbAIAgPS6jchPodgEAADpdRuRn0KxCQAA0us2Ij+FYhMAAKTXbUR+CsUmAABIr9uI/BSKzUKUv4s/s63+3vs9uC/lb6vtY6u/9X4XsFDWx6Z/yr8aP8vy3zkuHbDqv8VPvZ+bWbdxRv5wHx/yL+Id6N/yn41+k2XH8++4fId7LIf/n2xuWWwq/jyWA7W3F283f0jbP0f8Kevt0H/Hf49CYiKKT/o+sa+PT79Luz0ctRzPfpXlWHa7k95d+bMWHzv8+Xs78HGl3Ua2Wo5J3k59Tv1H72/egbLeb48e+72fL/vmt/6UqY7v3cbqFH/43qmWA/b6Q43kDcR/w3/r1IO2QrF5EcV9vxxYogrKo/z3lyL0tgf8apT1MSqyoDxiKRy8DfmkyUXMBPw5Pj5Pb2NnHaeW7WjaEVBlOf6PrC0W6xqj9H7ZbazGH8Ljw/CH8snV2qe8Q/vK5OfeckZSKDZPorgwOOvgEsHL6atjbnkloSyjSr4wuPIYtZVPcqccyxDLn5n4XNh+pmfzdu7lKH0cUpb6IsO+uxzbyxWe3cYK3NmybADrDyOLoTuaQrE5kHL2iMAoywUQhefJFBeYPjFcPXL5KR/LfJwtP7oyK38u4m0t6/HKy1WqSFJ8jr16AOsV75NlLga7jZkpHmXKvAH0+GTzz976HKVQbAZTvG25MKu0be2xFJ7M0xtEWU761QvMZ5aL6JJTNhTv4z52ptFbzq0Ub2++61LlmOXlTHsMUpZBrEqDDF7W0PpihG5jRop3zCq3MZ/xjuYDw8dXd4r7o/39GZQrNhUfXGYtDp7xvsQt0iCK90cXYW0/z6ziaFW6c0i7jFspPm5VvjAOORdG8HI8lqdyf6YuOruNmSg+iM9WCHiD/mijUCg2P6DMcHCJUOKqOCv33aMP2369E+9DJUbMlfLFpuLR2eoDLwtvO7/01vMs/vuP5WiXrSofj9Ldeeg2ZuDOkll2qGcObxT+d4/fkU3qYlNxkVltGsYZKDp3cF89+qztx7vzvpW26FRKF5uKL5C//I4JeEDp1Hnlis+hM+/DntOZ5q5Dt/FK7hy52+2o3RuFQrG5g8JI5jYUnS+4bx591PYb/sf7WJpbpGtKyWJT8fFr9sEXGz7Kqbgvsz5YHM37YorpUt3Gq7hTHp2z7qy72LVRKBSbGymz3SY5g09sPMH+oHh/u9u83k95n0t14aKUKzYV3za/0/Fr2Iic4v34jueCf/f640zdxrMpd7rSeOfXXh+1FIrNNxQKhM95Ll66EaqzeN3lbndaoqW5cHksS7t8l2qXcU2p/hDQUeG31RUfy9q/cyfu08uO5d3GMyl3vdJ45e1GoVBsPqG4QLj7gSWSbxvf7tuJlDvfaRnhX71+PpNSpthUXGh++fkb8b73ccGp3GUKwhYhfXpEt/Esim9vrjsC//Nyo1AoNjsU33JiTt0Ym0bdq1O40zKOL6Qve4BIKVFsKncvNBcfFUf+t8LdrR9dUnB2G8+gcGvqPW8U3XmcCsVmQ5n1Sc1MLi0WRlO40zLe0+PaaEr6YlOh0PzRoeLI/+bxb9vfh7/65dS7Vd3GkRSPGnClsc+XSfYKxeaDwm2Sc11WLIykcLFyrtNHypXUxabiAunLz2Bfwemfffyb9vfgf3b16ae6jaMoFJrH/VBwKhSbonDb/DqXvow5isJt8+u4+DvtoYXH32uX4VKrZfN36VMgPee+ebutKBSa251WcHYbR1AoND/3veBUbl9sKjzAcb3LX6nxCYXj0vXc/6cUnErmYpPt8L3fl/7qUbw/c07Yx/01fGpUtzGawgE9zrd5Fv6/q7ZMTik2FeY15eH515e9UuMohRGQPE4ZYVFSFpsKb8/Yrjv9QqHOOG74BV+3MZJX4LEi6xXDcd8OynLbYlOh0Mxn+MEqkkKhmc/wglNJV2xK1mN5Zl8eblF46PgzQ+9SdRujKBSaY3iOom8h9/7b1YYWmwqFZl4lCk6FQjOvoQWnkrHYZM75fu6z78cahdcoxhj24Ge3MYrCpPtxsp4shxWbCoVmfqfN2T1CodDMb1jBqWQsNnHMt9vpCg9WxXE/Dpm/2W2MoDAH5Z6GFBsKhWYdKR8aUig06/hh5CqKQrE5FxeafKaxxpzDe42fUigM7it8Q1XYnupJ9W1DCqMf9YRPy1AoTObCPj1G+O30buMnFEYP7i202FS8PbV/AzV8+TKCKyjMHa/rt95nepRCsQm8F35nodv4CYWD+r2FFZsKo1H1nfLC4FcU5o7X9q/e53qEQrEJbBO231m38Sgv3GpBcU8hxabCaNQchsy920rhmDSHkO9xVig2gW080BN27O42HqFwuxMWVWzyzrR5hN4K3UrJ+now7Bdy4lMoNoHt4u4q9BqPUBiFgn1cbCoUCfM59XvUFY+MMwVjLhHHFopNYLuw0c1u414Kt6qw+OiEoMw2T9Pr4hOcXwXm/cT8dL2/NcSWNvPcwlkv2twPw79/d6HcoahYti3fBbjLtvXRRYtCsQnsEzJQ0G3cQ+EhDqx9WmxWPxl4jqJP/h6dPVxcKS4W/K0YMz3cEjLF4h1l1m8T8XHW25YLyk+2LU95qrptfXTRolBsAvv80duX9uo27qEwtw5rhwsKpfL7NL0fhDzE0FJ8S9h9M8OJcujrkJQZL36/Xbz01vdTSsVt6/AcYIViE9jv4+NPt3ErhYeC0DpUbCoV59h5eX2L8rSnrRXvc5Uv8Nxnw/pLmWUk+Ipty4V6lW3r0IWdQrEJ7Pfxt8J1G7dS2HHROlpsVvt6Uy/vaYVAS3FhUHX/G/LtQoqnHrR/qyIXfFdvW9mL9kO39hTOWcAxHx2Tuo1bKLMc2BFrd7GpVBoh90MWl7+ofKF4P/Q80XY5swt/WEip2A9r3raGTMU4wssimft094MLyuzFpkfEvY7Lg2H+DNc8V3d5YKz6/nIm99WzfvU0FLf5InHmt/J8NAWq27iFMsNO6w3IO503FD/Q4Q3n6Unw8d/NO6xHtrhK/upIsVmlH1N93/dC8RSEarfWP74ts6ZUnu9rl45mPuNleixbu7wZ7J6Socx4zHaB43PS7otgxaPY/rczF0lHLf2668JYWeZBVzsmv/PR+5K7je8o3kDXC1GJi8vdG9ArigtVF59cKe4sNhUX7+3vyCjF93y/4mVcLW8FIfug4oN75X2vyrbl4q5d9qvteum0MlOx6XUJGwlXKs3ZHcl9EHls8mBWxn1nrz9767hVt/EdpdoG6RORC8zhIweKi6c777B7i83sB38fJNLcNn/Hy/pY5nY9MgoZ3VS8b7e/uwJ/TkOeMh9BybhteXk2H9eVGYrNodMtFH/Odxzp9LYRNgi1psxSdB7e7rqNryjutPUfz8w7zCWjBspdrxI3F5tK9lFNHxjKFJoLL/Nj2dv1yejjg7tScVSTbSvO5mO8Ur3YDPv6wHf8t1Z/d2benk+56FNcF1Qu5A9vf93GV5QKowg++aS4NaXcrejcU2xmfuK1ZDGw8LI/1qFdr2w+mgerVJ2rmeZBoL2UbNvW5ifTlarF5mkF0Zr/5uNvt8szCxd+px/nlWpvX1nsfiZj0W18Rck+inDpK2meUTyKV3le2VabNkYl+7zf9PPo3lFcFLTrlY1PZIf3V6XiPjXLtpWpCNlUiCkVi03382UXvv7bj2Vol6s6F5qX1QpKyQvldj226jY+o2Q+eXlnSD1aoHgKwuyjnFuLzcxXdqfdqhpNqXBAO1R8KVUeLlsLfQr/SkqmbWvTk7JKxWLz8jssSsV97ZVLC82FUrHgPLQ9dhufUbIWSik2nK2Uqrf+tthabGa9Uj58myArJfsFzu+95X5HqXbhdmg9M1MyXTS+nf+rVCs204yCK1UfxGv5bkiaekGpNjf20HSObuMzSsYCoeRIgTLrrYm3xZqStdj25zHkacQrKRVeDbSr35VKDyouys4BfkXJ8sDD25e8K5WKzXTnNiX7N0ttkW4/VCr166E7f93GHsUThdd/MIPSt6SUGQvOLcVm1h1rmtvnLSX7bbBdDwop1e4OzLxtZZle9XbkWKlSbKYafVt4maTyOWv3t06dQakwILA49HL3bmOPkm2O3RRzn5TZCs6XxaaSdUTq0HctV6Jkvu28q/+VLKNpW6QsHCIpWc4PL0etlCrFZtqHyJSqt9NTT2NRqsyLPTTVrNvYo2Squj/62qRslMwPXu31rtjMOiJV9lU0WynZRyU23d5Ssr/JoFX+6fN3lCzb1suRK6VCsXnoZH4mpcoo3Fr6Y7xS4Xb6oW8S6ja2lEwH91IPA22lzPLQ0LtiM+POlP7gHkXJPBl90y0u/9zq32Q3/Yj5Qsmwbb0cvVIqFJvpv1VKqXa+KnGMV0pcSLfLvUW3saVkObj7ynnKSfamzPBapKc7tZL1Fnr6g3sUJfPo5qbbXEqlyfTTj2oulCzb1tPBCCV7sVni4kTJfpekVebOlZK+DmiXeYtuY0vJcnCf+sCteAeueHti7VWxmfEhs9uMPC2UzAezt3ctmp/P7I7bVoa5m0/PE0r2YjPlAyw9SpXBkVKvHFMqzN3cXbx3G1tKhiuYW9zqVKq/PPdVsZnxRe5lDu5HKd6mfHfCJ4fsFzMvR5mVSvvHtE+gL5T1tpXloa2nD48q2YvNMq9eUzIOHvSUO8Yr2Y/T8cWmkmEOgYvd6d5/+IxS5Yqx51WxmfEJ4qnm/yp+2MzzqTKd/Pd4+QokpdILkKc6ZinLtuWLxszb1tMRZSVzsVlqBM6UCrfSy+2HSsaBmbUhxWaGicDTjxCsKdXmw6x1i03F69T+7NVKv9VA8YWgRxd8YKrw4MMWL+9g+L+vfjazcoXDmrJsWy7uK25b3YtIJfO67HrXbAZK9vnTJfdDJfsbaoYUm1dX2C66pnv6/B2l2ldYLZ4Vmxlvf5aZA6y4WHcfLif/qhcjb7Xrvtb+bGKV5t6tty0XDzNsW93pGErmYrPcg4pK9vNUuQJ+oWTeD4cUm1fvnLca1VwoVUc3nxWbGQ9KaS9ilPXJv/pDY3t13zjh9tXPZJf21p2yzLOcedvqnjeUzMVmxdu9GQcR1sq+aUTJvK0OKTavLHj8t283qrlQss/b6HlWbGa73ZLm9oriIirbQxZX6o44u331M5ll27Yqz+E9qjtFRsl6Aj/0ouyrKdnfC1l23rSSedQ4tthUrp5nN8VXUh6llHjBa+NZsZltBOWSEXPFn+nykEXmK9crPRuVyn7LbnHJrTtlxjm8R3UfElKy9svLucqZNeuRSruslSiZn/YPLzavHiKf9gXuWynVThrPis325662e2fZS/HF2vohixnmwp3h2TZUZV8YfutOuc0c3qPaPjOFYjOYknXEvPpDepmnKIQXm1fetrrdC5F7lCq3DhdfDppKxrl2odMz/PtkOfnfcZ5lpO5Jwu2rn8ks/Nadwra135fBCiVrsVn22QSFAn4A5eo7y6+EF5tX3rYq+xRZJCXzBtfTKzazXaF9fCGjeJ2YZzlI29/W/kxSH8+9U5jDG+PLCVGh2AymUGwO0qxPJuHFpg926z9wpuG3OatQsu7MPb1i88qLlp5d79dUlocsmAt3nh9GnpUq85d3neAU5vCO86WAUyg2gylZ+7T0e5StWZ9MwovNyzaidlnuTMlWrL1Sodh8emBXlocsvMze/pkLd40fDmb+36v/lhnbVh4UmydQ6NNBmvXJZJpis/zwdySlyonWesXmlSPkPd8e4FDaeZac/POoWmx+e22Twhze6315m4lCYRRMoU8HadYnk/Bic/3Lz1R+I4mkVJq32Ss2sx2MvDyc/HP74Rt4/L9X/y0zb1vMs8yhwrFoQbEZj2JznGmKzbJv/R9FqVIcVTrAI68fThT+36v/BmxBsXkChT4dpFmfTKYpNnevyOyUKgUbxSYiUGziUxSbJ1Do00Ga9clkjmKzXQ64W8qcbHsHeOZCYi+KTXzqy2uoFAqjYAp9OkizPpnEFZvKZa8aaZcF7pbSxWb7M8A7FJv42HobemxHFEbBFPp0kGZ9MgktNq96+rP0V0yNolT5JiGKTXzKD9h8e6p7tQ1RbGIPz3H/8p5FhcIomEKfDtKsTyZTFJtfihW4W8q8+oViE3t4ioVfDeRi8ukB7PHf238LLFzweBvxe0yffl3o4+faf5sBxWY8is1xKDZnpVBsYgYetfS35XikfvN3iCsUm1h41NLv7vXrsL58//krCoVRMIU+HaRZn0woNmelUGyimm+3MsVFwe6D05pCsXlf61HLH77GdK/H72p/fwYUm/EoNseh2JyVQrGJ7HzS8ajly1uZRygUm/fgke9Do5ZbKBRGwRT6dJBmfTKh2JyVUrnYzHowwnFho5ZbKBSb8/F8XR8b/Nn6+PbRqOUWj7/XLkcGFJvxKDbHCS02r/qKRIrNDoViE1daioLwUcstHn+7XSbUsoxaer5u+KjlFgqFUTCFPh2kWZ9M4opNa375Wf7oLcvdKWW+G7qz7BSbtRx+AGMUhWKzltNHLbd4LFO7rBlQbMaj2BxnimJTf7q/PHemVDnZUmzWsxQFHz+AMcpj2drlRh6H3jJwNoXCKJhCnw7SrE8mcxSbkvKEdyXFI01tP2XUKzZ9Emp/Dte4/FbmEUqVaSR3sOndqBkpFEbBFPp0kGZ9MgkvNq/6TutSB7AzKFl36Fav2OQW6DVS3so84rH87frhHN6G0o9abvFYl3b9MqDYjEexOU54sXnVRvRLb3nuTLmq8N+rV2z6JNX+HOKVHLXcSmnXF/FOfcvA2RQKo2AKfTpIsz6ZTFNs/ru3PHel/LTqm+x6xSajUvF+uJUp0089Udo+wOd8jB/ybtSMHuvb9kEGFJvxKDbHCS82r5pr93tvee5KqfRwRK/YrFQsZ1XiAYyRlKwntSqmHrXcQqEwCqbQp4M065NJeLF55Vw7HhJ6UCo9YPOl2HysQ/tzeK7sAxgjPfqk7Ss85yLA21Datwyc7dEnbT9lQLEZj2JznPBi88oRtX/2lumOFI9ItP2T1bNi0yNz7c/iLz5Y3+ZW5lHKlRe/2aV7N2pGCoVRMIU+HaRZn0zCi80r59oxb1OUv6/6pIJnxWaVVzeNdvtbmUcplaaTjOYTPKOWOz36re3LDCg241FsjhNbbFrzB870Z2957kapdAvdnhWbdx2V8oGYUcsA7j9p+/cOpn7LwJkUCqNgCn06SLM+mQwpNq+8hXv7W+lKpVvo9qzYvMMT6etbmYxaDqBUeQXYUdO8GzWjR9+2fZ4BxWY8is1xhhSbV25I3cLlLpSKBdqzYvNvq5+ZxVIUcCvzJI8+bz+Hyhi1PJFCYRRMoU8HadYnkyHF5tW3P297AFYqPn379AJBqfyQEA9gJKBUno7BWwYuplAYBVPo00Ga9clkSLF59aT8Wz4opFSdn/aq2Kwy/3R9K5NRy0SUSqP9t383ajYKhVEwhT4dpFmfTIYUmxmKntsdqJWq7xR8VWxefeHyjgsDRi2TW31eGXkEnFHLpBQKo2AKfTpIsz6ZxBebplz9kMqt5m4qlR+meVVsZp+3+VtvuZGLP6fVZ5aNR8UZCU9KoTAKptCngzTrk8mwYjPDOxJvM1qgVJ7b+PLCwP999bMZcbszOcXzZtvPLZPyJ7lZKRRGwRT6dJBmfTIZVmx6ztH6D13Bo6vTjxgo2U+k77wrNrOvH18mkJySfT4zo5tJKRRGwRT6dJBmfTIZVmxmObhPXQgo/rYgn6ja9a7kXbFZ4cGnW4yiK75jUfLpaCX76P8tRjcVXzy62CjxMN1jWdefUxYUm/EoNscZU2yakuXgPuWL3hXPZ6x8+3zxdn6tkn09365DdcqzecH+bNI/Qa1kHyH3RePUUzIUH7N6F8dpXxOmUBgFU+jTQZr1yWRosZnltTU+uE33xLCSYV5shC3FZoWpAr/0ln0WytaH/ry/pRv9VCqMkE990aLsOSekGP18LEe7bBlQbMaj2BxnaLHpW7zrP3alqUYNlCyFfIQtxWaFbxOadmRK8Um/Xd89Uox+KllPcmuz3on59HxwyeinQmEUTKFPB2nWJ5NxxaYpV78Cac0nvPKT8JUMD19F2jSao1QYyZ1uZEoZcdF4yeinUmHfme6iRRk15cdFiy9iPPo5pM8ef6P9uxlQbMaj2BxneLGZbQTOB7yyB3Klwu3kvbYWm1XeJfprb/krUp7NsRvhlNFP5az1+cQUF8YL5awLRQ9u+CLGx8mQixiFwiiYQp8O0qxPJsOLzYzzpHyyKTeHU5lljmZr82igkmmk/JXyt0KVqx9AGzL6+fh97d/KaIovDFCuHnD4aPTz8e/b35kBxWY8is1xxhabplx5wnrGJ7ISBYHik37WnTPCnmKz0hSCsgWncnWh+YyX6aMHsZQKDwotSr+6Tcm4vy6jn5tGjhUKo2AKfTpIsz6ZnFJsZi4QfMWb9naV4lvHFW77fWLXPEelyuimlSs4layF5uLjaQpKpbsEJQtOJfMI8h+9Ze5RKIyCKfTpIM36ZDK+2DQlc8Hk4iXsNl0ExSf8bPNdR9lbbFabt1qm4FQ86pe50LSPp8AolUY3rVTB6eVdLXtGmy9YFAqjYAp9OkizPpmcVmxWmCflHeDyh4cUF1Ozj2au7S02XYhXGt209A8NKZ7Tln272zwi9Y5SbQ60LwJSPzSkVLhYsc0XLAqFUTCFPh2kWZ9MTis2S82TklNHOhUXUC4yqxVREXYVm6ZUmru5SHEx01IqjaKHvThfqTa6ab4YSHUXZqFUuUj+vbf8zygURsEU+nSQZn0yOafYNKXiSIIPoCNfw+I5me6XO41ktnYXm6ZULMz9Oaf5piHFRXuVfnTfhY7sKVWeTG+lmWuu+D2sWYuHnl3TWhQKo2AKfTpIsz6ZnFpsVhxJWLjw/PgVLIr7wCf4uxeYa0eLzSrv3ezx9nTZCJX/tlQqECx83qLiUd2q++GlFy6Kj2XVBhB2T8NQKIyCKfTpIM36ZHJesWlKtYPTMy4WvMO4ADWPgPoEvvD8t+W/eZ39sxSXfYeKTVP8CpP291Xi7eLn3rpFU1xY+UKnwpy6niF3GJSKUzLWPDLt488pI52Kj29Vj+O7H9ZTKIyCKfTpIM36ZHJ6sVl5dBNjfFJsenuaoYh3weBbo+FfNqD4wqf6SPrQB6yUrCe/Pfz5+nMOv3hRfKvc22flOeWHHi5TKIyCKfTpIM36ZHJusWmKR/vWC4F7O1xsmuJRnfZ3VuYTuosGj7jt2kEVF9/LqPoMBZS5iBo6aqfMctGy8Lp8//Yl2dx//tnHv/F+5d8xS78cKsIVCqNgCn06SLM+mVxSbFaeJ4V4HxWb5t+x+n0z8v7idXym6q3xLU6Zl+i/s/qbs+ptO2u9fzODT+6eZO0Xis14FJvjnF9smuLRl/WC4L4iis3ZRqbwl4+3jT3891Z/G3PwceHwfF+FwiiYQp8O0qxPJtcUm6ZwYIeFFBQKFzBzcZEQPof1FYW7LvP59Lv0KYyCKfTpIM36ZHJpscloFCxs9Eqp+pQsvrrktT6K5yu2y4KaIu6aUBgFU+jTQZr1yeS6YtOUO8yTwmuht0qVmecv3sVvvc/2LAoPMdbngYyPHyxTKIyCKfTpIM36ZHJtsWlK9Xcl4jPRxSa3QmvzxcLl346jMEpe2+6TW49CYRRMoU8HadYnkxTFpouDyu9vw2dCi01T/F5ACs56/JmdOk/zGcXHJUbJa9r98vZnFAqjYAp9OkizPplcX2ya4uJgvWC4j/Bi0xTm3tWTotBcKBSc9YR+AYBCYRRMoU8HadYnkxzFpinVvzYOxwwpNk1hm6ojbDQqksK0jDpGfH8+hVEwhT4dpFmfTPIUm6b4K9HWC4g4PmFmnK4wrNg0hYIzv5SF5kJhWkZ+4YWmKRRGwRT6dJBmfTLJVWyawsT8Mfweyow7+dBi0xQKzrxSF5oLxQUnc8tzGlJomkJhFEyhTwdp1ieTfMWmKVk3xqq+ndCVWxabprjgZHQqlxKF5kJhDmc+wwpNUyiMgin06SDN+mSSttjkoB7n+8FYuW2xaQq3Q3PwZ5DqYaCtFI5NeQwvDhQKo2AKfTpIsz6Z5Cw2TeGg/rkfrvqVWxebprjgZLu6jvu+ZKG5pjDd5zq+WDllVFyhMAqm0KeDNOuTSd5i0xQXnBzUj/lye0m5fbFpircrvkzgfO7zy1/YHkXhG9DO53mzp12sKBRGwRT6dJBmfTLJXWwuFArOfbrzmBSKzRWFryU8zyXfdT6a4ve5MjXjHKdfrCgURsEU+nSQZn0yqVFsmsIowjZPby8pFJsNxcUCTxmPM8Vt81cUj5RnPYHOwMX8JRcrCoVRMIU+HaRZn0zqFJvmBRZGEfrcLz/3+m2hUGx2KNxWH8PvzZ3mtvk7ii+IOT7F8jHrp15/n+Hx99tlyoBiMx7F5ji1ik1TfhIe8PjRptEjhWLzBcXvImWU83PTj2Y+o/j4lPVkWomL9stfjaVQGAVT6NNBmvXJpF6xuVCYb/cXz2fdNHqkUGy+oXiUk23rmBQFQgYKFy7HpRkRVyiMgin06SDN+mRSt9g05c6vsXl727ylUGxupHiEilvr23hbdIF+m1vmW7g/Hv3CrfVtfHy67JZ5z2OZ2uXMgGIzHsXmOLWLzYVyt7lSm0cz1xSKzZ0UzxPOenC8mve5Q9vinSi+cOGNGs95/9p9MjrDY9na5c2AYjMexeY4cxSbpngUwbdf1is4G4/iHj4oKxSbBykUnf/DSOYBylJ0MtL5F/dFyiJzoVAYBVPo00Ga9clknmJzocw4iuC5Xx/PhVMoNj+keOrGXUepvm2HQpH5AfefuFi/45xOF9oeFEh1u/wZhcIomEKfDtKsTybzFZsLxUWnD2qVRxFCisyF4v7wjp7Jr71lzU5xweDpG7MXDMut8ls+XT6a4uL9DnODvY7lHh5TMh4zreyDeAp9OkizPpnsPn90GzNTXBT4gF7pQSIfmHc9/IPrKB7t9AF0lsLTBWbJ4qAqxRfHvniZ6YFHr4vXqcQoJoA8uo1VKJmLAg7ME1CWbaxa0eB9wiOYXORcTFkKz2ojnt8vUoTjGIDDuo0V+WAoywH9qlvt/tsUmJNSPKru9y1mLD69PC4uKQySU/xwmud4+nZU+zleaSkufQxjmgWAMN3GGfhg+Tho+gQ84qC+nNz9N1I/gYlx/Nk/toFl3tLoCx3/fv+d79ue8IBPYYqPVb5IWArQMy5k/HdcWPpv+gKKCxQAw3QbZ6V4ZMon52VkYbEUCmvLgXjhk4H/HQdlvKW4gPD2shQRi3Y761n//LLdMdJ0M4rv1vSOV8sF9Cs+pi0/v1yUGBcmAE7XbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAAIEK3EQAAAIjQbQQAAAAidBsBAACACN1GAAAA4HP//b//B3B+JA7XYURBAAAAAElFTkSuQmCC',
              alignment: 'left',
              width: 100,
              height: 40,
              marginTop: 40,

              marginLeft: 40,
              marginBottom: 40
            },
            {
              columns: [
                [
                  {
                    text: '',
                    margingTop: 10
                  },
                  {
                    text: 'Statement Date:',
                    fontSize: 10,
                    marginBottom: 10


                  },
                  { text: 'Statement Period:', fontSize: 10, },

                ],
                [
                  {
                    text: `15/12/2020`,
                    alignment: 'right',
                    fontSize: 10,
                    marginBottom: 10
                  },
                  {
                    text: `16/12/2020 - 15/12/2020`,
                    alignment: 'right',
                    fontSize: 10,
                  },
                  
                ],

              ],
              marginLeft: 40,
              marginRight: 40,


            },
            { canvas: [{ type: 'line', x1: 40, y1: 10, x2: 560, y2: 10, lineWidth: 0.1, }] },
          ]
        }
      },
     
      content: [

       //Summary
        {
          text: '',
          style: 'tableSectionHeader'
        },
       
        
        {
          text: 'Summary of Accounts',
          fontSize: 16,
          marginBottom: 10
        },
        {
          columns: [{
            text: 'Deem World Cash Up Credit Card',
            fontSize: 16,
            marginBottom: 10,
            color: '#6E3DEA'
          }
          ]

        },
        {
          columns: [
            [{ text: 'Card Number', marginBottom: 10 },
            { text: 'Total Payment Due (AED)', marginBottom: 10 },
            { text: 'Minimum Payment Due (AED)', marginBottom: 10 },
            { text: 'Payment Due Date', marginBottom: 10 },
            { text: 'Total Credit Limit (AED)', marginBottom: 10 },
            { text: 'Total Cash Limit (AED)', marginBottom: 10 },
            { text: 'Total Outstanding Balance (AED)', marginBottom: 10 },

            ],
            [
              { text: '4582xxxxxxxx1479', marginBottom: 10 },
              { text: '8,484.67', marginBottom: 10 },
              { text: '8,007.70', marginBottom: 10 },
              { text: 'Sunday, 24th January 2021', marginBottom: 10 },
              { text: '30,000.00', marginBottom: 10 },
              { text: '20,000.00', marginBottom: 10 },
              { text: '600', marginBottom: 10 },
            ]

          ],
          fontSize: 12,
          marginBottom: 10
        },
        {
          columns: [{
            text: 'Deem Everyday Account',
            fontSize: 12,
            marginBottom: 10,
            color: '#6E3DEA'
          }
          ]

        },
        {
          columns: [
            [{ text: 'Linked Card Number', marginBottom: 10 },
            { text: 'Total Balance (AED)', marginBottom: 10 },
            { text: 'Main Wallet Balance (AED)', marginBottom: 10 },
            { text: 'Savings Wallet Balance (AED)', marginBottom: 10 },],
            [
              { text: '4582xxxxxxxx1479', marginBottom: 10 },
              { text: '17,946.10', marginBottom: 10 },
              { text: '13,984.06', marginBottom: 10 },
              { text: '3,962.04', marginBottom: 10 },

            ]
          ],
          fontSize: 12,
          marginBottom: 10
        },
        //Personal Loan
        {
          columns: [{
            text: 'Deem Personal Loan',
            fontSize: 16,
            marginBottom: 10,
            color: '#6E3DEA'
          }
          ]

        },
        //
        {
          columns: [
            [
              { text: 'Loan Account Number', marginBottom: 10 },
              { text: 'Principal Outstanding (AED)', marginBottom: 10 },
              { text: 'Payment Due (AED)', marginBottom: 10 },
              { text: 'Towards Principal (AED)', marginBottom: 10, marginLeft: 10 },
              { text: 'Towards Interest (AED)', marginBottom: 10, marginLeft: 10 },
              { text: 'Late Payment Fee / Other Charges (AED)', marginBottom: 10, marginLeft: 10 },
              { text: 'Payment Due Date', marginBottom: 10, },
            ],

            [
              { text: 'XPIL123456789', marginBottom: 10 },
              { text: '19,630.42', marginBottom: 10 },
              { text: '1,974.00', marginBottom: 10 },
              { text: '1,384.00', marginBottom: 10 },
              { text: '540.00', marginBottom: 10 },
              { text: '50.00', marginBottom: 10 },
              { text: 'Friday, 1st January 2021', marginBottom: 10 },

            ]
          ],
          fontSize: 12,
          marginBottom: 90
        },
        //Double Secure
        {
          columns: [{
            text: 'Double Secure',
            fontSize: 16,
            marginBottom: 10,
            color: '#6E3DEA'
          }
          ]

        },
        {
          columns: [
            [
              { text: 'For more details and Terms & Conditions, please visit', marginBottom: 10 },
              { text: 'www.deem.io/cards/cash-up and www.deem.io/-', marginBottom: 10 },
              { text: 'cards/miles-up', marginBottom: 20 },
              { text: 'Monthly Billing Amount (AED)', marginBottom: 10 },

            ],

            [
              { text: '', marginBottom: 10 },
              { text: '', marginBottom: 10 },
              { text: '', marginBottom: 20 },
              { text: '', marginBottom: 30 },
              { text: '1,384.00', marginBottom: 10 },


            ]
          ],
          fontSize: 12,
          marginBottom: 10
        },
        //Credit Life Plus
        {
          columns: [{
            text: 'Deem Credit Life Plus',
            fontSize: 16,
            marginBottom: 10,
            color: '#6E3DEA'
          }
          ]
        },
        {
          columns: [
            [
              { text: 'For more details and Terms & Conditions, please visit', marginBottom: 10 },
              { text: 'www.deem.io/loans/personal', marginBottom: 20 },
              { text: 'Monthly Billing Amount (AED)', marginBottom: 10 },

            ],

            [
              { text: '', marginBottom: 50 },

              { text: '1,384.00', marginBottom: 10 },


            ]
          ],
          fontSize: 12,
          marginBottom: 360
        },
        //Credit Card Summary
        {
          text: 'Deem Credit Card',
          fontSize: 16,
          marginBottom: 10
        },
        {
          columns: [{
            text: 'Summary of Accounts',
            fontSize: 16,
            marginBottom: 10,
            color: '#6E3DEA'
          }
          ]

        },
        {
          columns: [
            [{ text: 'Card Number', marginBottom: 10 },
            { text: 'Previous Balance (AED', marginBottom: 10 },
            { text: 'Purchases/Cash Advance (AED)', marginBottom: 10 },
            { text: 'Interest/Charges (AED)', marginBottom: 10 },
            { text: 'Payments/Credits (AED)', marginBottom: 10 },
            { text: 'Cashback Credit (AED)', marginBottom: 10 },
            { text: 'Total Payment Due (AED)', marginBottom: 10 },
            { text: 'Current Balance (AED)', marginBottom: 10 },

            ],
            [
              { text: '4582xxxxxxxx1479', marginBottom: 10 },
              { text: '5,684.54', marginBottom: 10 },
              { text: '5,838.27', marginBottom: 10 },
              { text: '52.90', marginBottom: 10 },
              { text: '6179.50', marginBottom: 10 },
              { text: '0.00', marginBottom: 10 },
              { text: '5,398.86', marginBottom: 10 },
              { text: '2,101.14', marginBottom: 10 },
            ]

          ],
          fontSize: 12,
          marginBottom: 10
        },
        //Transaction
        {
          columns: [{
            text: 'Credit Card Transaction Details',
            fontSize: 16,
            marginBottom: 10,
            color: '#6E3DEA'
          }
          ]
        },
        {
          table: {
            headerRows: 1,
            widths: ['auto', 'auto', '*', 'auto', 'auto'],
            marginTop: 100,
            dontBreakRows: true,
            body: [

              [{
                text: 'Transaction date', fontSize: 9, marginBottom:10, border: [true, true, true, true],

              }, {
                text: 'Posting date',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

              }, {
                text: 'Description',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

              }, {
                text: 'Amount - Original Currency',marginBottom:10, fontSize: 9, border: [true, true, true, true], alignment: 'right'

              }, {
                text: 'Amount(AED)',  fontSize: 9,marginBottom:10, border: [true, true, true, true], alignment: 'right'

              },
            
            ],
              ...this.creditCardTransaction.transactions.map(t => ([{ text: t.transactionDate, fontSize: 8,  margin:[0,5,0,5]  }, { text: t.postingDate, fontSize: 8,  margin:[0,5,0,5]  }, { text: t.description, fontSize: 8, margin:[0,5,0,5] }, { text: t.amountOriginalCurrency, fontSize: 8,  margin:[0,5,0,5] , alignment: 'right' }, { text: t.amount, fontSize: 8,  margin:[0,5,0,5] , alignment: 'right' }])),

            ]
          },

           //layout: 'lightHorizontalLines',
         layout: {
          hLineColor: function (i, node) {
            return '#D8D8D8' ;
          },
          hLineWidth: function (i, node) {
           
            return i===0 ?0:0.20 ;
          },
          
          vLineWidth: function (i, node) {
            return 0;
          }
          
        }
            
          
        // }
        },
        //Tax Invoice
        {
          columns: [{
            text: 'Summary of Instalment Plans',
            fontSize: 12,
            marginBottom: 10,
            marginTop: 20,
            color: '#6E3DEA'
          }
          ]

        },
        {
          table: {
            headerRows: 1,
            widths: ['*', '*', '*', '*', '*','*'],
            marginTop: 100,
            dontBreakRows: true,
            body: [

              [{
                text: 'Date', fontSize: 8, marginBottom:10, border: [true, true, true, true],

              }, {
                text: 'Merchant Name',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

              }, {
                text: 'Original Amount (AED)', fontSize: 9,marginBottom:10, border: [true, true, true, true],

              }, {
                text: 'Outstanding Amount (AED)', fontSize: 9,marginBottom:10,  border: [true, true, true, true], 

              }, {
                text: 'Instalments Pending',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

              },
              {
                text: 'Tenor',  fontSize: 9,marginBottom:10, border: [true, true, true, true], alignment: 'right'

              },
            
            ],
             [ { text: 'N/A', fontSize: 8,margin:[0,5,0,5] }, { text: 'N/A', fontSize: 8,margin:[0,5,0,5] }, { text: 'N/A', fontSize: 8,margin:[0,5,0,5] }, { text: 'N/A', fontSize: 8, margin:[0,5,0,5] }, { text: 'N/A', fontSize: 8, margin:[0,5,0,5],  },{ text: 'N/A', fontSize: 8, margin:[0,5,0,5],alignment:'right' }]

            ]
          },

         // layout: 'noBorders'
         layout: {
          hLineColor: function (i, node) {
            return '#D8D8D8' ;
          },
          hLineWidth: function (i, node) {
           
            return i===0 ?0:0.20 ;
          },
          
          vLineWidth: function (i, node) {
            return 0;
          }
          
        }
        },

        {
          text: 'Tax Invoice',
          fontSize: 12,
          marginBottom: 10,
          marginTop: 20
        },
        {
          columns: [
            [{
            text: 'Deem Credit Card',
            fontSize: 12,
            marginBottom: 10,
            color: '#6E3DEA'
          }],
        
         
          ]

        },
        {
          columns: [
            [{ text: 'Invoice Date:', marginBottom: 10 },
            { text: 'Services Eligible for Value Added Tax:', marginBottom: 10 },
            { text: 'Total Fees Eligible for Value Added Tax:', marginBottom: 10 },
            { text: 'Total Value Added Tax for the Period', marginBottom: 10 },
           

            ],
            [
              { text: '15/12/2020', marginBottom: 10 },
              { text: 'As described in the list of transactions', marginBottom: 10 },
              { text: 'AED 2.65', marginBottom: 10 },
              { text: 'AED 45.68', marginBottom: 10 },
              
            ]

          ],
          fontSize: 8,
          marginBottom: 150
          //pageBreak: "after"
        },
        //
        {
          text: '',
          style: 'sectionHeader',
        },

        // {
        //   canvas: [
        //     {
        //       type: 'path',
        //       d: 'M 0,20 L 100,160 Q 130,200 150,120 C 190,-40 200,200 300,150 L 400,90',
        //       dash: {length: 5},
        //       // lineWidth: 10,
        //       lineColor: 'blue',
        //     },
        //    ]
        // },
        {
          text: 'Credit Card - Rewards / Spend Analyzer',
          fontSize: 12,
          marginBottom: 20,
          marginTop: 20
        },
        {
          columns: [{
            text: 'Spend Categories',
            fontSize: 12,
            marginBottom: 20,
            color: '#6E3DEA'
          }
          ]

        },
      {columns:[
       [{
          svg: svg,


        },
        {
          text:'Airlines',
          marginTop:10,
          fontSize:8,
          marginLeft:20
        }
      ],
      [{
        svg: svgHotels,


      },
      {
        text:'Hotels',
        marginTop:10,
        fontSize:8,
        marginLeft:20
      }
    ],
      [{
        svg: svgEnt,


      },
      {
        text:'Entertainment',
        marginTop:10,
        fontSize:8,
        marginLeft:20
      }
    ],
    [{
      svg: svgOthers,


    },
    {
      text:'Other Spends',
      marginTop:10,
      fontSize:8,
      marginLeft:20
    }
  ],
      ]}  ,
      {
        columns: [{
          text: 'Rewards Earned',
          fontSize: 12,
          marginBottom: 10,
          marginTop: 20,
          color: '#6E3DEA'
        }
        ]

      },
      {
        table: {
          headerRows: 1,
          widths: ['*', '*', '*' ],
          marginTop: 100,
          dontBreakRows: true,
          body: [

            [{
              text: 'Spend Categories', fontSize: 9, marginBottom:10, border: [true, true, true, true],

            }, {
              text: 'Amount Spent',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

            }, {
              text: 'Rewards Earned',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

            }, 
          
          ],
           [ { text: 'Airlines', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 1,206.94', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 12.69', fontSize: 8,margin:[0,5,0,5] }],
           [ { text: 'Hotels', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 980.12', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 49.01', fontSize: 8, margin:[0,5,0,5] }],
           [ { text: 'Entertainment', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 3,108.92', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 155.45', fontSize: 8, margin:[0,5,0,5] }],
           [ { text: 'Other Spends', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 5,893.82', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 58.94', fontSize: 8, margin:[0,5,0,5] }],
           [ { text: 'Bonus Rewards', fontSize: 8, margin:[0,5,0,5] }, { text: '---', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 72.94', fontSize: 8, margin:[0,5,0,5] }],
           [ { text: 'Total', fontSize: 8, margin:[0,5,0,5] }, { text: 'AED 11,189.8', fontSize: 8, margin:[0,5,0,5],color: '#6E3DEA' }, { text: 'AED 349.03', fontSize: 8, margin:[0,5,0,5],color: '#6E3DEA' }]
        ]
          
        },

       // layout: 'noBorders'
       layout: {
        hLineColor: function (i, node) {
          return '#D8D8D8' ;
        },
        hLineWidth: function (i, node) {
         
          return i===0 ?0:0.20 ;
        },
        
        vLineWidth: function (i, node) {
          return 0;
        }
        
      }
      },

      //Summary of Reward Points


      {
        columns: [{
          text: 'Summary of Reward Points',
          fontSize: 12,
          marginBottom: 10,
          marginTop: 20,
          color: '#6E3DEA'
        }]
        

      },
      {
        table: {
          headerRows: 1,
          widths: ['*', '*', '*','*','*' ],
          marginTop: 100,
          dontBreakRows: true,
          body: [

            [{
              text: 'Opening Balance (AED)', fontSize: 9, marginBottom:10, border: [true, true, true, true],

            }, {
              text: 'Earned (AED)',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

            }, {
              text: 'Purchased (AED)',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

            }, 
            {
              text: 'Redeemed (AED)',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

            }, 
            {
              text: 'Available Balance (AED',  fontSize: 9,marginBottom:10, border: [true, true, true, true],

            }, 
          
          ],
           [ { text: '11,206.94', fontSize: 8, margin:[0,5,0,5] }, { text: '349.03', fontSize: 8, margin:[0,5,0,5] }, { text: '166.83', fontSize: 8, margin:[0,5,0,5] },{ text: '297.67', fontSize: 8, margin:[0,5,0,5] },{ text: '13,621.09', fontSize: 8, margin:[0,5,0,5],  }],
           
        ]
        
        },

       // layout: 'noBorders'
       layout: {
        hLineColor: function (i, node) {
          return '#D8D8D8' ;
        },
        hLineWidth: function (i, node) {
         
          return i===0 ?0:0.20 ;
        },
        
        vLineWidth: function (i, node) {
          return 0;
        }
        
      }
     
      },
      {
        text:'',
        marginBottom:100
      },
      //Personal Loan

      {
        text: 'Deem Personal Loan',
        fontSize: 12,
        marginBottom: 10,
        marginTop: 10
      },
      {
        columns: [{
          text: 'Summary Of Accounts ',
          fontSize: 12,
          marginBottom: 10,
          color: '#6E3DEA'
        }
        ]

      },
      {
        columns:[
          [
            {text:'Loan Account Number',marginBottom:10},
            {text:'Principal Outstanding (AED)',marginBottom:10},
            {text:'Payment Due (AED)',marginBottom:10},
            {text:'Towards Principal (AED)', marginLeft:10,marginBottom:10},
            {text:'Towards Interest (AED)',marginLeft:10,marginBottom:10},
            {text:'Late Payment Fee / Other Charges (AED)',marginLeft:10,marginBottom:10},
            {text:'Payment Due Date',marginBottom:10},
            {text:'Amount Borrowed (AED)',marginBottom:10},
            {text:'Tenor (Months)',marginBottom:10},
            {text:'Interest Rate p.a. (%)',marginBottom:10},
            {text:'Monthly Instalment (AED)',marginBottom:10},
            {text:'Last Payment Amount (AED)',marginBottom:10},
            {text:'Last Payment Date',marginBottom:10},
            {text:'Instalments Remaining',marginBottom:10},
          ],
          [
            {text:'XPIL20200351398',marginBottom:10},
            {text:'19,630.42',marginBottom:10},
            {text:'1,974.00',marginBottom:10},
            {text:'1,384.00',marginBottom:10},
            {text:'540.00',marginBottom:10},
            {text:'50',marginBottom:10},
            {text:'02 Sept 2019',marginBottom:10},
            {text:'15,000.00',marginBottom:10},
            {text:'50',marginBottom:10},
            {text:'32.04',marginBottom:10},
            {text:'558.00',marginBottom:10},
            {text:'580.05',marginBottom:10},
            {text:'18/08/2020',marginBottom:10},
            {text:'36',marginBottom:10},
          ]
        ],
        fontSize:8,
        
      },
      {
        text: 'Tax Invoice',
        fontSize: 12,
        marginBottom: 10,
        marginTop: 10
      },
      {
        columns: [{
          text: 'Deem Personal Loan',
          fontSize: 12,
          marginBottom: 10,
          color: '#6E3DEA'
        }
        ]

      },
      {
        columns:[
          [
            {text:'InvoiceDate',marginBottom:10},
            {text:'Services Eligible for Value Added Tax:',marginBottom:60},
            {text:'Total Fees Eligible for Value Added Tax:',marginBottom:10},
            {text:'Total Value Added Tax for the Period', marginBottom:10},
           
          ],
          [
            {text:'15/12/2020',marginBottom:10},
            {text:'Processing Fee, Fee for Early Settlement of Loan,'},
            {text:'Loan Rescheduling Fee, Fee for Liability Letter ,',},
            {text:'and No Liability Certiﬁcates, Partial Payment'},
            {text:'Charges, Loan Top-Up Fee, Delayed Payment'},
            {text:'Penal Interest Charges, Loan Cancellation Fee'},
            {text:'and Deem Credit Life Plus.',marginBottom:10},
            {text:'AED 1.05',marginBottom:10},
            {text:'AED 32.76',marginBottom:10}
            
          ]
        ],
        fontSize:8,
        marginBottom:40,
        
      },
      {
        text: 'Terms & Conditions',
        fontSize: 12,
        marginBottom: 30,
        marginTop: 10
      },
      {
        columnGap: 30,
        alignment: 'justify',
        width: '50%',
        fontSize:8,
        columns: [
          [
            {
              text:'Important notes:',
              bold:true,
              marginBottom:10
            },
           {
             text:'1. Minimum payment due: You may pay your card balance in full or in partial payments. If you make a partial payment, you must pay at least the minimum amount due each month by the payment due date. Interest will be applicable if payments are not made in full.',
            marginBottom:10
          },{
            text:`2. Total cash limit: This limit reflects the amount that can be withdrawn by you in cash.`,
            marginBottom:10
          },
          {text:'3. Total credit limit: This is the limit available for you to make purchases.',
          marginBottom:10},
          {text:'4. Pursuant to united states government sanctions against certain countries, MasterCard® has advised that it will refuse to authorize card transactions originating in sanctioned countries. While the sanctions are in effect, you will not be able to use your Deem MasterCard® credit card in sanctioned countries.',
          marginBottom:10},
          {text:'5. In line with central bank regulations, Deem is required to ensure that we keep updated and valid records of all our customers, it is therefore your responsibility to provide Deem with your renewed ID documents including, but not limited to passport, residence visa, Emirates ID and ensure that you keep your personal data including contact information updated at all times. You are required to immediately update us of any changes in your license where applicable, contact details, including, but not limited to, e-mail address(es), telephone number(s), employment status, etc. Please send your updated information by the registered email to customercare@deem.io or call 800 3366.',
          marginBottom:10},
          {text:'6. Terms and conditions apply. Visit www.deem.io to view T&Cs and applicable fees and charges.',
          marginBottom:10},
          {
            text:'Insurance products:',
            bold:true,
            marginBottom:10
          },
          {text:'1. Deem Double Secure: An insurance plan which protects your credit card’s outstanding and extends an equivalent amount to you or your nominee in the unfortunate event of Death or Critical Illness of the primary credit card holder. The plan also pays 10% of your credit card’s outstanding in the event of Involuntary Loss of Employment. Further, in the event of an accident, the plan extends beneﬁts such as Hospital Cash Beneﬁt of AED 100/day and Ambulance/Taxi Fare of up to AED 500 subject to Terms & Conditions. Deem Double Secure comes with a free look period of 30 days. More details on the product are available on www.deem.io/cards/cash-up and www.deem.io/cards/miles-up.',
          marginBottom:10},
          {text:'2. Deem Credit Life Plus: Deem Credit Life Plus is an insurance plan which comes packaged with your Deem loan and protects your loan’s outstanding in the unfortunate event of Death and Terminal Illness. The plan also provisions for Repatriation due to Death, by taking care of the repatriation expenses of the deceased loan customer to their country of origin, subject to Terms & Conditions. For more details, visit www.deem.io/loans/personal.',
          marginBottom:10},
          ],
          
        ]
      },
        // {
        //   columns: [
        //     [{ qr: `${'Premananda Routray'}`, fit: '50' }],
        //     // [{ text: 'Signature', alignment: 'right', italics: true }],
        //   ]
        // },


      ],
      styles: {
        langA: {
          font: 'Moderat',
        },
        langB: {
          font: 'Moderat',
        },
        sectionHeader: {
          bold: true,
          //decoration: 'underline',
          fontSize: 12,
          margin: [0, 10, 0, 10]
        },
        spacer: {
          margin: [0, 0, 0, 40]
        },
        tableSectionHeader: {
          bold: true,
          //decoration: 'underline',
          fontSize: 10,
          margin: [0, 10, 0, 10]
        },
        tableSectionHeader2: {
          bold: true,
          //decoration: 'underline',
          fontSize: 10,
          margin: [0, 10, 0, 10]
        },
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5]
        },
        tableExample: {
          margin: [0, 0, 0, 10]
        },
        tableExample2: {
          margin: [0, 0, 0, 0]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        },

      },
      defaultStyle: {
        font: 'Harmattan'
      }
    };

    if (action === 'download') {

      pdfMake.createPdf(docDefinition).download(`${'Deem Statement_' + new Date().toLocaleString()}.pdf`);

    } else if (action === 'print') {
      pdfMake.createPdf(docDefinition).print();
    } else {
      if (this.getBrowserName() === 'edge' || this.getBrowserName() === 'ie') {
        const pdf = pdfMake.createPdf(docDefinition);
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          return new Promise((resolve) => {
            pdf.getBlob(blob => {
              window.navigator.msSaveOrOpenBlob(blob, `${new Date().toLocaleString()}.pdf`);
              //  resolve();
            });
          });
        } else {
          return new Promise((resolve) => {
            pdf.download(`${new Date().toLocaleString()}.pdf`, resolve);
          });
        }
      }
      else {
        const pdf = pdfMake.createPdf(docDefinition).open();
      }

    }

  }

}
