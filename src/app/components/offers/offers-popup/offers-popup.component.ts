import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-offers-popup',
  templateUrl: './offers-popup.component.html',
  styleUrls: ['./offers-popup.component.css']
})
export class OffersPopupComponent implements OnInit {
  imageUrl:string;
  desc:string;
  title:string;
  constructor(public dialogRef: MatDialogRef<OffersPopupComponent>) {
    this.imageUrl=localStorage.getItem('imageUrl');
    this.desc=localStorage.getItem('desc');
    this.title=localStorage.getItem('title');
   }

  ngOnInit(): void {
  }
  close() {
    this.dialogRef.close();
  }
}
