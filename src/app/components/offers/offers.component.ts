import { Component, OnInit } from '@angular/core';
import { CmsOffersService } from 'src/app/services/cms-offers/cms-offers.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { OffersPopupComponent } from './offers-popup/offers-popup.component';
import { Offers } from 'src/app/models/offers';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {
  pages: any = [];
  offers: Offers = new Offers();
  filtersLoaded: Promise<boolean>;
  // constructor(private cmsOffer: CmsOffersService, private domSanitizer: DomSanitizer, private appConfig:AppConfig) { 
  constructor(private matDialog: MatDialog, private statementService: StatementSummaryService, private cmsOffer: CmsOffersService, private domSanitizer: DomSanitizer, 
    private envService:EnviornmentService) {
    this.cmsOffer.getOffer(this.statementService.cardTypeImage).subscribe((data: any) => {
      if (data != null) {
        data.forEach(x => {
          this.pages.push(x)
        })

      }
    });

  }

  ngOnInit() {

  }
  transform(html: string): SafeHtml {
    return this.domSanitizer.bypassSecurityTrustHtml(html);
  }
  ngAfterViewInit() {

  }
  openOffer(imageUrl, title, desc): void {
    const dialogConfig = new MatDialogConfig();
    localStorage.setItem('imageUrl', `${this.envService.getEnviornment().baseUrlStrapi + imageUrl}`);
    localStorage.setItem('desc', desc);
    localStorage.setItem('title', title);
    const dialogRef = this.matDialog.open(OffersPopupComponent, {
      width: '50%',
      height: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {

    });

  }


}
