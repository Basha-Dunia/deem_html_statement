import { Component, OnInit } from '@angular/core';
import { ToLocaleStringService } from 'src/app/Helpers/to-locale-string.service';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { VatInvoice } from 'src/app/models/vat-invoice';
import { CreditCardSummaryService } from 'src/app/services/credit-card-summary/credit-card-summary.service';
import { LoginService } from 'src/app/services/login/login.service';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';
import { VatInvoiceService } from 'src/app/services/vat-invoice/vat-invoice.service';

@Component({
  selector: 'app-tax-invoice',
  templateUrl: './tax-invoice.component.html',
  styleUrls: ['./tax-invoice.component.css']
})
export class TaxInvoiceComponent implements OnInit {
  vatInvoice: VatInvoice=new VatInvoice();
  profileDetails:ProfileDetails=new ProfileDetails();
  statementDate:any;
  constructor(private vatInvoiceService: VatInvoiceService, private loginService: LoginService, private statmentService: StatementSummaryService,
    private cardSummaryService:CreditCardSummaryService, public localeStringService: ToLocaleStringService) {
    
   }

  async ngOnInit() {
    this.profileDetails=this.statmentService.profileDetails;
    if(this.cardSummaryService.selectedDate)
    {
      this.statementDate=this.cardSummaryService.selectedDate
      this.vatInvoice = await this.vatInvoiceService.getVatDetails(this.loginService.getCif(), this.cardSummaryService.selectedDate);
    }
   else {
    this.statementDate=this.statmentService.profileDetails.availableCardStatements[0].statementDate;
    this.vatInvoice = await this.vatInvoiceService.getVatDetails(this.loginService.getCif(), this.statmentService.profileDetails.availableCardStatements[0].statementDate);
    }
  
    

  }

}
