import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';

import { LoginService } from '../../services/login/login.service'

import { User } from 'src/app/models/user';
import { CreditCardTransactionService } from 'src/app/services/credit-card-transaction/credit-card-transaction.service';
import { AcceptOnlyNumberService } from 'src/app/Helpers/accept-only-number.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { InsuranceDetailsService } from 'src/app/services/insurance-details/insurance-details.service';
import { GetQuerystringValuesService } from 'src/app/Helpers/get-querystring-values.service';
import { GetCifByidService } from 'src/app/services/get-cif-byid/get-cif-byid.service';
import { MobileUser } from 'src/app/models/mobile-user';
import { CmsAuthService } from 'src/app/services/cms-auth/cms-auth.service';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { PersonalLoanService } from 'src/app/services/personal-loan/personal-loan.service';

//Restricts input for the given textbox to the given inputFilter function.

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],

})

export class LoginComponent implements OnInit {

  login: User = new User();
  loginMobile: MobileUser = new MobileUser();
  isEnabled = false;
  isEnabledMobile = true;
  queryStringPrams;
  isMobileScren;
  isQueryString;
  isNoQueryString;
  isQueryStringMessage;
  hide: string;
  profileDetails: ProfileDetails = new ProfileDetails();
    // initializing the status to false 
    status: boolean = false;
  constructor(private _router: Router, private loginService: LoginService,
    private statementService: StatementSummaryService, private creditCardTransactionService: CreditCardTransactionService
    , private acceptNumbersServices: AcceptOnlyNumberService, private insuranceDetailsService: InsuranceDetailsService
    , private breakpointObserver: BreakpointObserver, private getQueryStingService: GetQuerystringValuesService
    , private getCifByIdService: GetCifByidService,private cmsAuthService:CmsAuthService, private plService: PersonalLoanService) {
    
      this.queryStringPrams = JSON.parse(JSON.stringify(this.getQueryStingService.getQueryStringParameters()));
      breakpointObserver.observe([
        '(max-width: 700px)'
      ]).subscribe(result => {
        if (result.matches) {
          this.isMobileScren = true;
        } else {
          this.isMobileScren = false;
        }
      });
  }
  errorMessage: string = ''

  model = {
    cifid: '',
    password: ''
  };
  response = {
    stauts: '',
    description: '',
    cifNumber: ''

  }
  isUserExist: any;

  async ngOnInit() {

    if (this.queryStringPrams.cust_id != undefined) {
      setTimeout(() => { (<HTMLParagraphElement>document.getElementById('spinner')).removeAttribute('hidden') }, 0);

      this.isQueryStringMessage = '';

      this.isQueryString = true;
      this.isNoQueryString = false;
      await this.cmsAuthService.getCmsToken();
      this.loginMobile = await this.getCifByIdService.userLogIn(this.queryStringPrams.cust_id);
      if (this.loginMobile.error.errorDesc != 'error' && this.loginMobile.status === 'Success') {
        this.loginService.setCif(this.loginMobile.cifNumber);
        await this.statementService.getStatementSummary(this.loginService.getCif());
        if (this.statementService.profileDetails.error.errorDesc === 'error') {

          setTimeout(() => {
            this.isQueryStringMessage = "😞 Oops!! Something went wrong while processing your request."; (<HTMLDivElement>document.getElementById('spinner')).setAttribute('hidden', '');
          }, 0);

        }
        else {

          setTimeout(() => { (<HTMLParagraphElement>document.getElementById('spinner')).setAttribute('hidden', ''); this._router.navigate(['/statement/home']); }, 0);

        }

      } else if (this.loginMobile.error.errorDesc != 'error' && this.loginMobile.status === 'Failure') {

        setTimeout(() => {
          this.isQueryStringMessage = "Customer not found."; (<HTMLParagraphElement>document.getElementById('spinner')).setAttribute('hidden', '');
        }, 0);
      }
      else if (this.loginMobile.error.errorDesc == 'error') {

        setTimeout(() => {
          this.isQueryStringMessage = "😞 Oops!!  Something went wrong while processing your request.";
          (<HTMLParagraphElement>document.getElementById('spinner')).setAttribute('hidden', '');
        }, 0);
      }

    }
    else {
      this.isQueryString = false;
      this.isNoQueryString = true;
      setTimeout(() => {
        this.breakpointObserver.observe([
          '(max-width: 850px))'
        ]).subscribe(result => {
          if (result.matches) {
            this.isMobileScren = true;
            this.acceptNumbersServices.acceptOnlyNumbers(document.getElementById("inputPwd"), function (value) {
              return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
          } else {
            this.isMobileScren = false;
            this.acceptNumbersServices.acceptOnlyNumbers(document.getElementById("inputPwd"), function (value) {
              return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
          }
        });
      })
    }
  }

  async customerLogin(password: string) {
    // this.login=new User();
      if (password && password.length === 12) {
  
        // changing the status
        this.status = !this.status;
        this.errorMessage = '';
        this.loginService.loggedOnUser=new User();
        this.statementService.profileDetails=new ProfileDetails();
        // this.login.error.errorDesc='';
        // this.login.status='';

        this.isEnabled = true;
        await this.cmsAuthService.getCmsToken();
        this.loginService.loggedOnUser= await this.loginService.userLogIn(password);
        if (this.loginService.loggedOnUser.error.errorDesc  != 'error' && this.loginService.loggedOnUser.status.toLowerCase() === 'success') {
          await this.statementService.getStatementSummary(this.loginService.getCif());
          //console.log(this.statementService.profileDetails);
          this.statementService.profileDetails.personalLoanSummary = await this.plService.getLoanDetails(this.loginService.getCif(),this.statementService.profileDetails.statementDetils.statementDate);// this.statementService.profileDetails.statementDetils.statementDate
          if (this.statementService.profileDetails.error.errorDesc === 'error' ) {
            this.isEnabled = false;
            this.errorMessage = "Something went wrong while processing your request.";
             // changing the status
            this.status = !this.status;
          }
          else {
            this.isEnabled = false;
            this._router.navigate(['/statement/home']);
          }
  
        }
         if (this.loginService.loggedOnUser.error.errorDesc === 'error') {
        
          this.isEnabled = false;
           // changing the status
          this.status = !this.status;
          this.errorMessage = "Something went wrong while processing your request.";
        }
        if(this.loginService.loggedOnUser.status.toLowerCase()==='failed')  {
  
          this.errorMessage = "Password is incorrect";
          this.isEnabled = false;
           // changing the status
          this.status = !this.status;
        }
  
      }
  
      else {
        if (password.length > 0 && password.length < 12) {
          this.errorMessage = "Password is incorrect";
        }
        else
          this.errorMessage = "Password must not be empty.";
  
  
      }
  
      //this.isEnabled=false;
    }

  onpwdChange(searchValue: string): void {
    if (searchValue) {
      this.errorMessage = '';
    }
  }
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
