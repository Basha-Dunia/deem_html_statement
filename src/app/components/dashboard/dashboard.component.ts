import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { throwError } from 'rxjs';
import { RewardDetails } from 'src/app/models/reward-details';
import { CreditCardSummaryService } from 'src/app/services/credit-card-summary/credit-card-summary.service';
import { RewardDetailsService } from 'src/app/services/reward-details/reward-details.service';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css','./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  rewardDetails:RewardDetails=new RewardDetails();
  airlines:any;
  hotels:any;
  entertainments:any;
  otherSpend:any;
  rewards:any=[];
  circumference:any;
  rewardSummary:any=[];
  dataSourceRewards:MatTableDataSource<any>;
  dataSourceRewardSummary:MatTableDataSource<any>;
  displayedColumnsRewards: string[] = ['categories', 'amountSpent', 'rewardEarned'];
  displayedColRewardSummary:string[]=['openingBalance','earned','purchased','redeemed','availBalance']
  constructor(private rdService:RewardDetailsService,private statementService:StatementSummaryService
    ,private cardSummaryService: CreditCardSummaryService,) { 
    this.airlines=11;
    this.hotels=9;
    this.entertainments=28;
    this.otherSpend=52;
    this.circumference=100;
    this.rewards=[
      {
        categories:"Airlines",
        amountSpent:"AED 1,206.94",
        rewardEarned:"AED 12.69",
      
      },
      {
        categories:"Hotels",
        amountSpent:"AED 980.12",
        rewardEarned:"AED 49.01",
      
      },
      {
        categories:"Entertainment",
        amountSpent:"AED 3,108.92",
        rewardEarned:"AED 155.45",
      
      },
      {
        categories:"Other Spends",
        amountSpent:"AED 5,893.82",
        rewardEarned:"AED AED 155.45",
      
      },
      {
        categories:"Bonus Rewards",
        amountSpent:"---",
        rewardEarned:"AED 72.94",
      
      },
    ];
    this.rewardSummary=[{
      openingBalance:'---',
      earned:'---',
      purchased:'---',
      redeemed:'---',
      availBalance:'---'

    }]
  }

async  ngOnInit() {
    this.dataSourceRewards=new MatTableDataSource(this.rewards);
    this.dataSourceRewardSummary=new MatTableDataSource(this.rewardSummary);
    var stmtDate
    if (this.cardSummaryService.selectedDate) {
      stmtDate = this.cardSummaryService.selectedDate;
    }
    else {
      stmtDate = this.statementService.profileDetails.statementDetils.statementDate;
    }
    this.rewardDetails=await this.rdService.getRewardDetails(this.statementService.profileDetails.userDetails.cifNumber,stmtDate);
    this.airlines=this.rewardDetails.airlinesSpentPer >0 ? this.rewardDetails.airlinesSpentPer : 0.0001;
    this.hotels=this.rewardDetails.hotelsSpentPer >0 ? this.rewardDetails.hotelsSpentPer : 0.0001;
    this.entertainments=this.rewardDetails.entertainmentSpentPer >0 ? this.rewardDetails.entertainmentSpentPer : 0.0001;
     this.otherSpend=this.rewardDetails.othersSpentPer >0 ? this.rewardDetails.othersSpentPer : 0.0001;
    

  }

}
