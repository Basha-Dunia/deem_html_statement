import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  isMobileScren;
  constructor(private breakpointObserver: BreakpointObserver,) {
    breakpointObserver.observe([
      '(max-width: 768px)'
    ]).subscribe(result => {
      if (result.matches) {
        this.isMobileScren = true;
      } else {
        this.isMobileScren = false;
      }
    });
   }

  ngOnInit(): void {
  }

}
