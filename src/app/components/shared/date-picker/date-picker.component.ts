import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
export class DatePickerComponent implements OnInit {

  @Output() onChange = new EventEmitter();
    dateFilter: string;
  constructor() { }

  ngOnInit(): void {
    
  }
  change() {
    this.onChange.emit({value: this.dateFilter});
}
}
