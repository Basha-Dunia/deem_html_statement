import { Component, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { LoginService } from 'src/app/services/login/login.service';
import { Router } from '@angular/router';
import { CreditCardTransactionService } from 'src/app/services/credit-card-transaction/credit-card-transaction.service';
import { User } from 'src/app/models/user';
import { TransactionDetails } from 'src/app/models/transaction-details';
import { VatInvoiceService } from 'src/app/services/vat-invoice/vat-invoice.service';
import { VatInvoice } from 'src/app/models/vat-invoice';
import { PersonalLoanService } from 'src/app/services/personal-loan/personal-loan.service';
import { PersonalLoanSummary } from 'src/app/models/personal-loan-summary';
import { CreditCardSummaryService } from 'src/app/services/credit-card-summary/credit-card-summary.service';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import { InsuranceDetailsService } from 'src/app/services/insurance-details/insurance-details.service';
import { InsuranceDetails } from 'src/app/models/insurance-details';
import { GetCifByidService } from 'src/app/services/get-cif-byid/get-cif-byid.service';
import { MobileUser } from 'src/app/models/mobile-user';
@Component({
  selector: 'app-side-nav-bar',
  templateUrl: './side-nav-bar.component.html',
  styleUrls: ['./side-nav-bar.component.css', './side-nav-bar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  // animations: [
  //   trigger('indicatorRotate', [
  //     state('collapsed', style({transform: 'rotate(0deg)'})),
  //     state('expanded', style({transform: 'rotate(180deg)'})),
  //     transition('expanded <=> collapsed',
  //       animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
  //     ),
  //   ])
  // ]
})
export class SideNavBarComponent implements OnInit {

  profileDetails: ProfileDetails = new ProfileDetails();
  panelOpenState = false;
  creditCardMenuText = '';
  isMobileScren;
  isTaxInvoice;
  @Output() sidenavClose = new EventEmitter();
  constructor(private _router: Router, private loginService: LoginService,
    private statementService: StatementSummaryService,
    private trnsactionService: CreditCardTransactionService, private taxInvoiceService: VatInvoiceService
    , private plService: PersonalLoanService, private creditCardSummaryService: CreditCardSummaryService,
    private breakpointObserver:BreakpointObserver,private insuranceDetailsService:InsuranceDetailsService,
    private creditCardTransactionService: CreditCardTransactionService, private getCifByidService:GetCifByidService ) {

      breakpointObserver.observe([
        '(max-width: 768px)'
          ]).subscribe(result => {
            if (result.matches) {
              this.isMobileScren=true;
            } else {
              this.isMobileScren=false;
            }
          });
  }

  async ngOnInit() {
    this.profileDetails = this.statementService.profileDetails;
    if(this.profileDetails.personalLoanSummary || this.profileDetails.creditCardDetails.length>0)
    {
      this.isTaxInvoice=true;
     
    }
    else
    {
      this.isTaxInvoice=false;
    }
    if (this.creditCardSummaryService.cardSummaryDetails.length > 0) {
      this.profileDetails.creditCardDetails = this.creditCardSummaryService.cardSummaryDetails;
    }
    else {
      this.profileDetails.creditCardDetails = this.statementService.profileDetails.creditCardDetails;
    //this.profileDetails.noOfAvailableCards
    }
    // if (this.plService.presonlaLoan) {
    //   this.profileDetails.personalLoanSummary = this.plService.presonlaLoan;
     

    // }
    // else {
    //   this.profileDetails.personalLoanSummary = this.statementService.profileDetails.personalLoanSummary;
    // }
    if(this.profileDetails.creditCardDetails.length>0)
    {

    this.creditCardMenuText = `${this.profileDetails.creditCardDetails[0].cardType + " - " + this.profileDetails.creditCardDetails[0].primaryCardNumber.substr(this.profileDetails.creditCardDetails[0].primaryCardNumber.length - 6)}`
    }
  }
  getPrimaryCard(cardType:string,cardNumber:string){
    if(this.profileDetails.noOfAvailableCards.length>0)
    {
    //  if(this.profileDetails.creditCardDetails[0].primaryCardNumber !=cardNumber)
    //  {
      if(cardType.toLowerCase().includes('supplementary'))
      {
   return `${"Supplementary"+ " - " + cardNumber.substr(cardNumber.length - 6)}`
      }
      else
      {
        return `${this.profileDetails.creditCardDetails[0].cardType +  " - " + cardNumber.substr(cardNumber.length - 6)}`
      }
     //}
    }
  }
  logOut() {
    // this.loginService.loggedOnUser = new User();
    // this.statementService.profileDetails.creditCardDetails.length = 0;
    // this.statementService.profileDetails.availableLoanStatements.length = 0;
    // this.statementService.profileDetails.availableCardStatements.length = 0
    // this.trnsactionService.creditCardTransaction.transactions.length = 0
    // this.statementService.profileDetails = new ProfileDetails();
    // this.trnsactionService.transactionData = new TransactionDetails();
    // this.taxInvoiceService.vatInvoice = new VatInvoice();
    // this.plService.presonlaLoan = new PersonalLoanSummary();
    // this.insuranceDetailsService.insuranceDetails=new InsuranceDetails();
    // this.creditCardSummaryService.selectedDate=null;
    // this.creditCardSummaryService.cardSummaryDetails.length = 0
    // this.getCifByidService.loggedOnUserMob=new MobileUser();
    // this.loginService.setCif(null);
    localStorage.clear();
    sessionStorage.clear();
    location.reload();
    //this._router.navigate(['/login']);
  }
  public async  onSidenavClose (cardNumber:string) {
    if(cardNumber)
    {
      if(this.creditCardSummaryService.selectedDate)
      {
        
       await this.creditCardTransactionService.creditCardTransactionData(cardNumber.replace('XXXXXXXX','00000000'), this.creditCardSummaryService.selectedDate,this.loginService.getCif());
      }
      else
      {
      
       await this.creditCardTransactionService.creditCardTransactionData(cardNumber.replace('XXXXXXXX','00000000'), this.statementService.profileDetails.statementDetils.statementDate,this.loginService.getCif());
      }
    
    // this._router.navigate(['/statement/credit-card'])
    }
     this.sidenavClose.emit();
   }
}
