import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { element } from 'protractor';
import { EppCalculator } from 'src/app/models/epp-calcultor-model';
import { LoanCalculator } from 'src/app/models/loan-calculator-model';
import { AcceptOnlyNumberService } from 'src/app/Helpers/accept-only-number.service';
import { ToLocaleStringService } from 'src/app/Helpers/to-locale-string.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css'],
  // encapsulation: ViewEncapsulation.None
})

export class CalculatorComponent implements OnInit {
  eppErrorMessage: string;
  loanErrorMessage: string;
  isEnabled: false;
  eppTenors = [
    {
      id: 3,
      value: 3
    },
    {
      id: 6,
      value: 6
    },
    {
      id: 9,
      value: 9
    },
    {
      id: 12,
      value: 12
    },

  ];
  loanTenors = [
    {
      id: 12,
      value: 12
    },
    {
      id: 24,
      value: 24
    },
    {
      id: 36,
      value: 36
    },
    {
      id: 48,
      value: 48
    },

  ]
  eppCalculator: EppCalculator;
  loanCalculator: LoanCalculator;
  monthlyInstalment: any;
  monthlyInstalmentLoan: any;
  autoTicks = false;
  disabled = false;
  invert = false;
  max = 100000;
  min = 0;
  showTicks = false;
  step = 1000;
  thumbLabel = true;
  value = 0;
  vertical = false;
  tickInterval = 1;
  requestedAmount:number=0;
  constructor(private acceptNumbers:AcceptOnlyNumberService,public localeStringService: ToLocaleStringService) {
    this.eppCalculator = new EppCalculator();
    this.loanCalculator = new LoanCalculator();
    
   
  }

  ngOnInit(): void {
   
      const elements = ['txtreqrate', 'txtfees', 'txtreqamt1', 'txtroi'];
     
  }
  onLinkClick(event: MatTabChangeEvent) {


    if (event.index == 0) {

     
    }
    
    else if(event.index == 1) {
      setTimeout(() => {
        this.acceptNumbers.acceptOnlyNumbers(document.getElementById("txtreqamt1"), function (value) {
          return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
      }, 0);
      setTimeout(() => {
        this.acceptNumbers.acceptOnlyNumbers(document.getElementById("txtroi"), function (value) {
          return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
      }, 0);
    }
  }
 
  getSliderTickInterval(value) {
    if (value) {
      this.loanErrorMessage = '';
    }

    return 0;
  }

  updateSetting(event) {
    if (event.value) {
      
      this.loanErrorMessage = '';
    }
    this.requestedAmount=event.value;
   
    return 0;
  }
  
  formatLabel(value: number) {
   
    return value.toLocaleString();
  }

  calculateEpp(eppObj) {
    if (eppObj.eppAmount && eppObj.tenor && (eppObj.monthlyFlatInterestRate && eppObj.monthlyFlatInterestRate>0 )) {
      this.eppErrorMessage = '';
      var chk = ((eppObj.eppAmount / parseInt(eppObj.tenor)) + ((eppObj.monthlyFlatInterestRate / 100) * eppObj.eppAmount));
      this.eppCalculator.eppMonthlyInstallment = chk.toFixed(2);
    }
    else if (!eppObj.eppAmount || !eppObj.tenor || !eppObj.monthlyFlatInterestRate) {
      this.eppErrorMessage = 'Amount, Tenor and Rate of Interest must not be empty.';
      
    }
    else
    {
      this.eppCalculator.eppMonthlyInstallment='';
    }
  }
  calculateLoan(loanObj) {
    if (loanObj.loanTenor && (loanObj.monthlyLoanInterestRate && loanObj.monthlyLoanInterestRate>0)  && loanObj.loanAmount > 0) {
      if (!isNaN(loanObj.monthlyLoanInterestRate)) {
        this.loanCalculator.emi = '';
        this.loanErrorMessage = '';
        let interest = loanObj.monthlyLoanInterestRate / 1200;
        let top = Math.pow(1 + interest, parseInt(loanObj.loanTenor)); //Compute powers
        let bottom = top - 1;
        let ratio = top / bottom;
        let emi = loanObj.loanAmount * interest * ratio;
        this.loanCalculator.emi = emi.toFixed(2);
      }
      else {

        this.loanCalculator.emi = '';
        this.loanErrorMessage = 'Monthly rate must be a number';
      }

    }
    else if (!loanObj.loanTenor || !loanObj.monthlyLoanInterestRate || loanObj.loanAmount === 0) {
      this.loanErrorMessage = 'Amount, Tenor and Rate of Interest must not be empty.';
     
    }
    else
    {
      this.loanCalculator.emi = '';
    }

  }
  onFieldChange(value: string): void {
    if (value) {
      this.eppErrorMessage = '';
      this.loanErrorMessage = ''
    }
  }
  getAmount() {
    return this.eppCalculator.eppAmount != null ? parseInt('' + this.eppCalculator.eppAmount + '').toLocaleString() : '';
  }
  getLoanAmount() {
    return this.loanCalculator.loanAmount != null ? parseInt('' + this.loanCalculator.loanAmount + '').toLocaleString() : '';
  }
  resetEpp(eppObj) {
    eppObj.eppAmount = null;
    eppObj.tenor = null;
    eppObj.monthlyFlatInterestRate = null;
    this.monthlyInstalment = '';


  }
  resetLoan(loanObj) {
    loanObj.laonAmount = null;
    loanObj.loanTenor = null;
    loanObj.monthlyLoanInterestRate = null;
    this.monthlyInstalmentLoan = '';

  }

}
