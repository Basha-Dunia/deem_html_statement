import {
  Component, OnInit, ViewChild,
  ElementRef, Renderer2, ChangeDetectorRef,
} from '@angular/core';
import { CreditCardTransaction } from 'src/app/models/credit-card-transaction-model';
import { PdfService } from 'src/app/services/pdf/pdf.service';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { MatDialog } from '@angular/material/dialog';
import { EppTransactionComponent } from '../epp-transaction/epp-transaction.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { BreakpointObserver } from '@angular/cdk/layout';
import { NavigationEnd, Router } from '@angular/router';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { StatementSummaryService } from 'src/app/services/statement-summary/statement-summary.service';
import { CreditCardTransactionService } from 'src/app/services/credit-card-transaction/credit-card-transaction.service';
import { CreditCardSummaryService } from 'src/app/services/credit-card-summary/credit-card-summary.service';
import { LoginService } from 'src/app/services/login/login.service';
import { CreditCardDetails } from 'src/app/models/CreditCardDetails';
import { FormatDateService } from 'src/app/Helpers/format-date.service';
import { PayNowService } from 'src/app/Helpers/pay-now.service';
import { AcceptOnlyNumberService } from 'src/app/Helpers/accept-only-number.service';
import { CreditCardSummary } from 'src/app/models/credit-card-summary-model';
import { ToLocaleStringService } from 'src/app/Helpers/to-locale-string.service';
import { InstallmentDetail } from 'src/app/models/installment-detail';

pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.css', './credit-card.component.scss'],
  //encapsulation: ViewEncapsulation.None,

})
export class CreditCardComponent implements OnInit {
  @ViewChild('toDateRef') toDateElementRef: ElementRef
  tranactions: CreditCardTransaction;
  cardTransactions = [new CreditCardDetails()];
  installmentDetails = [new InstallmentDetail()];
  creditCardTransaction = new CreditCardTransaction();
  transactionData: any = [];
  installmentPlan: any = [];
  statementPeriodOne: any;
  statementPeriodTwo: any;
  private _searchTerm: string;
  private _searchTermFromDate: string;
  private _searchTermToDate: string; 0
  isMobileScren;
  profileDetails: ProfileDetails = new ProfileDetails();
  creditCardSummary: CreditCardSummary;
  //Data Soruce for mat table
  dataSourceInstallmentPlan: MatTableDataSource<any>;
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['date', 'postingDate', 'description', 'currencyType', 'amount'];
  displayedColumnsPlan: string[] = ['date', 'merchantName', 'originalAmount', 'outstandingAmount', 'installmentPending', 'tenor'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  get searchTerm(): string {
    return this._searchTerm;
  };
  set searchTerm(value: string) {
    this._searchTerm = value;

    this.cardTransactions = this.filterTransaction(value);//new MatTableDataSource(this.filterTransaction(value)) ;

  }

  get searchTermFromDate(): string {
    return this._searchTermFromDate;
  };
  set searchTermFromDate(value: string) {
    this._searchTermFromDate = value;
  }

  get searchTermToDate(): string {
    return this._searchTermToDate;
  };
  set searchTermToDate(value: string) {
    this._searchTermToDate = value;


    this.cardTransactions = this.dateFilterTransaction();
  }

  //Amount
  private _fromAmount: string;
  private _toAmount: string;
  get fromAmount(): string {
    return this._fromAmount;
  };
  set fromAmount(value: string) {
    this._fromAmount = value;
    //this.cardTransactions = this.amountFilterFromTransaction();

  }
  get toAmount(): string {
    return this._toAmount;
  };
  set toAmount(value: string) {
    this._toAmount = value;
   
    //this.cardTransactions = this.amountFilterTransaction();
  }
  filterTransaction(searchString: string) {
    if (this.searchTerm) {
      return this.cardTransactions.filter(trans => trans.description.toLowerCase().indexOf(this.searchTerm.toLowerCase()) !== -1
        || trans.date.toLowerCase().indexOf(this.searchTerm.toLocaleLowerCase()) !== -1 ||
        trans.postingDate.toLowerCase().indexOf(this.searchTerm.toLocaleLowerCase()) !== -1
        || trans.amount.toLocaleString().toLowerCase().indexOf(this.searchTerm.toLocaleLowerCase()) !== -1
        || trans.currencyType.toLowerCase().indexOf(this.searchTerm.toLocaleLowerCase()) !== -1)
    }
    else return this.cardTransactions = this.creditCardTransactionService.creditCardTransaction.transactions;
  }
  parseDate(dateStr) {
    var date = dateStr.split('/');
    var day = date[0];
    var month = date[1] - 1; //January = 0
    var year = date[2];
    return new Date(year, month, day);
  }
  dateFilterTransaction() {
    if (this.searchTermToDate) {
      this.creditCardTransactionService.creditCardTransaction.transactions;
      var fromDate = JSON.parse(JSON.stringify(this.searchTermFromDate))
      var date = fromDate.day < 10 ? '0' + '' + fromDate.day + '' : '' + fromDate.day + '';
      var month = fromDate.month < 10 ? '0' + '' + fromDate.month + '' : '' + fromDate.month + '';
      var newFromDate = `${fromDate.year + '-' + month + '-' + date}`;
      var toDate = JSON.parse(JSON.stringify(this.searchTermToDate))
      var date2 = toDate.day < 10 ? '0' + '' + toDate.day + '' : '' + toDate.day + '';
      var month2 = toDate.month < 10 ? '0' + '' + toDate.month + '' : '' + toDate.month + '';
      var newToDate = `${toDate.year + '-' + month2 + '-' + date2}`;

      return this.cardTransactions.filter(item =>
        new Date(item.date).getTime() >= new Date(newFromDate).getTime()
        && new Date(item.date).getTime() <= new Date(newToDate).getTime()
      );
    }
    else return this.cardTransactions = this.creditCardTransactionService.creditCardTransaction.transactions;

  }

  amountFilterTransaction() {

    if (this.toAmount) {
      this.creditCardTransactionService.creditCardTransaction.transactions;
      return this.cardTransactions.filter(item =>
        item.amount >= parseInt(this.fromAmount) &&
        item.amount <= parseInt(this.toAmount)
      );

    }
    else {
      return this.cardTransactions = this.creditCardTransactionService.creditCardTransaction.transactions;
    }

  }

  //creditCardTransaction = new CreditCardTransaction();
  public selected;
  show: boolean = false;

  constructor(private _router: Router, private pdfService: PdfService,
    public dialog: MatDialog, private breakpointObserver: BreakpointObserver,
    private renderer: Renderer2, public statementService: StatementSummaryService,
    private creditCardTransactionService: CreditCardTransactionService, private loginService: LoginService,
    private cdr: ChangeDetectorRef, private cardSummaryService: CreditCardSummaryService,
    private formatDate: FormatDateService, private payNowService: PayNowService,
    private acceptNumbersServices: AcceptOnlyNumberService, public localeStringService: ToLocaleStringService) {
    this._router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this._router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this._router.navigated = false;
        window.scrollTo(0, 0);
      }
    });
    breakpointObserver.observe([
      '(max-width:1024px)'
    ]).subscribe(result => {
      if (result.matches) {
        this.isMobileScren = true;

      }
    });
    this.installmentPlan = [
      {
        date: "N/A",
        merchantName: "N/A",
        originalAmount: "N/A",
        outstandingAmount: "N/A",
        installmentPending: "N/A",
        tenor: "N/A",
      }
    ]

  }

  ngOnInit() {



    this.profileDetails.creditCardDetails.length = 0;

    this.creditCardSummary = new CreditCardSummary();
    var stmtPeriod
    if (this.cardSummaryService.cardSummaryDetails.length > 0) {
      stmtPeriod = this.cardSummaryService.cardSummaryDetails[0].statementPeriod;
      this.creditCardSummary = this.cardSummaryService.cardSummaryDetails[0];
    }
    else {

      stmtPeriod = this.statementService.profileDetails.statementDetils.statementPeriod;
      this.creditCardSummary = this.statementService.profileDetails.creditCardDetails[0];

    }
    if (stmtPeriod) {
      this.statementPeriodOne = this.formatDate.dateFormat1(stmtPeriod.slice(0, 10));
      this.statementPeriodTwo = this.formatDate.dateFormat1(stmtPeriod.slice(13, 25));
    }
    if (this.profileDetails.availableCardStatements.length > 0) {

      this.selected = this.profileDetails.availableCardStatements[0].statementDate;

    }

    else {
      this.selected = "No Records";
    }

    this.creditCardTransactionService.creditCardTransaction.installmentDetails.length = 0;
    this.creditCardTransactionService.creditCardTransaction.transactions.length = 0
    this.cardTransactions = this.creditCardTransactionService.creditCardTransaction.transactions;
    this.installmentDetails = this.creditCardTransactionService.creditCardTransaction.installmentDetails;

var validate=function   validate(value){    
  var re = /^[A-Za-z ]+$/;
  if(re.test(value))
  {
    //
  }
   
  else
  {
//
  }
 //
}
    var getCellValue = function(tr, idx){ return tr.children[idx].innerText || tr.children[idx].textContent; }

var comparer = function(idx, asc) { return function(a, b) { return function(v1, v2) {
 

        return v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2);
    }
    
    (getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
}};

// do the work...
Array.prototype.slice.call(document.querySelectorAll('th')).forEach(function(th,id) {id++; if(id!=4 && id!=6 && id!=7  && id!=8 && id!=9){ th.addEventListener('click', function() {
        var table = th.parentNode
        while(table.tagName.toUpperCase() != 'TABLE') table = table.parentNode;
        Array.prototype.slice.call(table.querySelectorAll('tr:nth-child(n+2)'))
            .sort(comparer(Array.prototype.slice.call(th.parentNode.children).indexOf(th), this.asc = !this.asc))
            .forEach(function(tr) { table.appendChild(tr) });
    })
  }
});

   // this.dataSourceInstallmentPlan = new MatTableDataSource(this.installmentPlan);

  }
  ngOnChanges() {

  }
  async ngAfterViewChecked() {

  }

  async selectPeriod(stmtDate) {
    this.selected = '';
    this.profileDetails.creditCardDetails.length = 0
    this.profileDetails.creditCardDetails = await this.cardSummaryService.getCreditCardSummery(this.loginService.getCif(), stmtDate)
    var stmtPeriod = this.profileDetails.creditCardDetails[0].statementPeriod;
    this.statementPeriodOne = this.formatDate.dateFormat1(stmtPeriod.slice(0, 10));
    this.statementPeriodTwo = this.formatDate.dateFormat1(stmtPeriod.slice(13, 25));
    this.cardTransactions.length = 0;
    this.tranactions = new CreditCardTransaction()
    this.tranactions = await this.creditCardTransactionService.creditCardTransactionData(this.statementService.profileDetails.creditCardDetails[0].primaryCardNumber.replace('XXXXXXXX', '00000000'), stmtDate, this.loginService.getCif());
    this.cardTransactions = this.tranactions.transactions;


  }

  payNow() {

    var productId = this.statementService.profileDetails.creditCardDetails[0].primaryCardNumber//this.profileDetails.creditCardDetails[0].primaryCardNumber;
    var minimumDueAmount = this.statementService.profileDetails.creditCardDetails[0].minimumPaymentDue.toLocaleString();
    var totalDueAmount = this.statementService.profileDetails.creditCardDetails[0].totalPaymentDue.toLocaleString();
    var cif = this.loginService.getCif();
    this.payNowService.payNow('CreditCard', productId, minimumDueAmount, totalDueAmount, cif)
  }
  openEpp(): void {
    const dialogRef = this.dialog.open(EppTransactionComponent, {
      width: '75%',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {

    });

  }

  public getBrowserName() {
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        return 'edge';
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        return 'opera';
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        return 'chrome';
      case agent.indexOf('trident') > -1:
        return 'ie';
      case agent.indexOf('firefox') > -1:
        return 'firefox';
      case agent.indexOf('safari') > -1:
        return 'safari';
      default:
        return 'other';
    }
  }
  resetFields() {
    this.searchTerm = '';
    this.searchTermFromDate = '';
    this.searchTermToDate = '';
    this.fromAmount = '';
    this.toAmount = '';
    this.cardTransactions = this.creditCardTransactionService.creditCardTransaction.transactions;


    //this.renderer.setValue(this.toDateElementRef.nativeElement,'');
  }
  onFieldChangeDate(): void {

    this.fromAmount = '';
    this.toAmount = '';
    this.searchTerm = '';

  }
  onFieldChangeAmount() {

    (<HTMLInputElement>document.getElementById('frmDate')).value = '';
    (<HTMLInputElement>document.getElementById('toDate')).value = '';
    (<HTMLInputElement>document.getElementById('anyKey')).value = '';
  }
  onFieldChangeAnyKey() {

    (<HTMLInputElement>document.getElementById('frmDate')).value = '';
    (<HTMLInputElement>document.getElementById('toDate')).value = '';
    (<HTMLInputElement>document.getElementById('frmAmount')).value = '';
    (<HTMLInputElement>document.getElementById('toAmount')).value = '';

  }


  sendDataToEpp(element) {

    localStorage.setItem('eppData', JSON.stringify(element));
    // this.creditCardEppTransactionInteractionServiceService.getEppTransactionData(element);
  }
  hide() {
    this.show = !this.show ? true : false;
  }
  filteredAmountTrans() {

    if (this.toAmount) {
      this.searchTerm = '';
      this.searchTermFromDate = '';
      this.searchTermToDate = '';
      this.cardTransactions = this.creditCardTransactionService.creditCardTransaction.transactions;
      this.cardTransactions = this.cardTransactions.filter(el => {
        return el.amount >= parseInt(this.fromAmount)
          && el.amount <= parseInt(this.toAmount)
      });

    }
    else {

      return this.cardTransactions = this.creditCardTransactionService.creditCardTransaction.transactions;

    }
  }
  getCrColor(flag) {

    return flag == 'CR' ? '#6E3DEA' : '';
  }
  checkInstalmentValues(value) {
    return value != undefined ? value : 'N/A';
  }
  formatNumber(number)
  {
    return number != null ? parseFloat('' + number + '').toLocaleString() : '0.00';
  }
}