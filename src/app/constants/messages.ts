export class Messages {
    public static baseUrl:string = "http://localhost:8000/";
    //public static baseUrl:string = "https://myproduction.site.com/";
    public static countries = ["AD","AE","AF"];
    public static logInErrr='Field must not be empty';

   public static  baseUrlMule= "https://prodstat.deem.io/statementmuleapi";
   public static  baseUrlStrapi= "https://prodstat.deem.io/statementcmsapi";
}