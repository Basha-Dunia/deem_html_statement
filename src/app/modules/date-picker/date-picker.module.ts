
import { NgModule } from '@angular/core';

import {  NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateCustomParserFormatter } from '../../date-format';



@NgModule({
  imports: [
    NgbModule
  ],
  
  exports:[
    NgbModule
  ],
  providers:[ {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}]
})
export class DatePickerModule { }
