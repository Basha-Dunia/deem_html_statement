import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeLayoutComponent } from 'src/app/components/home-layout/home-layout.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { PaymentOptionsComponent } from 'src/app/components/payment-options/payment-options.component';
import { OffersComponent } from 'src/app/components/offers/offers.component';
import { TermsComponent } from 'src/app/components/terms/terms.component';
import { CalculatorComponent } from 'src/app/components/calculator/calculator.component';
import { CreditCardComponent } from 'src/app/components/credit-card/credit-card.component';
import { LoanComponent } from 'src/app/components/loan/loan.component';
import { EppTransactionComponent } from 'src/app/components/epp-transaction/epp-transaction.component';
import { EppThankYouComponent } from 'src/app/components/epp-transaction/epp-thank-you/epp-thank-you.component';
import { TaxInvoiceComponent } from 'src/app/components/tax-invoice/tax-invoice.component';
//import { CreditCardTransactionComponent } from '../app/components/credit-card-transaction/credit-card-transaction.component';

import { ErrorComponent } from 'src/app/core/error/error.component';
import {AuthGuardService} from 'src/app/auth-guard/auth-guard.service'
import { PageNotFoundComponent } from 'src/app/core/page-not-found/page-not-found.component';

const routes: Routes = [
 // { path: 'calculator', component: CalculatorComponent},
   {
     path: '', component: HomeLayoutComponent, canActivate: [AuthGuardService],children: [
   // {path: '', component: HomeLayoutComponent, canActivate: [AuthGuardService]},
      { path: 'home', component: HomeComponent,canActivate: [AuthGuardService] },
      { path: 'dashboard', component: DashboardComponent,canActivate: [AuthGuardService] },
      { path: 'payment-options', component: PaymentOptionsComponent,canActivate: [AuthGuardService] },
      { path: 'offers', component: OffersComponent,canActivate: [AuthGuardService] },
      { path: 'terms', component: TermsComponent,canActivate: [AuthGuardService] },
      { path: 'calculator', component: CalculatorComponent, canActivate: [AuthGuardService] },
      { path: 'credit-card', component: CreditCardComponent,canActivate: [AuthGuardService] },
      { path: 'loan', component: LoanComponent,canActivate: [AuthGuardService] },
     // { path: 'credit-card-transaction', component: CreditCardTransactionComponent,canActivate: [AuthGuardService] },
      { path: 'error', component: ErrorComponent,canActivate: [AuthGuardService] },
      {path: 'epp', component: EppTransactionComponent,canActivate: [AuthGuardService]},
      {path: 'epp-thank-you', component: EppThankYouComponent,canActivate: [AuthGuardService]},
      {path: 'tax-invoice', component: TaxInvoiceComponent,canActivate: [AuthGuardService]},
    
     ]}
     
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeComponentLayoutRoutingModule { }
