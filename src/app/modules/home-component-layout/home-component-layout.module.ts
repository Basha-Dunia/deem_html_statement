import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {HomeLayoutComponent} from 'src/app/components/home-layout/home-layout.component';
import {HomeComponent} from 'src/app/components/home/home.component';
import {SideNavBarComponent} from 'src/app/components/shared/side-nav-bar/side-nav-bar.component';
import {HeaderComponent} from 'src/app/components/shared/header/header.component';
import {FooterComponent} from 'src/app/components/shared/footer/footer.component';
import {CreditCardComponent} from 'src/app/components/credit-card/credit-card.component';
import {LoanComponent} from 'src/app/components/loan/loan.component';
import {CalculatorComponent} from 'src/app/components//calculator/calculator.component';
import {DashboardComponent} from 'src/app/components//dashboard/dashboard.component';
import {OffersComponent} from 'src/app/components//offers/offers.component';
import {OffersPopupComponent} from 'src/app/components//offers/offers-popup/offers-popup.component';
import {TermsComponent} from 'src/app/components//terms/terms.component';
import {PaymentOptionsComponent} from 'src/app/components//payment-options/payment-options.component';
import {TaxInvoiceComponent} from 'src/app/components//tax-invoice/tax-invoice.component';
import {EppTransactionComponent} from 'src/app/components//epp-transaction/epp-transaction.component';
import {EppThankYouComponent} from 'src/app/components//epp-transaction/epp-thank-you/epp-thank-you.component';
import {ErrorComponent} from '../../../app/core/error/error.component'
import { FlexLayoutModule } from '@angular/flex-layout';
import { HomeComponentLayoutRoutingModule } from './home-component-layout-routing.module';
import { MaterialModule } from '../../material/material.module';
import { MarkedPipe } from 'src/app/pipes/marked.pipe';
import { BnNgIdleService } from 'bn-ng-idle'; 
import { NavService } from 'src/app/helper-services/nav/nav.service';
@NgModule({
  declarations: [
    HomeLayoutComponent,HomeComponent,SideNavBarComponent,HeaderComponent,FooterComponent,
      CreditCardComponent,LoanComponent,CalculatorComponent,DashboardComponent,OffersComponent,OffersPopupComponent,
    TermsComponent,PaymentOptionsComponent,TaxInvoiceComponent,EppThankYouComponent,EppTransactionComponent,ErrorComponent,MarkedPipe],
  imports: [
    CommonModule,
    
    FormsModule,
    MaterialModule,
    HomeComponentLayoutRoutingModule,
    FlexLayoutModule
  ],
  providers: [
    DatePipe,
    NavService,
    BnNgIdleService
  ],
  exports:[
   
   // MaterialModule
  ]
})
export class HomeComponentLayoutModule { }
