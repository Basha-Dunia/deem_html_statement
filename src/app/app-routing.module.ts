import { NgModule } from '@angular/core';
import { Routes, RouterModule,PreloadAllModules } from '@angular/router';
// import { LoginComponent } from '../app/components/login/login.component';
import{HomeComponentLayoutModule} from'src/app/modules/home-component-layout/home-component-layout.module'
import { HomeLayoutComponent } from 'src/app/components/home-layout/home-layout.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { PaymentOptionsComponent } from 'src/app/components/payment-options/payment-options.component';
import { OffersComponent } from 'src/app/components/offers/offers.component';
import { TermsComponent } from 'src/app/components/terms/terms.component';
import { CalculatorComponent } from 'src/app/components/calculator/calculator.component';
import { CreditCardComponent } from 'src/app/components/credit-card/credit-card.component';
import { LoanComponent } from 'src/app/components/loan/loan.component';
//import { CreditCardTransactionComponent } from '../app/components/credit-card-transaction/credit-card-transaction.component';
import { LoginComponent } from '../app/components/login/login.component';
import { ErrorComponent } from '../app/core/error/error.component';
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component';
import {AuthGuardService} from './auth-guard/auth-guard.service'
import { EppTransactionComponent } from 'src/app/components/epp-transaction/epp-transaction.component';
import { EppThankYouComponent } from 'src/app/components/epp-transaction/epp-thank-you/epp-thank-you.component';
import { TaxInvoiceComponent } from 'src/app/components/tax-invoice/tax-invoice.component';
import { from } from 'rxjs';

//import { TestComponent } from './components/test/test.component';
const routes: Routes = [
  { path: 'login', component: LoginComponent, },
  
  { path: '404', component: PageNotFoundComponent },
  { path: 'login/:cust_id', component: LoginComponent },
    // {path: 'epp', component: EppTransactionComponent},
  // {path: 'pdf', component: TestPdfComponent},
   //{path: 'test', component: TestComponent},
   //{ path: 'calculator', component: CalculatorComponent},
  // {
  //   path: 'statement', component: HomeLayoutComponent, canActivate: [AuthGuardService],children: [
  //     { path: 'home', component: HomeComponent,canActivate: [AuthGuardService] },
  //     { path: 'dashboard', component: DashboardComponent,canActivate: [AuthGuardService] },
  //     { path: 'payment-options', component: PaymentOptionsComponent,canActivate: [AuthGuardService] },
  //     { path: 'offers', component: OffersComponent,canActivate: [AuthGuardService] },
  //     { path: 'terms', component: TermsComponent,canActivate: [AuthGuardService] },
  //     { path: 'calculator', component: CalculatorComponent, canActivate: [AuthGuardService] },
  //     { path: 'credit-card', component: CreditCardComponent,canActivate: [AuthGuardService] },
  //     { path: 'loan', component: LoanComponent,canActivate: [AuthGuardService] },
  //    // { path: 'credit-card-transaction', component: CreditCardTransactionComponent,canActivate: [AuthGuardService] },
  //     { path: 'error', component: ErrorComponent,canActivate: [AuthGuardService] },
  //     {path: 'epp', component: EppTransactionComponent,canActivate: [AuthGuardService]},
  //     {path: 'epp-thank-you', component: EppThankYouComponent,canActivate: [AuthGuardService]},
  //     {path: 'tax-invoice', component: TaxInvoiceComponent,canActivate: [AuthGuardService]},

  //   ]
  // },
  {
    path:'statement',
    loadChildren:'src/app/modules/home-component-layout/home-component-layout.module#HomeComponentLayoutModule'
  },
 
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{preloadingStrategy:PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
