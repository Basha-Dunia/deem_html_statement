import { NgModule } from '@angular/core';
// import { MatFormFieldModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
//import { MatButtonModule } from '@angular/material/button';
//import { MatIconModule } from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
//import {MatTooltipModule} from '@angular/material/tooltip';
//import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
//import {MatCheckboxModule} from '@angular/material/checkbox';
//import {MatGridListModule} from '@angular/material/grid-list';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';
//import {MatCardModule} from '@angular/material/card';
//import { MatNativeDateModule } from '@angular/material/core';
//import {MatDatepickerModule}from '@angular/material/datepicker'
import{MatSidenavModule} from '@angular/material/sidenav';
import{MatDividerModule} from '@angular/material/divider';
import{MatListModule} from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
// import {MatTableModule} from '@angular/material/table';
// import {MatPaginatorModule} from '@angular/material/paginator';
// import {MatSortModule} from '@angular/material/sort';
import {MatSliderModule} from '@angular/material/slider';
//import { MatMomentDateModule } from "@angular/material-moment-adapter";

const MaterialComponents=[
  //MatFormFieldModule,
  MatToolbarModule,
  //MatButtonModule,
  //MatIconModule,
  //MatTooltipModule,
  MatProgressSpinnerModule,
  //MatInputModule,
  MatSelectModule,
  //MatGridListModule,
  //MatCheckboxModule,
  MatTabsModule,
  MatDialogModule,
  //MatCardModule,
  //MatNativeDateModule,
  //MatDatepickerModule,
  MatSidenavModule,
  MatDividerModule,
  MatListModule,
  MatExpansionModule,
  //MatTableModule,
  //MatPaginatorModule,
 // MatSortModule,
  MatSliderModule

]

@NgModule({
 
  imports: [
    MaterialComponents
  ],
  exports:[
    MaterialComponents
  ]
})
export class MaterialModule { }
