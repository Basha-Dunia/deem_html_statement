import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
//import { HomeLayoutComponent } from './components/home-layout/home-layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';

import { HttpClient, HttpClientModule } from '@angular/common/http'
//import {DatePipe} from '@angular/common';

//import { DashboardComponent } from './components/dashboard/dashboard.component';
//import { PaymentOptionsComponent } from './components/payment-options/payment-options.component';
//import { OffersComponent } from './components/offers/offers.component';
//import { CalculatorComponent } from './components/calculator/calculator.component';
//import { TermsComponent } from './components/terms/terms.component';
//import { CreditCardComponent } from './components/credit-card/credit-card.component';
// { CreditCardTransactionComponent } from './components/credit-card-transaction/credit-card-transaction.component';
//import { LoanComponent } from './components/loan/loan.component';
//import { HomeComponent } from './components/home/home.component';

//import { ErrorComponent } from './core/error/error.component';
//Store Reducer
//import {CacheAPIPipe} from '../app/cacheApiPipe';
//Modules
//import { MaterialModule } from './material/material.module';
// import{MatDividerModule} from '@angular/material/divider';
//import{MatDividerModule} from '@angular/material/divider';
//import { HomeComponentModule } from './modules/home-component/home-component.module';
//import {ReactiveFormsModule} from '@angular/forms'
//Services
//import { AuthorizationInterceptor } from './interceptors/authorization/authorization.interceptor';
import { GlobalErrorHandler } from './core/global-error-handler.service';
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component';
import { AuthGuardService } from './auth-guard/auth-guard.service';
//  import { JsonAppConfigService } from '../config/json-app-config.service';
//  import { AppConfig } from 'src/config/app-config';
import { from } from 'rxjs';
//import { LoaderInterceptor } from './interceptors/loader/loader.interceptor';
//import { CacheInterceptor } from './interceptors/cache/cache.interceptor';
import { httpInterceptProviers } from './interceptors';
//import { GoogleTranslatorComponent } from './components/google-translator/google-translator.component';
//import { EppTransactionComponent } from './components/epp-transaction/epp-transaction.component';
import { EppTransactionService } from './services/epp-transaction/epp-transaction.service';
//import { HeaderComponent } from './components/shared/header/header.component';
//import { FooterComponent } from './components/shared/footer/footer.component';
//import { SideNavBarComponent } from './components/shared/side-nav-bar/side-nav-bar.component';

//import { DatePickerModule } from './modules/date-picker/date-picker.module';
//import { DatePickerComponent } from './components/shared/date-picker/date-picker.component';
//import { OffersPopupComponent } from './components/offers/offers-popup/offers-popup.component';
//import { TaxInvoiceComponent } from './components/tax-invoice/tax-invoice.component';
//import { EppThankYouComponent } from './components/epp-transaction/epp-thank-you/epp-thank-you.component';
//import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
//import { NgbDateCustomParserFormatter } from './date-format';
//import { TestComponent } from './components/test/test.component';
//import { MarkedPipe } from './pipes/marked.pipe';


// export function intializerFn(jsonAppConfigService: JsonAppConfigService) {
//   return () => {
//     return jsonAppConfigService.load();
//   }
// }

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    //HomeLayoutComponent,
    //DashboardComponent,
    //PaymentOptionsComponent,
    //OffersComponent,
    //CalculatorComponent,
    //TermsComponent,
    //CreditCardComponent,
    //CreditCardTransactionComponent,
   // LoanComponent,
    //HomeComponent,
    //ErrorComponent,
    PageNotFoundComponent,
    //CacheAPIPipe,
    //GoogleTranslatorComponent,
    //EppTransactionComponent,
    //HeaderComponent,
    //FooterComponent,
    //SideNavBarComponent,
    //DatePickerComponent,
    //OffersPopupComponent,
    //TaxInvoiceComponent,
    //EppThankYouComponent,
    //TestComponent,
    //MarkedPipe


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
   //MaterialModule,
   //MatDividerModule,
    //HomeComponentModule,
    HttpClientModule,
    //ReactiveFormsModule,
    //FlexLayoutModule,
   // BsDatepickerModule.forRoot(),
    //DatePickerModule,

  ],
  providers: [AuthGuardService,httpInterceptProviers,
   // {provide:AppConfig,deps:[HttpClient],useExisting:JsonAppConfigService},
   //{provide:APP_INITIALIZER,multi:true,deps:[JsonAppConfigService],useFactory:intializerFn},
   // { provide: ErrorHandler, useClass: GlobalErrorHandler, multi: false },
    EppTransactionService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
//