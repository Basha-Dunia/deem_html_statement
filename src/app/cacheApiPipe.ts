import {HttpClient} from '@angular/common/http';
import {Pipe,PipeTransform} from '@angular/core';
import { ifError } from 'assert';
@Pipe({
    name:'cacheapi',
    pure:false
})
export class CacheAPIPipe implements PipeTransform{
private cachedAPIData:any=null;
private cachedAPIUrl='';
constructor (private http:HttpClient)
{

}
transform(url:string):any{
  
    if(url!==this.cachedAPIUrl)
    {
        
        this.cachedAPIData=null;
        this.cachedAPIUrl=url;
        this.http.get(url).subscribe(result=>this.cachedAPIData=result);
    }
    else
    {
       
    }
    return this.cachedAPIData;
}
}