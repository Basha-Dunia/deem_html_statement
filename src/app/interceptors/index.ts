import { HTTP_INTERCEPTORS } from '@angular/common/http';
import{AuthorizationInterceptor} from './authorization/authorization.interceptor';
import { CacheInterceptor } from './cache/cache.interceptor';
import { LoaderInterceptor } from './loader/loader.interceptor';
export const httpInterceptProviers=[
   { provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
   { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true } 
];