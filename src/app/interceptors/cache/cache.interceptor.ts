import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable,of } from 'rxjs';
import { tap } from 'rxjs/operators';  
import { HttpCacheService } from 'src/app/helper-services/http-cache/http-cache.service';
 

@Injectable()
export class CacheInterceptor implements HttpInterceptor {

  constructor(private cacheService: HttpCacheService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cachedResponse:HttpResponse<any>;
    if(request.method !== 'GET') {  
      this.cacheService.invalidateCache();  
      return next.handle(request);  
    }  
  
    cachedResponse = this.cacheService.get(request.url);  
  
    // return cached response  
    if (cachedResponse) {  
    
      return of(cachedResponse);  
    }      
  
    // send request to server and add response to cache  
    return next.handle(request)  
      .pipe(  
        tap(event => {  
          if (event instanceof HttpResponse) {  
            this.cacheService.put(request.url, event);  
          }  
        })  
      );  
  }
}
