import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
// import { AppConfig } from '../../../config/app-config'
import { CmsAuthService } from 'src/app/services/cms-auth/cms-auth.service';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(private router: Router, private cmsAuthService:CmsAuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let tokonizedRequest: any;
    
    if (request.url.includes('statementcmsapi')) {
      if(request.url.includes('statementcmsapi/auth/local'))
     {
      return next.handle(request.clone());
     }
     else
     {
      tokonizedRequest = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.cmsAuthService.cmsToken}`
        }
      });
    }
    }
    // if (request.headers.get('noauath'))
    // {
    //   return next.handle(request.clone());
    // }
    // else if (request.url.includes('172.18.3.18')) {
    //   if('/profile/login/v1.0')
    //   {
    //     tokonizedRequest = request.clone({
    //       setHeaders: {
  
    //         consumerApp: 'dunia-app',
    //         Authorization: "Basic NzQxM2YwODVmOWZhNGJkZmFhMmVhMmM3OThmMzFjNTE6ZTQzMzJjN0YxMWZBNGUzOEFlMjU5YjAyYTEwMDVEMzc=",
    //         consumerTransId: `random-trans-id-300`,
    //         'Content-Type':'application/json',
    //         DuniaAuthorization: 'false',
    //       },
  
    //     });
    //   }
    //   else{
        
    //   tokonizedRequest = request.clone({
    //     setHeaders: {

    //       consumerApp: 'dunia-app',
    //       Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
    //       consumerTransId: `random-trans-id-300`,
    //       'Content-Type':'application/json',
    //       DuniaAuthorization: 'false',
    //     },


    //   });
    // }
    //   // return next.handle(tokonizedRequest).pipe(
    //   //   tap(
    //   //     event => {

    //   //     },
    //   //     err => {
    //   //       if (err.error.auth = false) {
    //   //         this.router.navigate(['/login']);
    //   //       }
    //   //     })
    //   // );
    // }
    else {
      return next.handle(request.clone());
    }
    return next.handle(tokonizedRequest);
  }
}
