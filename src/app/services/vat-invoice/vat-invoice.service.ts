import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { VatInvoice } from 'src/app/models/vat-invoice';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class VatInvoiceService {

  vatInvoice: VatInvoice=new VatInvoice();
  constructor(private http: HttpClient, private envService:EnviornmentService) {

  }
  async getVatDetails(cif, statementDate) {
    const body = {
      'cif-number': cif,
      'statement-date': statementDate
    }
    const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
      Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
      consumerTransId: `random-trans-id-300`,
      'Content-Type':'application/json',
      DuniaAuthorization: 'false', }) };
    await this.http.post(`${this.envService.getEnviornment().baseUrlMule}/vat/invoice/v1.0`, body,headers).map((data: Response) => {
      return data as any;
    }).toPromise().then(tx => {
      if(tx['credit-card']){
        this.vatInvoice.creditCardTaxDetail.totalEligibleTax = tx['credit-card']['total-eligible-tax'];
        this.vatInvoice.creditCardTaxDetail.totalValueAddedTax = tx['credit-card']['total-value-added-tax'];
      }
      if(tx['personal-loan']){
      this.vatInvoice.loanTaxDetail.totalEligibleTax = tx['personal-loan']['total-eligible-tax'];
      this.vatInvoice.loanTaxDetail.totalValueAddedTax = tx['personal-loan']['total-value-added-tax'];
      }
    }

    );
    return this.vatInvoice;
  }
}
