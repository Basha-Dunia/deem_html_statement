import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Offers } from '../../models/offers';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class CmsOffersService {

  //offers$:Observable<Offer[]>=this.subject.asObservable();
  offers: Offers = new Offers();
  //endpoint='http://localhost/deemStatement/wp-json/wp/v2'
  noAuthHeader = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'False' }) };
  constructor(private http: HttpClient, private envService:EnviornmentService) {

  }
  
  getOffer(cardType) {
  
    if (cardType) {
      if (cardType.toLowerCase().includes('platinum')) {
        return this.http.get(`${this.envService.getEnviornment().baseUrlStrapi}/platinums`);
      }
      else if (cardType.toLowerCase().includes('titanium')) {
        return this.http.get(`${this.envService.getEnviornment().baseUrlStrapi}/titatinum-offers`);
      }
      else if (cardType.toLowerCase().includes('wrold')) {
        return this.http.get(`${this.envService.getEnviornment().baseUrlStrapi}/world-offers`);
      }
    }
 

  }
 


}
