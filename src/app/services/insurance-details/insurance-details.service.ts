import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { InsuranceDetails } from 'src/app/models/insurance-details';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class InsuranceDetailsService {
  insuranceDetails: InsuranceDetails = new InsuranceDetails()
  constructor(private http: HttpClient, private envService:EnviornmentService) { }

  async getInsuranceDetails(cif, statementDate) {
    const body = { "cif-number": cif, "statement-date": statementDate };
    const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
      Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
      consumerTransId: `random-trans-id-300`,
      'Content-Type':'application/json',
      DuniaAuthorization: 'false', }) };
  await  this.http.post(`${this.envService.getEnviornment().baseUrlMule}/insurance-details/v1.0`, body,headers).map((data: Response) => {
      return data;
    }).toPromise().then(rs => {
      if (rs['insurance-details'] != null) {
        if(rs['insurance-details']['loan-insurance']!=null)
        {
        this.insuranceDetails.loanInsurance = rs['insurance-details']['loan-insurance'];
        }
        if(rs['insurance-details']['card-insurance']!=null)
        {
        this.insuranceDetails.cardInsurance = rs['insurance-details']['card-insurance'];
        }
      
        
      }
    })
    return this.insuranceDetails;
  }
}
