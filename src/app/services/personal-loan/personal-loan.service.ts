import { Injectable, } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PersonalLoanDetails } from 'src/app/models/personal-loan-details-model';
import { PersonalLoanSummary } from 'src/app/models/personal-loan-summary';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class PersonalLoanService {
  presonlaLoan:PersonalLoanSummary=new PersonalLoanSummary();
  constructor(private http: HttpClient, private envService:EnviornmentService) {
  

  }
  async getLoanDetails(cif, statementDate) {
    const body = {
      'cif-number': cif,
      'statement-date': statementDate

    }
    const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
      Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
      consumerTransId: `random-trans-id-300`,
      'Content-Type':'application/json',
      DuniaAuthorization: 'false', }) };
    await this.http.post(`${this.envService.getEnviornment().baseUrlMule}/personal/loan/v1.0`,body,headers).map((data:Response)=>{
      return data as any;
    }).toPromise().then(pd=>{
      if(pd['personal-loan-details'] !=null)
      {
        this.presonlaLoan.statementDate=pd['personal-loan-details']['statement-date'];
        this.presonlaLoan.statementPeriod=pd['personal-loan-details']['statement-period'];
        this.presonlaLoan.loanAccountNumber=pd['personal-loan-details']['loan-account-number'];
        this.presonlaLoan.principalOutstanding=pd['personal-loan-details']['principal-outstanding'];
        this.presonlaLoan.paymentDue=pd['personal-loan-details']['payment-due'];
        this.presonlaLoan.totalpaymentDue =pd["personal-loan-details"]["principal-outstanding"] + pd["personal-loan-details"]["towards-interest"] +pd["personal-loan-details"]["late-fee-other-charges"];
        this.presonlaLoan.towardsPrincipal=pd['personal-loan-details']['towards-principal'];
        this.presonlaLoan.towardsInterest=pd['personal-loan-details']['towards-interest'];
        this.presonlaLoan.lateFeeOtherCharges=pd['personal-loan-details']['late-fee-other-charges'];
        this.presonlaLoan.paymentDueDate=pd['personal-loan-details']['payment-due']>20?'Immediate':pd['personal-loan-details']['payment-due-date'];//pd['personal-loan-details']['payment-due-date'];
        this.presonlaLoan.dueOn=pd['personal-loan-details']['payment-due-date'];
        this.presonlaLoan.amountBorrowed=pd['personal-loan-details']['amount-borrowed'];
        this.presonlaLoan.tenor=pd['personal-loan-details']['tenor'];
        this.presonlaLoan.interestRatePa=pd['personal-loan-details']['interest-rate-pa'];
        this.presonlaLoan.monthlyInstallment=pd['personal-loan-details']['monthly-installment'] !=null?pd['personal-loan-details']['monthly-installment']:'0.00';
        this.presonlaLoan.lastPaymentAmount=pd['personal-loan-details']['last-payment-amount'];
        this.presonlaLoan.lastPaymentDate=pd['personal-loan-details']['last-payment-date'];
        this.presonlaLoan.installmentRemaining=pd['personal-loan-details']['installment-remaining'];
        this.presonlaLoan.npaStatus=pd['personal-loan-details']['npa-status'];
        this.presonlaLoan.totalEligibleTax=pd['personal-loan-details']['total-fees-eligible-for-value-added-tax'];
        this.presonlaLoan.totalValueAddedTax=pd['personal-loan-details']['total-value-added-tax-for-the-period'];
      }
    }).catch(error=>{
      
      
      
    })
   return this.presonlaLoan;
  }
}
