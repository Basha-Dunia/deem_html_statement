import { Injectable } from '@angular/core';
import {  HttpClient, HttpHeaders,  } from '@angular/common/http';

import{environment} from 'src/environments/environment';
//import * as Rx from "rxjs/Rx";
import 'rxjs/Rx';
// import { from, Observable, throwError } from 'rxjs';
// import { map, catchError } from 'rxjs/operators';

import { User } from 'src/app/models/user';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  loggedOnUser: User = new User();
  private _cifid: string = null;
  constructor(private http: HttpClient,private envService:EnviornmentService) { }
  async userLogIn(pwd) {
    if (pwd) {
//console.log(environment.production);
      //const headers = { 'content-type': 'application/json' }
      const body = {
        "user-id": pwd,
        "channel": "Mobile"
      };
      const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
      Authorization: "Basic NzQxM2YwODVmOWZhNGJkZmFhMmVhMmM3OThmMzFjNTE6ZTQzMzJjN0YxMWZBNGUzOEFlMjU5YjAyYTEwMDVEMzc=",
     //Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
     consumerTransId: `random-trans-id-300`,
      'Content-Type':'application/json',
      DuniaAuthorization: 'false', }) };
      await this.http.post(`${this.envService.getEnviornment().baseUrlMule}/profile/login/v1.0`, body,headers).map((data: Response) => {
        return data as any;

      }).toPromise().then(x => {
        this.loggedOnUser.cifNumber = x['cif-number'];
        this.loggedOnUser.description = x['description'];
        this.loggedOnUser.status = x['status'];
      }).catch(error => {
        this.loggedOnUser.error.errorDesc = 'error';
      })
      return this.loggedOnUser
    }
  }
  public getCif(): string {
    if (this.loggedOnUser != null) {
      if(!this._cifid)
      {
      this._cifid = this.loggedOnUser.cifNumber;
      }
      return this._cifid;
    }

  }
  public setCif(cif) {
    this._cifid = cif;
   
  }
}