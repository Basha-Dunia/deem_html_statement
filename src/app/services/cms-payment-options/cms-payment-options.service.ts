import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class CmsPaymentOptionsService {
  //endpoint='http://localhost/deemStatement/wp-json/wp/v2';
  noAuthHeader = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'False' }) };
  constructor(private http:HttpClient,private envService:EnviornmentService) { }
  
  getPaymentOptions(){
    return this.http.get(`${this.envService.getEnviornment().baseUrlStrapi}/payment-options`);
   }
}
//.pipe(catchError(this.handleError))