import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CreditCardSummary } from 'src/app/models/credit-card-summary-model';
//import { CreditCardDetails } from 'src/app/models/CreditCardDetails';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class CreditCardSummaryService {
   cardSummaryDetails:CreditCardSummary[]=[];
  selectedDate;
  constructor(private http:HttpClient,private envService:EnviornmentService) {


   }
  async getCreditCardSummery(cif,statementDate){
    this.selectedDate=statementDate;
    
    const body = {
      'cif-number': cif,
      'statement-date': statementDate

    }
   const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
   Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
   consumerTransId: `random-trans-id-300`,
   'Content-Type':'application/json',
   DuniaAuthorization: 'false', }) };
  await  this.http.post(`${this.envService.getEnviornment().baseUrlMule}/credit-card-summary/v1.0`,body,headers).map((data:Response)=>{
      return data as any;
    }).toPromise().then(sd=>{
     if(sd['credit-card-details']!=null){

      sd['credit-card-details'].forEach(element => {
        var cardSummary=new CreditCardSummary();
        cardSummary.cardType=element['card-type'];
        cardSummary.primaryCardNumber=element['primary-card-number'];
        cardSummary.accountNumber=element['account-number'];
        cardSummary.totalPaymentDue=element['total-payment-due'];
        cardSummary.minimumPaymentDue=element['minimum-payment-due'];
        cardSummary.paymentDueDate=element['payment-due-date'];
        cardSummary.totalCreditLimit=element['total-credit-limit'];
        cardSummary.totalCashLimit=element['total-cash-limit'];
        cardSummary.totalOutstandingBalance=element['total-outstanding-balance'];
        cardSummary.previousBalance=element['previous-balance'];
        cardSummary.interestCharges=element['interest-charges'];
        cardSummary.cashAdvance=element['cash-advance'];
        cardSummary.paymentCredit=element['payment-credit'];
        cardSummary.currentBalance=element['current-balance']; 
        cardSummary.statementPeriod=element['statement-period'] ;  
        this.cardSummaryDetails.push(cardSummary);
        
      });
      }
    })
    return this.cardSummaryDetails;
   }
}
