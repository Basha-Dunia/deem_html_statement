import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';
@Injectable({
  providedIn: 'root'
})
export class TermsService {
  
  constructor(private http:HttpClient,private envService:EnviornmentService) { }
  getTerms(query=null){
    // return this.http.get(`${this.appConfig.baseUrlWp}/media?${query}`);
    return   this.http.get(`${this.envService.getEnviornment().baseUrlStrapi}/terms-conditions`);
    }
}
