import { Injectable } from '@angular/core';
import {FormGroup,  FormControl, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { StatementSummaryService } from '../statement-summary/statement-summary.service';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class EppTransactionService {
  eppResponse:any; 
  constructor(private http:HttpClient,private envService:EnviornmentService,
    private statementSummaryService:StatementSummaryService) { }
  form:FormGroup=new FormGroup({
    amount:new FormControl('',[Validators.required,Validators.minLength(5)]),
    repaymentMethod:new FormControl('',Validators.required),
    isntallmentAmount:new FormControl(''),
    processingFee:new FormControl(''),
    foreClosureFee:new FormControl(''),
    terms:new FormControl('',Validators.required)
  });
  initializeFormGroup(){
    this.form.setValue({
      amount:'',
      repaymentMethod:'',
      isntallmentAmount:'',
      processingFee:'',
      foreClosureFee:'',
      terms:false

    });
  } 
  async eppService(eppTransactionData,parsedData,instalmentAmount){ 
    
    const body={"customer-id":this.statementSummaryService.profileDetails.userDetails.cifNumber,
    "card-no": parsedData.cardNo,
    "txn_reference_id":parsedData.txnReferenceId,
    "amount":instalmentAmount,
    "repayment-period":parseInt(eppTransactionData.tenor),
    "monthly-install-amount":instalmentAmount,
    "monthly_interest-rate":eppTransactionData.interestRate,
    "processing-fee":eppTransactionData.processingFee,
    "fore-closure-fee":eppTransactionData.foreClosureFee,
    "ni-plan-num":parseInt(eppTransactionData.niPlanNumber),
    "processing-fee-type":eppTransactionData.processingFeeType,
    "txn-posting-date":parsedData.postingDate,
    "txn-auth-code":eppTransactionData.txnAuthCode
  };
  const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
    Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
    consumerTransId: `random-trans-id-300`,
    'Content-Type':'application/json',
    DuniaAuthorization: 'false', }) };
  ///card/epp-request/v1.0
    await  this.http.post(`${this.envService.getEnviornment().baseUrlMule}/epp-request/v2.0`,body,headers).map((data: Response) => {
        return data as any;

      }).toPromise().then(x=>{
      this.eppResponse = x;
      })

    return  this.eppResponse;
  } 
}
