import { Injectable } from '@angular/core';
import {  HttpClient, HttpHeaders,  } from '@angular/common/http';
import { CreditCardTransaction } from 'src/app/models/credit-card-transaction-model';
import { CreditCardDetails } from 'src/app/models/CreditCardDetails';
import { EppTransaction } from 'src/app/models/epp-transaction';
import { InstallmentDetail } from 'src/app/models/installment-detail';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class CreditCardTransactionService {
  creditCardTransaction: CreditCardTransaction = new CreditCardTransaction();
  transactionData: any;
  
  constructor(private http: HttpClient,private envService:EnviornmentService) { }
  async creditCardTransactionData(cardNo, statementDate,cifNumber) {

    const body = {
      "card-no": cardNo,
      "statement-date": statementDate,
      "cif-number":cifNumber
    };
    const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
    Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
    consumerTransId: `random-trans-id-300`,
    'Content-Type':'application/json',
    DuniaAuthorization: 'false', }) };
    await this.http.post(`${this.envService.getEnviornment().baseUrlMule}/transaction/v1.0`, body,headers).map(data => {
      return data as any
    }).toPromise().then(x => {
      //  this.transactionData=x;
      if(x['transaction-details']!=null)
      {
      x['transaction-details']['transaction'].forEach(element => {

        var transaction = new CreditCardDetails();
        transaction.date = element['date'];
        transaction.postingDate = element['post-date'];
        transaction.description = element['description'];
        transaction.currencyType = element['currency'] + ' ' + element['original-amount']+ ' ' +element['debit-credit-flag']+'R';;
        transaction.amount = element['amount'];
        transaction.originalAmount = element['original-amount'];
        transaction.cardNo = element['card-no'];
        transaction.txnReferenceId = element['txn-reference-id'];
        transaction.eppEligible = element['epp-eligible'];
        transaction.eppRequested = element['epp-requested'];
        transaction.debitCreditFlag=element['debit-credit-flag']+'R';
        if (element['epp-eligible'] == 'Y') {
          element['epp-processing-details'].forEach(epp => {
            var eppTransaction = new EppTransaction();
            eppTransaction.tenor = epp['tenor'];
            eppTransaction.interestRate = epp['interest-rate'];
            eppTransaction.processingFee = epp['processing-fee'];
            eppTransaction.processingFeeType = epp['processing-fee-type'];
            eppTransaction.foreClosureFee = epp['fore-closure-fee'];
            eppTransaction.niPlanNumber = epp['ni-plan-number'];
            eppTransaction.txnAuthCode = epp['txn-auth-code'];
            transaction.eppTransactions.push(eppTransaction);
          });
        }
        this.creditCardTransaction.transactions.push(transaction)

        // this.creditCardTransaction.creditCardDetails.push( this.creditCardTransaction.creditCardDetail)

        //  } )

      })
    }
    if(x['transaction-details']['installment-details']!=null)
    {
      x['transaction-details']['installment-details'].forEach(element => {
        var installmentDetaill=new InstallmentDetail();
        installmentDetaill.statementDate=element['statement-date'];
        installmentDetaill.tenor=element['tenor'];
        installmentDetaill.pendingInstallments=element['pending-installments'];
        installmentDetaill.outstanding=element['outstanding'];
        this.creditCardTransaction.installmentDetails.push(installmentDetaill);
        
      });
    }
     

    }).catch(error => {
     // throw error;
      this.creditCardTransaction.error.errorDesc = 'error';
      // throw error;

    })

   

    return this.creditCardTransaction;
  }
}
