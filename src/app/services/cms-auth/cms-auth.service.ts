import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';
@Injectable({
  providedIn: 'root'
})
export class CmsAuthService {
  cmsToken;
  constructor(private http: HttpClient, 
    private envService:EnviornmentService) { }
 async getCmsToken() {
    const body = {
      "identifier": "dunia_admin",
      "password": "Admin#123"
    }
  await this.http.post(`${this.envService.getEnviornment().baseUrlStrapi}/auth/local`, body).map((data: Response) => {
      return data as any;
    }).toPromise().then(x=>{
      this.cmsToken=x['jwt'];
    }).catch(error=>{
      throw error;
      });
    
  }
}
