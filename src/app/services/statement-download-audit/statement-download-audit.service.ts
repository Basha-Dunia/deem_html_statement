import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';
@Injectable({
  providedIn: 'root'
})
export class StatementDownloadAuditService {

  constructor(private http: HttpClient, 
    private envService:EnviornmentService) { }
  async statementDownloadAudit(cif, statementDate,cardNumber) {
    const body = { "customer-id": cif, "statement-date": statementDate,"card-number":cardNumber };
    const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
      Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
      consumerTransId: `random-trans-id-300`,
      'Content-Type':'application/json',
      DuniaAuthorization: 'false', }) };
  await  this.http.post(`${this.envService.getEnviornment().baseUrlMule}/download/audit/v1.0`, body,headers).map((data: Response) => {
      return data;
    }).toPromise().then(rs => {
      if (rs['status'] != null) {
       
      }
    })
   
  }
}
