import { Injectable } from '@angular/core';
import { TransactionDetails } from 'src/app/models/transaction-details';

@Injectable({
  providedIn: 'root'
})
export class PdfService {

  transactions: TransactionDetails[] = [];
  constructor() { }
  getPDFData(){
    this.transactions = [{
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 600.00",
      amount: "600.00 C"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 14.00",
      amount: "	14.00 D"


    },
    {
      transactionDate: "04/08/2020",
      postingDate: "04/08/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "12/08/2020",
      postingDate: "13/08/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 4130.00",
      amount: "4130.00 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "BILLED FINANCE CHARGES",
      amountOriginalCurrency: "AED 393.26",
      amount: "393.26 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 88.36",
      amount: "88.36 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "VAT ON DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 4.42",
      amount: "4.42 D"


    },
    {
      transactionDate: "16/07/2020",
      postingDate: "16/07/2020",
      description: "RETAIL PURCHASE NATIONAL TAXI",
      amountOriginalCurrency: "AED 16.00",
      amount: "16.00 D"


    },
    {
      transactionDate: "18/07/2020",
      postingDate: "18/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 16.50",
      amount: "16.50 D"


    },
    {
      transactionDate: "19/07/2020",
      postingDate: "19/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 1900.00",
      amount: "1900.00 C"


    },
    {
      transactionDate: "25/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE GOVT OF SHARJAH",
      amountOriginalCurrency: "AED 740.98",
      amount: "740.98 D"


    },
    {
      transactionDate: "26/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE NEEL WA FURAT.COM",
      amountOriginalCurrency: "USD 18.00",
      amount: "68.10 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE ARABIA TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "VAT ON SERVICE CHARGE DEBIT NEEL WA FURAT.COM",
      amountOriginalCurrency: "AED 0.10",
      amount: "0.10 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.50",
      amount: "12.50 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE Etisalat Telecom Corp",
      amountOriginalCurrency: "AED 340.39",
      amount: "340.39 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 3494.75",
      amount: "3494.75 C"


    },
    {
      transactionDate: "30/07/2020",
      postingDate: "31/07/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 500.00",
      amount: "500.00 D"


    },
    {
      transactionDate: "31/07/2020",
      postingDate: "01/08/2020",
      description: "RETAIL PURCHASE WARDA AL MADINA SMKT",
      amountOriginalCurrency: "AED 63.80",
      amount: "63.80 D"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 600.00",
      amount: "600.00 C"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 14.00",
      amount: "	14.00 D"


    },
    {
      transactionDate: "04/08/2020",
      postingDate: "04/08/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "12/08/2020",
      postingDate: "13/08/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 4130.00",
      amount: "4130.00 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "BILLED FINANCE CHARGES",
      amountOriginalCurrency: "AED 393.26",
      amount: "393.26 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 88.36",
      amount: "88.36 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "VAT ON DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 4.42",
      amount: "4.42 D"


    },
    {
      transactionDate: "16/07/2020",
      postingDate: "16/07/2020",
      description: "RETAIL PURCHASE NATIONAL TAXI",
      amountOriginalCurrency: "AED 16.00",
      amount: "16.00 D"


    },
    {
      transactionDate: "18/07/2020",
      postingDate: "18/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 16.50",
      amount: "16.50 D"


    },
    {
      transactionDate: "19/07/2020",
      postingDate: "19/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 1900.00",
      amount: "1900.00 C"


    },
    {
      transactionDate: "25/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE GOVT OF SHARJAH",
      amountOriginalCurrency: "AED 740.98",
      amount: "740.98 D"


    },
    {
      transactionDate: "26/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE NEEL WA FURAT.COM",
      amountOriginalCurrency: "USD 18.00",
      amount: "68.10 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE ARABIA TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "VAT ON SERVICE CHARGE DEBIT NEEL WA FURAT.COM",
      amountOriginalCurrency: "AED 0.10",
      amount: "0.10 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.50",
      amount: "12.50 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE Etisalat Telecom Corp",
      amountOriginalCurrency: "AED 340.39",
      amount: "340.39 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 3494.75",
      amount: "3494.75 C"


    },
    {
      transactionDate: "30/07/2020",
      postingDate: "31/07/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 500.00",
      amount: "500.00 D"


    },
    {
      transactionDate: "31/07/2020",
      postingDate: "01/08/2020",
      description: "RETAIL PURCHASE WARDA AL MADINA SMKT",
      amountOriginalCurrency: "AED 63.80",
      amount: "63.80 D"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 600.00",
      amount: "600.00 C"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 14.00",
      amount: "	14.00 D"


    },
    {
      transactionDate: "04/08/2020",
      postingDate: "04/08/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "12/08/2020",
      postingDate: "13/08/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 4130.00",
      amount: "4130.00 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "BILLED FINANCE CHARGES",
      amountOriginalCurrency: "AED 393.26",
      amount: "393.26 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 88.36",
      amount: "88.36 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "VAT ON DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 4.42",
      amount: "4.42 D"


    },
    {
      transactionDate: "16/07/2020",
      postingDate: "16/07/2020",
      description: "RETAIL PURCHASE NATIONAL TAXI",
      amountOriginalCurrency: "AED 16.00",
      amount: "16.00 D"


    },
    {
      transactionDate: "18/07/2020",
      postingDate: "18/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 16.50",
      amount: "16.50 D"


    },
    {
      transactionDate: "19/07/2020",
      postingDate: "19/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 1900.00",
      amount: "1900.00 C"


    },
    {
      transactionDate: "25/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE GOVT OF SHARJAH",
      amountOriginalCurrency: "AED 740.98",
      amount: "740.98 D"


    },
    {
      transactionDate: "26/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE NEEL WA FURAT.COM",
      amountOriginalCurrency: "USD 18.00",
      amount: "68.10 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE ARABIA TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "VAT ON SERVICE CHARGE DEBIT NEEL WA FURAT.COM",
      amountOriginalCurrency: "AED 0.10",
      amount: "0.10 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.50",
      amount: "12.50 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE Etisalat Telecom Corp",
      amountOriginalCurrency: "AED 340.39",
      amount: "340.39 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 3494.75",
      amount: "3494.75 C"


    },
    {
      transactionDate: "30/07/2020",
      postingDate: "31/07/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 500.00",
      amount: "500.00 D"


    },
    {
      transactionDate: "31/07/2020",
      postingDate: "01/08/2020",
      description: "RETAIL PURCHASE WARDA AL MADINA SMKT",
      amountOriginalCurrency: "AED 63.80",
      amount: "63.80 D"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 600.00",
      amount: "600.00 C"


    },
    {
      transactionDate: "03/08/2020",
      postingDate: "03/08/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 14.00",
      amount: "	14.00 D"


    },
    {
      transactionDate: "04/08/2020",
      postingDate: "04/08/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "12/08/2020",
      postingDate: "13/08/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 4130.00",
      amount: "4130.00 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "BILLED FINANCE CHARGES",
      amountOriginalCurrency: "AED 393.26",
      amount: "393.26 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 88.36",
      amount: "88.36 D"


    },
    {
      transactionDate: "15/08/2020",
      postingDate: "15/08/2020",
      description: "VAT ON DEEM DOUBLE SECURE",
      amountOriginalCurrency: "AED 4.42",
      amount: "4.42 D"


    },
    {
      transactionDate: "16/07/2020",
      postingDate: "16/07/2020",
      description: "RETAIL PURCHASE NATIONAL TAXI",
      amountOriginalCurrency: "AED 16.00",
      amount: "16.00 D"


    },
    {
      transactionDate: "18/07/2020",
      postingDate: "18/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 16.50",
      amount: "16.50 D"


    },
    {
      transactionDate: "19/07/2020",
      postingDate: "19/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 1900.00",
      amount: "1900.00 C"


    },
    {
      transactionDate: "25/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE GOVT OF SHARJAH",
      amountOriginalCurrency: "AED 740.98",
      amount: "740.98 D"


    },
    {
      transactionDate: "26/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE CARS TAXI SERVICES CO",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE NEEL WA FURAT.COM",
      amountOriginalCurrency: "USD 18.00",
      amount: "68.10 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "27/07/2020",
      description: "RETAIL PURCHASE ARABIA TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "27/07/2020",
      postingDate: "29/07/2020",
      description: "VAT ON SERVICE CHARGE DEBIT NEEL WA FURAT.COM",
      amountOriginalCurrency: "AED 0.10",
      amount: "0.10 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.50",
      amount: "12.50 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE Etisalat Telecom Corp",
      amountOriginalCurrency: "AED 340.39",
      amount: "340.39 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "30/07/2020",
      description: "RETAIL PURCHASE DUBAI TAXI",
      amountOriginalCurrency: "AED 12.00",
      amount: "12.00 D"


    },
    {
      transactionDate: "29/07/2020",
      postingDate: "29/07/2020",
      description: "CARD PAYMENT",
      amountOriginalCurrency: "AED 3494.75",
      amount: "3494.75 C"


    },
    {
      transactionDate: "30/07/2020",
      postingDate: "31/07/2020",
      description: "RETAIL PURCHASE SMART DUBAI GOVERNMENT",
      amountOriginalCurrency: "AED 500.00",
      amount: "500.00 D"


    },
    {
      transactionDate: "31/07/2020",
      postingDate: "01/08/2020",
      description: "RETAIL PURCHASE WARDA AL MADINA SMKT",
      amountOriginalCurrency: "AED 63.80",
      amount: "63.80 D"


    },
    ]
return this.transactions;
  }
}
