import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProfileDetails } from 'src/app/models/profile-details.model';
import { AvailableCardStatement } from 'src/app/models/available-card-statement';
//import { AvailableLoanStatement } from 'src/app/models/available-loan-statement';
import { CreditCardSummary } from 'src/app/models/credit-card-summary-model';
import { NoOfAvailableCards } from 'src/app/models/No-of-cards-available';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';
@Injectable({
  providedIn: 'root'
})
export class StatementSummaryService {
  profileDetails: ProfileDetails = new ProfileDetails();
  cardTypeImage;
  public scope: Array<any> | boolean = false;
  constructor(private http: HttpClient, private envService:EnviornmentService) { }
  async getStatementSummary(query) {
    // return this.http.get(`${this.appConfig.baseUrlWp}/media?${query}`);
    const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
      Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
      consumerTransId: `random-trans-id-300`,
      'Content-Type':'application/json',
      DuniaAuthorization: 'false', }) };
    //users/statements/profile/details/v1.0
    await this.http.get(`${this.envService.getEnviornment().baseUrlMule}/profile/details/v1.0?cif-num=${query}`,headers).map((data: Response) => {
      return data as any;
    }).toPromise().then(sd => {
      this.profileDetails.userDetails.cifNumber = sd['user-details']['cif-number'];
      this.profileDetails.userDetails.firstName = sd['user-details']['first-name'];
      this.profileDetails.userDetails.middleName = sd['user-details']['middle-name'];
      this.profileDetails.userDetails.lastName = sd['user-details']['last-name'];
      this.profileDetails.userDetails.address1 = sd['user-details']['address1'];
      this.profileDetails.userDetails.address2 = sd['user-details']['address2'];
      this.profileDetails.userDetails.address3 = sd['user-details']['address3'];
      this.profileDetails.userDetails.address4 = sd['user-details']['address4'];
      this.profileDetails.userDetails.city = sd['user-details']['city'];
      this.profileDetails.userDetails.state = sd['user-details']['state'];
      this.profileDetails.userDetails.country = sd['user-details']['country'];
      this.profileDetails.userDetails.email = sd['user-details']['email'];
      this.profileDetails.userDetails.mobile = sd['user-details']['mobile'];
      this.profileDetails.userDetails.dob = sd['user-details']['dob'];
      if(sd['card-statement-detail']!=null)
      {
      this.profileDetails.statementDetils.statementDate = sd['card-statement-detail']['statement-date'];
      this.profileDetails.statementDetils.statementPeriod = sd['card-statement-detail']['statement-period'];
      }
     if(sd['credit-card-details']!=null)
     {
      sd['credit-card-details'].forEach(element => {
        var creditCardSummary = new CreditCardSummary();
        if (element) {
          creditCardSummary.cardType = element['card-type'];
          creditCardSummary.primaryCardNumber = element['primary-card-number'];
          creditCardSummary.accountNumber = element['account-number'];
          creditCardSummary.totalPaymentDue = element['total-payment-due'];
          creditCardSummary.minimumPaymentDue = element['minimum-payment-due'];
          creditCardSummary.paymentDueDate = element['payment-due-date'];
          creditCardSummary.totalCreditLimit = element['total-credit-limit'];
          creditCardSummary.totalCashLimit = element['total-cash-limit'];
          creditCardSummary.totalOutstandingBalance = element['total-outstanding-balance'];
          creditCardSummary.previousBalance=element['previous-balance'];
          creditCardSummary.cashAdvance=element['cash-advance'];
          creditCardSummary.interestCharges=element['interest-charges'];
          creditCardSummary.paymentCredit=element['payment-credit'];
          creditCardSummary.currentBalance=element['current-balance'];
          this.cardTypeImage=element['card-type'];
          this.profileDetails.creditCardDetails.push(creditCardSummary);
        }
      });
     
    }
    if(sd['no-of-cards-available']!=null)
    {
      sd['no-of-cards-available'].forEach(element => {
        if(element)
        {
          var cardsAvailbale=new NoOfAvailableCards();
          cardsAvailbale.primaryCardNumber=element['primary-card-number'];
          cardsAvailbale.accountNumber=element['account-number'];
          cardsAvailbale.cardType=element['card-type'];
          this.profileDetails.noOfAvailableCards.push(cardsAvailbale);
        }
      });
    }
    if( sd['available-statements']!=null)
    {
      sd['available-statements'].forEach(element => {
        if(element)
        {
        var cardStmt = new AvailableCardStatement();
        cardStmt.statementDate = element['statement-date'];
        this.profileDetails.availableCardStatements.push(cardStmt);
        }
       
      });
    }
    //   if(sd['available-loan-statements']!=null)
    //   {
    //   sd['available-loan-statements'].forEach(element => {
    //     if(element)
    //     {
    //     var loanStmt = new AvailableLoanStatement();
    //     loanStmt.statementDate = element['loan-statement-date'];
    //     this.profileDetails.availableLoanStatements.push(loanStmt);
    //     }
        
    //   });
    // }
      //Personal Loan Details
      if(sd['personal-loan-details'] !=null)
      {
        this.profileDetails.personalLoanSummary.statementPeriod=sd['personal-loan-details']['statement-date'];
      this.profileDetails.personalLoanSummary.statementPeriod=sd['personal-loan-details']['statement-period'];
      this.profileDetails.personalLoanSummary.loanAccountNumber=sd['personal-loan-details']['loan-account-number'];
      this.profileDetails.personalLoanSummary.principalOutstanding=sd['personal-loan-details']['principal-outstanding'];
      this.profileDetails.personalLoanSummary.paymentDue=sd['personal-loan-details']['payment-due'];
      this.profileDetails.personalLoanSummary.towardsPrincipal=sd['personal-loan-details']['towards-principal'];
      this.profileDetails.personalLoanSummary.towardsInterest=sd['personal-loan-details']['towards-interest'];
      this.profileDetails.personalLoanSummary.lateFeeOtherCharges=sd['personal-loan-details']['late-fee-other-charges'];
      this.profileDetails.personalLoanSummary.paymentDueDate=sd['personal-loan-details']['payment-due-date'];
      this.profileDetails.personalLoanSummary.amountBorrowed=sd['personal-loan-details']['amount-borrowed'];
      this.profileDetails.personalLoanSummary.tenor=sd['personal-loan-details']['tenor'];
      this.profileDetails.personalLoanSummary.interestRatePa=sd['personal-loan-details']['interest-rate-pa'];
      this.profileDetails.personalLoanSummary.monthlyInstallment=sd['personal-loan-details']['monthly-installment'];
      this.profileDetails.personalLoanSummary.lastPaymentAmount=sd['personal-loan-details']['last-payment-amount'];
      this.profileDetails.personalLoanSummary.lastPaymentDate=sd['personal-loan-details']['last-payment-date'];
      this.profileDetails.personalLoanSummary.installmentRemaining=sd['personal-loan-details']['installment-remaining'];
      }
      if (sd['insurance-details'] != null) {
        this.profileDetails.insuranceDetails.loanInsurance = sd['insurance-details']['loan-insurance'];
        this.profileDetails.insuranceDetails.cardInsurance = sd['insurance-details']['card-insurance'];
        
      }
    }).catch(error=>{
      this.profileDetails.error.errorDesc='error';
    })
    return this.profileDetails;
  }
}
