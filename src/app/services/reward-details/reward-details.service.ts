import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RewardDetails } from 'src/app/models/reward-details';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';

@Injectable({
  providedIn: 'root'
})
export class RewardDetailsService {
  rewardDetails: RewardDetails = new RewardDetails()
  constructor(private http: HttpClient, 
    private envService:EnviornmentService) { }

  async getRewardDetails(cif, statementDate) {
    const body = { "cif-number": cif, "statement-date": statementDate };
    const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
    Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
    consumerTransId: `random-trans-id-300`,
    'Content-Type':'application/json',
    DuniaAuthorization: 'false', }) };
  await  this.http.post(`${this.envService.getEnviornment().baseUrlMule}/rewards-summary/v1.0`, body,headers).map((data: Response) => {
      return data;
    }).toPromise().then(rs => {
      if (rs['rewards-details'] != null) {
        this.rewardDetails.amountSpent = rs['rewards-details']['amount-spent'] !=null ?rs['rewards-details']['amount-spent']:0.0;
        this.rewardDetails.rewardsEarned = rs['rewards-details']['rewards-earned']!=null ?rs['rewards-details']['rewards-earned']:0.0;
        this.rewardDetails.openingBalance = rs['rewards-details']['opening-balance']!=null ?rs['rewards-details']['opening-balance']:0.0;
        this.rewardDetails.earnedAed = rs['rewards-details']['earned-aed']!=null ?rs['rewards-details']['earned-aed']:0.0;
        this.rewardDetails.purchasedAed = rs['rewards-details']['purchased-aed']!=null ?rs['rewards-details']['purchased-aed']:0.0;
        this.rewardDetails.redeemedAed = rs['rewards-details']['redeemed-aed']!=null ?rs['rewards-details']['redeemed-aed']:0.0;
        this.rewardDetails.availableBalance = rs['rewards-details']['available-balance'] !=null ?rs['rewards-details']['available-balance']:0.0;
        this.rewardDetails.airlinesSpentPer = rs['rewards-details']['airlines-spent-per']!=null ?rs['rewards-details']['airlines-spent-per']:0.0;
        this.rewardDetails.hotelsSpentPer = rs['rewards-details']['hotels-spent-per']!=null ?rs['rewards-details']['hotels-spent-per']:0.0;
        this.rewardDetails.othersSpentPer = rs['rewards-details']['others-spent-per']!=null ?rs['rewards-details']['others-spent-per']:0.0;
        this.rewardDetails.entertainmentSpentPer = rs['rewards-details']['entertainment-spent-per']!=null ?rs['rewards-details']['entertainment-spent-per']:0.0;
      }
    })
    return this.rewardDetails;
  }
}

