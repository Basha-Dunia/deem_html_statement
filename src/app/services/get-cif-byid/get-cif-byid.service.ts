import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnviornmentService } from 'src/app/Helpers/enviornment.service';
import { MobileUser } from 'src/app/models/mobile-user';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class GetCifByidService {
  loggedOnUserMob:MobileUser=new MobileUser();
  private cifidMob:string=null;
  constructor(private http:HttpClient,private envService:EnviornmentService, private loginService:LoginService) { }

  async userLogIn(custId)
  {
    if(custId)
    {
    
      const headers = { headers: new HttpHeaders({ consumerApp: 'dunia-app',
      Authorization: "Basic ODdlMzYyNTE0NGQ4NGQ2OGIwOTVjY2QyYWIxNjRkNDk6RjllZmMwYmM0RGNGNERCQTg5NzdFNDdiOTYxNjZBNEI=",
      consumerTransId: `random-trans-id-300`,
      'Content-Type':'application/json',
      DuniaAuthorization: 'false', }) };
    const body={"customer-id": custId};

    await  this.http.post(`${this.envService.getEnviornment().baseUrlMule}/getcif/byid/v1.0`,body,headers).map((data: Response) => {
        return data as any;

      }).toPromise().then(x=>{
      
        this.loggedOnUserMob.cifNumber=x['cif-number'];
        this.loggedOnUserMob.status=x['status'];
        this.loggedOnUserMob.message=x['message'];
      }).catch(error=>{
      this.loggedOnUserMob.error.errorDesc='error';
      })
      return this.loggedOnUserMob
  }
}
}