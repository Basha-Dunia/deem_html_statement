import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) { }
  handleError(error: any) {
    const router = this.injector.get(Router);
    console.log(`Request URL:${router.url}`);
    if (error.error instanceof HttpErrorResponse) {
      console.error('Backend returned stauts code:', error.status);
      console.error('Response body:', error.message);
      router.navigate(['statement/error']);
    }
    else {
      if(error.message.includes('Cannot match any routes. URL Segment'))
      {
        router.navigate(['404']);
      }
      else
      {
        router.navigate(['statement/error']);
      }
      //console.error('An error occured:', error.message);
    }
   
  }

}
