import { Injectable } from '@angular/core';
import { AppConfig } from './app-config';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class JsonAppConfigService extends AppConfig {

  constructor(private http: HttpClient) { super(); }
  load() {
    return this.http.get<AppConfig>('app.config.json')
      .toPromise()
      .then(data => {
        if(data.env==="dev")
        {
          this.baseUrlMule = data.dev.baseUrlMule;
          this.baseUrlStrapi = data.dev.baseUrlStrapi;
          this.muleToken = data.dev.muleToken;
          this.strapiToken=data.dev.strapiToken
         // this.env = data.env
        }
        else
        {
          this.baseUrlMule = data.prod.baseUrlMule;
          this.baseUrlStrapi = data.prod.baseUrlStrapi;
          this.muleToken = data.prod.muleToken;
          this.strapiToken=data.prod.strapiToken
        }
       
      })
      .catch(() => {
        console.error('could not load the configuration');
      })
  }
}
