export abstract class AppConfig{
        env:string;
        baseUrlMule:string;
        baseUrlStrapi:string;
        muleToken:string;
        strapiToken:string;
        prod:AppConfig;
        dev:AppConfig;
        
}