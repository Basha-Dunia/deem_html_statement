
# Stage 1

FROM node:10.23.0-alpine as build-step

RUN mkdir -p /customer-statements-build

WORKDIR /customer-statements-build

COPY package.json /customer-statements-build

RUN npm install

#COPY  /src/assets/examples/fonts/* /customer-statements-build/node_modules/pdfmake/examples/fonts

#RUN gulp buildFonts

COPY . /customer-statements-build
#Added on 4th Jan 2022 to clear the browser cache before loading
RUN npm cache clear --force
RUN npm run build --prod --outputHashing=all


# Stage 2

FROM nginx:1.17.1-alpine

COPY --from=build-step /customer-statements-build/dist /usr/share/nginx/172.18.3.80
COPY --from=build-step /customer-statements-build/ssl.conf /etc/nginx/conf.d
COPY --from=build-step /customer-statements-build/default.conf /etc/nginx/conf.d/default.conf
#RUN rm -frv /etc/nginx/nginx.conf && ls
#RUN ls /etc/nginx/
#RUN rm -frv /etc/nginx/conf.d/* && ls
#COPY --from=build-step /customer-statements-build/nginx.conf /etc/nginx
#RUN ls /etc/nginx/
#CMD ["nginx", "-g", "daemon off;"]